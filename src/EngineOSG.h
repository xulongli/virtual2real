#ifndef __engineosg__
#define __engineosg__

#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
//include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <osgViewer/Viewer>
#include "Engine.h"
	using namespace std;

class EngineOSG : public Engine
{
  protected:
	//public:
	osg::ref_ptr<osgViewer::Viewer> viewer;

	void drawFromFileAndData();
	void drawPoints(vector<GLfloat> &vertices);
	void setupSphereOrientation();
	void getIntrisnicMatrix();

  public:
	EngineOSG();
	virtual ~EngineOSG();
	void init();
	void initDataFromFile();
	void initData();
	void initCamera();
	void initCanvas();
	void draw();
	void shutdown();
	friend class CaptureCB;
	friend class TextureCB;
	friend class KeyboardEventHandler;
};

void validateModelView(osg::ref_ptr<osgViewer::Viewer> viewer);
void printMVPInfo(osg::ref_ptr<osgViewer::Viewer> viewer);

#endif // __engineosg__
