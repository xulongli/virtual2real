#include <windows.h>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <iostream>
#include <queue>


#include "TravelManipulator.h"
#include "Camera.h"
#include "INIReader.h"
#include "PointCloud.h"
#include "AWGUI.h"
#include "Util.h"

int main()
{	
	// Read Config file
	INIReader reader("config/config.ini");

	string CALIB_FILE_PATH = reader.Get("Camera", "CALIB_FILE_PATH", "");
	//cout << "Calibration: " << CALIB_FILE_PATH << endl;

	osg::notify(osg::NOTICE) << " leexulong@gmail.com " << std::endl << std::endl;

	//创建Viewer对象
	osg::ref_ptr<osgViewer::Viewer>viewer = new osgViewer::Viewer();

	//添加状态事件
	//viewer.get()->addEventHandler( new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()) );

	//窗口大小变化事件
	//viewer.get()->addEventHandler(new osgViewer::WindowSizeHandler);

	//添加一些常用状态设置
	//viewer.get()->addEventHandler(new osgViewer::StatsHandler);

	// 把漫游器加入到场景中
	//osg::ref_ptr<TravelManipulator> mainCamera = new TravelManipulator(viewer);
	//viewer->setCameraManipulator(mainCamera);
	//mainCamera->m_pHostViewer = viewer;
	
	osg::ref_ptr<osg::MatrixTransform>root = new osg::MatrixTransform();

	// 读取地形 Set the ground (only receives shadow)
	osg::ref_ptr<osg::MatrixTransform> groundNode = new osg::MatrixTransform;
	CUtil *utl = new CUtil();
	utl->drawFloor(groundNode);
	root->addChild(groundNode.get());

	// Point cloud
	osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform;
	root->addChild(ptcNode.get());

	string PLY_FILE_NAME = reader.Get("Camera", "PLY_FILE_NAME", "");
	CPointCloud* pointCloud = new CPointCloud();
	pointCloud->loadPointCloud(PLY_FILE_NAME, ptcNode);

	// Light	
	osg::StateSet* state = root->getOrCreateStateSet();// create the lighting state set
	state->setMode(GL_LIGHTING, osg::StateAttribute::ON);
	state->setMode(GL_LIGHT0, osg::StateAttribute::ON); // for first light
	state->setMode(GL_LIGHT1, osg::StateAttribute::ON); // for second light

	osg::ref_ptr<osg::Light> light= new osg::Light;
	light->setAmbient(osg::Vec4(1.0, 1.0, 1.0, 1.0));
	light->setDiffuse(osg::Vec4(1.0, 1.0, 1.0, 1.0));
	light->setSpecular(osg::Vec4(1, 1, 1, 1));  // some examples don't have this one
	light->setPosition(osg::Vec4(0.0f, 10.0f, 10.0f,0.0));

	osg::ref_ptr<osg::LightSource> source = new osg::LightSource;
	source->setLight(light.get());
	root->addChild(source);	// create a light source object and add the light to it


	// VGA initialize 
	std::vector<CCamVGA*> camVGAList;
	std::vector<osg::ref_ptr<osg::MatrixTransform>> camVGAGeoList;

	for (int panelIdx = 1; panelIdx <= 20; panelIdx++){
		for (int camIdx = 1; camIdx <= 24; camIdx++)
		{
			//cout << "VGA Camera: " << panelIdx << "," << camIdx << endl;

			CCamVGA* camVGA = new CCamVGA(CALIB_FILE_PATH, panelIdx, camIdx);
			camVGAList.push_back(camVGA);

			osg::ref_ptr<osg::MatrixTransform> node = new osg::MatrixTransform;
			if (camVGA->drawVGACam(node))
				root->addChild(node);

			node->setNodeMask(VGA_NODE_MASK);
			camVGAGeoList.push_back(node);
		}
	}
	
	//HD initialize
	std::vector<CCamHD*> camHDList;
	std::vector<osg::ref_ptr<osg::MatrixTransform>> camHDGeoList;

	for (int camIdx = 1; camIdx <= 31; camIdx++)
	{
	//cout << "HD Camera: " << camIdx << endl;

	CCamHD* camHD = new CCamHD(CALIB_FILE_PATH, camIdx);
	camHDList.push_back(camHD);

	osg::ref_ptr<osg::MatrixTransform> node = new osg::MatrixTransform;
	if (camHD->drawHDCam(node))
		root->addChild(node);

	node->setNodeMask(HD_NODE_MASK);
	camHDGeoList.push_back(node);
	};

	//Kinect initialize
	std::vector<CCamKinect*> camKinectList;
	std::vector<osg::ref_ptr<osg::MatrixTransform>> camKinectGeoList;

	for (int camIdx = 1; camIdx <= 10; camIdx++)
	{
		//cout << "Kinect Camera: "<< camIdx << endl;

		CCamKinect* camKinect = new CCamKinect(CALIB_FILE_PATH, camIdx);
		camKinectList.push_back(camKinect);

		osg::ref_ptr<osg::MatrixTransform> node = new osg::MatrixTransform;
		if (camKinect->drawKinectCam(node))
			root->addChild(node);

		node->setNodeMask(Kinect_NODE_MASK);
		camKinectGeoList.push_back(node);
	};

	// Projector

	// Root node Rotation
	//osg::Matrix mx;
	//mx.makeRotate(osg::PI, osg::Vec3(1, 0, 0));
	//root->setMatrix(mx);

	// 优化场景数据
	osgUtil::Optimizer optimizer;
	optimizer.optimize(root.get());
	viewer->setSceneData(root.get());

	//相机
	int xoffset = 40;
	int yoffset = 40;

	// left window + left slave camera
	{
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 0;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// add this slave camera to the viewer, with a shift left of the projection matrix
		viewer->addSlave(camera.get(), osg::Matrixd::translate(1.0, 0.0, 0.0), osg::Matrixd());
		osg::View::Slave* slave = viewer->findSlaveForCamera(camera);

		// GUI
		osg::ref_ptr<TwGUIManager> twGUI = new TwGUIManager(root.get());
		viewer->addEventHandler(twGUI.get());
		//camera->setUpdateCallback(twGUI.get())
		camera->setFinalDrawCallback(twGUI.get());
	}

	// right window + right slave camera
	{
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;
	
		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());
	
		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// add this slave camera to the viewer, with a shift right of the projection matrix
		viewer->addSlave(camera.get(), osg::Matrixd::translate(-1.0, 0.0, 0.0), osg::Matrixd());
	}

	viewer->run();

	return 0;
}

