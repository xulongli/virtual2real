import cv2
import numpy as np
import os

WIDTH = 1280
HEIGHT = 800

for nWidth in reversed( range(1,int(HEIGHT/4)) ):
	
	img = np.zeros((HEIGHT,WIDTH,3), np.uint8)	#black
	white = 255*np.ones((1,WIDTH,3));

	startOffset = nWidth;
	endOffset = startOffset + nWidth-1;
	
	while endOffset < HEIGHT:
		img[startOffset:endOffset,:,:] = white
		
		startOffset += 2*nWidth;
		endOffset += 2*nWidth;
		print ('nWidth:{0}, startOffset:{1}, endOffset:{2}'.format(nWidth,startOffset,endOffset)	)
	
	#os.mkdir('./img_1')	
	cv2.imwrite('./img/1_{num:04d}.png'.format(num=nWidth), img) 
	# cv2.imwrite('c:/Review/{num:02d}.png'.format(num=nWidth), img) 

	
	
for nWidth in reversed( range(1,int(HEIGHT/4)) ):
	
	img = np.zeros((HEIGHT,WIDTH,3), np.uint8)	#black
	white = 255*np.ones((HEIGHT,1,3));

	startOffset = nWidth;
	endOffset = startOffset + nWidth-1;
	
	while endOffset < WIDTH:
		img[:,startOffset:endOffset,:] = white
		
		startOffset += 2*nWidth;
		endOffset += 2*nWidth;
		print(  'nWidth:{0}, startOffset:{1}, endOffset:{2}'.format(nWidth,startOffset,endOffset)	)
	
	#os.mkdir('./img_2')
	cv2.imwrite('./img/2_{num:04d}.png'.format(num=nWidth), img) 
	# cv2.imwrite('c:/Review/{num:02d}.png'.format(num=nWidth), img) 	