#pragma once
#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
//include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

// #define WINDOWS  /* uncomment this line to use it for windows.*/
#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

#include <iostream>
#include <stdio.h> /* defines FILENAME_MAX */
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <osgDB/WriteFile>
#include <osg/Camera>
#include "INIReader.h"

namespace virtual2real
{
	class CUtil
	{
	  public:
		void drawFloor(osg::ref_ptr<osg::MatrixTransform> node);
		void Capture(osgViewer::Viewer *pViewer, std::string outputImagePath);
		;
		void setLight(osg::Group *root);
	};

	class SnapImage : public osg::Camera::DrawCallback
	{

		SnapImage(const std::string &filename) : _filename(filename),
												 _snapImage(false)
		{
			_image = new osg::Image;
		}

		SnapImage()
		{
			_filename = "Screen.png";
			_snapImage = false;
			_image = new osg::Image;
		}
		static osg::ref_ptr<SnapImage> instance;

	  public:
		// instance
		static osg::ref_ptr<SnapImage> getInstance(void)
		{
			if (!instance)
				instance = new SnapImage();

			return instance;
		}

		virtual void operator()(osg::RenderInfo &renderInfo) const
		{

			if (!_snapImage)
				return;

			//osg::notify(osg::NOTICE) << "Camera callback" << std::endl;

			osg::Camera *camera = renderInfo.getCurrentCamera();
			osg::Viewport *viewport = camera ? camera->getViewport() : 0;

			//osg::notify(osg::NOTICE) << "Camera callback " << camera << " " << viewport << std::endl;

			if (viewport && _image.valid())
			{
				_image->readPixels(int(viewport->x()), int(viewport->y()), int(viewport->width()), int(viewport->height()),
								   GL_RGBA,
								   GL_UNSIGNED_BYTE);
				osgDB::writeImageFile(*_image, _filename);

				osg::notify(osg::NOTICE) << "Taken screenshot, and written to '" << _filename << "'" << std::endl;
			}

			_snapImage = false;
		}

		std::string _filename;
		mutable bool _snapImage;
		mutable osg::ref_ptr<osg::Image> _image;
	};

	struct SnapeImageHandler : public osgGA::GUIEventHandler
	{

		SnapeImageHandler(int key, SnapImage *si) : _key(key),
													_snapImage(si) {}

		bool handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
		{
			if (ea.getHandled())
				return false;

			switch (ea.getEventType())
			{
			case (osgGA::GUIEventAdapter::KEYUP):
			{
				if (ea.getKey() == _key)
				{
					osg::notify(osg::NOTICE) << "event handler" << std::endl;
					_snapImage->_snapImage = true;
					return true;
				}

				break;
			}
			default:
				break;
			}

			return false;
		}

		// Shader
		bool loadShader(osg::Shader *obj, const std::string &fileName);

		int _key;
		osg::ref_ptr<SnapImage> _snapImage;
	};

	osg::Geode *createScreenQuad(float width, float height, float scale = 1.0f);
	osg::Camera *createHUDCamera(double left, double right, double bottom, double top);
	osgText::Text *createText(const osg::Vec3 &pos, const std::string &content, float size);

	// Configure parameters
	class CConfig
	{
	  public:
		CConfig();

		static std::string CALIB_FILE_PATH;
		static std::string PLY_FILE_NAME;

		static int MONITOR_WIDTH;
		static int MONITOR_HEIGHT;

		static int PROJECTOR_WIDTH;
		static int PROJECTOR_HEIGHT;

		static bool DO_CALIBRATION;
		static int Calib_Project_Intervel;
		static bool Calib_Project_Loop;
		static std::string Calib_PROJ_IMG_0;
		static std::string Calib_PROJ_IMG_1;
		static std::string Calib_PROJ_IMG_2;
		static std::string Calib_PROJ_IMG_3;
		static std::string Calib_PROJ_IMG_4;
		static std::string Calib_PROJ_IMG_BLACK;

		static std::string PROJ_IMG_0;
		static std::string PROJ_IMG_1;
		static std::string PROJ_IMG_2;
		static std::string PROJ_IMG_3;
		static std::string PROJ_IMG_4;
	};

} // namespace virtual2real