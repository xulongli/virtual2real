#include <osg/Node>
#include <osgViewer/Viewer>
#include <osg/Program>
#include <osgDB/ReadFile>
#include <osg/Shader>
#include <osgViewer/ViewerEventHandlers>

static char const *vertexShader = {
	"varying vec4 color;\n"
	"void main(void ){\n"
	"color = gl_Vertex;\n"
	"gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;\n"
	"}\n"};
static char const *fragShader = {
	"varying vec4 color;\n"
	"void main(void){\n"
	"	gl_FragColor = clamp(color,0.0,1.0);\n"
	"}\n"};

class MVPCallback : public osg::Uniform::Callback
{
  public:
	MVPCallback(osg::Camera *camera) : mCamera(camera)
	{
	}
	virtual void operator()(osg::Uniform *uniform, osg::NodeVisitor *nv)
	{
		osg::Matrixd modelView = mCamera->getViewMatrix();
		osg::Matrixd projectM = mCamera->getProjectionMatrix();
		uniform->set(modelView * projectM);
	}

  private:
	osg::Camera *mCamera;
};

int main()
{
	osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer;
	osg::ref_ptr<osg::Node> node = osgDB::readNodeFile("glider.osg");

	osg::StateSet *ss = node->getOrCreateStateSet();
	osg::Program *program = new osg::Program;
	program->addShader(new osg::Shader(osg::Shader::FRAGMENT, fragShader));
	program->addShader(new osg::Shader(osg::Shader::VERTEX, vertexShader));
	ss->setAttributeAndModes(program, osg::StateAttribute::ON);

	viewer->addEventHandler(new osgViewer::WindowSizeHandler);
	viewer->setSceneData(node.get());
	return viewer->run();
}