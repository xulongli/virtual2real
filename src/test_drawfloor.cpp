// #include <windows.h>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <iostream>
#include <queue>
#include "PointCloud.h"
#include "Util.h"
#include "INIReader.h"

using namespace virtual2real;

int main()
{
	osg::ref_ptr<osg::MatrixTransform> node = new osg::MatrixTransform();

	CUtil *utl = new CUtil();
	utl->drawFloor(node);

	osgViewer::Viewer viewer;
	viewer.setSceneData(node);
	return viewer.run();
}
