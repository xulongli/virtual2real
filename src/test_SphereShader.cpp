#include <osg/ShapeDrawable>
#include <osg/Geometry>
#include <osg/StateSet>
#include <osgViewer/Viewer>
#include <osgGA/TrackballManipulator>
#include <iostream>

using namespace osg;

Node *startup()
{
	Group *scene = new Group();

	Geode *sphere = new Geode();
	sphere->addDrawable(new ShapeDrawable(new Sphere(Vec3(), 1)));

	StateSet *sphereStateSet = sphere->getOrCreateStateSet();
	sphereStateSet->ref();

	Program *programObject = new Program();
	Shader *vertexObject = new Shader(Shader::VERTEX);
	Shader *fragmentObject = new Shader(Shader::FRAGMENT);
	programObject->addShader(fragmentObject);
	programObject->addShader(vertexObject);

	vertexObject->loadShaderSourceFromFile("./shaders/gouraud.vert");
	fragmentObject->loadShaderSourceFromFile("./shaders/gouraud.frag");

	sphereStateSet->setAttributeAndModes(programObject, StateAttribute::ON);

	scene->addChild(sphere);
	return scene;
}

int main()
{
	Node *scene = startup();
	if (!scene)
		return 1;
	osgViewer::Viewer viewer;
	viewer.setSceneData(scene);
	viewer.setCameraManipulator(new osgGA::TrackballManipulator());
	viewer.realize();

	while (!viewer.done())
	{
		viewer.frame();
	}
}
