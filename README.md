

# Immigrate from svn to git (2019-03-01)
- immigrate command check /home/xulongli/Script/svn/README.md
- ohter checkout version files please check ./Repositories.locate.cache and virtual2real.locate.cache


# Install openscenegraph on ubuntu
```
# https://launchpad.net/~openmw/+archive/ubuntu/openmw
# https://askubuntu.com/questions/823007/building-openscenegraph-3-4-from-source-ubuntu
sudo add-apt-repository ppa:openmw/openmw
sudo apt-get update
sudo apt install openscenegraph-3.4
osgversion #OpenSceneGraph Library 3.4.1

cd ~ && wget -c https://github.com/openscenegraph/OpenSceneGraph/archive/OpenSceneGraph-3.4.0.zip
unzip OpenSceneGraph-3.4.0.zip
rm OpenSceneGraph-3.4.0.zip 

cd ~/OpenSceneGraph-OpenSceneGraph-3.4.0 && cmake . && make -j8 && sudo make install && export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib64 && ldconfig



# rm -rf /home/xulongli/OpenSceneGraph-OpenSceneGraph-3.4.

# Example model
cd ~ && git clone https://github.com/openscenegraph/OpenSceneGraph-Data.git
```

设置环境变量
```
export PATH=${PATH}:/home/xulongli/OpenSceneGraph-OpenSceneGraph-3.4.0/bin 
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib64:/home/xulongli/OpenSceneGraph-OpenSceneGraph-3.4.0/lib 
export OSG_FILE_PATH=/home/xulongli/OpenSceneGraph-Data:/home/xulongli/OpenSceneGraph-Data/Images 

echo "" >> ~/.bashrc
echo "# OpenSceneGraph" >> ~/.bashrc
echo "export PATH=${PATH}:/home/xulongli/OpenSceneGraph-OpenSceneGraph-3.4.0/bin" >> ~/.bashrc
echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib64:/home/xulongli/OpenSceneGraph-OpenSceneGraph-3.4.0/lib" >> ~/.bashrc
echo "export OSG_FILE_PATH=/home/xulongli/OpenSceneGraph-Data:/home/xulongli/OpenSceneGraph-Data/Images" >> ~/.bashrc
echo "OSG_NOTIFY_LEVEL=DEBUG_FP" >> ~/.bashrc
#echo "OSG_NOTIFY_LEVEL=NOTICE" >> ~/.bashrc
```

# Install opencv on ubuntu
```bash
sudo apt-get install cmake
sudo apt-get install python-devel numpy
sudo apt-get install gcc gcc-c++

sudo apt-get install gtk2-devel
sudo apt-get install libv4l-devel
sudo apt-get install ffmpeg-devel
sudo apt-get install gstreamer-plugins-base-devel


git clone https://github.com/opencv/opencv.git
wget -c https://codeload.github.com/opencv/opencv/zip/master
mdkir /home/xulongli/opencv-master/build && cd /home/xulongli/opencv-master/build
cmake .. && make -j8 && make install
```


Add opencv to cmake
```cmake
# Find OpenCV, you may need to set OpenCV_DIR variable
# to the absolute path to the directory containing OpenCVConfig.cmake file
# via the command line or GUI
# /usr/local/lib/cmake/
find_package(OpenCV REQUIRED)
```

例如:
```cmake
cmake_minimum_required(VERSION 3.5)#cmake版本
project( XULONGLI )#项目名称
find_package( OpenCV REQUIRED )#找到opencv源
add_executable( XULONGLI main.cpp )#
target_link_libraries( XULONGLI ${OpenCV_LIBS} )#添加opencv链接选项
```
# Install eigen on ubuntu
```bash
cd ~
wget -c https://github.com/eigenteam/eigen-git-mirror/archive/3.3.7.zip
unzip 3.3.7.zip && rm 3.3.7.zip
cp -r ~/eigen-git-mirror-3.3.7/Eigen /usr/local/include/
rm -rf ~/eigen-git-mirror-3.3.7
```

# Install SDL(Optional, for AntTweakBar example)
- SDL1
```bash
sudo apt-get install libsdl1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsdl-gfx1.2-dev
```

- SDL1 Github
```bash
sudo apt-get install x11proto-xext-dev
sudo apt-get install libxext-dev
cd ~ && wget -c  http://www.libsdl.org/release/SDL-1.2.15.zip
unzip SDL-1.2.15.zip
cd ~/SDL-1.2.15 && ./configure; make; make install
rm ~/SDL-1.2.15.zip && rm -rf SDL-1.2.15
```

- SDL2 Github
```bash
cd ~ && wget -c http://www.libsdl.org/release/SDL2-2.0.9.zip
unzip SDL2-2.0.9.zip
mkdir -p ~/SDL2-2.0.9/build  && cd ~/SDL2-2.0.9/build && ../configure && cmake .. && make -j8 && sudo make install
rm -rf ~/SDL2-2.0.9
rm SDL2-2.0.9.zip
```

# GLFW
Add the following line to /etc/apt/sources.list
`deb http://ppa.launchpad.net/keithw/glfw3/ubuntu trusty main`
Then execute these commands

```bash
sudo apt-get update
sudo apt-get install libglfw3 libglfw3-dev
sudo apt-get install libglfw2 libglfw-dev

```

# Install AntTweakBar on ubuntu
```bash
# download it from: https://sourceforge.net/projects/anttweakbar/files/latest/download?source=dlp
cd /home/xulongli/AntTweakBar/src && make
cd /home/xulongli/AntTweakBar/examples && make
mkdir /usr/local/include/AntTweakBar
cp -r /home/xulongli/AntTweakBar/include/* /usr/local/include/AntTweakBar
cp -r /home/xulongli/AntTweakBar/lib/libAntTweakBar.a /usr/local/lib6/
sudo cp /home/xulongli/AntTweakBar/lib/libAntTweakBar.so /usr/local/lib64/
sudo cp /home/xulongli/AntTweakBar/lib/libAntTweakBar.so.1 /usr/local/lib64/

```
# SLN to CMake
```
cd ~
git clone https://github.com/mrpi/proj2cmake.git
cd proj2cmake && cmake . && make

cd /virtual2real-git
~/proj2cmake/proj2cmake virtual2real.sln
```

# Code: windows to Linux
`include <windows.h>` no work anymore
```cpp
    #ifdef _WIN32
    // do something for windows like include <windows.h>
        include<windows.h>
    #elif defined __unix__
    // do something for unix like include <unistd.h>
        include<unistd.h>
    #elif defined __APPLE__
    // do something for mac
    #endif
```

# Code: Visual Studio to GCC/G++
- 测试方法: 

```bash
g++ test_drawfloor.cpp -lOpenThreads -losg -losgDB -losgUtil -losgGA -losgText -losgViewer -o xxx
g++ test_SphereShader.cpp -lOpenThreads -losg -losgDB -losgUtil -losgGA -losgText -losgViewer -o xxx

opt='-lOpenThreads -losg -losgDB -losgUtil -losgGA -losgText -losgViewer -o xxx'
g++ test_fullScreenTexture.cpp              $opt
g++ test_render2img.cpp                     $opt
g++ test_renderer.cpp                       $opt
g++ test_PerspectiveShader.cpp              $opt
g++ test_PerspectiveShader.bak.cpp          $opt
g++ test_FBO.cpp                            $opt
g++ test_perspectiveRender.cpp              $opt
g++ test_BrickShader.cpp                    $opt
g++ test_follow_camera.cpp                  $opt
g++ test_drawCamera.cpp                     $opt
g++ test_textureShader.cpp                  $opt
g++ test_calib2projMat.cpp                  $opt
g++ test_LightShader.cpp                    $opt
g++ test_perspectiveTextureShader.cpp       $opt
g++ test_PointCloud.cpp                     $opt
g++ test_multiple_windows.cpp               $opt
g++ test_multileProjector2TextureShader.cpp $opt
g++ test_texture.cpp                        $opt
g++ test_colorShader.cpp                    $opt
g++ test_SimpleShader.cpp                   $opt
g++ test_SphereShader.cpp                   $opt
g++ test_drawfloor.cpp                      $opt
g++ test_PerspectiveShader_sucess.cpp       $opt
```

统一缩减代码为
```cmake
# https://stackoverflow.com/questions/14306642/adding-multiple-executables-in-cmake
# https://answers.ros.org/question/284889/cmake-iterate-over-multiple-source-files-for-multiple-executables/
file( GLOB_RECURSE virtual2real_TEST_SRC RELATIVE "${virtual2real_SOURCE_DIR}" src/test_*.cpp)
print_list(${virtual2real_TEST_SRC})

foreach( file ${virtual2real_TEST_SRC})
    string( REPLACE ".cpp" "" name ${file})
    print_list(${name})
    print_list(${file})
    add_executable( ${name} ${file})
    target_link_libraries( ${name}  ${virtual2real_DEPS} ${virtual2real_ADDITIONAL_DEPS} ${SOLUTION_APP_DEPS} ${SOLUTION_GENERAL_DEPS})
endforeach( file ${virtual2real_TEST_SRC} )
```

- 错误: `PointCloud.h:20:7: error: extra qualification ‘CPointCloud::’ on member ‘loadPointCloud’ [-fpermissive]`

```
class CPointCloud
{
public:
	CPointCloud();
	void CPointCloud::loadPointCloud(std::string file, osg::ref_ptr<osg::MatrixTransform> node );

private:

	bool shader;
	bool usePointSprites;
	bool forcePointMode;
};
```
`g++`编译错误, `void CPointCloud::loadPointCloud(std::string file, osg::ref_ptr<osg::MatrixTransform> node );`应该删除`CPointCloud::`修改为`void loadPointCloud(std::string file, osg::ref_ptr<osg::MatrixTransform> node );`


- 错误: `src/Camera.h:320:57: error: ‘>>’ should be ‘> >’ within a nested template argument list`

```cpp
	std::vector< osg::ref_ptr<osg::PositionAttitudeTransform> > camVGAGeoList;
```


- 错误 `error: ‘osg::Texture2D::WrapParameter’ is not a class or namespace`
```cpp
    #include <osg/Texture>
	texture->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
```

- 错误 `test_drawfloor.cpp:(.text+0x74): undefined reference to `virtual2real::CUtil::drawFloor(osg::ref_ptr<osg::MatrixTransform>)'`
```cpp
opt='-lOpenThreads -losg -losgDB -losgUtil -losgGA -losgText -losgViewer -lGL -lGLU -lX11 -lXext -lXmu -lXrandr -lm -lGLEW -lglut -lAntTweakBar -o xxx'
g++ ini.c INIReader.cpp Util.cpp test_drawfloor.cpp $opt
```
暂时解决方法:
```cmake
    ADD_EXECUTABLE( ${name} 
            "src/INIReader.cpp"
            "src/PointCloud.cpp"
            "src/Util.cpp"
            "src/ini.c"    
            ${file})
```            


- [未解决]错误: `src/test_renderer.cpp:  mex.h not found`

```cmake
# /home/xulongli/software/MATLAB/R2016b/extern/include/mex.h

```

- 总结: 测试案例仅`src/test_renderer.cpp`没有编译通过


# change log

- git 尝试merg /xlBackup/DiskWin/c/Review/Code/virtual2real/backup
```
mkdir ~/virtual2real-git/landmarks/
cd /xlBackup/DiskWin/c/Review/Code/virtual2real/backup
cp "2016-4-25 201401 virtual2real.7z" ~/virtual2real-git/landmarks/2016-4-25-201401-virtual2real.7z
cp "2016-4-26 153944 virtual2real.7z" ~/virtual2real-git/landmarks/2016-4-26-153944-virtual2real.7z
7za a -t7z ~/virtual2real-git/landmarks/2016-4-30-023638-virtual2real.7z "2016-4-30 023638 virtual2real"  
7za a -t7z ~/virtual2real-git/landmarks/2016-5-3-173236-virtual2real.7z "2016-5-3 173236 virtual2real"  
7za a -t7z ~/virtual2real-git/landmarks/2016-5-6-041658-Success.7z "2016-5-6 041658 Success"  
7za a -t7z ~/virtual2real-git/landmarks/2016-5-8-173851-GUI修改-前天基础存在旋转PI问题.7z "2016-5-8 173851 GUI修改-前天基础存在旋转PI问题"  
```


- git 尝试merge /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn
```bash
cd /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn
#7za a -t7z ~/virtual2real-git/landmarks/src.7z src
7za a -t7z ~/virtual2real-git/landmarks/src_13.7z src_13
7za a -t7z ~/virtual2real-git/landmarks/src_14.7z src_14
7za a -t7z ~/virtual2real-git/landmarks/src_5.7z src_5
7za a -t7z ~/virtual2real-git/landmarks/src_6.7z src_6
7za a -t7z ~/virtual2real-git/landmarks/src_before_version6_add_ver16.7z src_before_version6_add_ver16
7za a -t7z ~/virtual2real-git/landmarks/src_v1.7z src_v1
7za a -t7z ~/virtual2real-git/landmarks/src_v2.7z src_v2
7za a -t7z ~/virtual2real-git/landmarks/src_v3.7z src_v3
7za a -t7z ~/virtual2real-git/landmarks/src_work.7z src_work

```

然后单独对src进行merge
```bash
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/src  ~/virtual2real-git/
```

然后单独对projct文件进行merge
```bash
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/virtual2real.vcxproj  ~/virtual2real-git/
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/virtual2real.sln  ~/virtual2real-git/
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/virtual2real.vcxproj*  ~/virtual2real-git/

```


然后单独对其他目录进行merge
```bash
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/doc  ~/virtual2real-git/
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/config  ~/virtual2real-git/
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/shaders  ~/virtual2real-git/
cp -r /xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/script  ~/virtual2real-git/
```


[未做处理]其他比较大的文件夹处理
```bash
/xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/render # 744M
/xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/model # 524N, 可以忽略,删除了ply,obj文件,但是保留了7z源文件
/xlBackup/DiskWin/c/Review/Code/virtual2real/version0/svn/calib # 2.1G, 160224_ultimatum1_00002366_VGA等等未包含...
/xlBackup/DiskWin/c/Review/Code/virtual2real/version6
/xlBackup/DiskWin/c/Review/Code/virtual2real/version6_add_ver16
/xlBackup/DiskWin/c/Review/Code/virtual2real/....
```

- git 尝试merge /xlBackup/NAS_Posefs4b_xulongli/v2r
```bash
cp /xlBackup/NAS_Posefs4b_xulongli/v2r/build.bat ~/virtual2real-git/build.windows.bat
```

# 测试案例情况统计: 
## 编译
- [x] src/test_BrickShader.cpp
- [x] src/test_FBO.cpp
- [x] src/test_multiple_windows.cpp
- [x] src/test_PointCloud.cpp
- [x] src/test_texture.cpp
- [x] src/test_calib2projMat.cpp
- [x] src/test_follow_camera.cpp
- [x] src/test_perspectiveRender.cpp
- [x] src/test_render2img.cpp
- [x] src/test_textureShader.cpp
- [x] src/test_colorShader.cpp
- [x] src/test_fullScreenTexture.cpp
- [x] src/test_PerspectiveShader.cpp
- [x] src/test_renderer.cpp
- [x] src/test_drawCamera.cpp
- [x] src/test_LightShader.cpp
- [x] src/test_PerspectiveShader.bak.cpp
- [x] src/test_SimpleShader.cpp
- [x] src/test_drawfloor.cpp
- [x] src/test_multileProjector2TextureShader.cpp
- [x] src/test_perspectiveTextureShader.cpp
- [x] src/test_SphereShader.cpp
- [x] src/test_PerspectiveShader_sucess.cpp
- [ ] src/test_renderer.cpp


## 运行
- [ ] bin/test_BrickShader
- [ ] bin/test_FBO
- [x] bin/test_multiple_windows
- [x] bin/test_PointCloud
- [ ] bin/test_texture
- [ ] bin/test_calib2projMat
- [ ] bin/test_follow_camera
- [ ] bin/test_perspectiveRender
- [ ] bin/test_render2img
- [ ] bin/test_textureShader
- [x] bin/test_colorShader
- [x] bin/test_fullScreenTexture
- [ ] bin/test_PerspectiveShader
- [ ] bin/test_renderer
- [x] bin/test_drawCamera
- [x] bin/test_LightShader
- [ ] bin/test_PerspectiveShader.bak
- [x] bin/test_SimpleShader
- [ ] bin/test_drawfloor
- [ ] bin/test_multileProjector2TextureShader
- [x] bin/test_perspectiveTextureShader
- [x] bin/test_SphereShader
- [ ] bin/test_PerspectiveShader_sucess

# Run it!
```
- 编译工程
- 配置OSG自带例子的模型Openscenegraph-Data, 设置OSG_FILE_PATH环境变量
- 配置model目录下模型,部分是7z格式需要解压
- 配置config/config.ini文件中所指向的文件路径
- ./bin/virtual2real
```