#include "Camera.h"
#include "INIReader.h"
using namespace std;
using namespace Eigen;

#define Z_UP_ALONG_Y
//#define Y_DOWN_ALONG_Z

string CCamera::calibPath = "./calib/";

CCamera::CCamera()
{

	// Color for camera edge
	eg_color = new osg::Vec4Array;
	eg_color_VGA = new osg::Vec4Array;
	eg_color_HD = new osg::Vec4Array;
	eg_color_Kinect = new osg::Vec4Array;
	eg_color_Projector = new osg::Vec4Array;

	eg_color->push_back(osg::Vec4(1.0, 0.0, 0.0, 0.5));				  //Red
	eg_color_VGA->push_back(osg::Vec4(1.0, 0.0, 0.0, 0.5));			  //Red
	eg_color_HD->push_back(osg::Vec4(0.0, 1.0, .0, 0.5));			  //Green
	eg_color_Kinect->push_back(osg::Vec4(0.1f, 0.1f, 0.8f, 0.5));	 //Blue
	eg_color_Projector->push_back(osg::Vec4(0.39f, 0.25f, 0.0, 0.5)); //#ffa000 ORANGE

	//eg_color->push_back(			osg::Vec4(1.0, 0.0, 0.0, 1.0)); //Red
	//eg_color_VGA->push_back(		osg::Vec4(0.0, 1.0, 0.0, 1.0)); //Green
	//eg_color_HD->push_back(			osg::Vec4(0.0, 0.0, 1.0, 1.0)); //Blue
	//eg_color_Kinect->push_back(		osg::Vec4(0.39f, 0.25f, 0.0, 1.0)); //#ffa000 ORANGE
	//eg_color_Projector->push_back(	osg::Vec4(0.32f, 0.187f, 0.22f, 1.0)); //#D07090 PALEVIOLETRED
}

CCamera::~CCamera()
{
}

void CCamera::quat2rot(double *q, double *R)
{
	// QUAT2ROT - Quaternion to rotation matrix transformation
	//
	//  Usage: quat2rot(q, R); q: quaternion, R: rotation matrix

	double x, y, z, w, x2, y2, z2, w2, xy, xz, yz, wx, wy, wz;
	double norm_q = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);

	x = q[0] / norm_q;
	y = q[1] / norm_q;
	z = q[2] / norm_q;
	w = q[3] / norm_q;

	x2 = x * x;
	y2 = y * y;
	z2 = z * z;
	w2 = w * w;
	xy = 2 * x * y;
	xz = 2 * x * z;
	yz = 2 * y * z;
	wx = 2 * w * x;
	wy = 2 * w * y;
	wz = 2 * w * z;

	// Format: {R[0] R[1] R[2]; R[3] R[4] R[5]; R[6] R[7] R[8];}
	R[0] = w2 + x2 - y2 - z2;
	R[1] = xy - wz;
	R[2] = xz + wy;
	R[3] = xy + wz;
	R[4] = w2 - x2 + y2 - z2;
	R[5] = yz - wx;
	R[6] = xz - wy;
	R[7] = yz + wx;
	R[8] = w2 - x2 - y2 + z2;
}

osg::ref_ptr<osg::Geometry> CCamera::drawLineSegment(osg::Vec3 sp, osg::Vec3 ep, osg::Vec4 color, float fLineWidth)
{
	//osg::Vec3 sp(0, -180, 120);
	//osg::Vec3 ep(0, 480, 120);
	osg::ref_ptr<osg::Geometry> beam(new osg::Geometry);
	osg::ref_ptr<osg::Vec3Array> points = new osg::Vec3Array;
	points->push_back(sp);
	points->push_back(ep);
	osg::ref_ptr<osg::Vec4Array> _color = new osg::Vec4Array;
	_color->push_back(color);
	beam->setVertexArray(points.get());
	beam->setColorArray(_color.get());
	beam->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);
	beam->addPrimitiveSet(new osg::DrawArrays(GL_LINES, 0, 2));

	// transpanrent
	osg::LineWidth *lineWidth = new osg::LineWidth();
	lineWidth->setWidth(fLineWidth);
	osg::ref_ptr<osg::StateSet> ss = beam->getOrCreateStateSet();
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	ss->setAttributeAndModes(lineWidth, osg::StateAttribute::ON);

	return beam;
}
osg::ref_ptr<osg::Geometry> CCamera::drawLineSegment(osg::Vec3 sp, osg::Vec3 ep)
{
	//osg::Vec3 sp(0, -180, 120);
	//osg::Vec3 ep(0, 480, 120);
	osg::ref_ptr<osg::Geometry> beam(new osg::Geometry);
	osg::ref_ptr<osg::Vec3Array> points = new osg::Vec3Array;
	points->push_back(sp);
	points->push_back(ep);
	//osg::ref_ptr<osg::Vec4Array> color = new osg::Vec4Array;
	//color->push_back(osg::Vec4(1.0, 0.0, 0.0, 1.0));
	beam->setVertexArray(points.get());
	beam->setColorArray(eg_color.get());
	beam->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);
	beam->addPrimitiveSet(new osg::DrawArrays(GL_LINES, 0, 2));

	// transpanrent
	osg::ref_ptr<osg::StateSet> ss = beam->getOrCreateStateSet();
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	return beam;
}

osg::ref_ptr<osg::Group> CCamera::drawCamAxis()
{
	// if (this->camIdx != 10 || this->panelIdx != 0)
	// 	return NULL;

	osg::Vec3 eye = osg::Vec3(0, 0, 0);
	osg::Vec3 cent = osg::Vec3(0, 0, 1);
	osg::Vec3 up = osg::Vec3(0, -1, 0);

	//osg::Matrixd q1 = osg::Matrixd::inverse(osgR);
	osg::Quat q1 = osgQ.inverse();

	osg::Vec3 T = osgT;

	eye = q1 * (eye - T);
	cent = q1 * (cent - T) - eye;
	up = q1 * (up - T) - eye;

	osg::Vec3 zAxis = cent;
	osg::Vec3 yAxis = -up;
	osg::Vec3 xAxis = cent ^ (-up); //Cross product

	osg::ref_ptr<osg::Group> node = new osg::Group;

	// node->addChild( drawLineSegment(osg::Vec3(0, 0, 0), osg::Vec3(1, 0, 0), osg::Vec4(1, 0, 0, 1),2.0 ));
	// node->addChild( drawLineSegment(osg::Vec3(0, 0, 0), osg::Vec3(0, 1, 0), osg::Vec4(0, 1, 0, 1),2.0 ));
	// node->addChild( drawLineSegment(osg::Vec3(0, 0, 0), osg::Vec3(0, 0, 1), osg::Vec4(0, 0, 1, 1),2.0 ));

	// node->addChild(drawLineSegment(eye, eye + xAxis / xAxis.normalize()*100, osg::Vec4(1, 0, 0, 1)));
	// node->addChild(drawLineSegment(eye, eye + yAxis / yAxis.normalize()*100, osg::Vec4(0, 1, 0, 1)));
	// node->addChild(drawLineSegment(eye, eye + zAxis / zAxis.normalize()*100, osg::Vec4(0, 0, 1, 1)));

	node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), xAxis / xAxis.normalize(), osg::Vec4(1, 0, 0, 1)));
	node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), yAxis / yAxis.normalize(), osg::Vec4(0, 1, 0, 1)));
	node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), zAxis / zAxis.normalize(), osg::Vec4(0, 0, 1, 1)));

	// CCamUtil::getInstance()->printVec(osgR*osgCent+osgT, "RX+T=0");
	//
	// CCamUtil::getInstance()->printVec(xAxis, "xAxis");
	// CCamUtil::getInstance()->printVec(yAxis, "yAxis");
	// CCamUtil::getInstance()->printVec(zAxis, "zAxis");
	//
	// CCamUtil::getInstance()->printVec(eye, "eye");
	// CCamUtil::getInstance()->printVec(cent, "cent");
	// CCamUtil::getInstance()->printVec(up, "up");
	//
	// CCamUtil::getInstance()->printVec(osgCent, "osgCent");
	// CCamUtil::getInstance()->printVec(osgQ, "osgQ");
	// CCamUtil::getInstance()->printMatrix(osgR, "osgR");

	return node;
}

void CCamera::drawCamera(osg::ref_ptr<osg::PositionAttitudeTransform> node)
{

	// http://trac.openscenegraph.org/projects/osg//wiki/Support/Tutorials/BasicGeometry
	// http://stackoverflow.com/questions/11489391/openscenegraph-drawing-a-3d-wall
	// http://domedb.perception.cs.cmu.edu/tools/generator/test/
	osg::ref_ptr<osg::Vec3Array> cameraVertices = new osg::Vec3Array;
	osg::ref_ptr<osg::Geometry> cameraGeometry = new osg::Geometry();
	osg::ref_ptr<osg::Geode> cameraGeode = new osg::Geode();

	cameraGeode->addDrawable(cameraGeometry);
	node->addChild(cameraGeode);

	if (0)
	{
		// vertices
		cameraVertices->push_back(osg::Vec3(-0.4, 0.3, 0.5)); //
		cameraVertices->push_back(osg::Vec3(-0.4, -0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(0.4, -0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(0.4, 0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(-0.4, 0.3, -1)); //
		cameraVertices->push_back(osg::Vec3(-0.4, -0.3, -1));
		cameraVertices->push_back(osg::Vec3(0.4, -0.3, -1));
		cameraVertices->push_back(osg::Vec3(0.4, 0.3, -1));
		cameraVertices->push_back(osg::Vec3(-1.2, 0.9, -2)); //
		cameraVertices->push_back(osg::Vec3(-1.2, -0.9, -2));
		cameraVertices->push_back(osg::Vec3(1.2, -0.9, -2));
		cameraVertices->push_back(osg::Vec3(1.2, 0.9, -2));
		cameraGeometry->setVertexArray(cameraVertices);

		// Quad Face
		osg::DrawElementsUInt *camBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		camBase->push_back(0);
		camBase->push_back(1);
		camBase->push_back(2);
		camBase->push_back(3);
		camBase->push_back(0);
		camBase->push_back(4);
		camBase->push_back(5);
		camBase->push_back(1);
		camBase->push_back(0);
		camBase->push_back(3);
		camBase->push_back(7);
		camBase->push_back(4);
		camBase->push_back(0);
		camBase->push_back(4);
		camBase->push_back(5);
		camBase->push_back(1);
		camBase->push_back(3);
		camBase->push_back(2);
		camBase->push_back(6);
		camBase->push_back(7);
		camBase->push_back(2);
		camBase->push_back(1);
		camBase->push_back(5);
		camBase->push_back(6);
		camBase->push_back(5);
		camBase->push_back(4);
		camBase->push_back(7);
		camBase->push_back(6);
		camBase->push_back(5);
		camBase->push_back(4);
		camBase->push_back(8);
		camBase->push_back(9);
		camBase->push_back(5);
		camBase->push_back(9);
		camBase->push_back(10);
		camBase->push_back(6);
		camBase->push_back(7);
		camBase->push_back(6);
		camBase->push_back(10);
		camBase->push_back(11);
		camBase->push_back(7);
		camBase->push_back(11);
		camBase->push_back(8);
		camBase->push_back(4);
		camBase->push_back(9);
		camBase->push_back(8);
		camBase->push_back(11);
		camBase->push_back(10);
		cameraGeometry->addPrimitiveSet(camBase);
	}
	else
	{
		double s = 0.1;
		float width_2 = 1.0;
		float height_2 = 0.6;
		float depth = 3.0;

#ifdef Y_DOWN_ALONG_Z
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0));
		cameraVertices->push_back(osg::Vec3d(-width_2, height_2, depth)); //
		cameraVertices->push_back(osg::Vec3d(-width_2, -height_2, depth));
		cameraVertices->push_back(osg::Vec3d(width_2, -height_2, depth));
		cameraVertices->push_back(osg::Vec3d(width_2, height_2, depth));
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, height_2, depth), osg::Vec3d(-width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, -height_2, depth), osg::Vec3d(width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, -height_2, depth), osg::Vec3d(width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, height_2, depth), osg::Vec3d(-width_2, height_2, depth)));

		// axis
		// node->addChild(drawCamAxis());

#endif

#ifdef Z_UP_ALONG_Y
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0));
		cameraVertices->push_back(osg::Vec3d(-width_2, depth, height_2)); //
		cameraVertices->push_back(osg::Vec3d(-width_2, depth, -height_2));
		cameraVertices->push_back(osg::Vec3d(width_2, depth, -height_2));
		cameraVertices->push_back(osg::Vec3d(width_2, depth, height_2));
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, depth, height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, depth, height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, depth, height_2), osg::Vec3d(-width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, depth, -height_2), osg::Vec3d(width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, depth, -height_2), osg::Vec3d(width_2, depth, height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, depth, height_2), osg::Vec3d(-width_2, depth, height_2)));

		// axis
		// node->addChild(drawCamAxis());

		// Text
		if (panelIdx == HD_PANEL_INDEX)
		{
			osgText::Text *textNode = new osgText::Text();
			stringstream label;
			label << "HD"
				  << "_" << camIdx << endl;

			textNode->setCharacterSize(0.1);
			textNode->setFont("impact.ttf");
			textNode->setText(label.str());
			textNode->setAxisAlignment(osgText::Text::SCREEN);

			node->addChild(textNode);
		}

		if (panelIdx == Kinect_PANEL_INDEX)
		{
			osgText::Text *textNode = new osgText::Text();
			stringstream label;
			label << "Kt"
				  << "_" << camIdx << endl;

			textNode->setCharacterSize(0.1);
			textNode->setFont("impact.ttf");
			textNode->setText(label.str());
			textNode->setAxisAlignment(osgText::Text::SCREEN);

			node->addChild(textNode);
		}

#endif

#ifdef Y_UP_ALONG_Z
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, 0.9, 2) * s, osg::Vec3d(-1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, -0.9, 2) * s, osg::Vec3d(1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, -0.9, 2) * s, osg::Vec3d(1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, 0.9, 2) * s, osg::Vec3d(-1.2, 0.9, 2) * s));
#endif

#ifdef Y_UP_ALONG_Z_MINUS
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0) * s);
		cameraVertices->push_back(osg::Vec3d(-1.0, 0.6, -3) * s); //
		cameraVertices->push_back(osg::Vec3d(-1.0, -0.6, -3) * s);
		cameraVertices->push_back(osg::Vec3d(1.0, -0.6, -3) * s);
		cameraVertices->push_back(osg::Vec3d(1.0, 0.6, -3) * s);
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.0, 0.6, -3) * s, osg::Vec3d(-1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.0, -0.6, -3) * s, osg::Vec3d(1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.0, -0.6, -3) * s, osg::Vec3d(1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.0, 0.6, -3) * s, osg::Vec3d(-1.0, 0.6, -3) * s));
#endif

		//// Quad Face
		osg::ref_ptr<osg::DrawElementsUInt> camBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		camBase->push_back(2);
		camBase->push_back(1);
		camBase->push_back(4);
		camBase->push_back(3);
		cameraGeometry->addPrimitiveSet(camBase);

		osg::ref_ptr<osg::DrawElementsUInt> camBase3 = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
		camBase3->push_back(0);
		camBase3->push_back(1);
		camBase3->push_back(2);
		camBase3->push_back(0);
		camBase3->push_back(2);
		camBase3->push_back(3);
		camBase3->push_back(0);
		camBase3->push_back(3);
		camBase3->push_back(4);
		camBase3->push_back(0);
		camBase3->push_back(4);
		camBase3->push_back(1);
		cameraGeometry->addPrimitiveSet(camBase3);

		//Color
		osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array; // eg_color
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		cameraGeometry->setColorArray(colors);
		cameraGeometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

		// transpanrent
		osg::ref_ptr<osg::StateSet> ss = cameraGeode->getOrCreateStateSet();
		ss->setMode(GL_BLEND, osg::StateAttribute::ON);
		ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	}

	// Rotation + tranlation + scale
	double s = 0.1;

	node->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)) * osgQ.inverse());
	node->setScale(osg::Vec3(s, s, s));
	node->setPosition(osgCent);

	// if ((camIdx == 10) && (panelIdx == HD_PANEL_INDEX))
	// {
	// 	osg::Vec3 vec = node->getPosition();
	// 	CCamUtil::getInstance()->printVec(vec, "HD10");
	// }
}

bool CCamera::readMatrix(const char *filename, float *mat)
{
	std::ifstream file(filename);
	std::string str = "";
	//float _mat[14];

	if (file.fail())
	{
		return true;
	}
	else
	{
		int i = 0;
		while (file >> mat[i])
		{
			i++;
		}
	}

	//cout << _mat[0] << "," << _mat[1] << "," << _mat[2] << endl;

	return false;
};

bool CCamera::isValidCam()
{
	return bValid;
}

void CCamera::setDrawColor(int NODE_MASK)
{
	switch (NODE_MASK)
	{
	case VGA_NODE_MASK:
		eg_color = eg_color_VGA;
		break;

	case HD_NODE_MASK:
		eg_color = eg_color_HD;
		break;

	case Kinect_NODE_MASK:
		eg_color = eg_color_Kinect;
		break;

	case Proj_NODE_MASK:
		eg_color = eg_color_Projector;
		break;

	default:
		eg_color = eg_color_VGA;
		break;
	}
}

void CCamera::setName(int _panelIdx, int _camIdx)
{
	stringstream _name;
	_name << "VGA_" << _panelIdx << "_" << _camIdx;

	this->name = _name.str();
	this->panelIdx = _panelIdx;
	this->camIdx = _camIdx;
}

bool CCamera::initCameraPrameters(string _calibPath, int _panelIdx, int _camIdx)
{
	std::stringstream intrFilePath;
	std::stringstream extrFilePath;

	intrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << ".txt";
	extrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << "_ext.txt";

	//cout << "intrFilePath: " << intrFilePath.str() << endl;
	//cout << "extFilePath: " << extrFilePath.str() << endl;

	// Intrinsic Parameter : (panelIdx)_(camIdx).txt
	//
	//	Composed of 11 floating point numbers as follows.
	//	K11 K12 K13 K21 K22 K23 K31 K32 K33 distortionParam1 distortionParam2 xxx
	//	All the sequences contain rectified images, thus both distortion parameters are zeros.
	//
	float intr_mat[14];
	if (readMatrix(intrFilePath.str().c_str(), intr_mat))
		return true;

	// Extrinsic Parameter : (panelIdx)_(camIdx)_ext.txt
	//
	// 	Composed of 7 floating point numbers
	// 	The first 4 numbers are quaternion representation of R
	// 	The last 3 numbers are the camera center, which is - invR x t
	// 	Thus, t = -R*Center
	float extr_mat[7];
	if (readMatrix(extrFilePath.str().c_str(), extr_mat))
		return true;

	// Intrisic matrix
	matM << intr_mat[0], intr_mat[1], intr_mat[2],
		intr_mat[3], intr_mat[4], intr_mat[5],
		intr_mat[6], intr_mat[7], intr_mat[8];

	// Distortion matrix
	Map<RowVectorXf> _matDistortion(intr_mat + 9, 5);
	matDistortion = _matDistortion;

	// Rotation quternion
	Map<RowVectorXf> _matRq(extr_mat, 4);
	matRq = _matRq;

	// Camera center
	Map<RowVectorXf> _vecCent(extr_mat + 4, 3);
	vecCent = _vecCent;

	// Rotation matrix
	//double q[4] = { matRq(0), matRq(1), matRq(2), matRq(3) };
	double q[4] = {matRq(1), matRq(2), matRq(3), matRq(0)}; // xyzw
	double rot[9];
	quat2rot(q, rot);
	matR << rot[0], rot[1], rot[2],
		rot[3], rot[4], rot[5],
		rot[6], rot[7], rot[8];

	// @TODO
	// osgR = osg::Matrixd(matR(0,0), matR(1,0), matR(2,0), 0,
	// 					matR(0,1), matR(1,1), matR(2,1), 0,
	// 					matR(0,2), matR(1,2), matR(2,2), 0,
	// 					matR(0,3), matR(1,3), matR(2,3), 1);
	//
	osgR = osg::Matrixd(rot[0], rot[1], rot[2], 0,
						rot[3], rot[4], rot[5], 0,
						rot[6], rot[7], rot[8], 0,
						0, 0, 0, 1);

	//osgQ = osg::Quat(q[0],q[1],q[2],q[3]);

	// Notice: Original Calibration data [w x y z]
	// (1) inline Quat( value_type x, value_type y, value_type z, value_type w )
	// (2) quat2rot() [x y z w]
	osgQ = osg::Quat(_matRq(1), _matRq(2), _matRq(3), _matRq(0)); // xyzw
	osgCent = osg::Vec3(_vecCent(0), _vecCent(1), _vecCent(2));

	vecT = -matR * vecCent.transpose();

	// @ATTENSION: -q*t vs q*(-t) is different or not?
	osgT = osgQ * (-osgCent);

	//osgT = osg::Vec3(vecT(0), vecT(1), vecT(2));
	// std::cout << "vecT: \n" << vecT << endl;
	// CCamUtil::getInstance()->printVec(osgT, "osgT");

	// Translate vector, t = -R*Center			@TODO

	// std::cout << "matM: \n" << matM << endl;
	// std::cout << "matDistortion: \n" << matDistortion << endl;
	// std::cout << "matRq: \n" << matRq << endl;
	// std::cout << "vecT: \n" << vecT << endl;
	// std::cout << "vecCent: \n" << vecCent << endl;

	// TEST Caculate T
	// osg::Vec3 T1 = osgQ*(-osgCent);
	// osg::Vec3 T2 = osg::Vec3(vecT(0), vecT(1), vecT(2));	// -matR*vecCent.transpose();
	// osg::Vec3 T3 = osgR*(-osgCent);
	// osg::Vec3 T4 = osgR*osgCent; T4 = -T4;
	// osg::Vec3 T5 = osgQ*osgCent; T5 = -T5;
	// osg::Vec3 T6 = -osgQ*(-osgCent);
	//
	//
	// CCamUtil::getInstance()->printVec(T1, "T1");
	// CCamUtil::getInstance()->printVec(T2, "T2");
	// CCamUtil::getInstance()->printVec(T3, "T3");
	// CCamUtil::getInstance()->printVec(T4, "T4");
	// CCamUtil::getInstance()->printVec(T5, "T5");
	// CCamUtil::getInstance()->printVec(T6, "T6");
	// CCamUtil::getInstance()->printVec(osgT, "osgT");

	// TEST Cent
	// CCamUtil::getInstance()->printVec(osgCent, "CanCent");
	// CCamUtil::getInstance()->printVec(osgQ.inverse()*(-T1), "CanCent_Recalc");

	// TEST T
	// CCamUtil::getInstance()->printVec(osgR*osgCent + osgT, "RX+T=0");
	// CCamUtil::getInstance()->printVec(osgQ*osgCent + osgT, "RX+T=0");

	return false;
}

osg::Matrixd CCamera::getProjectionMatrix(int width, int height, float zfar, float znear)
{
	float depth = zfar - znear;
	float q = -(zfar + znear) / depth;
	float qn = -2 * (zfar * znear) / depth;

	float _K00 = matM(0, 0);
	float _K01 = matM(0, 1);
	float _K02 = matM(0, 2);
	float _K11 = matM(1, 1);
	float _K12 = matM(1, 2);

	float x0 = 0.0f;
	float y0 = 0.0f;

	osg::Matrixd p = osg::Matrixf(2 * _K00 / width, -2 * _K01 / width, (-2 * _K02 + width + 2 * x0) / width, 0,
								  0, 2 * _K11 / height, (2 * _K12 - height + 2 * y0) / height, 0,
								  0, 0, q, qn,
								  0, 0, -1, 0);

	osg::Matrixd pT;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			pT(i, j) = p(j, i);
		}
	}

	return pT;
	//return osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));
}

osg::Matrixd CCamera::getModelViewMatrix()
{
	// Usage:
	// Create parent MatrixTransform to transform the view volume by
	// the inverse ModelView matrix.
	// osg::MatrixTransform* mt = new osg::MatrixTransform;
	// mt->setMatrix(osg::Matrixd::inverse(mv));
	// mt->addChild(geode);

	//return osgR;

	//#ifdef Y_UP_ALONG_Z_MINUS
	//	parentView.makeRotate(-osg::PI_2, osg::Vec3(1, 0, 0));
	//#endif

	// osg::Matrixd modelViewMat = osgR;
	// modelViewMat.makeTranslate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));
	//
	// //osg::Matrixd modelViewMat = osg::Matrixd();
	// modelViewMat.makeTranslate(parentView.getTrans());
	//
	// return modelViewMat;
	//return parentView*modelViewMat;

	// return osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));

	osg::Quat q = (osg::Quat(osg::PI_2 / 2, osg::Vec3(1, 0, 0)) * osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3)));
	double rVec[9];
	double qVec[4] = {q.x(), q.y(), q.z(), q.w()};
	quat2rot(qVec, rVec);

	return osg::Matrixd(rVec);
}

//*********************************************
//				VGA
//*********************************************

CCamVGA::CCamVGA(string _calibPath, int _panelIdx, int _camIdx)
{
	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing VGA camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamVGA::drawVGACam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawVGACam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(VGA_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	//osg::Vec3f pos = geoCam->getBound().center();
	// osg::Vec3f pos = osg::Vec3(vecCent(0), vecCent(1), vecCent(2));
	// osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	// float scale = 0.0001* geoCam->getBound().radius();
	//
	// osg::Matrix m;
	// 	m.makeScale(osg::Vec3(scale,scale,scale));
	// m.setTrans(pos);
	// m.setRotate(quat);
	//
	// geoCam->setMatrix(m);

	//geoCam->setMatrix(osg::Matrix::rotate(quat) * osg::Matrix::translate(pos));

	// rotation @TODO getMatrix and setMatrix
	//geoCam->setMatrix(osg::Matrix::rotate( osg::Vec4(matRq(0), matRq(1), matRq(2), matRq(3) )));

	// position

	// Rotatiton + Postion
	//osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	//geoCam->setMatrix( osg::Matrixd() *osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	//osg::Matrixd osgRt(	matR(0, 0), matR(1, 0), matR(2, 0), 0,
	//					matR(0, 1), matR(1, 1), matR(2, 1), 0,
	//					matR(0, 2), matR(1, 2), matR(2, 2), 0,
	//					matR(0, 3), matR(1, 3), matR(2, 3), 1);

	//geoCam->setMatrix(osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));
	return true;
}

osg::ref_ptr<osg::Camera> CCamVGA::createVirtualCam()
{
	osg::ref_ptr<osg::Camera> renderCam;

	//renderCam->setProjectionMatrix();
	//renderCam->setClearColor();
	//renderCam->setCullMask();

	return renderCam;
}

//*********************************************
//				HD
//*********************************************

CCamHD::CCamHD(string _calibPath, int _camIdx)
{

	int _panelIdx = HD_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing HD camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamHD::drawHDCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawHDCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(HD_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	// Rotatiton + Postion
	//osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	//geoCam->setMatrix(osg::Matrix::rotate(quat) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	return true;
}

//*********************************************
//				Kinect
//*********************************************

CCamKinect::CCamKinect(string _calibPath, int _camIdx)
{
	int _panelIdx = Kinect_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing Kinect camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamKinect::drawKinectCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawKinectCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(Kinect_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	// Rotatiton + Postion
	// osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	// geoCam->setMatrix(osg::Matrix::rotate(quat) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	return true;
}

//*********************************************
//				Projctor
//*********************************************

CCamProj::CCamProj(string _calibPath, int _camIdx)
{
	int _panelIdx = Proj_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing Projctor camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamProj::drawProjCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawProjCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(Proj_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	// Rotatiton + Postion
	// osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	// geoCam->setMatrix( osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	return true;
}

//*********************************************
//				Util
//*********************************************
osg::ref_ptr<CCamUtil> CCamUtil::instance = NULL;

// VGA initialize
void CCamUtil::initialVGA(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{

	for (int panelIdx = 1; panelIdx <= 20; panelIdx++)
	{												 //20
		for (int camIdx = 1; camIdx <= 24; camIdx++) //24
		{
			//if (camIdx == 5)
			//	continue;
			//cout << "VGA Camera: " << panelIdx << "," << camIdx << endl;

			CCamVGA *camVGA = new CCamVGA(CALIB_FILE_PATH, panelIdx, camIdx);
			camVGAList.push_back(camVGA);

			osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
			if (camVGA->drawVGACam(node))
				scene->addChild(node);

			node->setNodeMask(VGA_NODE_MASK);
			camVGAGeoList.push_back(node);
		}
	}
}

//HD initialize
void CCamUtil::initialHD(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{

	for (int camIdx = 1; camIdx <= 31; camIdx++)
	{
		//cout << "HD Camera: " << camIdx << endl;

		CCamHD *camHD = new CCamHD(CALIB_FILE_PATH, camIdx);
		camHDList.push_back(camHD);

		osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
		if (camHD->drawHDCam(node))
			scene->addChild(node);

		node->setNodeMask(HD_NODE_MASK);
		camHDGeoList.push_back(node);
	};
}

//Kinect initialize
void CCamUtil::initialKinect(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{

	for (int camIdx = 1; camIdx <= 10; camIdx++)
	{
		//cout << "Kinect Camera: "<< camIdx << endl;

		CCamKinect *camKinect = new CCamKinect(CALIB_FILE_PATH, camIdx);
		camKinectList.push_back(camKinect);

		osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
		if (camKinect->drawKinectCam(node))
			scene->addChild(node);

		node->setNodeMask(Kinect_NODE_MASK);
		camKinectGeoList.push_back(node);
	};
}

// Projector
void CCamUtil::initialProj(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{
}

// VGA get instance by index
CCamVGA *CCamUtil::getVGAInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamVGA *>::iterator it = camVGAList.begin(); it != camVGAList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// HD get instance by index
CCamHD *CCamUtil::getHDInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamHD *>::iterator it = camHDList.begin(); it != camHDList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// Kinect get instance by index
CCamKinect *CCamUtil::getKinectInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamKinect *>::iterator it = camKinectList.begin(); it != camKinectList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// HD get instance by index
CCamProj *CCamUtil::getProjInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamProj *>::iterator it = camProjList.begin(); it != camProjList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

void CCamUtil::getMatrixOfProjector(int panelIdx, int camIdx)
{
	//	Used for setting slave viewer camera
	// 1. get in/extrisic matrix from projector calibration data
	// 2. get master render camera tranlate and rotation data
	// 3. get salve render camera tranlate and rotation data, return

	CCamProj *camProj = getProjInstance(Proj_PANEL_INDEX, 1);

	osg::Matrixd myCameraMatrix;
	osg::Matrixd cameraRotation;
	osg::Matrixd cameraTrans;

	cameraRotation.makeRotate(
		osg::DegreesToRadians(-20.0), osg::Vec3(0, 1, 0), // roll
		osg::DegreesToRadians(-15.0), osg::Vec3(1, 0, 0), // pitch
		osg::DegreesToRadians(10.0), osg::Vec3(0, 0, 1)); // heading

	// 60 meters behind and 7 meters above the tank model
	//cameraTrans.makeTranslate(10, -50, 15);
	cameraTrans.makeTranslate(1.0f, -5.0f, 1.5f);

	myCameraMatrix = cameraRotation * cameraTrans;
	osg::Matrixd i = myCameraMatrix.inverse(myCameraMatrix);
	//viewer.setViewByMatrix((
	//	Producer::Matrix(i.ptr()))
	//	* Producer::Matrix::rotate(-M_PI / 2.0, 1, 0, 0));
}

void CCamUtil::createSlaveViewer()
{
	int xoffset = 40;
	int yoffset = 40;

	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->x = xoffset + 600;
	traits->y = yoffset + 0;
	traits->width = 600;
	traits->height = 480;
	traits->windowDecoration = true;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;

	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);
	camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

	// add this slave camera to the viewer, with a shift left of the projection matrix
	viewer->addSlave(camera.get(), osg::Matrixd::translate(-1.0, 0.0, 0.0), osg::Matrixd());
}

void CCamUtil::createVGASlaveViewer(int panelIdx, int camIdx)
{

	CCamVGA *vgaCam = getVGAInstance(panelIdx, camIdx);
	if (vgaCam)
	{
		// Set parnet camer to zero
		// viewer->getCamera()->setProjectionMatrix(osg::Matrixd());

		// P
		osg::Matrixd proj = vgaCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);

		// P*V
		// proj.makeTranslate(-proj.getTrans()); // trans to zeros
		// proj.makeRotate(proj.getRotate().inverse()); // rotate to identity
		//
		//
		osg::Quat q;
		q.set(osg::Matrixd::inverse(vgaCam->osgR));
		proj.makeTranslate(vgaCam->osgCent); //trans to camera
		proj.makeRotate(q);					 // rotate to camera

		osg::Matrixd mv = vgaCam->getModelViewMatrix();

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the projection matrix
		camera->setProjectionMatrix(proj);
		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera->setViewMatrix(mv);

		// MVP
		viewer->addSlave(camera.get(), osg::Matrixd(), osg::Matrixd());

		//std::cout << camera->getProjectionMatrix() << "," << camera->getViewMatrix() << endl;
	}
}

osg::ref_ptr<osg::Camera> CCamUtil::createHDMainViewer(int panelIdx, int camIdx)
{

	CCamHD *hdCam = getHDInstance(panelIdx, camIdx);
	if (hdCam)
	{

		osg::Matrixd proj = hdCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);

		osg::Matrixd mv = hdCam->getModelViewMatrix();

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::Camera> camera = viewer->getCamera();

		// set the projection matrix
		camera->setProjectionMatrix(proj);

		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		mv.makeTranslate(hdCam->osgCent); //trans to camera

		//osg::Vec3 cent = osg::Vec3(-4, 0, -3);
		//osg::Vec3 up = osg::Vec3(0, -1, 0);

		// osg::Vec3 cent = osg::Vec3(0, -1, 0);	cent = hdCam->osgQ*cent;		// R*[0 0 1]calib ==>	R*[0 1 0]
		// osg::Vec3 up = osg::Vec3(0, 0, 1);		up = hdCam->osgQ*up;			// R*[0 1 0]calib ==>	R*[0 0 -1]

		// osg::Vec3 eye = osg::Vec3(hdCam->vecCent(0), hdCam->vecCent(2), -hdCam->vecCent(1)); // Zosg  = Ycalib ;	Yosg  = -Zcalib;		Xosg = Xcalib;
		// osg::Vec3 cent = osg::Vec3(0, 0, 1);	cent = hdCam->osgQ*cent;	cent = osg::Vec3(cent.x(), cent.z(), -cent.y());
		// osg::Vec3 up = osg::Vec3(0, 1, 0);		up = hdCam->osgQ*up;		up = osg::Vec3(up.x(), up.z(), -up.y());

		osg::Vec3 eye = hdCam->osgCent;		   // Zosg  = Ycalib ;	Yosg  = -Zcalib;		Xosg = Xcalib;
		osg::Vec3 cent = osg::Vec3(-4, 2, -4); // osg::Vec3 cent = osg::Vec3(0, 0, 1);		cent = hdCam->osgQ.inverse()*cent;	cent = osg::Vec3(cent.x(), -cent.z(), cent.y());
		osg::Vec3 up = osg::Vec3(0, -1, 0);	//up = hdCam->osgQ.inverse()*up;		up = osg::Vec3(up.x(), -up.z(), up.y());

		// osg::Vec3 eye = osg::Vec3(hdCam->vecCent(0), hdCam->vecCent(1), hdCam->vecCent(2)); // Zosg  = Ycalib ;	Yosg  = -Zcalib;		Xosg = Xcalib;
		// osg::Vec3 cent = osg::Vec3(0, 0, 1);	cent = hdCam->osgQ*cent;	cent = osg::Vec3(cent.x(), cent.z(), -cent.y());
		// osg::Vec3 up = osg::Vec3(0, 1, 0);		up = hdCam->osgQ*up;		up = osg::Vec3(up.x(), up.z(), -up.y());

		mv = osg::Matrixd();
		mv.makeLookAt(eye, cent, up);
		camera->setViewMatrix(mv);

		printVec(eye, "eye");
		printVec(cent, "cent");
		printVec(up, "up");

		// MVP
		//viewer->addSlave(camera.get());

		// Print matrix
		osg::Matrixd hdSalveProjMat = camera->getProjectionMatrix();
		osg::Matrixd hdSalveViewMat = camera->getViewMatrix();
		osg::Vec3 vecProjTrans = hdSalveProjMat.getTrans();
		osg::Vec3 vecViewjTrans = hdSalveViewMat.getTrans();

		printMatrix(hdSalveProjMat, "hdSalveProjMat");
		printMatrix(hdSalveViewMat, "hdSalveViewMat");
		printVec(vecProjTrans, "vecProjTrans");
		printVec(vecViewjTrans, "vecViewjTrans");

		return camera;
	}

	return NULL;
}

osg::ref_ptr<osg::Camera> CCamUtil::createHDSlaveViewer(int panelIdx, int camIdx)
{
	CCamHD *hdCam = getHDInstance(panelIdx, camIdx);
	cout << "get camera: " << hdCam->camIdx << "," << hdCam->panelIdx << endl
		 << endl;

	if (hdCam)
	{

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		camera->setProjectionResizePolicy(osg::Camera::HORIZONTAL);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the projection matrix
		osg::Matrixd proj = hdCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);
		camera->setProjectionMatrix(proj);

		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		//mv.makeTranslate(hdCam->osgT); //trans to camera

		osg::Vec3 eye = osg::Vec3(0, 0, 0);  // Camera coordinate, camera cent
		osg::Vec3 cent = osg::Vec3(0, 0, 1); // Camera coordinate, looking direction
		osg::Vec3 up = osg::Vec3(0, -1, 0);  // Camera coordinate, up direction

		osg::Quat q1 = hdCam->osgQ.inverse();					  // Camera coordinate to  world coordinate system roation( both Y down for sure)
		osg::Vec3 T = hdCam->osgT;								  // Camera coordinate to  world coordinate system translation( both Y down for sure)
		osg::Quat q2 = osg::Quat(-osg::PI_2, osg::Vec3(1, 0, 0)); // Y down tp Z Up

		eye = q2 * q1 * (eye - T);		   // eye = hdCam->osgCent;
		cent = q2 * q1 * (cent - T) - eye; // RX+T = cent	// @TODO
		up = q2 * q1 * (up - T) - eye;	 // @TODO

		cent = cent / cent.normalize();
		up = up / up.normalize();

		//eye = hdCam->osgCent;
		//cent = osg::Vec3f(-4, -0, -3);
		//cent = osg::Vec3f(0, 0, 0);
		//up = osg::Vec3(0, -1, 0);

		osg::Matrixd mv = osg::Matrixd();
		////mv.makeTranslate(hdCam->osgCent); //trans to camera
		mv.makeLookAt(eye, cent, up);
		camera->setViewMatrix(osg::Matrix::lookAt(eye, cent, up));

		// camera->setViewMatrix(osg::Matrix::lookAt(osg::Vec3(0,0,0), osg::Vec3(10, 10, -80), osg::Vec3(0, -1, 0)));

		// printVec(osg::Matrixd::inverse(hdCam->osgR) * (osg::Vec3(0, 0, 1) - hdCam->osgT), "001");
		// printVec(osg::Matrixd::inverse(hdCam->osgR) * (osg::Vec3(0, 1, 0) - hdCam->osgT), "010");
		// printVec(osg::Matrixd::inverse(hdCam->osgR) * (osg::Vec3(1, 0, 0) - hdCam->osgT), "100");

		// osg::Matrixd cameraRotation;
		// cameraRotation.makeRotate(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0))*osg::Quat(osg::PI_2, osg::Vec3(0, 0, 1))*hdCam->osgQ.inverse());
		// osg::Matrixd cameraTrans;
		// cameraTrans.makeTranslate(hdCam->osgCent);
		// osg::Matrixd mv = cameraRotation * cameraTrans;
		// camera->setViewMatrix(osg::Matrixd::inverse(mv));

		// MVP
		viewer->stopThreading();
		viewer->addSlave(camera.get());
		//viewer->removeSlave(viewer->findSlaveIndexForCamera(camera.get()));
		viewer->startThreading();

		// Print matrix
		cout << "up*cent: " << up * cent << endl;

		printVec(eye, "eye");
		printVec(hdCam->osgCent, "eye-osgCent");
		// printVec(hdCam->osgT, "osgT");
		// printVec(-hdCam->osgT, "-osgT");
		//
		printVec(cent, "cent");
		printVec(up, "up");
		//
		// printMatrix(mv, "mv");
		//
		printMatrix(camera->getViewMatrix(), "ViewMatrix");
		printVec(camera->getViewMatrix().getTrans(), "ViewMatrixTrans");

		printMatrix(camera->getProjectionMatrix(), "ProjMatrix");
		printVec(camera->getProjectionMatrix().getTrans(), "ProjMatrixTrans");

		osg::Vec3 eyeRecalc;
		osg::Vec3 centRecalc;
		osg::Vec3 upRecalc;
		camera->getViewMatrixAsLookAt(eyeRecalc, centRecalc, upRecalc);

		printVec(eyeRecalc, "eyeRecalc");
		printVec(centRecalc, "centRecalc");
		printVec(upRecalc, "upRecalc");

		// printVec(q1 * (osg::Vec3(0, 0, 0) - T), "eye1");
		// printVec(hdCam->osgCent , "eye2");
		// printVec(hdCam->osgT, "eye3");
		//
		return camera;
	}

	return NULL;
}

void CCamUtil::createKinectSlaveViewer(int panelIdx, int camIdx)
{

	CCamKinect *kinectCam = getKinectInstance(panelIdx, camIdx);
	if (kinectCam)
	{
		// Set parnet camer to zero
		// viewer->getCamera()->setProjectionMatrix(osg::Matrixd());

		// P
		osg::Matrixd proj = kinectCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);

		osg::Quat q;
		q.set(osg::Matrixd::inverse(kinectCam->osgR));
		proj.makeTranslate(kinectCam->osgCent); //trans to camera
		proj.makeRotate(q);						// rotate to camera

		osg::Matrixd mv = kinectCam->getModelViewMatrix();

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the projection matrix
		camera->setProjectionMatrix(proj);
		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera->setViewMatrix(mv);

		// MVP
		viewer->addSlave(camera.get(), osg::Matrixd(), osg::Matrixd());

		//std::cout << camera->getProjectionMatrix() << "," << camera->getViewMatrix() << endl;
	}
}

void CCamUtil::createSlaveViewerOfProjector(int camIdx)
{
	INIReader reader("config/config.ini");
	int MONITOR_WIDTH = reader.GetReal("Config", "MONITOR_WIDTH", 1280);
	int MONITOR_HEIGHT = reader.GetReal("Config", "MONITOR_HEIGHT", 800);
	int PROJECTOR_WIDTH = reader.GetReal("Config", "PROJECTOR_WIDTH", 1280);
	int PROJECTOR_HEIGHT = reader.GetReal("Config", "PROJECTOR_HEIGHT", 800);
	std::cout << MONITOR_WIDTH << MONITOR_HEIGHT << PROJECTOR_WIDTH << PROJECTOR_HEIGHT << std::endl;

	int xoffset = MONITOR_WIDTH;
	int yoffset = 0;

	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->x = xoffset + PROJECTOR_WIDTH * (camIdx + 1);
	traits->y = yoffset + 0;
	traits->width = PROJECTOR_WIDTH;
	traits->height = PROJECTOR_HEIGHT;
	traits->windowDecoration = false;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;

	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);
	camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

	// add this slave camera to the viewer, with a shift right of the projection matrix
	viewer->addSlave(camera.get(), osg::Matrixd::translate(-1.0, 0.0, 0.0), osg::Matrixd());
}

//Print osg::Matrixd
void CCamUtil::printMatrix(osg::Matrixd mat, std::string name)
{
	std::cout << name << ": " << std::endl;
	for (unsigned int row = 0; row < 4; ++row)
	{
		for (unsigned int col = 0; col < 4; ++col)
		{
			std::cout << std::setw(10) << mat(row, col);
			if (col != 3)
			{
				std::cout << ", ";
			}
		}
		std::cout << std::endl;
	}
}

void CCamUtil::printVec(osg::Vec3 vec, std::string name)
{
	std::cout << name << ": " << vec.x() << "," << vec.y() << "," << vec.z() << std::endl;
}

void CCamUtil::printVec(osg::Quat vec, std::string name)
{
	std::cout << name << ": " << vec.x() << "," << vec.y() << "," << vec.z() << "," << vec.w() << std::endl;
}

osg::ref_ptr<CCamUtil> CCamUtil::getInstance(void)
{
	// if (!instance)
	// 	instance = new CCamUtil();

	return instance;
}

CCamUtil::CCamUtil(osg::ref_ptr<osgViewer::Viewer> _viewer)
{
	viewer = _viewer;
}

void CCamUtil::drawCamAxis(osg::MatrixTransform *paNode, int panelIdx, int camIdx)
{
	if (panelIdx == 0 && camIdx == 1)
	{
		//	if (panelIdx > 0 && panelIdx <= 20)
		//		CCamVGA* cam = getVGAInstance(panelIdx, camIdx);
		//	else if(panelIdx == 0)
		//		CCamHD* cam = getHDInstance(panelIdx, camIdx);
		//	else if (panelIdx == 50)
		//		CCamKinect* cam = getKinectInstance(panelIdx, camIdx);
		//	else
		//		cout<<"Error!"<<endl;

		CCamHD *cam = getHDInstance(panelIdx, camIdx);

		osg::Vec3 eye = osg::Vec3(0, 0, 0);
		osg::Vec3 cent = osg::Vec3(0, 0, 1);
		osg::Vec3 up = osg::Vec3(0, -1, 0);

		//osg::Matrixd q1 = osg::Matrixd::inverse(osgR);
		osg::Quat q1 = cam->osgQ.inverse();

		osg::Vec3 T = cam->osgT;

		eye = q1 * (eye - T);
		cent = q1 * (cent - T) - eye;
		up = q1 * (up - T) - eye;

		osg::Vec3 zAxis = cent;
		osg::Vec3 yAxis = -up;
		osg::Vec3 xAxis = cent ^ (-up); //Cross product

		osg::ref_ptr<osg::Group> node = new osg::Group;

		node->addChild(cam->drawLineSegment(eye, eye + xAxis / xAxis.normalize() * 0.2, osg::Vec4(1, 0, 0, 1)));
		node->addChild(cam->drawLineSegment(eye, eye + yAxis / yAxis.normalize() * 0.2, osg::Vec4(0, 1, 0, 1)));
		node->addChild(cam->drawLineSegment(eye, eye + zAxis / zAxis.normalize() * 0.2, osg::Vec4(0, 0, 1, 1)));

		CCamUtil::getInstance()->printVec(cam->osgR * cam->osgCent + cam->osgT, "RX+T=0");

		CCamUtil::getInstance()->printVec(xAxis, "xAxis");
		CCamUtil::getInstance()->printVec(yAxis, "yAxis");
		CCamUtil::getInstance()->printVec(zAxis, "zAxis");

		CCamUtil::getInstance()->printVec(eye, "eye");
		CCamUtil::getInstance()->printVec(cent, "cent");
		CCamUtil::getInstance()->printVec(up, "up");

		CCamUtil::getInstance()->printVec(cam->osgCent, "osgCent");
		CCamUtil::getInstance()->printVec(cam->osgQ, "osgQ");
		CCamUtil::getInstance()->printMatrix(cam->osgR, "osgR");

		paNode->addChild(node);
	}
}

void CCamUtil::drawCamLabel(osg::MatrixTransform *paNode, int panelIdx, int camIdx)
{
	if (panelIdx == 0 && camIdx == 1)
	{
		//	if (panelIdx > 0 && panelIdx <= 20)
		//		CCamVGA* cam = getVGAInstance(panelIdx, camIdx);
		//	else if(panelIdx == 0)
		//		CCamHD* cam = getHDInstance(panelIdx, camIdx);
		//	else if (panelIdx == 50)
		//		CCamKinect* cam = getKinectInstance(panelIdx, camIdx);
		//	else
		//		cout<<"Error!"<<endl;

		CCamHD *cam = getHDInstance(panelIdx, camIdx);

		osgText::Text *textNode = new osgText::Text();
		stringstream label;
		label << panelIdx << "_" << camIdx << endl;

		textNode->setCharacterSize(25);
		textNode->setFont("impact.ttf");
		textNode->setText(label.str());
		textNode->setAxisAlignment(osgText::Text::SCREEN);
		textNode->setPosition(cam->osgCent);
		textNode->setColor(osg::Vec4(199, 77, 15, 1));

		paNode->addChild(textNode);
	}
}