#include <iostream>
#include <sstream>
#include <stdio.h>
#include <iomanip>
#include <vector>
#include <fstream>
#include <Eigen/Dense>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/Group>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osgViewer/Viewer>
#include <osg/PositionAttitudeTransform>
#include <osg/LineWidth>
#include <osgText/Text>
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/Shape>
#include <osg/MatrixTransform>
#include <osg/PositionAttitudeTransform>

#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>
#include "Util.h"
#include "Camera.h"
#include "PointCloud.h"
#include "INIReader.h"

using namespace std;
using namespace cv;
using namespace virtual2real;

static char const *vertexShader = {
	"#version 330 		\n"
	"uniform mat4 matMVP;\n"
	"uniform mat4 matFlipGL2OSG;\n"
	"void main(void )	\n"
	"{\n"
	// "	gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;\n"
	// "	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;	\n;"
	"   gl_Position = matFlipGL2OSG * matMVP * gl_Vertex;\n"
	//" gl_Position = _step+  gl_ModelViewProjectionMatrix*gl_Vertex;\n"
	"}\n"};
static char const *fragShader = {
	"#version 330 		\n"
	"void main(void){\n"
	"	gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);  	\n"
	//"	gl_FragColor = gl_Color; \n"
	"}\n"};

void build_opengl_projection_for_intrinsics(GLfloat *frustum, double fx, double fy, double skew, double u0, double v0, int width, int height, double near_clip, double far_clip)
{
#if 1
	double l = 0.0, r = 1.0 * width, b = 0.0, t = 1.0 * height;
	double tx = -(r + l) / (r - l), ty = -(t + b) / (t - b), tz = -(far_clip + near_clip) / (far_clip - near_clip);

	double ortho[16] = {2.0 / (r - l), 0.0, 0.0, tx,
						0.0, 2.0 / (t - b), 0.0, ty,
						0.0, 0.0, -2.0 / (far_clip - near_clip), tz,
						0.0, 0.0, 0.0, 1.0};

	double Intrinsic[16] = {fx, skew, u0, 0.0,
							0.0, fy, v0, 0.0,
							0.0, 0.0, -(near_clip + far_clip), +near_clip * far_clip,
							//0.0, 0.0, -(near_clip+far_clip)/2.0 , 0.0,
							0.0, 0.0, 1.0, 0.0};

	Mat orthoMat = Mat(4, 4, CV_64F, ortho);
	Mat IntrinsicMat = Mat(4, 4, CV_64F, Intrinsic);
	Mat Frustrum = orthoMat * IntrinsicMat;
	Frustrum = Frustrum.t();

	double *data = (double *)Frustrum.data;
	for (int i = 0; i < 16; ++i)
	{
		frustum[i] = data[i];
	}
#endif
}

class MVPCallback : public osg::Uniform::Callback
{
  public:
	MVPCallback(osg::Camera *camera, osg::Matrixd *mvp) : mCamera(camera), matMVP(mvp)
	{
	}
	virtual void operator()(osg::Uniform *uniform, osg::NodeVisitor *nv)
	{
		if (0)
		{
			osg::Matrixd modelView = mCamera->getViewMatrix();
			osg::Matrixd projectM = mCamera->getProjectionMatrix();
			uniform->set(modelView * projectM);
		}
		else
		{
			// uniform->set(osg::Matrixd::identity());
			uniform->set(*matMVP);
		}
	}

  private:
	osg::Camera *mCamera;
	osg::Matrixd *matMVP;
};

class _MyNodeVisitor : public osg::NodeVisitor
{
  public:
	_MyNodeVisitor() : osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN) {}

	virtual void apply(osg::Geode &node)
	{
		for (int i = 0; i < node.getNumParents(); ++i)
		{
			osg::Geometry *polyGeom = dynamic_cast<osg::Geometry *>(node.getDrawable(i));

			if (!polyGeom)
				return;

			polyGeom->setVertexAttribArray(0, polyGeom->getVertexArray());
			polyGeom->setVertexAttribBinding(0, osg::Geometry::BIND_PER_VERTEX);
			polyGeom->setVertexAttribArray(1, polyGeom->getColorArray());
			polyGeom->setVertexAttribBinding(1, osg::Geometry::BIND_PER_VERTEX);
			polyGeom->setVertexAttribArray(2, polyGeom->getNormalArray());
			polyGeom->setVertexAttribBinding(2, polyGeom->getNormalBinding());
		}
	}
};

void quat2rot(double *q, double *R)
{
	// QUAT2ROT - Quaternion to rotation matrix transformation
	//
	//  Usage: quat2rot(q, R); q: quaternion, R: rotation matrix

	double x, y, z, w, x2, y2, z2, w2, xy, xz, yz, wx, wy, wz;
	double norm_q = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);

	x = q[0] / norm_q;
	y = q[1] / norm_q;
	z = q[2] / norm_q;
	w = q[3] / norm_q;

	x2 = x * x;
	y2 = y * y;
	z2 = z * z;
	w2 = w * w;
	xy = 2 * x * y;
	xz = 2 * x * z;
	yz = 2 * y * z;
	wx = 2 * w * x;
	wy = 2 * w * y;
	wz = 2 * w * z;

	// Format: {R[0] R[1] R[2]; R[3] R[4] R[5]; R[6] R[7] R[8];}
	R[0] = w2 + x2 - y2 - z2;
	R[1] = xy - wz;
	R[2] = xz + wy;
	R[3] = xy + wz;
	R[4] = w2 - x2 + y2 - z2;
	R[5] = yz - wx;
	R[6] = xz - wy;
	R[7] = yz + wx;
	R[8] = w2 - x2 - y2 + z2;
}

int main()
{
	// Read Config file
	INIReader reader("config/config.ini");
	string CALIB_FILE_PATH = reader.Get("Config", "CALIB_FILE_PATH", "");
	//cout << "Calibration: " << CALIB_FILE_PATH << endl;

	osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer;
	osg::Group *root = new osg::Group;

	// Viewport,Camera
	Mat_<double> mvp;
	{
		double fx = 1430.13;
		double fy = 1426.45;
		double skew = 0;
		double u0 = 949.221;
		double v0 = 559.319;
		int imageWidth = 1920;
		int imageHeight = 1080;
		double near_clip = 0.001;
		double far_clip = 10000;

		cout << "fx:" << fx << " fy:" << fy << " skew:" << skew << " u0:" << u0 << " v0:" << v0 << " width:" << imageWidth << " height:" << imageHeight << " near_clip:" << near_clip << " far_clip:" << far_clip << endl;

		double matRq[4] = {0.999036, -0.0410261, -0.0120331, 0.00996186};
		double q[4] = {matRq[1], matRq[2], matRq[3], matRq[0]};
		double rot[9];
		quat2rot(q, rot);
		Eigen::Matrix3f m_R;
		m_R << rot[0], rot[1], rot[2],
			rot[3], rot[4], rot[5],
			rot[6], rot[7], rot[8];

		//Eigen::RowVectorXf vecCent;//  = { -4.01396, 0.777917, -7.46611 };
		// vecCent << -4.01396f, 0.777917f, -7.46611f;

		// Eigen::MatrixXf vecT = -m_R*vecCent.transpose();

		cout << "Rotation Matrix: \n"
			 << m_R << endl;
		cout << "Rotation Matrix Transpose: \n"
			 << m_R.transpose() << endl;

		// ************************
		// Han's Code Start
		// ************************

		// View matrix
		GLfloat m_modelViewMatGL[16];
		m_modelViewMatGL[0] = m_R(0, 0);
		m_modelViewMatGL[1] = m_R(1, 0);
		m_modelViewMatGL[2] = m_R(2, 0);
		m_modelViewMatGL[3] = 0;
		m_modelViewMatGL[4] = m_R(0, 1);
		m_modelViewMatGL[5] = m_R(1, 1);
		m_modelViewMatGL[6] = m_R(2, 1);
		m_modelViewMatGL[7] = 0;
		m_modelViewMatGL[8] = m_R(0, 2);
		m_modelViewMatGL[9] = m_R(1, 2);
		m_modelViewMatGL[10] = m_R(2, 2);
		m_modelViewMatGL[11] = 0;
		m_modelViewMatGL[12] = 3.84110649383770; //4th col
		m_modelViewMatGL[13] = -0.0810546740216264;
		m_modelViewMatGL[14] = 7.59599626878569;
		m_modelViewMatGL[15] = 1;

		// Projector matrix
		GLfloat m_projMatGL[16];
		build_opengl_projection_for_intrinsics(m_projMatGL, fx, fy, skew, u0, v0, imageWidth, imageHeight, near_clip, far_clip);

		Mat_<double> ProjMat(4, 4);
		Mat_<double> viewModelMat(4, 4);

		double *ProjMatPtr = (double *)ProjMat.data;
		double *viewModelMatPtr = (double *)viewModelMat.data;
		for (int i = 0; i < 16; ++i)
		{
			ProjMatPtr[i] = m_projMatGL[i];
			viewModelMatPtr[i] = m_modelViewMatGL[i];
		}
		mvp = ProjMat.t() * viewModelMat.t();
		mvp = mvp.t();

		// cout << "viewModelMa \n" << viewModelMat << endl;
		// cout << "ProjMat \n" << ProjMat << endl;
		// cout << "mvp\n" << mvp << endl;

		// ************************
		// Han's Code End
		// ************************
	}

	// OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
	osg::Matrix osgMVP;
	osgMVP(0, 0) = mvp(0);
	osgMVP(0, 1) = mvp(1);
	osgMVP(0, 2) = mvp(2);
	osgMVP(0, 3) = mvp(3);
	osgMVP(1, 0) = mvp(4);
	osgMVP(1, 1) = mvp(5);
	osgMVP(1, 2) = mvp(6);
	osgMVP(1, 3) = mvp(7);
	osgMVP(2, 0) = mvp(8);
	osgMVP(2, 1) = mvp(9);
	osgMVP(2, 2) = mvp(10);
	osgMVP(2, 3) = mvp(11);
	osgMVP(3, 0) = mvp(12);
	osgMVP(3, 1) = mvp(13);
	osgMVP(3, 2) = mvp(14);
	osgMVP(3, 3) = mvp(15); // T

	osg::Matrix osgFlip;
	osgFlip.rotate(osg::PI, osg::Vec3(1.0f, 0.0f, 0.0f));

	// Cow
	osg::ref_ptr<osg::PositionAttitudeTransform> cowNode = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::Node> cowNodeGeo = osgDB::readNodeFile("cow.osg"); //CreateNode();
	cowNode->addChild(cowNodeGeo);
	//cowNode->setScale(osg::Vec3(1000, 1000, 1000));
	//root->addChild(cowNode);

	// Dyna Human
	osg::ref_ptr<osg::PositionAttitudeTransform> dynaPT = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::Node> dynaNode = osgDB::readNodeFile("./model/dyna_male_50002_hips_81.osgt");
	dynaPT->addChild(dynaNode);
	dynaPT->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)));
	dynaPT->setScale(osg::Vec3f(1.f, 1.f, 1.f));
	dynaPT->setPosition(osg::Vec3f(-4, 1, -3));
	root->addChild(dynaPT);

	//Point cloud
	osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform;
	string PLY_FILE_NAME = reader.Get("Config", "PLY_FILE_NAME", "");
	CPointCloud *pointCloud = new CPointCloud();
	pointCloud->loadPointCloud(PLY_FILE_NAME, ptcNode);
	//scene->addChild(ptcNode);
	root->addChild(ptcNode);

	// Shader
	osg::ref_ptr<osg::Shader> _vShader = new osg::Shader(osg::Shader::VERTEX, vertexShader);
	osg::ref_ptr<osg::Shader> _fShader = new osg::Shader(osg::Shader::FRAGMENT, fragShader);
	osg::ref_ptr<osg::Program> _program = new osg::Program;
	_program->addShader(_vShader.get());
	_program->addShader(_fShader.get());

	// Node to accept Shader
	_MyNodeVisitor visitor;
	root->accept(visitor);
	osg::ref_ptr<osg::StateSet> _stateSet = root->getOrCreateStateSet();
	// Option 1) Direct pass Value
	osg::ref_ptr<osg::Uniform> uMVP = new osg::Uniform("matMVP", osgMVP);
	osg::ref_ptr<osg::Uniform> uFlipGL2OSG = new osg::Uniform("matFlipGL2OSG", osgFlip);

	_stateSet->setAttributeAndModes(_program.get(), osg::StateAttribute::ON);

	// Option 2) Callback
	//uMVP->setUpdateCallback(new MVPCallback(viewer->getCamera(), &osgMVP));
	//_stateSet->addUniform(uMVP.get());
	//_stateSet->setAttributeAndModes(_program.get(), osg::StateAttribute::ON);

	int xoffset = 0;
	int yoffset = 0;

	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->x = xoffset + 0;
	traits->y = yoffset + 0;
	traits->width = 1920;
	traits->height = 1080;
	traits->windowDecoration = false;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;

	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	osg::ref_ptr<osg::Camera> camera = viewer->getCamera();
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);
	camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));
	camera->setPostDrawCallback(SnapImage::getInstance());

	viewer->setSceneData(root);
	viewer->realize();
	// osg::State* state = viewer->getCamera()->getGraphicsContext()->getState();
	// state->setUseModelViewAndProjectionUniforms(true);

	bool capture = true;

	//viewer->run();
	while (!viewer->done())
	{
		viewer->frame();

		// Start capture
		if (capture)
		{
			osg::ref_ptr<SnapImage> snap = SnapImage::getInstance();
			stringstream _name;
			_name << "render/00003000/View_" << 15 << ".png";
			snap->_snapImage = true;
			snap->_filename = _name.str();
		}

		capture = false;
	}

	return 0;
}
