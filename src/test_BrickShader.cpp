// http://blog.csdn.net/zhuyingqingfen/article/details/44101619#comments
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/Shape>
#include <osg/ShapeDrawable>
#include <osg/MatrixTransform>

static char const *vertexShader = {
	"#version 140		\n"
	"in vec4 MCvertex; \n"
	"in vec4 MCcolor;	\n"
	"in vec3 MCnormal;		\n"
	"in vec3 osg_SimulationTime;		\n"
	"uniform mat4 osg_ModelViewMatrix;	\n"
	"uniform mat4 osg_ModelViewProjectionMatrix;\n"
	"uniform mat3 osg_NormalMatrix;\n"
	"uniform vec3 LightPosition ;\n"
	"uniform vec3 _step = vec3(10,10,0.01);\n "
	"const float SpecularContribution = 0.3;\n"
	"const float DiffuseContribution = 1.0 - SpecularContribution;\n"
	"out float LightIntensity;\n"
	"out vec2 MCposition;\n"
	"void main()\n"
	"{\n"
	"	  vec3 ecPosition = vec3(osg_ModelViewMatrix * MCvertex);		\n"
	"	  vec3 tnorm = normalize(osg_NormalMatrix * MCnormal);		\n"
	"	  vec3 lightVec = normalize(vec3(LightPosition[0],LightPosition[1],LightPosition[2]*osg_SimulationTime) - ecPosition);	\n"
	"	  vec3 reflectVec = reflect(-lightVec, tnorm);		\n"
	"	  vec3 viewVec = normalize(-ecPosition);	\n"
	"	  float diffuse = max(dot(lightVec, tnorm), 0.0);	\n"
	"	  float spec = 0.0;		\n"
	"	  if (diffuse > 0.0)	\n"
	"   {\n"
	"		  spec = max(dot(reflectVec, viewVec), 0.0);\n"
	"		  spec = pow(spec, 16.0);\n"
	" 	}\n"
	"  	LightIntensity = DiffuseContribution * diffuse + SpecularContribution * spec;\n"
	"	  MCposition = MCvertex.xy;\n"
	"	  gl_Position = _step + osg_ModelViewProjectionMatrix * MCvertex;\n"
	"} \n"};

static char const *fragShader = {
	" #version 140 \n"
	" uniform vec3 BrickColor = vec3(0,1,1), MortarColor = vec3(1,0,1);\n"
	" uniform vec2 BrickSize = vec2(0.3,0.15);\n"
	" uniform vec2 BrickPct = vec2(0.9,0.85);\n"
	" in vec2 MCposition;\n"
	" in float LightIntensity;\n"
	" out vec4 FragColor;\n"
	" void main()\n"
	" {\n"
	"	 vec3 color;\n"
	"	 vec2 position, useBrick;\n"
	"	position = MCposition / BrickSize;\n"
	"	  if (fract(position.y * 0.5) > 0.5)\n"
	"				position.x += 0.5;\n"
	"		position = fract(position);\n"
	"		useBrick = step(position, BrickPct);\n"
	"		color = mix(MortarColor, BrickColor, useBrick.x * useBrick.y);\n"
	"		color *= LightIntensity;\n"
	"		FragColor = vec4(color, 1.0);\n"
	"}\n"};

osg::MatrixTransform *lightPos = new osg::MatrixTransform;

class _MyNodeVisitor : public osg::NodeVisitor
{
  public:
	_MyNodeVisitor() : osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN) {}

	virtual void apply(osg::Geode &node)
	{
		for (int i = 0; i < node.getNumParents(); ++i)
		{
			osg::Geometry *polyGeom = dynamic_cast<osg::Geometry *>(node.getDrawable(i));

			if (!polyGeom)
				return;

			polyGeom->setVertexAttribArray(0, polyGeom->getVertexArray());
			polyGeom->setVertexAttribBinding(0, osg::Geometry::BIND_PER_VERTEX);
			polyGeom->setVertexAttribArray(1, polyGeom->getColorArray());
			polyGeom->setVertexAttribBinding(1, osg::Geometry::BIND_PER_VERTEX);
			polyGeom->setVertexAttribArray(2, polyGeom->getNormalArray());
			polyGeom->setVertexAttribBinding(2, polyGeom->getNormalBinding());
		}
	}
};

class LightPosCallback : public osg::Uniform::Callback
{
  public:
	LightPosCallback()
	{
	}
	virtual void operator()(osg::Uniform *uniform, osg::NodeVisitor *nv)
	{
		osg::Matrix m = lightPos->getMatrix();
		osg::Vec3f _lp = m.getTrans();

		uniform->set(_lp);
	}
};

osg::Node *createlight()
{
	osg::ShapeDrawable *sun_sd = new osg::ShapeDrawable;
	osg::Sphere *sun_sphere = new osg::Sphere;
	sun_sphere->setName("SunSphere");
	sun_sphere->setRadius(0.5);
	sun_sd->setShape(sun_sphere);
	sun_sd->setColor(osg::Vec4(1.0, 0.0, 0.0, 1.0));

	osg::Geode *sun_geode = new osg::Geode;
	sun_geode->setName("SunGeode");
	sun_geode->addDrawable(sun_sd);

	return sun_geode;
}

class KeyboardEventHandler : public osgGA::GUIEventHandler
{
  public:
	KeyboardEventHandler() {}

	virtual bool handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &)
	{
		switch (ea.getEventType())
		{
		case (osgGA::GUIEventAdapter::KEYDOWN):
		{
			if (ea.getKey() == 'x') //��x����ת
			{
				osg::Matrix trans = lightPos->getMatrix();
				trans = trans * osg::Matrix::rotate(osg::PI_2 / 10, osg::X_AXIS);
				lightPos->setMatrix(trans);
			}
			if (ea.getKey() == 'y') //��y����ת
			{
				osg::Matrix trans = lightPos->getMatrix();
				trans = trans * osg::Matrix::rotate(osg::PI_2 / 10, osg::Y_AXIS);
				lightPos->setMatrix(trans);
			}
			if (ea.getKey() == 'z') //��z����ת
			{
				osg::Matrix trans = lightPos->getMatrix();
				trans = trans * osg::Matrix::rotate(osg::PI_2 / 10, osg::Z_AXIS);
				lightPos->setMatrix(trans);
			}
		}
		}

		return false;
	}
};
int main()
{
	osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer;
	osg::ref_ptr<osg::Node> geode = osgDB::readNodeFile("cow.osg"); //CreateNode();

	_MyNodeVisitor visitor;
	geode->accept(visitor);

	osg::ref_ptr<osg::StateSet> _stateSet = geode->getOrCreateStateSet();

	osg::ref_ptr<osg::Shader> _vShader = new osg::Shader(osg::Shader::VERTEX, vertexShader);
	osg::ref_ptr<osg::Shader> _fShader = new osg::Shader(osg::Shader::FRAGMENT, fragShader);
	osg::ref_ptr<osg::Program> _program = new osg::Program;
	_program->addShader(_vShader.get());
	_program->addShader(_fShader.get());

	_program->addBindAttribLocation("MCvertex", 0);
	_program->addBindAttribLocation("MCcolor", 1);
	_program->addBindAttribLocation("MCnormal", 2);

	osg::ref_ptr<osg::Uniform> M4 = new osg::Uniform("LightPosition", osg::Vec3d(2, 0, 0));
	M4->setUpdateCallback(new LightPosCallback());
	_stateSet->addUniform(M4.get());

	_stateSet->setAttributeAndModes(_program.get(), osg::StateAttribute::ON);

	lightPos = new osg::MatrixTransform;
	lightPos->setMatrix(osg::Matrix::translate(0, 0, 5));
	lightPos->addChild(createlight());

	osg::Group *root = new osg::Group;
	//root->addChild(osgDB::readNodeFile("d:/ah64_apache.3ds"));
	root->addChild(lightPos);
	root->addChild(geode);

	//viewer->addEventHandler(new KeyboardEventHandler());
	viewer->setSceneData(root);
	viewer->setUpViewInWindow(35, 35, 1024, 800);

	viewer->realize();
	osg::State *state = viewer->getCamera()->getGraphicsContext()->getState();
	state->setUseModelViewAndProjectionUniforms(true);

	return viewer->run();
}