function plotFaceNoNormal( x,y,z,Index )
%PLOTFACE Summary of this function goes here
%   Detailed explanation goes here
hold on;
for cnt = 1:numel(Index)
    if cnt+1 <= numel(Index)
        i = Index(cnt);
        j = Index(cnt+1);
    else
        i = Index(cnt);
        j = Index(1);
    end
	
   line( [x(i);x(j)],[y(i);y(j)],[z(i);z(j)]);    
   text(x(i),y(i),z(i),num2str(i));
end



%% plot normal for varyfication
xx = x(Index);
yy = y(Index);
zz = z(Index);

Xc = mean(xx);
Yc = mean(yy);
Zc = mean(zz);

vecA = [ x(Index(2))-x(Index(1)),y(Index(2))-y(Index(1)),z(Index(2))-z(Index(1))];
vecB = [ x(Index(3))-x(Index(2)),y(Index(3))-y(Index(2)),z(Index(3))-z(Index(2))];


vecN = cross(vecA,vecB);
vecN = 0.4*vecN/norm(vecN);
T = vecN + [Xc,Yc,Zc];
% T = 0.2 * T/norm(T);

% plot3(x,z,y,'go');
% plot3(Xc,Zc,Yc,'ro')
% plot3(T(1),T(2),T(3),'ro')


%line([Xc,T(1)],[Yc,T(2)],[Zc,T(3)])

end


