/********************************************************************
	created:	2012/01/10
	created:	10:1:2012   9:24
	filename: 	d:\Dev\Golf\2012-1-10\GolfGame\src\CameraManager.h
	file path:	d:\Dev\Golf\2012-1-10\GolfGame\src
	file base:	CameraManager
	file ext:	h
	author:		lixulong@yeah.net
	
	purpose:	�������
				
				�� ������������
				�� �������ģʽ��TV�����桢�������ȵ�
				�� ���ּ�����Ϣ��Ӧ

*********************************************************************/

#pragma once

#include <osgViewer/Viewer>

#include <osg/LineSegment>
#include <osg/Point>
#include <osg/Geometry>
#include <osg/Node>
#include <osg/Geode>
#include <osg/Group>

#include <osgGA/MatrixManipulator>

#include <osgUtil/IntersectVisitor>
#include <osg/Viewport>
#include <osg/Matrix>
#include <osg/Camera>
#include <osg/StateAttribute>
#include <vector>
#include "BulletPhysic.h"
#include "StructCourseInfo.h"
//���������ݹ���
#include "HoleDataManager.h"

class TravelManipulator : public osgGA::MatrixManipulator
{

  public:
	TravelManipulator(PhysicsManager *physicsmgr, osg::ref_ptr<osgViewer::Viewer> viewer);
	~TravelManipulator(void);

	//��ʼ�����λ��
	bool initialCamera();

	// �����������ӵ�������
	//static TravelManipulator * TravelToScence(osg::ref_ptr<osgViewer::Viewer>viewer);

	// ��ǰλ��
	osg::Vec3 m_vPosition;

	// ��ת�Ƕ�
	osg::Vec3 m_vRotation;

	osg::ref_ptr<osgViewer::Viewer> m_pHostViewer;

	//�������
	bool following;
	bool cam_or_revcam;
	bool dynamic_cam;
	bool tv_cam;
	bool slider;

	int cameraMode; //1Ϊ������棬2Ϊ������������3Ϊ��̬�����4Ϊtv�����
  private:
	// �ƶ��ٶ�
	float m_fMoveSpeed;
	PhysicsManager *m_pPhysicsmgr;
	osg::ref_ptr<osgViewer::Viewer> _viewer;
	osg::Vec3 changeangle;
	void Dynamic_angle(osg::Vec3 Ball_Camera, osg::Vec3 &Angle);
	void TV_angle(osg::Vec3 Ball_Camera, osg::Vec3 &Angle);
	void GetPointAngle(osg::Vec3 campos, osg::Vec3 changepoint, osg::Vec3 &point, osg::Vec3 &angle);

	bool raiseCameraSignal;

  public:
	osg::Vec3 bodypos;
	// ������״̬
	bool m_bLeftButtonDown;
	osg::Vec3 BenchpolePos;
	//ָ���������
	osg::Vec3 benchpos;
	bool markflag;
	bool initialangle;
	bool changecamera;
	float maxheight;

	// ���λ��
	float m_fpushX;
	float m_fpushY;
	void pickUp(osg::ref_ptr<osgViewer::Viewer> Viewer, const osgGA::GUIEventAdapter &ea);
	// ���þ���
	virtual void setByMatrix(const osg::Matrix &matrix);

	// ���������
	virtual void setByInverseMatrix(const osg::Matrix &matrix);

	// ��ȡ����
	virtual osg::Matrixd getMatrix() const;

	// ��ȡ�����
	virtual osg::Matrixd getInverseMatrix() const;

	// �¼���������
	virtual bool handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &us);

	// ��Ļ�Ƕ�
	float m_fAngle;

	// λ�ñ任
	bool ChangePosition(osg::Vec3 &delta);
	bool ChangePositionToTaget(osg::Vec3 taget);
	/*  	bool SteppingChangePositionToTaget(osg::Vec3 taget);*/
	// ��ײ���״̬
	bool m_bPeng;

	// �����ٶ�
	float getSpeed();

	void setSpeed(float &);

	// ���ó�ʼλ��
	void SetPosition(osg::Vec3 &position);

	osg::Vec3 GetPosition();

	osg::Vec3 GetBenchpolePosition();

	bool FollowingCamera();
	bool ReverCamera();
	bool Dynamic_Camera();
	bool TV_Camera();
	//�õ�aim����
	osg::Vec3 GetAimPoint(osg::Vec3 BallCor);

	osg::Vec3 CameraComputeAng(osg::Vec3 a);
	osg::Vec3 CameraComputeAngforchangposition(osg::Vec3 a);
	//	float CameraComputeAng(osg::Vec3 ballPos,osg::Vec3 TagetPos);
	osg::Vec3 CameraComputePos(osg::Vec3 ballPos, osg::Vec3 TagetPos);
	float computeCameraToBenchpoleDis(osg::Vec3 BenchpolePos);
	osg::Vec3 computeCameraToTargetAng(osg::Vec3 BenchpolePos);
	//�õ���aim�����
	float GetAimDis();
	osg::Vec3 getPiontFromm_vRotation();

  public:
	osg::Vec3 pos;		  //�����λ��ʸ��
	osg::Vec3 mTargetPos; //С���ʵʱλ��
	osg::Vec3 camangle;   //����Ƕȡ���a,b,c����a���������ǣ�a=PI/2ʱΪˮƽ�Ӿ���bΪ����ת�ǣ�b=0ʱΪ�����Ӿ���cΪƫ���ǣ�c=PI/2+x��xΪ�趨�ĺ���
	osg::Vec3 toCam;	  //�����Ŀ������

	//��ǰ�����꣬holeData[Current_Hole].mHole
	osg::Vec3 hole_dir; //��ȡ��ǰ������
	osg::Vec3 Tee_dir;
	osg::Vec3 balltohole; //С�����򶴵�λʸ
	osg::Vec3 balltocam;  //С���������λʸ
	osg::Vec3 Teetohole;
	//���»�ȡС���ٶ�
	btVector3 speedXYZ;
	float speedx;
	float speedy;
	float speedz;
	float speed;
	float speedXOY;		   //��XOYƽ���ϵķ�����ģ
	float balltocamlength; //��XOYƽ���ϵķ�����ģ

	float dis; //���������С���ˮƽ����
	float dis2;
	float dis3; //���������С��Ŀռ����
	float dis4;
	float sita1, sita2, sita3;
};