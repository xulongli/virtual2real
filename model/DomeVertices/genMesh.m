
function meshIdx = genMesh( x,y,z,Index )
%PLOTFACE Summary of this function goes here
%   Detailed explanation goes here


xx = x(Index);
yy = y(Index);
zz = z(Index);

Xc = mean(xx);
Yc = mean(yy);
Zc = mean(zz);

meshIdx = [];

for cnt = 1:numel(Index)
    if cnt+1 <= numel(Index)
        i = Index(cnt);
        j = Index(cnt+1);
    else
        i = Index(cnt);
        j = Index(1);
    end
    meshIdx = [meshIdx; ]
end


hold on;
for cnt = 1:numel(Index)
    if cnt+1 <= numel(Index)
        i = Index(cnt);
        j = Index(cnt+1);
    else
        i = Index(cnt);
        j = Index(1);
    end
    
    line( [x(i);x(j)],[z(i);z(j)],[y(i);y(j)]);
    
    text(x(i),z(i),y(i),num2str(i));

end

%% plot normal for varyfication
xx = x(Index);
zz = z(Index);
yy = y(Index);

Xc = mean(xx);
Zc = mean(zz);
Yc = mean(yy);

vecA = [ x(Index(2))-x(Index(1)),z(Index(2))-z(Index(1)),y(Index(2))-y(Index(1))];
vecB = [ x(Index(3))-x(Index(2)),z(Index(3))-z(Index(2)),y(Index(3))-y(Index(2))];

vecN = cross(vecA,vecB);
vecN = 0.5*vecN/norm(vecN);
T = vecN + [Xc,Yc,Zc];
% T = 0.2 * T/norm(T);

line([Xc,T(1)],[Zc,T(3)],[Yc,T(2)])

end



 
 
	