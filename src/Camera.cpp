#include "Camera.h"
#include "INIReader.h"
using namespace std;
using namespace Eigen;

//#define USE_HANS_CODE

#define Z_UP_ALONG_Y
//#define Y_DOWN_ALONG_Z

string CCamera::calibPath = "./calib/";

static char const *vertexShader = {
	//"#version 110 		\n"
	"uniform vec3 lightPosition;\n"
	"varying vec3 normal, eyeVec, lightDir;\n"
	"in vec3 vertex_pos; \n"
	"uniform mat4 matMVP;\n"
	"uniform mat4 matModelView;\n"
	"void main(void )	\n"
	"{\n"
	//"    vec4 vertexInEye = gl_ModelViewMatrix * gl_Vertex;\n"
	////"    vec4 vertexInEye = matModelView * gl_Vertex;\n"
	//"    eyeVec = -vertexInEye.xyz;\n"
	//"    lightDir = vec3(lightPosition - vertexInEye.xyz);\n"
	//"    normal = gl_NormalMatrix * gl_Normal;\n"
	"    gl_Position = ftransform();\n"
	"	gl_Position.x = 1.0-gl_Position.x; \n"
	//"	gl_Position = matMVP * vec4(vertex_pos,1.0); \n	"
	//"	gl_Position.y = 1.0-gl_Position.y; \n"
	"}\n"};
static char const *fragShader = {
	//"#version 110 		\n"
	"in vec3 vertex_color; \n"
	"uniform vec4 lightDiffuse;\n"
	"uniform vec4 lightSpecular;\n"
	"uniform float shininess;\n"
	"varying vec3 normal, eyeVec, lightDir;\n"
	"void main (void)\n"
	"{\n"
	//"  vec4 finalColor = gl_FrontLightModelProduct.sceneColor;\n"
	//"  vec3 N = normalize(normal);\n"
	//"  vec3 L = normalize(lightDir);\n"
	//"  float lambert = dot(N,L);\n"
	//"  if (lambert > 0.0)\n"
	//"  {\n"
	//"    finalColor += lightDiffuse * lambert;\n"
	//"    vec3 E = normalize(eyeVec);\n"
	//"    vec3 R = reflect(-L, N);\n"
	//"    float specular = pow(max(dot(R, E), 0.0), shininess);\n"
	//"    finalColor += lightSpecular * specular;\n"
	//"  }\n"
	//"  gl_FragColor = finalColor;\n"
	//"  gl_FragColor = vec4(normalize(finalColor.xyz),finalColor.w);\n "
	"  gl_FragColor = vec4(0.1,0.8,0.5,0.8); \n"
	"}\n"};

CCamera::CCamera()
{

	// Color for camera edge
	eg_color = new osg::Vec4Array;
	eg_color_VGA = new osg::Vec4Array;
	eg_color_HD = new osg::Vec4Array;
	eg_color_Kinect = new osg::Vec4Array;
	eg_color_Projector = new osg::Vec4Array;

	eg_color->push_back(osg::Vec4(1.0, 0.0, 0.0, 0.5));				  //Red
	eg_color_VGA->push_back(osg::Vec4(1.0, 0.0, 0.0, 0.5));			  //Red
	eg_color_HD->push_back(osg::Vec4(0.0, 1.0, .0, 0.5));			  //Green
	eg_color_Kinect->push_back(osg::Vec4(0.1f, 0.1f, 0.8f, 0.5));	 //Blue
	eg_color_Projector->push_back(osg::Vec4(0.39f, 0.25f, 0.0, 0.5)); //#ffa000 ORANGE
}

CCamera::~CCamera()
{
}

void CCamera::quat2rot(double *q, double *R)
{
	// QUAT2ROT - Quaternion to rotation matrix transformation
	//
	//  Usage: quat2rot(q, R); q: quaternion, R: rotation matrix

	double x, y, z, w, x2, y2, z2, w2, xy, xz, yz, wx, wy, wz;
	double norm_q = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);

	x = q[0] / norm_q;
	y = q[1] / norm_q;
	z = q[2] / norm_q;
	w = q[3] / norm_q;

	x2 = x * x;
	y2 = y * y;
	z2 = z * z;
	w2 = w * w;
	xy = 2 * x * y;
	xz = 2 * x * z;
	yz = 2 * y * z;
	wx = 2 * w * x;
	wy = 2 * w * y;
	wz = 2 * w * z;

	// Format: {R[0] R[1] R[2]; R[3] R[4] R[5]; R[6] R[7] R[8];}
	R[0] = w2 + x2 - y2 - z2;
	R[1] = xy - wz;
	R[2] = xz + wy;
	R[3] = xy + wz;
	R[4] = w2 - x2 + y2 - z2;
	R[5] = yz - wx;
	R[6] = xz - wy;
	R[7] = yz + wx;
	R[8] = w2 - x2 - y2 + z2;
}

osg::ref_ptr<osg::Geometry> CCamera::drawLineSegment(osg::Vec3 sp, osg::Vec3 ep, osg::Vec4 color, float fLineWidth)
{
	//osg::Vec3 sp(0, -180, 120);
	//osg::Vec3 ep(0, 480, 120);
	osg::ref_ptr<osg::Geometry> beam(new osg::Geometry);
	osg::ref_ptr<osg::Vec3Array> points = new osg::Vec3Array;
	points->push_back(sp);
	points->push_back(ep);
	osg::ref_ptr<osg::Vec4Array> _color = new osg::Vec4Array;
	_color->push_back(color);
	beam->setVertexArray(points.get());
	beam->setColorArray(_color.get());
	beam->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);
	beam->addPrimitiveSet(new osg::DrawArrays(GL_LINES, 0, 2));

	// transpanrent
	osg::LineWidth *lineWidth = new osg::LineWidth();
	lineWidth->setWidth(fLineWidth);
	osg::ref_ptr<osg::StateSet> ss = beam->getOrCreateStateSet();
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	ss->setAttributeAndModes(lineWidth, osg::StateAttribute::ON);

	return beam;
}
osg::ref_ptr<osg::Geometry> CCamera::drawLineSegment(osg::Vec3 sp, osg::Vec3 ep)
{
	//osg::Vec3 sp(0, -180, 120);
	//osg::Vec3 ep(0, 480, 120);
	osg::ref_ptr<osg::Geometry> beam(new osg::Geometry);
	osg::ref_ptr<osg::Vec3Array> points = new osg::Vec3Array;
	points->push_back(sp);
	points->push_back(ep);
	//osg::ref_ptr<osg::Vec4Array> color = new osg::Vec4Array;
	//color->push_back(osg::Vec4(1.0, 0.0, 0.0, 1.0));
	beam->setVertexArray(points.get());
	beam->setColorArray(eg_color.get());
	beam->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);
	beam->addPrimitiveSet(new osg::DrawArrays(GL_LINES, 0, 2));

	// transpanrent
	osg::ref_ptr<osg::StateSet> ss = beam->getOrCreateStateSet();
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	return beam;
}

// Draw camera
void CCamera::drawCamera(osg::ref_ptr<osg::PositionAttitudeTransform> node)
{

	// http://trac.openscenegraph.org/projects/osg//wiki/Support/Tutorials/BasicGeometry
	// http://stackoverflow.com/questions/11489391/openscenegraph-drawing-a-3d-wall
	// http://domedb.perception.cs.cmu.edu/tools/generator/test/
	osg::ref_ptr<osg::Vec3Array> cameraVertices = new osg::Vec3Array;
	osg::ref_ptr<osg::Geometry> cameraGeometry = new osg::Geometry();
	osg::ref_ptr<osg::Geode> cameraGeode = new osg::Geode();

	cameraGeode->addDrawable(cameraGeometry);
	node->addChild(cameraGeode);

	if (0)
	{
		// vertices
		cameraVertices->push_back(osg::Vec3(-0.4, 0.3, 0.5)); //
		cameraVertices->push_back(osg::Vec3(-0.4, -0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(0.4, -0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(0.4, 0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(-0.4, 0.3, -1)); //
		cameraVertices->push_back(osg::Vec3(-0.4, -0.3, -1));
		cameraVertices->push_back(osg::Vec3(0.4, -0.3, -1));
		cameraVertices->push_back(osg::Vec3(0.4, 0.3, -1));
		cameraVertices->push_back(osg::Vec3(-1.2, 0.9, -2)); //
		cameraVertices->push_back(osg::Vec3(-1.2, -0.9, -2));
		cameraVertices->push_back(osg::Vec3(1.2, -0.9, -2));
		cameraVertices->push_back(osg::Vec3(1.2, 0.9, -2));
		cameraGeometry->setVertexArray(cameraVertices);

		// Quad Face
		osg::DrawElementsUInt *camBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		camBase->push_back(0);
		camBase->push_back(1);
		camBase->push_back(2);
		camBase->push_back(3);
		camBase->push_back(0);
		camBase->push_back(4);
		camBase->push_back(5);
		camBase->push_back(1);
		camBase->push_back(0);
		camBase->push_back(3);
		camBase->push_back(7);
		camBase->push_back(4);
		camBase->push_back(0);
		camBase->push_back(4);
		camBase->push_back(5);
		camBase->push_back(1);
		camBase->push_back(3);
		camBase->push_back(2);
		camBase->push_back(6);
		camBase->push_back(7);
		camBase->push_back(2);
		camBase->push_back(1);
		camBase->push_back(5);
		camBase->push_back(6);
		camBase->push_back(5);
		camBase->push_back(4);
		camBase->push_back(7);
		camBase->push_back(6);
		camBase->push_back(5);
		camBase->push_back(4);
		camBase->push_back(8);
		camBase->push_back(9);
		camBase->push_back(5);
		camBase->push_back(9);
		camBase->push_back(10);
		camBase->push_back(6);
		camBase->push_back(7);
		camBase->push_back(6);
		camBase->push_back(10);
		camBase->push_back(11);
		camBase->push_back(7);
		camBase->push_back(11);
		camBase->push_back(8);
		camBase->push_back(4);
		camBase->push_back(9);
		camBase->push_back(8);
		camBase->push_back(11);
		camBase->push_back(10);
		cameraGeometry->addPrimitiveSet(camBase);
	}
	else
	{
		double s = 0.1;
		float width_2 = 1.066667;
		float height_2 = 0.6;
		float depth = 2.0;

#ifdef Y_DOWN_ALONG_Z
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0));
		cameraVertices->push_back(osg::Vec3d(-width_2, height_2, depth)); //
		cameraVertices->push_back(osg::Vec3d(-width_2, -height_2, depth));
		cameraVertices->push_back(osg::Vec3d(width_2, -height_2, depth));
		cameraVertices->push_back(osg::Vec3d(width_2, height_2, depth));
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, height_2, depth), osg::Vec3d(-width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, -height_2, depth), osg::Vec3d(width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, -height_2, depth), osg::Vec3d(width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, height_2, depth), osg::Vec3d(-width_2, height_2, depth)));

		// axis
		// node->addChild(drawCamAxis());

#endif

#ifdef Z_UP_ALONG_Y
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0));
		cameraVertices->push_back(osg::Vec3d(-width_2, depth, height_2)); //
		cameraVertices->push_back(osg::Vec3d(-width_2, depth, -height_2));
		cameraVertices->push_back(osg::Vec3d(width_2, depth, -height_2));
		cameraVertices->push_back(osg::Vec3d(width_2, depth, height_2));
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, depth, height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, depth, height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, depth, height_2), osg::Vec3d(-width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, depth, -height_2), osg::Vec3d(width_2, depth, -height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, depth, -height_2), osg::Vec3d(width_2, depth, height_2)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, depth, height_2), osg::Vec3d(-width_2, depth, height_2)));

		// axis
		// node->addChild(drawCamAxis());

#endif

#ifdef Y_UP_ALONG_Z
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, 0.9, 2) * s, osg::Vec3d(-1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, -0.9, 2) * s, osg::Vec3d(1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, -0.9, 2) * s, osg::Vec3d(1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, 0.9, 2) * s, osg::Vec3d(-1.2, 0.9, 2) * s));
#endif

#ifdef Y_UP_ALONG_Z_MINUS
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0) * s);
		cameraVertices->push_back(osg::Vec3d(-1.0, 0.6, -3) * s); //
		cameraVertices->push_back(osg::Vec3d(-1.0, -0.6, -3) * s);
		cameraVertices->push_back(osg::Vec3d(1.0, -0.6, -3) * s);
		cameraVertices->push_back(osg::Vec3d(1.0, 0.6, -3) * s);
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.0, 0.6, -3) * s, osg::Vec3d(-1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.0, -0.6, -3) * s, osg::Vec3d(1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.0, -0.6, -3) * s, osg::Vec3d(1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.0, 0.6, -3) * s, osg::Vec3d(-1.0, 0.6, -3) * s));
#endif

		//// Quad Face
		osg::ref_ptr<osg::DrawElementsUInt> camBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		camBase->push_back(2);
		camBase->push_back(1);
		camBase->push_back(4);
		camBase->push_back(3);
		cameraGeometry->addPrimitiveSet(camBase);

		osg::ref_ptr<osg::DrawElementsUInt> camBase3 = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
		camBase3->push_back(0);
		camBase3->push_back(1);
		camBase3->push_back(2);
		camBase3->push_back(0);
		camBase3->push_back(2);
		camBase3->push_back(3);
		camBase3->push_back(0);
		camBase3->push_back(3);
		camBase3->push_back(4);
		camBase3->push_back(0);
		camBase3->push_back(4);
		camBase3->push_back(1);
		cameraGeometry->addPrimitiveSet(camBase3);

		//Color
		osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array; // eg_color
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		colors->push_back(osg::Vec4(0.3f, 0.3f, 0.3f, 0.6f));
		cameraGeometry->setColorArray(colors);
		cameraGeometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

		// transpanrent
		osg::ref_ptr<osg::StateSet> ss = cameraGeode->getOrCreateStateSet();
		ss->setMode(GL_BLEND, osg::StateAttribute::ON);
		ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	}

	// // Rotation + tranlation + scale
	// double s = 0.1;
	//
	// node->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0))*osgQ.inverse());
	// node->setScale(osg::Vec3(s, s, s));
	// node->setPosition(osgCent);
}

// Draw Text Label
void CCamera::drawCamLabel(osg::ref_ptr<osg::PositionAttitudeTransform> node)
{
	osg::ref_ptr<osgText::Text> textNode = new osgText::Text();
	stringstream label;

	if (panelIdx == HD_PANEL_INDEX)
		label << "H"
			  << "_" << camIdx << endl;
	else if (panelIdx == Kinect_PANEL_INDEX)
		label << "K"
			  << "_" << camIdx << endl;
	else if (panelIdx == Proj_PANEL_INDEX)
		label << "P"
			  << "_" << camIdx << endl;
	else
		label << "V"
			  << "_" << panelIdx << "_" << camIdx << endl;

	textNode->setCharacterSize(0.1);
	textNode->setFont("/fonts/arial.ttf");
	textNode->setText(label.str());
	textNode->setAxisAlignment(osgText::Text::SCREEN);
	textNode->setPosition(osg::Vec3(0, 0, 0));
	textNode->setColor(osg::Vec4(199, 77, 15, 1));

	//textNode->setNodeMask(NODEMASK);

	node->addChild(textNode);
	// node->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0))*osgQ.inverse());
	// node->setPosition(osgCent);
}

// Draw Axis
void CCamera::drawCamAxis(osg::ref_ptr<osg::PositionAttitudeTransform> node)
{
#if NULL
	{
		osg::Vec3 eye = osg::Vec3(0, 0, 0);
		osg::Vec3 cent = osg::Vec3(0, 0, 1);
		osg::Vec3 up = osg::Vec3(0, -1, 0);

		//osg::Matrixd q1 = osg::Matrixd::inverse(osgR);
		osg::Quat q1 = osgQ.inverse();

		osg::Vec3 T = osgT;

		eye = q1 * (eye - T);
		cent = q1 * (cent - T) - eye;
		up = q1 * (up - T) - eye;

		osg::Vec3 zAxis = cent;
		osg::Vec3 yAxis = up;
		osg::Vec3 xAxis = up ^ cent; //Cross product

		float s = 5;
		node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), xAxis / xAxis.normalize() * s, osg::Vec4(1, 0, 0, 1)));
		node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), yAxis / yAxis.normalize() * s, osg::Vec4(0, 1, 0, 1)));
		node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), zAxis / zAxis.normalize() * s, osg::Vec4(0, 0, 1, 1)));
	}
#else
	{
		osg::Vec3 eye = osg::Vec3(0, 0, 0);
		osg::Vec3 cent = osg::Vec3(0, 1, 0);
		osg::Vec3 up = osg::Vec3(0, 0, 1); // osg::Vec3 up = osg::Vec3(0, -1, 0);

		osg::Vec3 zAxis = cent;
		osg::Vec3 yAxis = up;
		osg::Vec3 xAxis = up ^ cent; //Cross product

		// node->addChild(drawLineSegment(eye, eye�� + xAxis / xAxis.normalize()*3, osg::Vec4(1, 0, 0, 1)));
		// node->addChild(drawLineSegment(eye, eye + yAxis / yAxis.normalize()*3, osg::Vec4(0, 1, 0, 1)));
		// node->addChild(drawLineSegment(eye, eye + zAxis / zAxis.normalize()*3, osg::Vec4(0, 0, 1, 1)));

		float s = 2;
		node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), xAxis / xAxis.normalize() * s, osg::Vec4(1, 0, 0, 1)));
		node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), yAxis / yAxis.normalize() * s, osg::Vec4(0, 1, 0, 1)));
		node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), zAxis / zAxis.normalize() * s, osg::Vec4(0, 0, 1, 1)));
	}
#endif
}

bool CCamera::readMatrix(const char *filename, float *mat)
{
	std::ifstream file(filename);
	std::string str = "";
	//float _mat[14];

	if (file.fail())
	{
		return true;
	}
	else
	{
		int i = 0;
		while (file >> mat[i])
		{
			i++;
		}
	}

	//cout << _mat[0] << "," << _mat[1] << "," << _mat[2] << endl;

	return false;
};

bool CCamera::isValidCam()
{
	return bValid;
}

void CCamera::setDrawColor(int NODE_MASK)
{
	switch (NODE_MASK)
	{
	case VGA_NODE_MASK:
		eg_color = eg_color_VGA;
		break;

	case HD_NODE_MASK:
		eg_color = eg_color_HD;
		break;

	case Kinect_NODE_MASK:
		eg_color = eg_color_Kinect;
		break;

	case Proj_NODE_MASK:
		eg_color = eg_color_Projector;
		break;

	default:
		eg_color = eg_color_VGA;
		break;
	}
}

void CCamera::setName(int _panelIdx, int _camIdx)
{
	stringstream _name;
	_name << "VGA_" << _panelIdx << "_" << _camIdx;

	this->name = _name.str();
	this->panelIdx = _panelIdx;
	this->camIdx = _camIdx;
}

bool CCamera::initCameraPrameters(string _calibPath, int _panelIdx, int _camIdx)
{
	std::stringstream intrFilePath;
	std::stringstream extrFilePath;

	intrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << ".txt";
	extrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << "_ext.txt";

	//cout << "intrFilePath: " << intrFilePath.str() << endl;
	//cout << "extFilePath: " << extrFilePath.str() << endl;

	// Intrinsic Parameter : (panelIdx)_(camIdx).txt
	//
	//	Composed of 11 floating point numbers as follows.
	//	K11 K12 K13 K21 K22 K23 K31 K32 K33 distortionParam1 distortionParam2 xxx
	//	All the sequences contain rectified images, thus both distortion parameters are zeros.
	//
	// float intr_mat[14];
	if (readMatrix(intrFilePath.str().c_str(), intr_mat))
		return true;

	// Extrinsic Parameter : (panelIdx)_(camIdx)_ext.txt
	//
	// 	Composed of 7 floating point numbers
	// 	The first 4 numbers are quaternion representation of R
	// 	The last 3 numbers are the camera center, which is - invR x t
	// 	Thus, t = -R*Center
	// float extr_mat[7];
	if (readMatrix(extrFilePath.str().c_str(), extr_mat))
		return true;

	// Intrisic matrix
	matM << intr_mat[0], intr_mat[1], intr_mat[2],
		intr_mat[3], intr_mat[4], intr_mat[5],
		intr_mat[6], intr_mat[7], intr_mat[8];

	// Distortion matrix
	Map<RowVectorXf> _matDistortion(intr_mat + 9, 5);
	matDistortion = _matDistortion;

	// Rotation quternion
	Map<RowVectorXf> _matRq(extr_mat, 4);
	matRq = _matRq;

	// Camera center
	Map<RowVectorXf> _vecCent(extr_mat + 4, 3);
	vecCent = _vecCent;

	// Rotation matrix
	//double q[4] = { matRq(0), matRq(1), matRq(2), matRq(3) };
	double q[4] = {matRq(1), matRq(2), matRq(3), matRq(0)}; // xyzw	 -> result left hand?
	double rot[9];
	quat2rot(q, rot);
	matR << rot[0], rot[1], rot[2],
		rot[3], rot[4], rot[5],
		rot[6], rot[7], rot[8];

	// @TODO
	// osgR = osg::Matrixd(matR(0,0), matR(1,0), matR(2,0), 0,
	// 					matR(0,1), matR(1,1), matR(2,1), 0,
	// 					matR(0,2), matR(1,2), matR(2,2), 0,
	// 					matR(0,3), matR(1,3), matR(2,3), 1);
	//
	osgR = osg::Matrixd(rot[0], rot[1], rot[2], 0,
						rot[3], rot[4], rot[5], 0,
						rot[6], rot[7], rot[8], 0,
						0, 0, 0, 1);

	//osgQ = osg::Quat(q[0],q[1],q[2],q[3]);

	// Notice: Original Calibration data [w x y z]
	// (1) inline Quat( value_type x, value_type y, value_type z, value_type w )
	// (2) quat2rot() [x y z w]
	//osgQ = osg::Quat(_matRq(0),_matRq(1), _matRq(2), _matRq(3));
	osgQ = osg::Quat(_matRq(1), _matRq(2), _matRq(3), _matRq(0)); // xyzw -> result left hand?

	osgCent = osg::Vec3(_vecCent(0), _vecCent(1), _vecCent(2));

	vecT = -matR * vecCent.transpose();

	// @ATTENSION: -q*t vs q*(-t) is different or not?
	osgT = osgQ * (-osgCent);

	//osgT = osg::Vec3(vecT(0), vecT(1), vecT(2));
	// std::cout << "vecT: \n" << vecT << endl;
	// CCamUtil::getInstance()->printVec(osgT, "osgT");

	// Translate vector, t = -R*Center			@TODO

	// std::cout << "matM: \n" << matM << endl;
	// std::cout << "matDistortion: \n" << matDistortion << endl;
	// std::cout << "matRq: \n" << matRq << endl;
	// std::cout << "vecT: \n" << vecT << endl;
	// std::cout << "vecCent: \n" << vecCent << endl;

	// TEST Caculate T
	// osg::Vec3 T1 = osgQ*(-osgCent);
	// osg::Vec3 T2 = osg::Vec3(vecT(0), vecT(1), vecT(2));	// -matR*vecCent.transpose();
	// osg::Vec3 T3 = osgR*(-osgCent);
	// osg::Vec3 T4 = osgR*osgCent; T4 = -T4;
	// osg::Vec3 T5 = osgQ*osgCent; T5 = -T5;
	// osg::Vec3 T6 = -osgQ*(-osgCent);
	//
	//
	// CCamUtil::getInstance()->printVec(T1, "T1");
	// CCamUtil::getInstance()->printVec(T2, "T2");
	// CCamUtil::getInstance()->printVec(T3, "T3");
	// CCamUtil::getInstance()->printVec(T4, "T4");
	// CCamUtil::getInstance()->printVec(T5, "T5");
	// CCamUtil::getInstance()->printVec(T6, "T6");
	// CCamUtil::getInstance()->printVec(osgT, "osgT");

	// TEST Cent
	// CCamUtil::getInstance()->printVec(osgCent, "CanCent");
	// CCamUtil::getInstance()->printVec(osgQ.inverse()*(-T1), "CanCent_Recalc");

	// TEST T
	// CCamUtil::getInstance()->printVec(osgR*osgCent + osgT, "RX+T=0");
	// CCamUtil::getInstance()->printVec(osgQ*osgCent + osgT, "RX+T=0");

	return false;
}

osg::Matrixd CCamera::getProjectionMatrix(int width, int height, float zfar, float znear)
{
	float depth = zfar - znear;
	float q = -(zfar + znear) / depth;
	float qn = -2 * (zfar * znear) / depth;

	float _K00 = matM(0, 0);
	float _K01 = matM(0, 1);
	float _K02 = matM(0, 2);
	float _K11 = matM(1, 1);
	float _K12 = matM(1, 2);

	// fx = _K00;            cx = _K02;  Tx = 0.0;
	// fy = _K11; cy = _K12;  Ty = 0.0;

	float x0 = 0.0f;
	float y0 = 0.0f;

	osg::Matrixd p;
	bool _y_up = true;
	if (_y_up)
	{
		p = osg::Matrixf(2 * _K00 / width, -2 * _K01 / width, (-2 * _K02 + width + 2 * x0) / width, 0,
						 0, -2 * _K11 / height, (-2 * _K12 + height + 2 * y0) / height, 0,
						 0, 0, q, qn,
						 0, 0, -1, 0);
	}
	else
	{
		p = osg::Matrixf(2 * _K00 / width, -2 * _K01 / width, (-2 * _K02 + width + 2 * x0) / width, 0,
						 0, 2 * _K11 / height, (2 * _K12 - height + 2 * y0) / height, 0,
						 0, 0, q, qn,
						 0, 0, -1, 0);
	}
	//osg::Matrixd p = osg::Matrixf(2 * _K00 / width, -2 * _K01 / width, (-2 * _K02 + width + 2 * x0) / width, 0,
	//	0, 2 * _K11 / height, (2 * _K12 - height + 2 * y0) / height, 0,
	//	0, 0, q, qn,
	//	0, 0, -1, 0);
	//
	osg::Matrixd pT;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			pT(i, j) = p(j, i);
		}
	}

	return pT;
	//return osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));
}

osg::Matrixd CCamera::updateMVPMatrix(double fx, double fy, double skew, double u0, double v0,
									  int imageWidth, int imageHeight,
									  double near_clip, double far_clip, double matRq[4], float vecCent[3])
{
	/*
	00_15.txt:		1430.13		0	949.221		0		1426.45	559.319			0		0		1			0	0	0	0	0
	00_15_ext.txt:	0.999036 -0.0410261 -0.0120331 0.00996186 -4.01396 0.777917 -7.46611
	*/

	//double fx = 1430.13;
	//double fy = 1426.45;
	//double skew = 0;
	//double u0 = 949.221;
	//double v0 = 559.319;
	//int imageWidth = 1920;
	//int imageHeight = 1080;
	//double near_clip = 0.001;
	//double far_clip = 10000;
	//double matRq[4] = { 0.999036, -0.0410261, -0.0120331, 0.00996186 };
	//float vecCent[3] = { -4.01396, 0.777917, -7.46611 };

	cout << "fx:" << fx << " fy:" << fy << " skew:" << skew << " u0:" << u0 << " v0:" << v0 << " width:" << imageWidth << " height:" << imageHeight << " near_clip:" << near_clip << " far_clip:" << far_clip << endl;

	double q[4] = {matRq[1], matRq[2], matRq[3], matRq[0]};
	double rot[9];
	quat2rot(q, rot);
	Eigen::Matrix3f m_R;
	m_R << rot[0], rot[1], rot[2],
		rot[3], rot[4], rot[5],
		rot[6], rot[7], rot[8];

	Map<RowVectorXf> _vecCent(vecCent, 3);
	Eigen::MatrixXf vecT = -m_R * _vecCent.transpose();

	cout << "Rotation Matrix: \n"
		 << m_R << endl;
	cout << "Rotation Matrix Transpose: \n"
		 << m_R.transpose() << endl;
	cout << "Translate vector: \n"
		 << vecT << endl;

	// ************************
	// Han's Code Start
	// ************************

	// View matrix
	GLfloat m_modelViewMatGL[16];
	m_modelViewMatGL[0] = m_R(0, 0);
	m_modelViewMatGL[1] = m_R(1, 0);
	m_modelViewMatGL[2] = m_R(2, 0);
	m_modelViewMatGL[3] = 0;
	m_modelViewMatGL[4] = m_R(0, 1);
	m_modelViewMatGL[5] = m_R(1, 1);
	m_modelViewMatGL[6] = m_R(2, 1);
	m_modelViewMatGL[7] = 0;
	m_modelViewMatGL[8] = m_R(0, 2);
	m_modelViewMatGL[9] = m_R(1, 2);
	m_modelViewMatGL[10] = m_R(2, 2);
	m_modelViewMatGL[11] = 0;
	m_modelViewMatGL[12] = vecT(0); //4th col
	m_modelViewMatGL[13] = vecT(1);
	m_modelViewMatGL[14] = vecT(2);
	m_modelViewMatGL[15] = 1;

	// Projector matrix
	GLfloat m_projMatGL[16];
	build_opengl_projection_for_intrinsics(m_projMatGL, fx, fy, skew, u0, v0, imageWidth, imageHeight, near_clip, far_clip);

	Mat_<double> ProjMat(4, 4);
	Mat_<double> viewModelMat(4, 4);

	double *ProjMatPtr = (double *)ProjMat.data;
	double *viewModelMatPtr = (double *)viewModelMat.data;
	for (int i = 0; i < 16; ++i)
	{
		ProjMatPtr[i] = m_projMatGL[i];
		viewModelMatPtr[i] = m_modelViewMatGL[i];
	}

	Mat_<double> mvp;
	mvp = ProjMat.t() * viewModelMat.t();
	mvp = mvp.t();

	// cout << "viewModelMa \n" << viewModelMat << endl;
	// cout << "ProjMat \n" << ProjMat << endl;
	// cout << "mvp\n" << mvp << endl;

	// ************************
	// Han's Code End
	// ************************

	osg::Matrixd slaveMVPMat;
	// OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
	slaveMVPMat(0, 0) = mvp(0);
	slaveMVPMat(0, 1) = mvp(1);
	slaveMVPMat(0, 2) = mvp(2);
	slaveMVPMat(0, 3) = mvp(3);
	slaveMVPMat(1, 0) = mvp(4);
	slaveMVPMat(1, 1) = mvp(5);
	slaveMVPMat(1, 2) = mvp(6);
	slaveMVPMat(1, 3) = mvp(7);
	slaveMVPMat(2, 0) = mvp(8);
	slaveMVPMat(2, 1) = mvp(9);
	slaveMVPMat(2, 2) = mvp(10);
	slaveMVPMat(2, 3) = mvp(11);
	slaveMVPMat(3, 0) = mvp(12);
	slaveMVPMat(3, 1) = mvp(13);
	slaveMVPMat(3, 2) = mvp(14);
	slaveMVPMat(3, 3) = mvp(15); // T

	return slaveMVPMat;
}

osg::Matrixd CCamera::updateMVPMatrix(int imageWidth, int imageHeight, double near_clip, double far_clip)
{
	/*
	00_15.txt:		1430.13		0	949.221		0		1426.45	559.319			0		0		1			0	0	0	0	0
	00_15_ext.txt:	0.999036 -0.0410261 -0.0120331 0.00996186 -4.01396 0.777917 -7.46611
	*/

	//double fx = 1430.13;
	//double fy = 1426.45;
	//double skew = 0;
	//double u0 = 949.221;
	//double v0 = 559.319;
	//int imageWidth = 1920;
	//int imageHeight = 1080;
	//double near_clip = 0.001;
	//double far_clip = 10000;
	//double matRq[4] = { 0.999036, -0.0410261, -0.0120331, 0.00996186 };
	//float vecCent[3] = { -4.01396, 0.777917, -7.46611 };

	double q[4] = {matRq[1], matRq[2], matRq[3], matRq[0]};
	double rot[9];
	quat2rot(q, rot);
	Eigen::Matrix3f m_R;
	m_R << rot[0], rot[1], rot[2],
		rot[3], rot[4], rot[5],
		rot[6], rot[7], rot[8];

	Eigen::MatrixXf vecT = -m_R * vecCent.transpose();

	double fx = intr_mat[0]; // 1430.13;
	double fy = intr_mat[4]; // 1426.45;
	double skew = 0;
	double u0 = intr_mat[2]; // 949.221;
	double v0 = intr_mat[5]; // 559.319;

	cout << "fx:" << fx << " fy:" << fy << " skew:" << skew << " u0:" << u0 << " v0:" << v0 << " width:" << imageWidth << " height:" << imageHeight << " near_clip:" << near_clip << " far_clip:" << far_clip << endl;
	//
	// cout << "Rotation Matrix: \n" << m_R << endl;
	// cout << "Rotation Matrix Transpose: \n" << m_R.transpose() << endl;
	// cout << "Translate vector: \n" << vecT << endl;

	// ************************
	// Han's Code Start
	// ************************

	// View matrix
	GLfloat m_modelViewMatGL[16];
	m_modelViewMatGL[0] = m_R(0, 0);
	m_modelViewMatGL[1] = m_R(1, 0);
	m_modelViewMatGL[2] = m_R(2, 0);
	m_modelViewMatGL[3] = 0;
	m_modelViewMatGL[4] = m_R(0, 1);
	m_modelViewMatGL[5] = m_R(1, 1);
	m_modelViewMatGL[6] = m_R(2, 1);
	m_modelViewMatGL[7] = 0;
	m_modelViewMatGL[8] = m_R(0, 2);
	m_modelViewMatGL[9] = m_R(1, 2);
	m_modelViewMatGL[10] = m_R(2, 2);
	m_modelViewMatGL[11] = 0;
	m_modelViewMatGL[12] = vecT(0); //4th col
	m_modelViewMatGL[13] = vecT(1);
	m_modelViewMatGL[14] = vecT(2);
	m_modelViewMatGL[15] = 1;

	// Projector matrix
	GLfloat m_projMatGL[16];
	build_opengl_projection_for_intrinsics(m_projMatGL, fx, fy, skew, u0, v0, imageWidth, imageHeight, near_clip, far_clip);

	Mat_<double> ProjMat(4, 4);
	Mat_<double> viewModelMat(4, 4);

	double *ProjMatPtr = (double *)ProjMat.data;
	double *viewModelMatPtr = (double *)viewModelMat.data;
	for (int i = 0; i < 16; ++i)
	{
		ProjMatPtr[i] = m_projMatGL[i];
		viewModelMatPtr[i] = m_modelViewMatGL[i];
	}

	Mat_<double> mvp;
	mvp = ProjMat.t() * viewModelMat.t();
	mvp = mvp.t();

	// cout << "viewModelMa \n" << viewModelMat << endl;
	// cout << "ProjMat \n" << ProjMat << endl;
	// cout << "mvp\n" << mvp << endl;

	// ************************
	// Han's Code End
	// ************************

	osg::Matrixd slaveMVPMat;

	// OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
	slaveMVPMat(0, 0) = mvp(0);
	slaveMVPMat(0, 1) = mvp(1);
	slaveMVPMat(0, 2) = mvp(2);
	slaveMVPMat(0, 3) = mvp(3);
	slaveMVPMat(1, 0) = mvp(4);
	slaveMVPMat(1, 1) = mvp(5);
	slaveMVPMat(1, 2) = mvp(6);
	slaveMVPMat(1, 3) = mvp(7);
	slaveMVPMat(2, 0) = mvp(8);
	slaveMVPMat(2, 1) = mvp(9);
	slaveMVPMat(2, 2) = mvp(10);
	slaveMVPMat(2, 3) = mvp(11);
	slaveMVPMat(3, 0) = mvp(12);
	slaveMVPMat(3, 1) = mvp(13);
	slaveMVPMat(3, 2) = mvp(14);
	slaveMVPMat(3, 3) = mvp(15); // T

	Mat_<double> _projMat = ProjMat.t();
	osg::Matrixd _osg_projMat;
	// OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
	_osg_projMat(0, 0) = _projMat(0);
	_osg_projMat(0, 1) = _projMat(1);
	_osg_projMat(0, 2) = _projMat(2);
	_osg_projMat(0, 3) = _projMat(3);
	_osg_projMat(1, 0) = _projMat(4);
	_osg_projMat(1, 1) = _projMat(5);
	_osg_projMat(1, 2) = _projMat(6);
	_osg_projMat(1, 3) = _projMat(7);
	_osg_projMat(2, 0) = _projMat(8);
	_osg_projMat(2, 1) = _projMat(9);
	_osg_projMat(2, 2) = _projMat(10);
	_osg_projMat(2, 3) = _projMat(11);
	_osg_projMat(3, 0) = _projMat(12);
	_osg_projMat(3, 1) = _projMat(13);
	_osg_projMat(3, 2) = _projMat(14);
	_osg_projMat(3, 3) = _projMat(15); // T

	Mat_<double> _viewModelMat = viewModelMat.t();
	osg::Matrixd _osg_viewModelMat;
	// OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
	_osg_viewModelMat(0, 0) = _viewModelMat(0);
	_osg_viewModelMat(0, 1) = _viewModelMat(1);
	_osg_viewModelMat(0, 2) = _viewModelMat(2);
	_osg_viewModelMat(0, 3) = _viewModelMat(3);
	_osg_viewModelMat(1, 0) = _viewModelMat(4);
	_osg_viewModelMat(1, 1) = _viewModelMat(5);
	_osg_viewModelMat(1, 2) = _viewModelMat(6);
	_osg_viewModelMat(1, 3) = _viewModelMat(7);
	_osg_viewModelMat(2, 0) = _viewModelMat(8);
	_osg_viewModelMat(2, 1) = _viewModelMat(9);
	_osg_viewModelMat(2, 2) = _viewModelMat(10);
	_osg_viewModelMat(2, 3) = _viewModelMat(11);
	_osg_viewModelMat(3, 0) = _viewModelMat(12);
	_osg_viewModelMat(3, 1) = _viewModelMat(13);
	_osg_viewModelMat(3, 2) = _viewModelMat(14);
	_osg_viewModelMat(3, 3) = _viewModelMat(15); // T

	//CCamUtil::getInstance()->shaderMVPMat = slaveMVPMat; // return instead set
	CCamUtil::getInstance()->shaderProjMat = _osg_projMat;
	CCamUtil::getInstance()->shaderViewMat = _osg_viewModelMat;

	return slaveMVPMat;
}

void CCamera::createSlaveViewer(osg::ref_ptr<osg::Camera> &camera, int width, int height, int xOffset, int yOffset)
{

	if (isValidCam() && camera)
	{
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xOffset;
		traits->y = yOffset;
		traits->width = width;
		traits->height = height;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		if (panelIdx == Proj_PANEL_INDEX)
			traits->windowDecoration = false;
		else
			traits->windowDecoration = true;

		cout << "Create slaver viewer " << camIdx << ": " << traits->x << "," << traits->y << "," << traits->width << "," << traits->height << endl;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		//osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		camera->setProjectionResizePolicy(osg::Camera::HORIZONTAL);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);

		//Method 1
		// set the projection matrix
		// osg::Matrixd proj = getProjectionMatrix(width, height, 0.01, 100);
		// camera->setProjectionMatrix(proj);

		// Method 2
		//osg::Matrixd cameraRotation = osg::Matrixd::rotate(osg::Quat(osg::PI, osg::Vec3(1, 0, 0))) * osg::Matrixd::inverse(osgR);
		/*
		osg::Matrixd cameraRotation = osg::Matrixd::rotate(osg::Quat(osg::PI, osg::Vec3(0, 1, 0))*osgQ.inverse()); //*osg::Quat(osg::PI_2, osg::Vec3(0, 0, 1))*
		osg::Matrixd cameraTrans = osg::Matrixd::translate(osgCent);
		camera->setViewMatrix(osg::Matrixd::inverse(cameraRotation * cameraTrans));
		*/

		// Method 3
		//updateMVPMatrix(width, height);
		//slaveMVPUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "slaveMVPMat");
		//slaveMVPUniform->setUpdateCallback(new UniformUpdate(&slaveMVPMat));
	}
}

void CCamera::setSlaveViewer(osg::ref_ptr<osg::Camera> camera, int width, int height)
{

	if (isValidCam() && camera)
	{

		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);

		if (1)
		{
			// Method 0
			// set the projection matrix
			osg::Matrixd proj = getProjectionMatrix(width, height, 0.01, 100);
			camera->setProjectionMatrix(proj);

			// Method 1
			osg::Vec3 eye = osg::Vec3(0, 0, 0);
			osg::Vec3 cent = osg::Vec3(0, 0, 1);
			osg::Vec3 up = osg::Vec3(0, -1, 0);

			//osg::Matrixd q1 = osg::Matrixd::inverse(osgR);
			osg::Quat q1 = osgQ.inverse();

			osg::Vec3 T = osgT;

			eye = q1 * (eye - T);
			cent = q1 * (cent - T) - eye;
			up = q1 * (up - T) - eye;

			osg::Vec3 zAxis = cent;
			osg::Vec3 yAxis = up;
			osg::Vec3 xAxis = up ^ cent; //Cross product

			osg::Matrixd mv;
			mv.makeLookAt(eye, zAxis, yAxis);
			camera->setViewMatrix(mv);

			// Method 2  2016-7-16 ���ִ˴�����work�ķǳ��ã�һֱ���õĶ��Ǵ˴��ķ�����Ⱦ������������shader�еķ�������Ҫ�Ƚ�
			osg::Matrixd cameraRotation = osg::Matrixd::rotate(osg::Quat(osg::PI, osg::Vec3(0, 1, 0)) * osgQ.inverse());
			osg::Matrixd cameraTrans = osg::Matrixd::translate(osgCent);
			camera->setViewMatrix(osg::Matrixd::inverse(cameraRotation * cameraTrans));
		}

		// CCamUtil::getInstance()->printMatrix(proj, "projMat");
	}
}

// Create slave viewer
void CCamera::createSlaveViewerShader(osg::ref_ptr<osg::Camera> &camera, int width, int height, int xOffset, int yOffset)
{
	if (isValidCam() && camera)
	{
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xOffset;
		traits->y = yOffset;
		traits->width = width;
		traits->height = height;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		cout << "Create slaver viewer " << camIdx << ": " << traits->x << "," << traits->y << "," << traits->width << "," << traits->height << endl;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		//osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		camera->setProjectionResizePolicy(osg::Camera::HORIZONTAL);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(1.0f, 1.0, 1.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);

		// Data
		Eigen::Matrix3f m_R = matR.transpose(); //@TODO

		double fx = matM(0, 0);
		double fy = matM(1, 1);
		double skew = matM(0, 1);
		double u0 = matM(0, 2);
		double v0 = matM(1, 2);
		int imageWidth = width;
		int imageHeight = height;
		double near_clip = 0.01;
		double far_clip = 100;

		// cout << "fx:" << fx << " fy:" << fy << " skew:" << skew << " u0:" << u0 << " v0:" << v0 << " width:" << imageWidth << " height:" << imageHeight << " near_clip:" << near_clip << " far_clip:" << far_clip << endl;

		// ************************
		// Han's Code Start
		// ************************

		// View matrix

		GLfloat m_modelViewMatGL[16];
		m_modelViewMatGL[0] = m_R(0, 0);
		m_modelViewMatGL[1] = m_R(1, 0);
		m_modelViewMatGL[2] = m_R(2, 0);
		m_modelViewMatGL[3] = 0;
		m_modelViewMatGL[4] = m_R(0, 1);
		m_modelViewMatGL[5] = m_R(1, 1);
		m_modelViewMatGL[6] = m_R(2, 1);
		m_modelViewMatGL[7] = 0;
		m_modelViewMatGL[8] = m_R(0, 2);
		m_modelViewMatGL[9] = m_R(1, 2);
		m_modelViewMatGL[10] = m_R(2, 2);
		m_modelViewMatGL[11] = 0;
		m_modelViewMatGL[12] = m_R(0, 0); //4th col
		m_modelViewMatGL[13] = m_R(1, 0);
		m_modelViewMatGL[14] = m_R(2, 0);
		m_modelViewMatGL[15] = 1;

		// Projector matrix
		GLfloat m_projMatGL[16];
		build_opengl_projection_for_intrinsics(m_projMatGL, fx, fy, skew, u0, v0, width, height, near_clip, far_clip);

		Mat_<double> ProjMat(4, 4);
		Mat_<double> viewModelMat(4, 4);

		double *ProjMatPtr = (double *)ProjMat.data;
		double *viewModelMatPtr = (double *)viewModelMat.data;
		for (int i = 0; i < 16; ++i)
		{
			ProjMatPtr[i] = m_projMatGL[i];
			viewModelMatPtr[i] = m_modelViewMatGL[i];
		}
		Mat_<double> mvp = ProjMat.t() * viewModelMat.t();
		mvp = mvp.t();

		// ************************
		// Han's Code End
		// ************************

		// Ortho
		// camera->setProjectionMatrixAsOrtho(-100,100,-60,60,0.01,100);
		osg::Matrixd osgProjMat;
		osg::Matrixd osgViewModelMat;

		// OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
		osgProjMat(0, 0) = m_projMatGL[0];
		osgProjMat(0, 1) = m_projMatGL[1];
		osgProjMat(0, 2) = m_projMatGL[2];
		osgProjMat(0, 3) = m_projMatGL[3];
		osgProjMat(1, 0) = m_projMatGL[4];
		osgProjMat(1, 1) = m_projMatGL[5];
		osgProjMat(1, 2) = m_projMatGL[6];
		osgProjMat(1, 3) = m_projMatGL[7];
		osgProjMat(2, 0) = m_projMatGL[8];
		osgProjMat(2, 1) = m_projMatGL[9];
		osgProjMat(2, 2) = m_projMatGL[10];
		osgProjMat(2, 3) = m_projMatGL[11];
		osgProjMat(3, 0) = m_projMatGL[12];
		osgProjMat(3, 1) = m_projMatGL[13];
		osgProjMat(3, 2) = m_projMatGL[14];
		osgProjMat(3, 3) = m_projMatGL[15]; // T

		osgViewModelMat(0, 0) = m_modelViewMatGL[0];
		osgViewModelMat(0, 1) = m_modelViewMatGL[1];
		osgViewModelMat(0, 2) = m_modelViewMatGL[2];
		osgViewModelMat(0, 3) = m_modelViewMatGL[3];
		osgViewModelMat(1, 0) = m_modelViewMatGL[4];
		osgViewModelMat(1, 1) = m_modelViewMatGL[5];
		osgViewModelMat(1, 2) = m_modelViewMatGL[6];
		osgViewModelMat(1, 3) = m_modelViewMatGL[7];
		osgViewModelMat(2, 0) = m_modelViewMatGL[8];
		osgViewModelMat(2, 1) = m_modelViewMatGL[9];
		osgViewModelMat(2, 2) = m_modelViewMatGL[10];
		osgViewModelMat(2, 3) = m_modelViewMatGL[11];
		osgViewModelMat(3, 0) = m_modelViewMatGL[12];
		osgViewModelMat(3, 1) = m_modelViewMatGL[13];
		osgViewModelMat(3, 2) = m_modelViewMatGL[14];
		osgViewModelMat(3, 3) = m_modelViewMatGL[15]; // T

		// camera->setProjectionMatrix(osgProjMat);
		// camera->setViewMatrix(osg::Matrixd::inverse(osgViewModelMat));

		// Project matrix
		//uncomment the next line in order to tell OSG to use the calculated near and far values instead of OSG's auto-calculation
		//OSG's auto-calculation seems better...Also the next line somehow causes the depth buffer to be ~0
		//	viewer->getCamera()->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);
		//	viewer->getCamera()->setComputeNearFarMode(osg::CullSettings::COMPUTE_NEAR_FAR_USING_PRIMITIVES);
		osg::Matrix opm(osgProjMat);
		camera->setProjectionMatrix(opm);
		CCamUtil::getInstance()->printMatrix(osgProjMat, "osgProjMat");

		// Model view matrix
		osg::Matrix mvm(osgViewModelMat);
		CCamUtil::getInstance()->printMatrix(mvm, "modelview");

		osg::Matrix mm(getModelMatrix(CCamUtil::getInstance()->_viewer));
		osg::Matrix mmi(osg::Matrix::inverse(mm));
		osg::Matrix oviewm(mmi * mvm);
		CCamUtil::getInstance()->printMatrix(oviewm, "Caculated View");
		// validateModelView(CCamUtil::getInstance()->_viewer);
		camera->setViewMatrix(oviewm);

		// Camera cent
		CCamUtil::getInstance()->printVec(osgCent, "Cent");
	}
}

// Set slave viewer
void CCamera::setSlaveViewerShader(osg::ref_ptr<osg::Camera> camera, int width, int height)
{

	if (isValidCam() && camera)
	{
		// Data
		Eigen::Matrix3f m_R = matR.transpose(); //@TODO

		double fx = matM(0, 0);
		double fy = matM(1, 1);
		double skew = matM(0, 1);
		double u0 = matM(0, 2);
		double v0 = matM(1, 2);
		int imageWidth = width;
		int imageHeight = height;
		double near_clip = 0.001;
		double far_clip = 1000;

		cout << "fx:" << fx << " fy:" << fy << " skew:" << skew << " u0:" << u0 << " v0:" << v0 << " width:" << imageWidth << " height:" << imageHeight << " near_clip:" << near_clip << " far_clip:" << far_clip << endl;

		// ************************
		// Han's Code Start
		// ************************

		// View matrix

		GLfloat m_modelViewMatGL[16];
		m_modelViewMatGL[0] = m_R(0, 0);
		m_modelViewMatGL[1] = m_R(1, 0);
		m_modelViewMatGL[2] = m_R(2, 0);
		m_modelViewMatGL[3] = 0;
		m_modelViewMatGL[4] = m_R(0, 1);
		m_modelViewMatGL[5] = m_R(1, 1);
		m_modelViewMatGL[6] = m_R(2, 1);
		m_modelViewMatGL[7] = 0;
		m_modelViewMatGL[8] = m_R(0, 2);
		m_modelViewMatGL[9] = m_R(1, 2);
		m_modelViewMatGL[10] = m_R(2, 2);
		m_modelViewMatGL[11] = 0;
		m_modelViewMatGL[12] = m_R(0, 0); //4th col
		m_modelViewMatGL[13] = m_R(1, 0);
		m_modelViewMatGL[14] = m_R(2, 0);
		m_modelViewMatGL[15] = 1;

		// Projector matrix
		GLfloat m_projMatGL[16];
		build_opengl_projection_for_intrinsics(m_projMatGL, fx, fy, skew, u0, v0, width, height, near_clip, far_clip);

		Mat_<double> ProjMat(4, 4);
		Mat_<double> viewModelMat(4, 4);

		double *ProjMatPtr = (double *)ProjMat.data;
		double *viewModelMatPtr = (double *)viewModelMat.data;
		for (int i = 0; i < 16; ++i)
		{
			ProjMatPtr[i] = m_projMatGL[i];
			viewModelMatPtr[i] = m_modelViewMatGL[i];
		}
		Mat_<double> mvp = ProjMat.t() * viewModelMat.t();
		mvp = mvp.t();

		// ************************
		// Han's Code End
		// ************************

		// Ortho
		// camera->setProjectionMatrixAsOrtho();
		osg::Matrixd osgProjMat;
		osg::Matrixd osgViewModelMat;

		// OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
		osgProjMat(0, 0) = m_projMatGL[0];
		osgProjMat(0, 1) = m_projMatGL[1];
		osgProjMat(0, 2) = m_projMatGL[2];
		osgProjMat(0, 3) = m_projMatGL[3];
		osgProjMat(1, 0) = m_projMatGL[4];
		osgProjMat(1, 1) = m_projMatGL[5];
		osgProjMat(1, 2) = m_projMatGL[6];
		osgProjMat(1, 3) = m_projMatGL[7];
		osgProjMat(2, 0) = m_projMatGL[8];
		osgProjMat(2, 1) = m_projMatGL[9];
		osgProjMat(2, 2) = m_projMatGL[10];
		osgProjMat(2, 3) = m_projMatGL[11];
		osgProjMat(3, 0) = m_projMatGL[12];
		osgProjMat(3, 1) = m_projMatGL[13];
		osgProjMat(3, 2) = m_projMatGL[14];
		osgProjMat(3, 3) = m_projMatGL[15]; // T

		osgViewModelMat(0, 0) = m_modelViewMatGL[0];
		osgViewModelMat(0, 1) = m_modelViewMatGL[1];
		osgViewModelMat(0, 2) = m_modelViewMatGL[2];
		osgViewModelMat(0, 3) = m_modelViewMatGL[3];
		osgViewModelMat(1, 0) = m_modelViewMatGL[4];
		osgViewModelMat(1, 1) = m_modelViewMatGL[5];
		osgViewModelMat(1, 2) = m_modelViewMatGL[6];
		osgViewModelMat(1, 3) = m_modelViewMatGL[7];
		osgViewModelMat(2, 0) = m_modelViewMatGL[8];
		osgViewModelMat(2, 1) = m_modelViewMatGL[9];
		osgViewModelMat(2, 2) = m_modelViewMatGL[10];
		osgViewModelMat(2, 3) = m_modelViewMatGL[11];
		osgViewModelMat(3, 0) = m_modelViewMatGL[12];
		osgViewModelMat(3, 1) = m_modelViewMatGL[13];
		osgViewModelMat(3, 2) = m_modelViewMatGL[14];
		osgViewModelMat(3, 3) = m_modelViewMatGL[15]; // T

		camera->setProjectionMatrix(osgProjMat);
		camera->setViewMatrix(osg::Matrixd::inverse(osgViewModelMat));
	}
}

void CCamera::SettingModelViewMatrixGL() //to visualize in opengl
{
#if 0
	//ModelViewMat
	//colwise
	m_modelViewMatGL[0] = m_R.at<double>(0, 0);
	m_modelViewMatGL[1] = m_R.at<double>(1, 0);
	m_modelViewMatGL[2] = m_R.at<double>(2, 0);
	m_modelViewMatGL[3] = 0;
	m_modelViewMatGL[4] = m_R.at<double>(0, 1);
	m_modelViewMatGL[5] = m_R.at<double>(1, 1);
	m_modelViewMatGL[6] = m_R.at<double>(2, 1);
	m_modelViewMatGL[7] = 0;
	m_modelViewMatGL[8] = m_R.at<double>(0, 2);
	m_modelViewMatGL[9] = m_R.at<double>(1, 2);
	m_modelViewMatGL[10] = m_R.at<double>(2, 2);
	m_modelViewMatGL[11] = 0;
	m_modelViewMatGL[12] = m_t.at<double>(0, 0); //4th col
	m_modelViewMatGL[13] = m_t.at<double>(1, 0);
	m_modelViewMatGL[14] = m_t.at<double>(2, 0);
	m_modelViewMatGL[15] = 1;


	if (m_heightExpected == 0)
	{
		printf("## ERROR: SettingModelViewMatrixGL:: sensortype is not defined\n");
		return;
	}
	int imageWidth = m_widthExpected;
	int imageHeight = m_heightExpected;
	/*if(m_actualPanelIdx==0 || m_actualPanelIdx==50)
	{
	imageWidth =1920;
	imageHeight=1080;
	}
	else
	{
	imageWidth =640;
	imageHeight= 480;
	}*/
	//printMatrix("test",m_K);
	/*build_opengl_projection_for_intrinsics(m_frustumGL,m_viewPortGL
	,m_K.at<double>(0,0),m_K.at<double>(1,1),m_K.at<double>(0,1),m_K.at<double>(0,2),m_K.at<double>(1,2)
	,imageWidth,imageHeight,10,100);*/
	build_opengl_projection_for_intrinsics(m_projMatGL
		, m_K.at<double>(0, 0), m_K.at<double>(1, 1), m_K.at<double>(0, 1), m_K.at<double>(0, 2), m_K.at<double>(1, 2)
		, imageWidth, imageHeight, g_nearPlaneForDepthRender, g_farPlaneForDepthRender);

	Mat_<double> ProjMat(4, 4);
	Mat_<double> viewModelMat(4, 4);
	double*  ProjMatPtr = (double*)ProjMat.data;
	double*  viewModelMatPtr = (double*)viewModelMat.data;
	for (int i = 0; i<16; ++i)
	{
		ProjMatPtr[i] = m_projMatGL[i];
		viewModelMatPtr[i] = m_modelViewMatGL[i];
	}
	Mat_<double> mvp = ProjMat.t()*viewModelMat.t();
	mvp = mvp.t();

	/*
	//Simple test
	Mat_<double> test(4,1);
	test(0,0) = 0;
	test(1,0) = 0;
	test(2,0) = g_farPlaneForDepthRender;
	test(3,0) = 1;
	test = ProjMat.t() * test;
	test = test / test(3,0);
	printf("tset: %f, %f, %f\n",test(0,0),test(1,0),test(2,0));*/

	double* mvpPtr = (double*)mvp.data;
	for (int i = 0; i<16; ++i)
	{
		m_mvpMatGL[i] = mvpPtr[i];

	}

#endif
}

void CCamera::build_opengl_projection_for_intrinsics(GLfloat *frustum, double fx, double fy, double skew, double u0, double v0, int width, int height, double near_clip, double far_clip)
{
#if 1
	double l = 0.0, r = 1.0 * width, b = 0.0, t = 1.0 * height;
	double tx = -(r + l) / (r - l), ty = -(t + b) / (t - b), tz = -(far_clip + near_clip) / (far_clip - near_clip);

	double ortho[16] = {2.0 / (r - l), 0.0, 0.0, tx,
						0.0, 2.0 / (t - b), 0.0, ty,
						0.0, 0.0, -2.0 / (far_clip - near_clip), tz,
						0.0, 0.0, 0.0, 1.0};

	double Intrinsic[16] = {fx, skew, u0, 0.0,
							0.0, fy, v0, 0.0,
							0.0, 0.0, -(near_clip + far_clip), +near_clip * far_clip,
							//0.0, 0.0, -(near_clip+far_clip)/2.0 , 0.0,
							0.0, 0.0, 1.0, 0.0};

	Mat orthoMat = Mat(4, 4, CV_64F, ortho);
	Mat IntrinsicMat = Mat(4, 4, CV_64F, Intrinsic);
	Mat Frustrum = orthoMat * IntrinsicMat;
	Frustrum = Frustrum.t();

	double *data = (double *)Frustrum.data;
	for (int i = 0; i < 16; ++i)
	{
		frustum[i] = data[i];
	}
#endif
}

//the viewMatrix in osg doesn't hold the final model view matrix as in OpenGL.
//need to add to osg's viewMatrix other transformation which are done on graph.
//not so easy to calculate...
//in validateModelView we verify that it is indeed correct
//Actually, if using FlattenMatrixTransformsVisitor and SetMatrixTransformsToIdentityVisitor then the modelmatrix
//is just the center translation (from centerModel)
osg::Matrix CCamera::getModelMatrix(osg::ref_ptr<osgViewer::Viewer> viewer)
{
	osg::Matrix mm;
	//should probably traverse all children
	osg::Node *node = viewer->getSceneData();
	if (NULL != node)
	{
		osg::Group *gr;
		do
		{
			gr = node->asGroup();
			if (gr != NULL && gr->getNumChildren() > 0)
			{
				node = gr->getChild(0);
			}

		} while (NULL != gr && gr->getNumChildren() > 0);
		if (NULL != node)
		{
			osg::MatrixList ml = node->getWorldMatrices();
			for (osg::MatrixList::const_iterator mli = ml.begin(); mli != ml.end(); mli++)
			{
				mm = mm * (*mli);
			}
		}
	}

	return mm;
}

// verifies that OpenGL modelview matrix is equal to OSG's (since we manually calculate OSG's model matrix)
void CCamera::validateModelView(osg::ref_ptr<osgViewer::Viewer> viewer)
{
	const unsigned int QUADLET = 4;

	osg::Matrix vm = viewer->getCamera()->getViewMatrix();
	osg::Matrix mm = getModelMatrix(viewer);
	osg::Matrix mvm(mm * vm);
	GLdouble OGLmvm[QUADLET * QUADLET];
	glGetDoublev(GL_MODELVIEW_MATRIX, OGLmvm);
	osg::Matrix::value_type *OSGmvm = mvm.ptr();
	osg::Matrix::value_type MAX_MVM_DIFF_ERR = 0.3;
	for (unsigned int i = 0; i < QUADLET * QUADLET; i++)
	{
		if (fabs(OSGmvm[i] - OGLmvm[i]) > MAX_MVM_DIFF_ERR)
		{
			cout << "Error: OGL modelview is different than OSG's. Either OGL modelview is the identity matrix or could not calculate the model transformations matrix." << endl;
			throw "Error: OGL modelview is different than OSG's. Either OGL modelview is the identity matrix or could not calculate the model transformations matrix.";
		}
	}
}
//*********************************************
//				VGA
//	Panel Index: 1~20
//  Camera Index: 1~24
//*********************************************

CCamVGA::CCamVGA(string _calibPath, int _panelIdx, int _camIdx)
{
	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing VGA camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamVGA::drawVGACam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawVGACam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(VGA_NODE_MASK);

	// Start draw
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeCam = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeLabel = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeAxis = new osg::PositionAttitudeTransform;

	drawCamera(nodeCam);
	drawCamLabel(nodeLabel);
	drawCamAxis(nodeAxis);

	// nodeCam->setNodeMask(VGA_NODE_MASK);
	// nodeLabel->setNodeMask(VGA_LABEL_MASK);
	// nodeAxis->setNodeMask(VGA_AXIS_MASK);
	nodeCam->setNodeMask(0x0);
	nodeLabel->setNodeMask(0x0);
	nodeAxis->setNodeMask(0x0);

	nodeCam->setName("VGA_NODE");
	nodeLabel->setName("VGA_LABEL");
	nodeAxis->setName("VGA_AXIS");

	geoCam->addChild(nodeCam);
	geoCam->addChild(nodeLabel);
	geoCam->addChild(nodeAxis);

	// Rotation + tranlation + scale
	double s = 0.1;

	geoCam->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)) * osgQ.inverse());
	geoCam->setScale(osg::Vec3(s, s, s));
	geoCam->setPosition(osgCent);

	return true;
}

osg::ref_ptr<osg::Camera> CCamVGA::createVirtualCam()
{
	osg::ref_ptr<osg::Camera> renderCam;

	//renderCam->setProjectionMatrix();
	//renderCam->setClearColor();
	//renderCam->setCullMask();

	return renderCam;
}

//*********************************************
//				HD
// Camera Index: 0~30
//*********************************************

CCamHD::CCamHD(string _calibPath, int _camIdx)
{

	int _panelIdx = HD_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing HD camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamHD::drawHDCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawHDCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(HD_NODE_MASK);

	// Start draw
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeCam = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeLabel = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeAxis = new osg::PositionAttitudeTransform;

	drawCamera(nodeCam);
	drawCamLabel(nodeLabel);
	drawCamAxis(nodeAxis);

	// nodeCam->setNodeMask(HD_NODE_MASK);
	// nodeLabel->setNodeMask(HD_LABEL_MASK);
	// nodeAxis->setNodeMask(HD_AXIS_MASK);
	nodeCam->setNodeMask(0x0);
	nodeLabel->setNodeMask(0x0);
	nodeAxis->setNodeMask(0x0);

	nodeCam->setName("HD_NODE");
	nodeLabel->setName("HD_LABEL");
	nodeAxis->setName("HD_AXIS");

	geoCam->addChild(nodeCam);
	geoCam->addChild(nodeLabel);
	geoCam->addChild(nodeAxis);

	// Rotation + tranlation + scale
	double s = 0.1;

	geoCam->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)) * osgQ.inverse());
	geoCam->setScale(osg::Vec3(s, s, s));
	geoCam->setPosition(osgCent);

	return true;
}

//*********************************************
//				Kinect
// Camera Index: 1~10
//*********************************************

CCamKinect::CCamKinect(string _calibPath, int _camIdx)
{
	int _panelIdx = Kinect_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing Kinect camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamKinect::drawKinectCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawKinectCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(Kinect_NODE_MASK);

	// Start draw
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeCam = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeLabel = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeAxis = new osg::PositionAttitudeTransform;

	drawCamera(nodeCam);
	drawCamLabel(nodeLabel);
	drawCamAxis(nodeAxis);

	// nodeCam->setNodeMask(Kinect_NODE_MASK);
	// nodeLabel->setNodeMask(Kinect_LABEL_MASK);
	// nodeAxis->setNodeMask(Kinect_AXIS_MASK);
	nodeCam->setNodeMask(0x0);
	nodeLabel->setNodeMask(0x0);
	nodeAxis->setNodeMask(0x0);

	nodeCam->setName("Kinect_NODE");
	nodeLabel->setName("Kinect_LABEL");
	nodeAxis->setName("Kinect_AXIS");

	geoCam->addChild(nodeCam);
	geoCam->addChild(nodeLabel);
	geoCam->addChild(nodeAxis);

	// Rotation + tranlation + scale
	double s = 0.1;

	geoCam->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)) * osgQ.inverse());
	geoCam->setScale(osg::Vec3(s, s, s));
	geoCam->setPosition(osgCent);

	return true;
}

//*********************************************
//				Projctor
// Camera Index: 0~4
//*********************************************

CCamProj::CCamProj(string _calibPath, int _camIdx)
{
	int _panelIdx = Proj_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing Projctor camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamProj::drawProjCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawProjCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(Proj_NODE_MASK);

	// Start draw
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeCam = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeLabel = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::PositionAttitudeTransform> nodeAxis = new osg::PositionAttitudeTransform;

	drawCamera(nodeCam);
	drawCamLabel(nodeLabel);
	drawCamAxis(nodeAxis);

	//nodeCam->setNodeMask(Proj_NODE_MASK);
	//nodeLabel->setNodeMask(Proj_LABEL_MASK);
	//nodeAxis->setNodeMask(Proj_AXIS_MASK);
	nodeCam->setNodeMask(0x0);
	nodeLabel->setNodeMask(0x0);
	nodeAxis->setNodeMask(0x0);

	nodeCam->setName("Proj_NODE");
	nodeLabel->setName("Proj_LABEL");
	nodeAxis->setName("Proj_AXIS");

	geoCam->addChild(nodeCam);
	geoCam->addChild(nodeLabel);
	geoCam->addChild(nodeAxis);

	// Rotation + tranlation + scale
	double s = 0.1;

	geoCam->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)) * osgQ.inverse());
	geoCam->setScale(osg::Vec3(s, s, s));
	geoCam->setPosition(osgCent);
	return true;
}

//*********************************************
//				Util
//*********************************************
osg::ref_ptr<CCamUtil> CCamUtil::instance = NULL;

osg::ref_ptr<CCamUtil> CCamUtil::getInstance(void)
{
	if (!instance)
		instance = new CCamUtil();

	return instance;
}

CCamUtil::CCamUtil()
{
	visCam[4] = false;
	visCamAxis[4] = false;
	visCamIndex[4] = false;

	preVisCam[4] = false;
	preVisCamAxis[4] = false;
	preVisCamIndex[4] = false;

	rendering2Img = false;

	Calib_Project_Intervel = 5;
	Calib_PROJ_0_IMG = "./render/calib_parten/p0.jpg";
	Calib_PROJ_1_IMG = "./render/calib_parten/p1.jpg";
	Calib_PROJ_2_IMG = "./render/calib_parten/p2.jpg";
	Calib_PROJ_3_IMG = "./render/calib_parten/p3.jpg";
	Calib_PROJ_4_IMG = "./render/calib_parten/p4.jpg";
}

// VGA initialize
void CCamUtil::initialVGA(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{
	vgaGroup = new osg::Group;

	for (int panelIdx = 1; panelIdx <= 20; panelIdx++)
	{												 //20
		for (int camIdx = 1; camIdx <= 24; camIdx++) //24
		{
			//cout << "VGA Camera: " << panelIdx << "," << camIdx << endl;

			CCamVGA *camVGA = new CCamVGA(CALIB_FILE_PATH, panelIdx, camIdx);
			camVGAList.push_back(camVGA);

			// Draw Camera
			osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
			if (camVGA->drawVGACam(node))
				vgaGroup->addChild(node);

			// Image size
			camVGA->imgWidth = VGA_WIDTH;
			camVGA->imgHeight = VGA_HEIGHT;

			camVGAGeoList.push_back(node);
		}
	}
	//vgaGroup->setNodeMask(VGA_NODE_MASK);

	scene->addChild(vgaGroup);
}

//HD initialize
void CCamUtil::initialHD(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{
	hdGroup = new osg::Group;

	for (int camIdx = 0; camIdx <= 30; camIdx++) // Note: 0~30 not 1~31
	{
		//cout << "HD Camera: " << camIdx << endl;

		CCamHD *camHD = new CCamHD(CALIB_FILE_PATH, camIdx);
		camHDList.push_back(camHD);

		osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
		if (camHD->drawHDCam(node))
			hdGroup->addChild(node);

		// Image size
		camHD->imgWidth = HD_WIDTH;
		camHD->imgHeight = HD_HEIGHT;

		camHDGeoList.push_back(node);
	};

	//hdGroup->setNodeMask(HD_NODE_MASK);
	scene->addChild(hdGroup);
}

//Kinect initialize
void CCamUtil::initialKinect(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{
	kinectGroup = new osg::Group;

	for (int camIdx = 1; camIdx <= 10; camIdx++) // Note: 1~10
	{
		//cout << "Kinect Camera: "<< camIdx << endl;

		CCamKinect *camKinect = new CCamKinect(CALIB_FILE_PATH, camIdx);
		camKinectList.push_back(camKinect);

		osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
		if (camKinect->drawKinectCam(node))
			kinectGroup->addChild(node);

		// Image size
		camKinect->imgWidth = Kinect_WIDTH;
		camKinect->imgHeight = Kinect_HEIGHT;

		camKinectGeoList.push_back(node);
	};

	//kinectGroup->setNodeMask(Kinect_NODE_MASK);
	scene->addChild(kinectGroup);
}

// Projector
void CCamUtil::initialProj(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{
	projGroup = new osg::Group;

	for (int camIdx = 0; camIdx <= 4; camIdx++) // Note: 0~4
	{
		//cout << "Projector Camera: "<< camIdx << endl;

		CCamProj *camProj = new CCamProj(CALIB_FILE_PATH, camIdx);
		camProjList.push_back(camProj);

		osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
		if (camProj->drawProjCam(node))
			projGroup->addChild(node);

		// Image size
		camProj->imgWidth = Projector_WIDTH;
		camProj->imgHeight = Projector_HEIGHT;

		camProjGeoList.push_back(node);
	};

	//projGroup->setNodeMask(Proj_NODE_MASK);
	scene->addChild(projGroup);
}

// VGA get instance by index
CCamVGA *CCamUtil::getVGAInstance(int panelIdx, int camIdx)
{
	if (camIdx <= 0 || camIdx > 24) //VGA camIdx: 1~24
	{
		cout << "Index error" << endl;
		return NULL;
	}

	if (panelIdx <= 0 || panelIdx > 20) //VGA panelIdx: 1~20
	{
		cout << "Index error" << endl;
		return NULL;
	}

	for (std::vector<CCamVGA *>::iterator it = camVGAList.begin(); it != camVGAList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// HD get instance by index
CCamHD *CCamUtil::getHDInstance(int panelIdx, int camIdx)
{
	if (camIdx < 0 || camIdx >= 31) //HD camIdx: 0~30
	{
		cout << "Index error" << endl;
		return NULL;
	}

	for (std::vector<CCamHD *>::iterator it = camHDList.begin(); it != camHDList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// Kinect get instance by index
CCamKinect *CCamUtil::getKinectInstance(int panelIdx, int camIdx)
{
	if (camIdx <= 0 || camIdx > 10) //Kinect camIdx: 1~10
	{
		cout << "Index error" << endl;
		return NULL;
	}
	for (std::vector<CCamKinect *>::iterator it = camKinectList.begin(); it != camKinectList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// Projector get instance by index
CCamProj *CCamUtil::getProjInstance(int panelIdx, int camIdx)
{
	if (camIdx < 0 || camIdx > 4) //Kinect camIdx: 0~4
	{
		cout << "Index error" << endl;
		return NULL;
	}

	for (std::vector<CCamProj *>::iterator it = camProjList.begin(); it != camProjList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

void CCamUtil::calcDomeCent()
{
	osg::Vec3 cent(0.0, 0.0, 0.0);
	int count = 0;

	for (int panelIdx = 1; panelIdx <= 20; panelIdx++)
		for (int camIdx = 1; camIdx < 24; camIdx++)
		{
			CCamVGA *camVGA = getVGAInstance(panelIdx, camIdx);
			if (camVGA && camVGA->isValidCam())
			{
				cent += camVGA->osgCent;
				count++;
			}
		}

	if (count)
		domeCent = cent / count;
	else
		domeCent = osg::Vec3(0.0, 0.0, 0.0);

	cout << "Dome cent: " << domeCent.x() << "," << domeCent.y() << "," << domeCent.z() << endl;
}

osg::ref_ptr<osg::Camera> CCamUtil::createHDSlaveViewer_SUCCESS(int panelIdx, int camIdx)
{
	CCamHD *hdCam = getHDInstance(panelIdx, camIdx);

	if (hdCam && hdCam->isValidCam())
	{

		int xoffset = 0;
		int yoffset = 0;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = HD_WIDTH;
		traits->height = HD_HEIGHT;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());
		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		camera->setProjectionResizePolicy(osg::Camera::HORIZONTAL);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the projection matrix
		osg::Matrixd proj = hdCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.001, 100);
		camera->setProjectionMatrix(proj);

		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);

		// Method 2
		osg::Matrixd cameraRotation = osg::Matrixd::rotate(osg::Quat(osg::PI, osg::Vec3(1, 0, 0)) * hdCam->osgQ.inverse()); //*osg::Quat(osg::PI_2, osg::Vec3(0, 0, 1))*
		osg::Matrixd cameraTrans = osg::Matrixd::translate(hdCam->osgCent);
		camera->setViewMatrix(osg::Matrixd::inverse(cameraRotation * cameraTrans));

		// MVP
		//viewer->stopThreading();
		//viewer->addSlave(camera.get());
		//viewer->removeSlave(viewer->findSlaveIndexForCamera(camera.get()));
		//viewer->startThreading();

		return camera;
	}

	return NULL;
}

osg::ref_ptr<osg::Camera> CCamUtil::getSetOrCreateSlaveViewer(int panelIdx, int camIdx)
{
	if (panelIdx == HD_PANEL_INDEX)
	{
		CCamHD *hdCam = getHDInstance(panelIdx, camIdx);

		if (hdCam && hdCam->isValidCam())
		{
			if (mainSlave == NULL)
			{
				cout << "Create slave from HD" << endl;
				mainSlave = new osg::Camera;

				//hdCam->createSlaveViewerShader(mainSlave, HD_WIDTH, HD_HEIGHT);
				hdCam->createSlaveViewer(mainSlave, HD_WIDTH, HD_HEIGHT);
				updateMVPMatrix(hdCam);

				// @TODO Shader
				osg::ref_ptr<osg::Shader> _vShader = new osg::Shader(osg::Shader::VERTEX, vertexShader);
				osg::ref_ptr<osg::Shader> _fShader = new osg::Shader(osg::Shader::FRAGMENT, fragShader);

				osg::ref_ptr<osg::Program> _program = new osg::Program;
				_program->addShader(_vShader.get());
				_program->addShader(_fShader.get());

				_slaveMVPUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "matMVP");
				_slaveMVPUniform->setUpdateCallback(new UniformUpdate(&_slaveMVPMat));

				_slaveModelViewUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "matModelView");
				_slaveModelViewUniform->setUpdateCallback(new UniformUpdate(&shaderViewMat));

				osg::ref_ptr<osg::Uniform> lightPosUniform = new osg::Uniform("lightPosition", osg::Vec3());
				//lightPosUniform->setUpdateCallback(new UniformUpdate(&shaderLightPos));
				//lightPosUniform->setUpdateCallback(new UniformUpdate(&(hdCam->osgCent)));
				lightPosUniform->setUpdateCallback(new CLightPosCallback);

				osg::ref_ptr<osg::StateSet> _stateSet = sceneNode->getOrCreateStateSet();
				_stateSet->addUniform(_slaveMVPUniform.get());
				_stateSet->addUniform(lightPosUniform.get());
				_stateSet->addUniform(new osg::Uniform("lightDiffuse", osg::Vec4(0.8f, 0.8f, 0.8f, 1.0f)));
				_stateSet->addUniform(new osg::Uniform("lightSpecular", osg::Vec4(1.0f, 1.0f, 0.4f, 1.0f)));
				_stateSet->addUniform(new osg::Uniform("shininess", 64.0f));

				_stateSet->setAttributeAndModes(_program.get(), osg::StateAttribute::ON);
			}
			else
			{
				//hdCam->setSlaveViewerShader(mainSlave, HD_WIDTH, HD_HEIGHT);
				hdCam->setSlaveViewer(mainSlave, HD_WIDTH, HD_HEIGHT);
				updateMVPMatrix(hdCam);
				cout << "Set slave from HD" << endl;
			}
			return mainSlave;
		}
		return NULL;
	}
	else if (panelIdx >= 1 && panelIdx <= 20) // VGA
	{
		CCamVGA *vgaCam = getVGAInstance(panelIdx, camIdx);
		if (vgaCam && vgaCam->isValidCam())
		{
			if (vgaSlave == NULL)
			{
				cout << "Create slave from VGA" << endl;
				vgaSlave = new osg::Camera;

				//hdCam->createSlaveViewerShader(mainSlave, HD_WIDTH, HD_HEIGHT);
				vgaCam->createSlaveViewer(vgaSlave, VGA_WIDTH, VGA_HEIGHT);
				updateMVPMatrix(vgaCam);

				// @TODO Shader
				osg::ref_ptr<osg::Shader> _vShader = new osg::Shader(osg::Shader::VERTEX, vertexShader);
				osg::ref_ptr<osg::Shader> _fShader = new osg::Shader(osg::Shader::FRAGMENT, fragShader);

				osg::ref_ptr<osg::Program> _program = new osg::Program;
				_program->addShader(_vShader.get());
				_program->addShader(_fShader.get());

				_slaveMVPUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "matMVP");
				_slaveMVPUniform->setUpdateCallback(new UniformUpdate(&_slaveMVPMat));

				_slaveModelViewUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "matModelView");
				_slaveModelViewUniform->setUpdateCallback(new UniformUpdate(&shaderViewMat));

				osg::ref_ptr<osg::Uniform> lightPosUniform = new osg::Uniform("lightPosition", osg::Vec3());
				//lightPosUniform->setUpdateCallback(new UniformUpdate(&shaderLightPos));
				//lightPosUniform->setUpdateCallback(new UniformUpdate(&(hdCam->osgCent)));
				lightPosUniform->setUpdateCallback(new CLightPosCallback);

				osg::ref_ptr<osg::StateSet> _stateSet = sceneNode->getOrCreateStateSet();
				_stateSet->addUniform(_slaveMVPUniform.get());
				_stateSet->addUniform(lightPosUniform.get());
				_stateSet->addUniform(new osg::Uniform("lightDiffuse", osg::Vec4(0.8f, 0.8f, 0.8f, 1.0f)));
				_stateSet->addUniform(new osg::Uniform("lightSpecular", osg::Vec4(1.0f, 1.0f, 0.4f, 1.0f)));
				_stateSet->addUniform(new osg::Uniform("shininess", 64.0f));

				_stateSet->setAttributeAndModes(_program.get(), osg::StateAttribute::ON);
			}
			else
			{
				vgaCam->setSlaveViewer(vgaSlave, VGA_WIDTH, VGA_HEIGHT);
				updateMVPMatrix(vgaCam);
				cout << "Set slave from VGA" << endl;
			}
			return vgaSlave;
		}
		return NULL;
	}
	else if (panelIdx == Proj_PANEL_INDEX)
	{
		CCamProj *projCam = getProjInstance(panelIdx, camIdx);

		if (projCam && projCam->isValidCam())
		{
			if (projSlave[camIdx] == NULL)
			{
				cout << "Create slave from Projector" << endl;
				projSlave[camIdx] = new osg::Camera;

				int xOffset = Monitor_WIDTH + camIdx * Projector_WIDTH;
				projCam->createSlaveViewer(projSlave[camIdx], Projector_WIDTH, Projector_HEIGHT, xOffset, 0);
			}
			else
			{
				cout << "Set slave from Projector" << endl;
				projCam->setSlaveViewer(projSlave[camIdx], Projector_WIDTH, Projector_HEIGHT);
			}

			return projSlave[camIdx];
		}
	}
	else if (panelIdx == Kinect_PANEL_INDEX)
	{
		CCamKinect *kinectCam = getKinectInstance(panelIdx, camIdx);
		if (kinectCam && kinectCam->isValidCam())
		{
			if (mainSlave == NULL)
			{
			}
			else
			{
			}
			return mainSlave;
		}
		return NULL;
	}

	return NULL;
}

//Print osg::Matrixd
void CCamUtil::printMatrix(osg::Matrixd mat, std::string name)
{
	std::cout << name << ": " << std::endl;
	for (unsigned int row = 0; row < 4; ++row)
	{
		for (unsigned int col = 0; col < 4; ++col)
		{
			std::cout << std::setw(10) << mat(row, col);
			if (col != 3)
			{
				std::cout << ", ";
			}
		}
		std::cout << std::endl;
	}
}

void CCamUtil::printVec(osg::Vec3 vec, std::string name)
{
	std::cout << name << ": " << vec.x() << "," << vec.y() << "," << vec.z() << std::endl;
}

void CCamUtil::printVec(osg::Quat vec, std::string name)
{
	std::cout << name << ": " << vec.x() << "," << vec.y() << "," << vec.z() << "," << vec.w() << std::endl;
}

// Draw camera
void CCamUtil::drawOrHideCamera(bool reDraw, int camType)
{

	// draw or hide
	if (reDraw)
	{
		switch (camType)
		{
		case VGA_NODE_MASK:
			setNamedNode("VGA_NODE", VGA_NODE_MASK, vgaGroup);
			break;
		case HD_NODE_MASK:
			setNamedNode("HD_NODE", HD_NODE_MASK, hdGroup);
			break;
		case Kinect_NODE_MASK:
			setNamedNode("Kinect_NODE", Kinect_NODE_MASK, kinectGroup);
			break;
		case Proj_NODE_MASK:
			setNamedNode("Proj_NODE", Proj_NODE_MASK, projGroup);
			break;
		default:
			setNamedNode("VGA_NODE", VGA_NODE_MASK, vgaGroup);
			setNamedNode("HD_NODE", HD_NODE_MASK, hdGroup);
			setNamedNode("Kinect_NODE", Kinect_NODE_MASK, kinectGroup);
			setNamedNode("Proj_NODE", Proj_NODE_MASK, projGroup);
			break;
		}
	}
	else
	{
		switch (camType)
		{
		case VGA_NODE_MASK:
			setNamedNode("VGA_NODE", 0x0, vgaGroup);
			break;
		case HD_NODE_MASK:
			setNamedNode("HD_NODE", 0x0, hdGroup);
			break;
		case Kinect_NODE_MASK:
			setNamedNode("Kinect_NODE", 0x0, kinectGroup);
			break;
		case Proj_NODE_MASK:
			setNamedNode("Proj_NODE", 0x0, projGroup);
			break;
		default:
			setNamedNode("VGA_NODE", 0x0, vgaGroup);
			setNamedNode("HD_NODE", 0x0, hdGroup);
			setNamedNode("Kinect_NODE", 0x0, kinectGroup);
			setNamedNode("Proj_NODE", 0x0, projGroup);
			break;
		}
	}
}

// Draw label
void CCamUtil::drawOrHideCamLabel(bool reDraw, int camType)
{
	// draw or hide
	if (reDraw)
	{
		switch (camType)
		{
		case VGA_NODE_MASK:
			setNamedNode("VGA_LABEL", VGA_LABEL_MASK, vgaGroup);
			break;
		case HD_NODE_MASK:
			setNamedNode("HD_LABEL", HD_LABEL_MASK, hdGroup);
			break;
		case Kinect_NODE_MASK:
			setNamedNode("Kinect_LABEL", Kinect_LABEL_MASK, kinectGroup);
			break;
		case Proj_NODE_MASK:
			setNamedNode("Proj_LABEL", Proj_LABEL_MASK, projGroup);
			break;
		default:
			setNamedNode("VGA_LABEL", VGA_LABEL_MASK, vgaGroup);
			setNamedNode("HD_LABEL", HD_LABEL_MASK, hdGroup);
			setNamedNode("Kinect_LABEL", Kinect_LABEL_MASK, kinectGroup);
			setNamedNode("Proj_LABEL", Proj_LABEL_MASK, projGroup);
			break;
		}
	}
	else
	{
		switch (camType)
		{
		case VGA_NODE_MASK:
			setNamedNode("VGA_LABEL", 0x0, vgaGroup);
			break;
		case HD_NODE_MASK:
			setNamedNode("HD_LABEL", 0x0, hdGroup);
			break;
		case Kinect_NODE_MASK:
			setNamedNode("Kinect_LABEL", 0x0, kinectGroup);
			break;
		case Proj_NODE_MASK:
			setNamedNode("Proj_LABEL", 0x0, projGroup);
			break;
		default:
			setNamedNode("VGA_LABEL", 0x0, vgaGroup);
			setNamedNode("HD_LABEL", 0x0, hdGroup);
			setNamedNode("Kinect_LABEL", 0x0, kinectGroup);
			setNamedNode("Proj_LABEL", 0x0, projGroup);
			break;
		}
	}
}

// Draw axis
void CCamUtil::drawOrHideCamAxis(bool reDraw, int camType)
{
	// draw or hide
	if (reDraw)
	{
		switch (camType)
		{
		case VGA_NODE_MASK:
			setNamedNode("VGA_AXIS", VGA_AXIS_MASK, vgaGroup);
			break;
		case HD_NODE_MASK:
			setNamedNode("HD_AXIS", HD_AXIS_MASK, hdGroup);
			break;
		case Kinect_NODE_MASK:
			setNamedNode("Kinect_AXIS", Kinect_AXIS_MASK, kinectGroup);
			break;
		case Proj_NODE_MASK:
			setNamedNode("Proj_AXIS", Proj_AXIS_MASK, projGroup);
			break;
		default:
			setNamedNode("VGA_AXIS", VGA_AXIS_MASK, vgaGroup);
			setNamedNode("HD_AXIS", HD_AXIS_MASK, hdGroup);
			setNamedNode("Kinect_AXIS", Kinect_AXIS_MASK, kinectGroup);
			setNamedNode("Proj_AXIS", Proj_AXIS_MASK, projGroup);
			break;
		}
	}
	else
	{
		switch (camType)
		{
		case VGA_NODE_MASK:
			setNamedNode("VGA_AXIS", 0x0, vgaGroup);
			break;
		case HD_NODE_MASK:
			setNamedNode("HD_AXIS", 0x0, hdGroup);
			break;
		case Kinect_NODE_MASK:
			setNamedNode("Kinect_AXIS", 0x0, kinectGroup);
			break;
		case Proj_NODE_MASK:
			setNamedNode("Proj_AXIS", 0x0, projGroup);
			break;
		default:
			setNamedNode("VGA_AXIS", 0x0, vgaGroup);
			setNamedNode("HD_AXIS", 0x0, hdGroup);
			setNamedNode("Kinect_AXIS", 0x0, kinectGroup);
			setNamedNode("Proj_AXIS", 0x0, projGroup);
			break;
		}
	}
}

osg::Node *CCamUtil::setNamedNode(const std::string &searchName, const int setNodeMask, osg::Node *currNode)
{
	osg::Group *currGroup;
	osg::Node *foundNode;

	// check to see if we have a valid (non-NULL) node.
	// if we do have a null node, return NULL.
	if (!currNode)
	{
		return NULL;
	}

	// We have a valid node, check to see if this is the node we
	// are looking for. If so, return the current node.
	if (currNode->getName() == searchName)
	{
		return currNode;
	}

	// We have a valid node, but not the one we are looking for.
	// Check to see if it has children (non-leaf node). If the node
	// has children, check each of the child nodes by recursive call.
	// If one of the recursive calls returns a non-null value we have
	// found the correct node, so return this node.
	// If we check all of the children and have not found the node,
	// return NULL
	currGroup = currNode->asGroup(); // returns NULL if not a group.
	if (currGroup)
	{
		for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
		{
			foundNode = setNamedNode(searchName, setNodeMask, currGroup->getChild(i));
			if (foundNode)							 // found a match!
				foundNode->setNodeMask(setNodeMask); // found a match!
		}
		return NULL; // We have checked each child node - no match found.
	}
	else
	{
		return NULL; // leaf node, no match
	}
}

osg::Node *CCamUtil::setNamedNode(const int searchNodeMask, const int setNodeMask, osg::Node *currNode)
{
	osg::Group *currGroup;
	osg::Node *foundNode;

	// check to see if we have a valid (non-NULL) node.
	// if we do have a null node, return NULL.
	if (!currNode)
	{
		return NULL;
	}

	// We have a valid node, check to see if this is the node we
	// are looking for. If so, return the current node.
	if (currNode->getNodeMask() == searchNodeMask)
	{
		return currNode;
	}

	// We have a valid node, but not the one we are looking for.
	// Check to see if it has children (non-leaf node). If the node
	// has children, check each of the child nodes by recursive call.
	// If one of the recursive calls returns a non-null value we have
	// found the correct node, so return this node.
	// If we check all of the children and have not found the node,
	// return NULL
	currGroup = currNode->asGroup(); // returns NULL if not a group.
	if (currGroup)
	{
		for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
		{
			foundNode = setNamedNode(searchNodeMask, setNodeMask, currGroup->getChild(i));
			if (foundNode)
				foundNode->setNodeMask(setNodeMask); // found a match!
		}
		return NULL; // We have checked each child node - no match found.
	}
	else
	{
		return NULL; // leaf node, no match
	}
}

void CCamUtil::updateMVPMatrix(CCamera *cam)
{
	_slaveMVPMat = cam->updateMVPMatrix(cam->imgWidth, cam->imgHeight);
}

void CCamUtil::reproject2Projector()
{
	// Basic  reference:
	//	http://ccw1986.blogspot.com/2014/01/opencv-matrix-basics.html

	for (int projId = 0; projId < 4; projId++)
	{
		// Projector 1
		CCamProj *camProj = getProjInstance(Proj_PANEL_INDEX, projId);
		if (camProj == NULL)
			continue;

		// 3D points list
		vector<Point3d> object_points;

		// VGA camera cent
		for (int i = 1; i <= 20; i++)	 // PanelIdx
			for (int j = 1; j <= 24; j++) // CamIdx
			{
				CCamVGA *camVGA = getVGAInstance(i, j);
				if (camVGA && camVGA->isValidCam())
				{
					object_points.push_back(Point3f(camVGA->osgCent.x(), camVGA->osgCent.y(), camVGA->osgCent.z()));
					// cout << "VGA camera:  PanelIdx " << i << "CamIdx " << j << endl;
				}
			}

		// HD camera cent
		// for (int j = 0; j <= 30; j++) // CamIdx
		// {
		// 	CCamHD* camHD = getHDInstance(HD_PANEL_INDEX, j);
		// 	if (camHD && camHD->isValidCam())
		// 	{
		// 		object_points.push_back(Point3f(camHD->osgCent.x(), camHD->osgCent.y(), camHD->osgCent.z()));
		// 		// cout << "HD camera:  CamIdx " << j << endl;
		// 	}
		// }

		if (object_points.size() < 1)
			continue;

		// rotation vector
		//float _rvec[4] = { camProj->extr_mat[0], camProj->extr_mat[1], camProj->extr_mat[2], camProj->extr_mat[3] };
		//Mat rvec(1, 4, CV_32F, _rvec);
		//
		double _rMat[9] = {camProj->osgR(0, 0), camProj->osgR(0, 1), camProj->osgR(0, 2),
						   camProj->osgR(1, 0), camProj->osgR(1, 1), camProj->osgR(1, 2),
						   camProj->osgR(2, 0), camProj->osgR(2, 1), camProj->osgR(2, 2)};
		Mat rMat(3, 3, CV_64F, _rMat);

		// translate vector
		double _tvec[3] = {camProj->osgT.x(), camProj->osgT.y(), camProj->osgT.z()};
		Mat tvec(1, 3, CV_64F, _tvec);

		// distortion vector, note gaku's is(a0,a1,a2,p1,p0)?? ,different from Opencv(a0,a1,a2,p0,p1),
		double _distCoeffs[5] = {camProj->intr_mat[9], camProj->intr_mat[10], camProj->intr_mat[11], camProj->intr_mat[12], camProj->intr_mat[13]}; // Gaku's formulation: a0,a1,a2,p1,p0
		Mat distCoeffs(1, 5, CV_64F, _distCoeffs);

		// camera matrix
		double _camMatrix[9] = {camProj->intr_mat[0], camProj->intr_mat[1], camProj->intr_mat[2], camProj->intr_mat[3], camProj->intr_mat[4], camProj->intr_mat[5], camProj->intr_mat[6], camProj->intr_mat[7], camProj->intr_mat[8]};
		Mat camMatrix(3, 3, CV_64F, _camMatrix);
		// cout << camMatrix << endl;

		// reprojected 2d points
		vector<Point2d> projectedPoints;

		//cout << "object_points:" << object_points << endl;
		//cout << "rMat:" << rMat << endl;
		//cout << "tvec:" << tvec << endl;
		//cout << "camMatrix:" << camMatrix << endl;
		//cout << "distCoeffs:" << distCoeffs << endl;

		// Draw and write image: distort Han
		{
			// Reprojector 3d to projector view, the distCoeffs is zeros, we are using self-define distortion
			double _0_distCoeffs[5] = {0, 0, 0, 0, 0};
			Mat distCoeffs(1, 5, CV_64F, _0_distCoeffs);
			projectPoints(object_points, rMat, tvec, camMatrix, distCoeffs, projectedPoints);

			Mat img(Projector_HEIGHT, Projector_WIDTH, CV_8UC3, Scalar(0));
			for (std::vector<Point2d>::iterator it = projectedPoints.begin(); it != projectedPoints.end(); ++it)
			{
				if (it->x < 0 || it->x > Projector_WIDTH)
					continue;

				if (it->y < 0 || it->y > Projector_HEIGHT)
					continue;

				// cout << "Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				//circle(img, *it, 2, Scalar(0, 0, 255), -1, 8);
				//circle(img, *it, 4, Scalar(0, 255, 0), 1, 8);

				Point2d pt = *it;

				// Distort the points
				pt = ApplyDistort_openCV(*it, _distCoeffs, camMatrix); //ApplyUndistort_openCV
				circle(img, pt, 2, Scalar(0, 0, 255), -1, 8);
				circle(img, pt, 4, Scalar(0, 255, 0), 1, 8);

				// cout << "Ideal   : Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				// cout << "Original: Reprojected point: x=" << pt.x << ",y= " << pt.y << endl;
			}
			ostringstream fileName;
			fileName << "./render/p" << projId << "_0_HanDistort.jpg";
			imwrite(fileName.str().c_str(), img);
		}

		// Draw and write image: distort Han with wrong order
		if (0)
		{
			// Reprojector 3d to projector view, the distCoeffs is zeros, we are using self-define distortion
			double _0_distCoeffs[5] = {0, 0, 0, 0, 0};
			double _cv_distCoeffs_wrong[5] = {camProj->intr_mat[9], camProj->intr_mat[10], camProj->intr_mat[11], camProj->intr_mat[13], camProj->intr_mat[13]}; // Opencv's order: a0,a1,a2,p0,p1, here is wrong
			Mat distCoeffs(1, 5, CV_64F, _0_distCoeffs);
			projectPoints(object_points, rMat, tvec, camMatrix, distCoeffs, projectedPoints);

			Mat img(Projector_HEIGHT, Projector_WIDTH, CV_8UC3, Scalar(0));
			for (std::vector<Point2d>::iterator it = projectedPoints.begin(); it != projectedPoints.end(); ++it)
			{
				if (it->x < 0 || it->x > Projector_WIDTH)
					continue;

				if (it->y < 0 || it->y > Projector_HEIGHT)
					continue;

				// cout << "Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				//circle(img, *it, 2, Scalar(0, 0, 255), -1, 8);
				//circle(img, *it, 4, Scalar(0, 255, 0), 1, 8);

				Point2d pt = *it;

				// Distort the points
				pt = ApplyDistort_openCV(*it, _cv_distCoeffs_wrong, camMatrix); //ApplyUndistort_openCV
				circle(img, pt, 2, Scalar(0, 0, 255), -1, 8);
				circle(img, pt, 4, Scalar(0, 255, 0), 1, 8);

				// cout << "Ideal   : Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				// cout << "Original: Reprojected point: x=" << pt.x << ",y= " << pt.y << endl;
			}
			ostringstream fileName;
			fileName << "./render/p" << projId << "_0_HanDistortWrong.jpg";
			imwrite(fileName.str().c_str(), img);
		}

		// Draw and write image: Ideal
		{
			Mat img(Projector_HEIGHT, Projector_WIDTH, CV_8UC3, Scalar(0));
			for (std::vector<Point2d>::iterator it = projectedPoints.begin(); it != projectedPoints.end(); ++it)
			{
				if (it->x < 0 || it->x > Projector_WIDTH)
					continue;

				if (it->y < 0 || it->y > Projector_HEIGHT)
					continue;

				// cout << "Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				circle(img, *it, 2, Scalar(0, 0, 255), -1, 8);
				circle(img, *it, 4, Scalar(0, 255, 0), 1, 8);

				Point2d pt = *it;
			}
			ostringstream fileName;
			fileName << "./render/p" << projId << "_1_Ideal.jpg";
			imwrite(fileName.str().c_str(), img);
		}

		// Draw and write image: distort Opencv
		{
			// Distortion using han's code
			vector<Point2d> projectedPoints;

			//double _cv_distCoeffs[5] = { camProj->intr_mat[9], camProj->intr_mat[10], camProj->intr_mat[11], camProj->intr_mat[13], camProj->intr_mat[12] }; // Opencv's order: a0,a1,a2,p0,p1
			double _cv_distCoeffs[5] = {camProj->intr_mat[9], camProj->intr_mat[10], camProj->intr_mat[13], camProj->intr_mat[12], camProj->intr_mat[11]}; // Opencv's order: a0,a1,a2,p0,p1
			Mat cv_distCoeffs(1, 5, CV_64F, _cv_distCoeffs);
			projectPoints(object_points, rMat, tvec, camMatrix, cv_distCoeffs, projectedPoints);

			Mat img(Projector_HEIGHT, Projector_WIDTH, CV_8UC3, Scalar(0));
			for (std::vector<Point2d>::iterator it = projectedPoints.begin(); it != projectedPoints.end(); ++it)
			{
				if (it->x < 0 || it->x > Projector_WIDTH)
					continue;

				if (it->y < 0 || it->y > Projector_HEIGHT)
					continue;

				// cout << "Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				//circle(img, *it, 2, Scalar(0, 0, 255), -1, 8);
				//circle(img, *it, 4, Scalar(0, 255, 0), 1, 8);

				Point2d pt = *it;

				circle(img, pt, 2, Scalar(0, 0, 255), -1, 8);
				circle(img, pt, 4, Scalar(0, 255, 0), 1, 8);

				// cout << "Ideal   : Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				// cout << "Original: Reprojected point: x=" << pt.x << ",y= " << pt.y << endl;
			}
			ostringstream fileName;
			fileName << "./render/p" << projId << "_2_cvDistort.jpg";
			imwrite(fileName.str().c_str(), img);
		}

		// Draw and write image: distort Opencv with wrong order
		if (0)
		{
			// Distortion using han's code
			vector<Point2d> projectedPoints;

			double _cv_distCoeffs_wrong[5] = {camProj->intr_mat[9], camProj->intr_mat[10], camProj->intr_mat[11], camProj->intr_mat[12], camProj->intr_mat[13]}; // Opencv's order: a0,a1,a2,p0,p1, here is wrong
			Mat cv_distCoeffs(1, 5, CV_64F, _cv_distCoeffs_wrong);
			projectPoints(object_points, rMat, tvec, camMatrix, cv_distCoeffs, projectedPoints);

			Mat img(Projector_HEIGHT, Projector_WIDTH, CV_8UC3, Scalar(0));
			for (std::vector<Point2d>::iterator it = projectedPoints.begin(); it != projectedPoints.end(); ++it)
			{
				if (it->x < 0 || it->x > Projector_WIDTH)
					continue;

				if (it->y < 0 || it->y > Projector_HEIGHT)
					continue;

				// cout << "Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				//circle(img, *it, 2, Scalar(0, 0, 255), -1, 8);
				//circle(img, *it, 4, Scalar(0, 255, 0), 1, 8);

				Point2d pt = *it;

				circle(img, pt, 2, Scalar(0, 0, 255), -1, 8);
				circle(img, pt, 4, Scalar(0, 255, 0), 1, 8);

				// cout << "Ideal   : Reprojected point: x=" << it->x << ",y= " << it->y << endl;
				// cout << "Original: Reprojected point: x=" << pt.x << ",y= " << pt.y << endl;
			}
			ostringstream fileName;
			fileName << "./render/p" << projId << "_2_cvDistortWrong.jpg";
			imwrite(fileName.str().c_str(), img);
		}
	}
}

void CCamUtil::reproject2HD()
{
	// Basic  reference:
	//	http://ccw1986.blogspot.com/2014/01/opencv-matrix-basics.html

	for (int hdId = 0; hdId < 31; hdId++)
	{
		// Projector 1
		CCamHD *camHD = getHDInstance(HD_PANEL_INDEX, hdId);
		if (camHD == NULL)
			continue;

		// 3D points list
		vector<Point3d> object_points;

		// VGA camera cent
		for (int i = 1; i <= 20; i++)	 // PanelIdx
			for (int j = 1; j <= 24; j++) // CamIdx
			{
				CCamVGA *camVGA = getVGAInstance(i, j);
				if (camVGA && camVGA->isValidCam())
				{
					object_points.push_back(Point3f(camVGA->osgCent.x(), camVGA->osgCent.y(), camVGA->osgCent.z()));
					// cout << "VGA camera:  PanelIdx " << i << ",CamIdx " << j << endl;
				}
			}

		// HD camera cent
		for (int j = 0; j <= 30; j++) // CamIdx
		{
			CCamHD *_camHD = getHDInstance(HD_PANEL_INDEX, j);
			if (_camHD && _camHD->isValidCam())
			{
				object_points.push_back(Point3f(_camHD->osgCent.x(), _camHD->osgCent.y(), _camHD->osgCent.z()));
				// cout << "HD camera:  CamIdx " << j << endl;
			}
		}

		// Projector center
		for (int j = 0; j <= 4; j++) // CamIdx
		{
			CCamProj *camProj = getProjInstance(Proj_PANEL_INDEX, j);
			if (camProj && camProj->isValidCam())
			{
				object_points.push_back(Point3f(camProj->osgCent.x(), camProj->osgCent.y(), camProj->osgCent.z()));
				// cout << "Projector camera:  CamIdx " << j << endl;
			}
		}

		if (object_points.size() < 1)
			continue;

		// rotation vector
		//float _rvec[4] = { camHD->extr_mat[0], camHD->extr_mat[1], camHD->extr_mat[2], camHD->extr_mat[3] };
		//Mat rvec(1, 4, CV_32F, _rvec);
		//
		double _rMat[9] = {camHD->osgR(0, 0), camHD->osgR(0, 1), camHD->osgR(0, 2),
						   camHD->osgR(1, 0), camHD->osgR(1, 1), camHD->osgR(1, 2),
						   camHD->osgR(2, 0), camHD->osgR(2, 1), camHD->osgR(2, 2)};
		Mat rMat(3, 3, CV_64F, _rMat);

		// translate vector
		double _tvec[3] = {camHD->osgT.x(), camHD->osgT.y(), camHD->osgT.z()};
		Mat tvec(1, 3, CV_64F, _tvec);

		// distortion vector
		double _distCoeffs[5] = {camHD->intr_mat[9], camHD->intr_mat[10], camHD->intr_mat[11], camHD->intr_mat[12], camHD->intr_mat[13]};
		double _0_distCoeffs[5] = {0, 0, 0, 0, 0};
		Mat distCoeffs(1, 5, CV_64F, _0_distCoeffs);

		// camera matrix
		double _camMatrix[9] = {camHD->intr_mat[0], camHD->intr_mat[1], camHD->intr_mat[2], camHD->intr_mat[3], camHD->intr_mat[4], camHD->intr_mat[5], camHD->intr_mat[6], camHD->intr_mat[7], camHD->intr_mat[8]};
		Mat camMatrix(3, 3, CV_64F, _camMatrix);

		// reprojected 2d points
		vector<Point2d> projectedPoints;

		//cout << "object_points:" << object_points << endl;
		//cout << "rMat:" << rMat << endl;
		//cout << "tvec:" << tvec << endl;
		//cout << "camMatrix:" << camMatrix << endl;
		//cout << "distCoeffs:" << distCoeffs << endl;

		// Reprojector 3d to projector view, the distCoeffs is zeros, we are using self-define distortion
		projectPoints(object_points, rMat, tvec, camMatrix, distCoeffs, projectedPoints);

		// Draw and write image
		ostringstream hdInFileName;
		hdInFileName << "./render/HDInput_160523/00_" << std::setfill('0') << std::setw(2) << hdId << ".png";
		Mat img = imread(hdInFileName.str().c_str());
		for (std::vector<Point2d>::iterator it = projectedPoints.begin(); it != projectedPoints.end(); ++it)
		{
			if (it->x < 0 || it->x > HD_WIDTH)
				continue;

			if (it->y < 0 || it->y > HD_HEIGHT)
				continue;

			Point2d pt = *it;

			// Distort the points
			pt = ApplyUndistort_openCV(*it, _distCoeffs, camMatrix);

			circle(img, pt, 2, Scalar(0, 0, 255), -1, 8);

			// cout << "Ideal   : Reprojected point: x=" << it->x << ",y= " << it->y << endl;
			// cout << "Original: Reprojected point: x=" << pt.x << ",y= " << pt.y << endl;
		}
		ostringstream fileName;
		fileName << "./render/hd" << hdId << ".jpg";
		imwrite(fileName.str().c_str(), img);
	}
}

Point_<double> CCamUtil::ApplyUndistort_openCV(const Point_<double> &pt_d, double m_distortionParams[5], Mat m_K)
{
	//printf("## WARNING: ApplyUndistort_openCV is not implemented yet\n");
	//return pt_d;
	/*
	Mat_<float> dist(1,5); dist <<0,0,0,0,0;//m_distortionParams[0],m_distortionParams[1],m_distortionParams[2],m_distortionParams[4],m_distortionParams[4];


	Mat_<Point2d> input(1,1);
	input(0) = pt_d;
	Mat_<Point2d> output(1,1);
	undistortPoints(input,output,m_K,dist);
	Point2d pt_u = output(0);
	return pt_u;
	*/

	Mat_<float> dist(1, 5);
	dist << m_distortionParams[0], m_distortionParams[1], m_distortionParams[4], m_distortionParams[3], m_distortionParams[2]; //3,4 order should be swtiched Gakus' vs Opencv
	//	(x',y') = undistort(x",y",dist_coeffs)
	/*	Mat pt_d_Mat = Mat::ones(3,1,CV_64F);
	Mat pt_d_Mat = Mat::ones(3,1,CV_64F);

	pt_d_Mat.at<double>(0,0) = pt_d.x;
	pt_d_Mat.at<double>(1,0) = pt_d.y;
	/*pt_d_Mat = m_invK*pt_d_Mat;
	pt_d_Mat /= pt_d_Mat.at<double>(2,0);
	*/
	//Mat_<float> cam(3,3); cam << m_K.at<double>(0,0),m_K.at<double>(0,1),m_K.at<double>(0,2),m_K.at<double>(1,0),m_K.at<double>(1,1),m_K.at<double>(1,2),m_K.at<double>(2,0),m_K.at<double>(2,1),m_K.at<double>(2,2);//1531.49,0,1267.78,0,1521.439,952.078,0,0,1;
	Mat_<Point2d> input(1, 1);
	input(0) = Point2d(pt_d.x, pt_d.y);
	Mat_<Point2d> output(1, 1);
	undistortPoints(input, output, m_K, dist);
	//cout <<m_K<<endl;
	//cout <<output<<endl;

	Mat pt_u_Mat = Mat::ones(3, 1, CV_64F);
	pt_u_Mat.at<double>(0, 0) = output(0).x;
	pt_u_Mat.at<double>(1, 0) = output(0).y;
	pt_u_Mat = m_K * pt_u_Mat;
	pt_u_Mat /= pt_u_Mat.at<double>(2, 0);
	//cout << pt_u_Mat<<endl;
	Point2d pt_u;
	pt_u.x = pt_u_Mat.at<double>(0, 0);
	pt_u.y = pt_u_Mat.at<double>(1, 0);
	//printf("%f,%f -> %f,%f\n",input(0).x,input(0).y,pt_u.x,pt_u.y);
	return pt_u;
}

//Parameter order is from Gaku's
//Note that the order is different from OpenCV
Point_<double> CCamUtil::ApplyDistort_openCV(const Point_<double> &pt_d, double m_distortionParams[5], Mat m_K) //from ideal_image coord to image coord
{
	Mat m_invK = m_K.inv();

	Mat pt_d_Mat = Mat::ones(3, 1, CV_64F);
	pt_d_Mat.at<double>(0, 0) = pt_d.x;
	pt_d_Mat.at<double>(1, 0) = pt_d.y;
	pt_d_Mat = m_invK * pt_d_Mat;
	pt_d_Mat /= pt_d_Mat.at<double>(2, 0);

	double xn = pt_d_Mat.at<double>(0, 0);
	double yn = pt_d_Mat.at<double>(1, 0);
	double r2 = xn * xn + yn * yn;
	double r4 = r2 * r2;
	double r6 = r2 * r4;
	double X2 = xn * xn;
	double Y2 = yn * yn;
	double XY = xn * yn;

	double a0 = m_distortionParams[0], a1 = m_distortionParams[1], a2 = m_distortionParams[2];
	double p0 = m_distortionParams[3], p1 = m_distortionParams[4]; //p0, p1 order is switched compared to opencv parameters, @TODO

	// cout << "a0:" << a0 << " a1:" << a1 << " a2:" << a2 << " p0" << p0 << " p1:" << p1 << endl;
	double radial = 1.0 + a0 * r2 + a1 * r4 + a2 * r6;
	double tangential_x = p0 * (r2 + 2.0 * X2) + 2.0 * p1 * XY;
	double tangential_y = p1 * (r2 + 2.0 * Y2) + 2.0 * p0 * XY;

	Point2d pt_u;
	pt_d_Mat.at<double>(0, 0) = radial * xn + tangential_x;
	pt_d_Mat.at<double>(1, 0) = radial * yn + tangential_y;
	pt_d_Mat = m_K * pt_d_Mat;
	pt_d_Mat /= pt_d_Mat.at<double>(2, 0);

	pt_u.x = pt_d_Mat.at<double>(0, 0);
	pt_u.y = pt_d_Mat.at<double>(1, 0);

	return pt_u;
}

bool CCamUtil::projectPattern(int projIdx)
{
	for (int idx = 0; idx <= 4; idx++)
	{
		CCamProj *projCam = getProjInstance(Proj_PANEL_INDEX, idx);
		std::string fileName;

		if (projSlave[idx] == NULL)
		{
			cout << "Initial Projector windows first!" << endl;
			continue;
		}

		// Show image or black
		if (idx == projIdx)
			fileName = virtual2real::CConfig::Calib_PROJ_IMG_0;
		else
			fileName = virtual2real::CConfig::Calib_PROJ_IMG_BLACK;

		cout << "Set projector " << idx << " pattern as: " << fileName << endl;
		osg::ref_ptr<osg::Camera> hudCamera = virtual2real::createHUDCamera(0.0, 1.0, 0.0, 1.0);
		hudCamera->addChild(virtual2real::createScreenQuad(1.0f, 1.0f));

		osg::ref_ptr<osg::Image> image = osgDB::readImageFile(fileName);
		osg::ref_ptr<osg::Texture2D> tex2DHUD = new osg::Texture2D;
		tex2DHUD->setImage(image);
		tex2DHUD->setDataVariance(osg::Object::DYNAMIC);
		hudCamera->getOrCreateStateSet()->setTextureAttributeAndModes(0, tex2DHUD.get());
		//sceneNode->asGroup()->addChild(hudCamera.get());
		projSlave[idx]->addChild(hudCamera.get());
	}

	return 0;
}
