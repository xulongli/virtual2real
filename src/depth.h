#ifndef __depth__
#define __depth__
#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#pragma comment(lib, "opengl32")
#pragma comment(lib, "glu32")
#elif defined __unix__
// do something for unix like include <unistd.h>
//include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif



#include <gl/GL.h>
#include <cstdio>
void getRenderedImage(GLubyte *image, int gWidth, int gHeight, unsigned char *imageOutput);
void CopyAndModifyDepth(GLfloat *depth, int gWidth , int gHeight, double *imgDepth);
void getDepthOutput(double *imgDepth, int gWidth , int gHeight, double *depthOutput);
void WriteDepthFile(const char *filename, double *imgDepth, int gWidth , int gHeight);


class Depth {

public:
	Depth();
	~Depth();

};
#endif // __depth__
