
#ifdef _WIN32
// do something for windows like include <windows.h>
#include <windows.h>
#include <direct.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <osg/TexMat>
#include <osg/Program>
#include <osg/Texture>
#include <osg/Texture2D>

#include <osgDB/ReadFile>
#include <osgDB/FileUtils>

#include "Camera.h"
#include "INIReader.h"
#include "Util.h"
#include "PointCloud.h"

#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <sys/stat.h>

using namespace virtual2real;
using namespace std;
using namespace Eigen;
using namespace cv;

namespace render2img
{

// MVP matrix
osg::Matrixd slaveMVPMat;

// ModelView matrix
osg::Matrixd slaveModelViewMat;

// Projection matrix
osg::Matrixd slaveProjectionMat;

using namespace virtual2real;

bool loadShaderSource(osg::Shader *obj, const std::string &fileName)
{
	std::string fqFileName = osgDB::findDataFile(fileName);
	if (fqFileName.length() == 0)
	{
		std::cout << "File \"" << fileName << "\" not found." << std::endl;
		return false;
	}
	bool success = obj->loadShaderSourceFromFile(fqFileName.c_str());
	if (!success)
	{
		std::cout << "Couldn't load file: " << fileName << std::endl;
		return false;
	}
	else
	{
		return true;
	}
}

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string currentDateTime()
{
	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	return buf;
}

void build_opengl_projection_for_intrinsics(GLfloat *frustum, double fx, double fy, double skew, double u0, double v0, int width, int height, double near_clip, double far_clip)
{
#if 1
	double l = 0.0, r = 1.0 * width, b = 0.0, t = 1.0 * height;
	double tx = -(r + l) / (r - l), ty = -(t + b) / (t - b), tz = -(far_clip + near_clip) / (far_clip - near_clip);

	double ortho[16] = {2.0 / (r - l), 0.0, 0.0, tx,
						0.0, 2.0 / (t - b), 0.0, ty,
						0.0, 0.0, -2.0 / (far_clip - near_clip), tz,
						0.0, 0.0, 0.0, 1.0};

	double Intrinsic[16] = {fx, skew, u0, 0.0,
							0.0, fy, v0, 0.0,
							0.0, 0.0, -(near_clip + far_clip), +near_clip * far_clip,
							//0.0, 0.0, -(near_clip+far_clip)/2.0 , 0.0,
							0.0, 0.0, 1.0, 0.0};

	Mat orthoMat = Mat(4, 4, CV_64F, ortho);
	Mat IntrinsicMat = Mat(4, 4, CV_64F, Intrinsic);
	Mat Frustrum = orthoMat * IntrinsicMat;
	Frustrum = Frustrum.t();

	double *data = (double *)Frustrum.data;
	for (int i = 0; i < 16; ++i)
	{
		frustum[i] = data[i];
	}
#endif
}

void quat2rot(double *q, double *R)
{
	// QUAT2ROT - Quaternion to rotation matrix transformation
	//
	//  Usage: quat2rot(q, R); q: quaternion, R: rotation matrix

	double x, y, z, w, x2, y2, z2, w2, xy, xz, yz, wx, wy, wz;
	double norm_q = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);

	x = q[0] / norm_q;
	y = q[1] / norm_q;
	z = q[2] / norm_q;
	w = q[3] / norm_q;

	x2 = x * x;
	y2 = y * y;
	z2 = z * z;
	w2 = w * w;
	xy = 2 * x * y;
	xz = 2 * x * z;
	yz = 2 * y * z;
	wx = 2 * w * x;
	wy = 2 * w * y;
	wz = 2 * w * z;

	// Format: {R[0] R[1] R[2]; R[3] R[4] R[5]; R[6] R[7] R[8];}
	R[0] = w2 + x2 - y2 - z2;
	R[1] = xy - wz;
	R[2] = xz + wy;
	R[3] = xy + wz;
	R[4] = w2 - x2 + y2 - z2;
	R[5] = yz - wx;
	R[6] = xz - wy;
	R[7] = yz + wx;
	R[8] = w2 - x2 - y2 + z2;
}
bool readMatrix(const char *filename, float *mat)
{
	std::ifstream file(filename);
	std::string str = "";
	//float _mat[14];

	if (file.fail())
	{
		return true;
	}
	else
	{
		int i = 0;
		while (file >> mat[i])
		{
			i++;
		}
	}

	//cout << _mat[0] << "," << _mat[1] << "," << _mat[2] << endl;

	return false;
};

bool setMVPMatrix(int imageWidth, int imageHeight, double near_clip, double far_clip, string _calibPath, int _panelIdx, int _camIdx, osg::Matrixd &_slaveMVPMat, osg::Matrixd &_slaveModelViewMat, osg::Matrixd &_slaveProjectionMat)
{
	/*
	00_15.txt:		1430.13		0	949.221		0		1426.45	559.319			0		0		1			0	0	0	0	0
	00_15_ext.txt:	0.999036 -0.0410261 -0.0120331 0.00996186 -4.01396 0.777917 -7.46611
	*/

	//double fx = 1430.13;
	//double fy = 1426.45;
	//double skew = 0;
	//double u0 = 949.221;
	//double v0 = 559.319;
	//int imageWidth = 1920;
	//int imageHeight = 1080;
	//double near_clip = 0.001;
	//double far_clip = 10000;
	//double matRq[4] = { 0.999036, -0.0410261, -0.0120331, 0.00996186 };
	//float vecCent[3] = { -4.01396, 0.777917, -7.46611 };

	std::stringstream intrFilePath;
	std::stringstream extrFilePath;

	intrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << ".txt";
	extrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << "_ext.txt";

	float intr_mat[14];
	if (readMatrix(intrFilePath.str().c_str(), intr_mat))
	{
		//_slaveMVPMat = osg::Matrixd::identity();
		cout << "Lost camera: " << _panelIdx << "," << _camIdx << endl;
		return false;
	}

	float extr_mat[7];
	if (readMatrix(extrFilePath.str().c_str(), extr_mat))
	{
		//_slaveMVPMat = osg::Matrixd::identity();
		cout << "Lost camera: " << _panelIdx << "," << _camIdx << endl;
		return false;
	}
	Map<RowVectorXf> _matRq(extr_mat, 4);
	Eigen::RowVectorXf matRq = _matRq;

	Map<RowVectorXf> _vecCent(extr_mat + 4, 3);
	Eigen::RowVectorXf vecCent = _vecCent;

	double q[4] = {matRq[1], matRq[2], matRq[3], matRq[0]};
	double rot[9];
	quat2rot(q, rot);
	Eigen::Matrix3f m_R;
	m_R << rot[0], rot[1], rot[2],
		rot[3], rot[4], rot[5],
		rot[6], rot[7], rot[8];

	Eigen::MatrixXf vecT = -m_R * vecCent.transpose();

	double fx = intr_mat[0]; // 1430.13;
	double fy = intr_mat[4]; // 1426.45;
	double skew = 0;
	double u0 = intr_mat[2]; // 949.221;
	double v0 = intr_mat[5]; // 559.319;

	cout << "fx:" << fx << " fy:" << fy << " skew:" << skew << " u0:" << u0 << " v0:" << v0 << " width:" << imageWidth << " height:" << imageHeight << " near_clip:" << near_clip << " far_clip:" << far_clip << endl;
	//
	// cout << "Rotation Matrix: \n" << m_R << endl;
	// cout << "Rotation Matrix Transpose: \n" << m_R.transpose() << endl;
	// cout << "Translate vector: \n" << vecT << endl;

	// ************************
	// Han's Code Start
	// ************************

	// View matrix
	GLfloat m_modelViewMatGL[16];
	m_modelViewMatGL[0] = m_R(0, 0);
	m_modelViewMatGL[1] = m_R(1, 0);
	m_modelViewMatGL[2] = m_R(2, 0);
	m_modelViewMatGL[3] = 0;
	m_modelViewMatGL[4] = m_R(0, 1);
	m_modelViewMatGL[5] = m_R(1, 1);
	m_modelViewMatGL[6] = m_R(2, 1);
	m_modelViewMatGL[7] = 0;
	m_modelViewMatGL[8] = m_R(0, 2);
	m_modelViewMatGL[9] = m_R(1, 2);
	m_modelViewMatGL[10] = m_R(2, 2);
	m_modelViewMatGL[11] = 0;
	m_modelViewMatGL[12] = vecT(0); //4th col
	m_modelViewMatGL[13] = vecT(1);
	m_modelViewMatGL[14] = vecT(2);
	m_modelViewMatGL[15] = 1;

	// Projector matrix
	GLfloat m_projMatGL[16];
	build_opengl_projection_for_intrinsics(m_projMatGL, fx, fy, skew, u0, v0, imageWidth, imageHeight, near_clip, far_clip);

	Mat_<double> ProjMat(4, 4);
	Mat_<double> viewModelMat(4, 4);

	double *ProjMatPtr = (double *)ProjMat.data;
	double *viewModelMatPtr = (double *)viewModelMat.data;
	for (int i = 0; i < 16; ++i)
	{
		ProjMatPtr[i] = m_projMatGL[i];
		viewModelMatPtr[i] = m_modelViewMatGL[i];
	}

	Mat_<double> mvp;
	mvp = ProjMat.t() * viewModelMat.t();
	mvp = mvp.t();

	//cout << "viewModelMa \n" << viewModelMat << endl;
	//cout << "ProjMat \n" << ProjMat << endl;
	//cout << "mvp\n" << mvp << endl;

	// ************************
	// Han's Code End
	// ************************

	// MVP matrix , OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
	Mat_<double> _mvp = mvp;
	_slaveMVPMat(0, 0) = _mvp(0);
	_slaveMVPMat(0, 1) = _mvp(1);
	_slaveMVPMat(0, 2) = _mvp(2);
	_slaveMVPMat(0, 3) = _mvp(3);
	_slaveMVPMat(1, 0) = _mvp(4);
	_slaveMVPMat(1, 1) = _mvp(5);
	_slaveMVPMat(1, 2) = _mvp(6);
	_slaveMVPMat(1, 3) = _mvp(7);
	_slaveMVPMat(2, 0) = _mvp(8);
	_slaveMVPMat(2, 1) = _mvp(9);
	_slaveMVPMat(2, 2) = _mvp(10);
	_slaveMVPMat(2, 3) = _mvp(11);
	_slaveMVPMat(3, 0) = _mvp(12);
	_slaveMVPMat(3, 1) = _mvp(13);
	_slaveMVPMat(3, 2) = _mvp(14);
	_slaveMVPMat(3, 3) = _mvp(15); // T

	// Modelview Matrix,  OpenCV/OSG row major, OpenGL: translation components occupy the 13th, 14th, and 15th elements of the 16-element matrix
	Mat_<double> _viewModelMat = viewModelMat; //	viewModelMat.t()
	_slaveModelViewMat(0, 0) = _viewModelMat(0);
	_slaveModelViewMat(0, 1) = _viewModelMat(1);
	_slaveModelViewMat(0, 2) = _viewModelMat(2);
	_slaveModelViewMat(0, 3) = _viewModelMat(3);
	_slaveModelViewMat(1, 0) = _viewModelMat(4);
	_slaveModelViewMat(1, 1) = _viewModelMat(5);
	_slaveModelViewMat(1, 2) = _viewModelMat(6);
	_slaveModelViewMat(1, 3) = _viewModelMat(7);
	_slaveModelViewMat(2, 0) = _viewModelMat(8);
	_slaveModelViewMat(2, 1) = _viewModelMat(9);
	_slaveModelViewMat(2, 2) = _viewModelMat(10);
	_slaveModelViewMat(2, 3) = _viewModelMat(11);
	_slaveModelViewMat(3, 0) = _viewModelMat(12);
	_slaveModelViewMat(3, 1) = _viewModelMat(13);
	_slaveModelViewMat(3, 2) = _viewModelMat(14);
	_slaveModelViewMat(3, 3) = _viewModelMat(15); // T

	// Projection Matrix
	Mat_<double> _ProjMat = ProjMat; //	ProjMat.t()
	_slaveProjectionMat(0, 0) = _ProjMat(0);
	_slaveProjectionMat(0, 1) = _ProjMat(1);
	_slaveProjectionMat(0, 2) = _ProjMat(2);
	_slaveProjectionMat(0, 3) = _ProjMat(3);
	_slaveProjectionMat(1, 0) = _ProjMat(4);
	_slaveProjectionMat(1, 1) = _ProjMat(5);
	_slaveProjectionMat(1, 2) = _ProjMat(6);
	_slaveProjectionMat(1, 3) = _ProjMat(7);
	_slaveProjectionMat(2, 0) = _ProjMat(8);
	_slaveProjectionMat(2, 1) = _ProjMat(9);
	_slaveProjectionMat(2, 2) = _ProjMat(10);
	_slaveProjectionMat(2, 3) = _ProjMat(11);
	_slaveProjectionMat(3, 0) = _ProjMat(12);
	_slaveProjectionMat(3, 1) = _ProjMat(13);
	_slaveProjectionMat(3, 2) = _ProjMat(14);
	_slaveProjectionMat(3, 3) = _ProjMat(15); // T

	// ASSERT: MVP = Projection * View * Model
	/*	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(position, 1.0);
		vec4 vertexWorldSpacePosition =  ModelMatrix * vec4(position, 1.0);
		vec4 vertexCameraSpacePosition = ViewMatrix * vertexWorldSpacePosition;
		gl_Position = MVP_Matrix * vec4(position, 1.0); 	*/

	// cout << "MVP - ProjMat * ModelViewMat: " << _mvp.t() - _ProjMat.t()*_viewModelMat.t() << endl;

	return true;
}

} // namespace render2img

using namespace render2img;

//int test_render2img_main(int argc, char** argv)
int main(int argc, char **argv)
{
	bool SET_TEXTURE = false;
	bool RENDER_VGA = false;
	bool RENDER_HD = false;
	bool RENDER_KINECT = false;
	std::string outFolder;
	std::string modelFile;

	if (argc < 5)
		cout << "Usage: virtual2real [LOAD_MODEL] [SAVE_FOLDER] [RENDER_MODE] [RENDER_TEXTURE]";
	else
	{
		modelFile = argv[1];
		outFolder = argv[2];

// All render output in "render"
//std::string  outPath = "render/" + outFolder;
#if defined _MSC_VER
		_mkdir(outFolder.c_str());
#elif defined __GNUC__
		mkdir(outFolder.c_str(), 0777);
#endif

		int renderMode = atoi(argv[3]);
		switch (renderMode)
		{
		case (1):
			RENDER_VGA = true;
			break;
		case (2):
			RENDER_HD = true;
			break;
		case (3):
			RENDER_KINECT = true;
			break;
		default:
			RENDER_VGA = true;
			break;
		}
		SET_TEXTURE = atoi(argv[4]);
	}

	// Read Config file
	INIReader reader("config/config.ini");
	string CALIB_FILE_PATH = reader.Get("Config", "CALIB_FILE_PATH", "");
	//cout << "Calibration: " << CALIB_FILE_PATH << endl;

	// LOG
	ofstream logFile;
	std::string logPath = outFolder + "/render.log";
	logFile.open(logPath.c_str());
	logFile << currentDateTime() << "\n";
	logFile << CALIB_FILE_PATH << "\n";
	logFile.close();

	///����Viewer����
	osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer();
	osg::ref_ptr<osg::PositionAttitudeTransform> scene = new osg::PositionAttitudeTransform;

	// ��ȡ���� Set the ground (only receives shadow)
	CUtil *utl = new CUtil();
	osg::ref_ptr<osg::MatrixTransform> groundNode = new osg::MatrixTransform;
	utl->drawFloor(groundNode);
	//scene->addChild(groundNode);

	// Dyna Human
	//osg::ref_ptr<osg::Node> dynaNode = osgDB::readNodeFile("./model/dyna_male_50002_hips_81.osgt");
	osg::ref_ptr<osg::Node> dynaNode = osgDB::readNodeFile(modelFile);
	scene->addChild(dynaNode);

	if (SET_TEXTURE)
	{
		// set up the texture state.
		osg::Texture2D *texture = new osg::Texture2D;
		texture->setDataVariance(osg::Object::DYNAMIC);				// protect from being optimized away as static state.
		texture->setBorderColor(osg::Vec4(1.0f, 1.0f, 1.0f, 0.5f)); // only used when wrap is set to CLAMP_TO_BORDER
		osg::Image *img;
		img = osgDB::readImageFile("model/checkerboard.jpg");

		if (!img)
			std::cout << "Texture image not found!" << std::endl;

		texture->setImage(img);
		texture->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
		texture->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
		texture->setWrap(osg::Texture::WRAP_R, osg::Texture::REPEAT);

		osg::Material *material = new osg::Material;
		osg::StateSet *stateset = dynaNode->getOrCreateStateSet();

		stateset->setTextureAttribute(0, texture, osg::StateAttribute::ON);
		stateset->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
		stateset->setTextureMode(0, GL_TEXTURE_GEN_S, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
		stateset->setTextureMode(0, GL_TEXTURE_GEN_T, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
		stateset->setAttribute(material, osg::StateAttribute::OVERRIDE);
		stateset->setMode(GL_BLEND, osg::StateAttribute::ON);
		stateset->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

		float scale = 10;
		osg::TexMat *texmat = new osg::TexMat;
		texmat->setScaleByTextureRectangleSize(true);
		texmat->setMatrix(osg::Matrix::scale(scale, scale, 1.0));
		stateset->setTextureAttributeAndModes(0, texmat, osg::StateAttribute::ON);
	}

	// Point cloud
	// osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform;
	// string PLY_FILE_NAME = reader.Get("Config", "PLY_FILE_NAME", "");
	// CPointCloud* pointCloud = new CPointCloud();
	// pointCloud->loadPointCloud(PLY_FILE_NAME, ptcNode);
	// scene->addChild(ptcNode);

	// Axis
	// osg::ref_ptr<osg::Node> axisNode = osgDB::readNodeFile("axes.osgt");
	// scene->addChild(axisNode);

	//Shader
	{
		osg::ref_ptr<osg::Program> program = new osg::Program;
		osg::Shader *render2imgVertexObject = new osg::Shader(osg::Shader::VERTEX);
		osg::Shader *render2imgFragmentObject = new osg::Shader(osg::Shader::FRAGMENT);
		program->addShader(render2imgVertexObject);
		program->addShader(render2imgFragmentObject);

		bool status1 = loadShaderSource(render2imgVertexObject, "shaders/render2img.vert");
		bool status2 = loadShaderSource(render2imgFragmentObject, "shaders/render2img.frag");

		if (!status1 && !status2)
			return -1;

		//program->addShader(new osg::Shader(osg::Shader::VERTEX, vertSource_render2img));
		//program->addShader(new osg::Shader(osg::Shader::FRAGMENT, fragSource_render2img));

		osg::StateSet *stateset = scene->getOrCreateStateSet();
		stateset->setAttributeAndModes(program.get());
		stateset->addUniform(new osg::Uniform("lightDiffuse", osg::Vec4(0.8f, 0.8f, 0.8f, 1.0f)));
		stateset->addUniform(new osg::Uniform("lightSpecular", osg::Vec4(1.0f, 1.0f, 0.4f, 1.0f)));
		stateset->addUniform(new osg::Uniform("shininess", 64.0f));

		osg::ref_ptr<osg::Uniform> lightPos = new osg::Uniform("lightPosition", osg::Vec3());
		lightPos->setUpdateCallback(new CLightPosCallback);
		stateset->addUniform(lightPos.get());

		// MVP matrix
		osg::ref_ptr<osg::Uniform> slaveMVPUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "matMVP");
		slaveMVPUniform->setUpdateCallback(new UniformUpdate(&slaveMVPMat));
		stateset->addUniform(slaveMVPUniform.get());

		// ModelView matrix
		osg::ref_ptr<osg::Uniform> slaveModelUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "matModelView");
		slaveModelUniform->setUpdateCallback(new UniformUpdate(&slaveModelViewMat));
		stateset->addUniform(slaveModelUniform.get());

		float x, y;
		for (int i = 0; i < 5; i++)
		{
			std::stringstream name;
			name << "lights[" << i << "].Position";
			x = 20.0f * cosf((osg::PI * 2 / 5) * i);
			y = 1.0f * sinf((osg::PI * 2 / 5) * i);

			stateset->addUniform(new osg::Uniform(name.str().c_str(), osg::Vec4(x, y, 100.0f, 1.0f)));
		}

		stateset->addUniform(new osg::Uniform("lights[0].Intensity", osg::Vec3(0.0f, 0.8f, 0.8f)));
		stateset->addUniform(new osg::Uniform("lights[1].Intensity", osg::Vec3(0.0f, 0.0f, 0.8f)));
		stateset->addUniform(new osg::Uniform("lights[2].Intensity", osg::Vec3(0.8f, 0.0f, 0.0f)));
		stateset->addUniform(new osg::Uniform("lights[3].Intensity", osg::Vec3(0.0f, 0.8f, 0.0f)));
		stateset->addUniform(new osg::Uniform("lights[4].Intensity", osg::Vec3(0.8f, 0.8f, 0.8f)));

		stateset->addUniform(new osg::Uniform("Kd", osg::Vec3(0.1f, 0.1f, 0.1f)));
		stateset->addUniform(new osg::Uniform("Ks", osg::Vec3(0.9f, 0.9f, 0.9f)));
		stateset->addUniform(new osg::Uniform("Ka", osg::Vec3(0.1f, 0.1f, 0.1f)));
		stateset->addUniform(new osg::Uniform("Shininess", 64.0f));
	}

	// Windows
	int xoffset = -2000;
	int yoffset = -2000;

	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->x = xoffset + 0;
	traits->y = yoffset + 0;
	if (RENDER_VGA)
	{
		traits->width = 640;
		traits->height = 480;
	}
	else if (RENDER_HD || RENDER_KINECT)
	{
		traits->width = 1920;
		traits->height = 1080;
	}
	traits->windowDecoration = false;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;

	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);
	camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 0.0f));
	camera->setPostDrawCallback(SnapImage::getInstance());

	// add this slave camera to the viewer, with a shift left of the projection matrix
	viewer->addSlave(camera.get(), osg::Matrixd::translate(0.0, 0.0, 0.0), osg::Matrixd());
	std::cout << "Create main slave 0: " << traits->x << "," << traits->y << "," << traits->width << "," << traits->height << endl;

	// �Ż���������
	osgUtil::Optimizer optimizer;
	optimizer.optimize(scene.get());

	//Light
	viewer->setLightingMode(osg::View::SKY_LIGHT);
	utl->setLight(scene);

	viewer->setCameraManipulator(new osgGA::TrackballManipulator);
	viewer->setSceneData(scene);
	viewer->realize();

	int kinectIdx = 0;
	int hdIdx = 0;
	int vgaIdx = 0;

	while (!viewer->done())
	{
		viewer->frame();

		// VGA
		if (RENDER_VGA && !SnapImage::getInstance()->_snapImage)
		{
			int panelIdx = int(vgaIdx / 24) + 1;
			int camIdx = vgaIdx % 24 + 1;
			cout << "VGA: " << panelIdx << "_" << camIdx << endl;

			bool status = setMVPMatrix(640, 480, 0.001, 100000, CALIB_FILE_PATH, panelIdx, camIdx, slaveMVPMat, slaveModelViewMat, slaveProjectionMat);
			if (status)
			{
				stringstream _name;
				_name << outFolder << "/" << std::setfill('0') << std::setw(2) << panelIdx << "_" << std::setfill('0') << std::setw(2) << camIdx << ".png";
				SnapImage::getInstance()->_snapImage = true;
				SnapImage::getInstance()->_filename = _name.str();
			}

			vgaIdx++;

			if ((panelIdx == 20) && (camIdx == 24))
			{
				RENDER_VGA = false;
			}
		}

		// HD
		if (RENDER_HD && !SnapImage::getInstance()->_snapImage)
		{
			int panelIdx = 0;
			int camIdx = hdIdx % 32;
			cout << "HD: " << panelIdx << "_" << camIdx << endl;

			bool status = setMVPMatrix(1920, 1080, 0.001, 100000, CALIB_FILE_PATH, panelIdx, camIdx, slaveMVPMat, slaveModelViewMat, slaveProjectionMat);
			if (status)
			{
				stringstream _name;
				_name << outFolder << "/" << std::setfill('0') << std::setw(2) << panelIdx << "_" << std::setfill('0') << std::setw(2) << camIdx << ".png";
				SnapImage::getInstance()->_snapImage = true;
				SnapImage::getInstance()->_filename = _name.str();
			}

			hdIdx++;

			if ((panelIdx == 0) && (camIdx == 31))
			{
				RENDER_HD = false;
			}
		}

		// Kinect
		if (RENDER_KINECT && !SnapImage::getInstance()->_snapImage)
		{
			int panelIdx = 50;
			int camIdx = kinectIdx % 10 + 1;
			cout << "Kinect: " << panelIdx << "_" << camIdx << endl;

			bool status = setMVPMatrix(1920, 1080, 0.001, 100000, CALIB_FILE_PATH, panelIdx, camIdx, slaveMVPMat, slaveModelViewMat, slaveProjectionMat);
			if (status)
			{
				stringstream _name;
				_name << outFolder << "/" << std::setfill('0') << std::setw(2) << panelIdx << "_" << std::setfill('0') << std::setw(2) << camIdx << ".png";
				SnapImage::getInstance()->_snapImage = true;
				SnapImage::getInstance()->_filename = _name.str();
			}

			kinectIdx++;

			if ((panelIdx == 50) && (camIdx == 10))
			{
				RENDER_KINECT = false;
			}
		}

		// ALL done
		if (!RENDER_KINECT && !RENDER_VGA && !RENDER_HD && !SnapImage::getInstance()->_snapImage)
		{
			viewer->setDone(true);
		}
	}
	return 0;
}
