//in vec3 vertex_pos;
in vec3 VertexPosition;
in vec3 VertexNormal;

uniform vec3 lightPosition;
varing vec3 normal;
out vec3 eyeVec, lightDir;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 MVP;

uniform mat4 matMVP;
uniform mat4 matModelView;

void main(void)
{
	vec4 vertexInEye = matModelView * VertexPosition;
	eyeVec = -vertexInEye.xyz;
	lightDir = vec3(lightPosition - vertexInEye.xyz);
	
	normal = gl_NormalMatrix * gl_Normal;
	
	gl_Position = matMVP * vec4(VertexPosition, 1.0);
	gl_Position.y = 1.0 - gl_Position.y;
}