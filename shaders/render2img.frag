#version 110 		

uniform vec4 lightDiffuse;
uniform vec4 lightSpecular;
uniform float shininess;
varying vec3 normal, eyeVec, lightDir;
	
void main (void)
{
	vec4 finalColor = gl_FrontLightModelProduct.sceneColor;
	vec3 N = normalize(normal);
	vec3 L = normalize(lightDir);
	
	float lambert = dot(N,L);
	if (lambert > 0.0)
	{
		finalColor += lightDiffuse * lambert;
		vec3 E = normalize(eyeVec);
		vec3 R = reflect(-L, N);
		float specular = pow(max(dot(R, E), 0.0), shininess);
		finalColor += lightSpecular * specular;
	}
	
	gl_FragColor = finalColor;
	//gl_FragColor = vec4(0.1,0.8,0.5,0.8); 
}