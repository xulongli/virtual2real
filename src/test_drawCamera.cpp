#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
//include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <iostream>
#include <queue>
#include "PointCloud.h"

	osg::ref_ptr<osg::Geometry> drawLineSegment(osg::Vec3 sp, osg::Vec3 ep)
{
	//osg::Vec3 sp(0, -180, 120);
	//osg::Vec3 ep(0, 480, 120);
	osg::ref_ptr<osg::Geometry> beam(new osg::Geometry);
	osg::ref_ptr<osg::Vec3Array> points = new osg::Vec3Array;
	points->push_back(sp);
	points->push_back(ep);
	osg::ref_ptr<osg::Vec4Array> color = new osg::Vec4Array;
	color->push_back(osg::Vec4(1.0, 0.0, 0.0, 1.0));
	beam->setVertexArray(points.get());
	beam->setColorArray(color.get());
	beam->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);
	beam->addPrimitiveSet(new osg::DrawArrays(GL_LINES, 0, 2));

	return beam;
}
int main()
{
	osg::ref_ptr<osg::Vec3Array> cameraVertices = new osg::Vec3Array;
	osg::ref_ptr<osg::Geometry> cameraGeometry = new osg::Geometry();
	osg::ref_ptr<osg::Geode> cameraGeode = new osg::Geode();
	osg::ref_ptr<osg::Group> node = new osg::Group();

	cameraGeode->addDrawable(cameraGeometry);
	node->addChild(cameraGeode);

	// Vertices
	cameraVertices->push_back(osg::Vec3(0, 0, 0));
	cameraVertices->push_back(osg::Vec3(-1.2, 0.9, -2)); //
	cameraVertices->push_back(osg::Vec3(-1.2, -0.9, -2));
	cameraVertices->push_back(osg::Vec3(1.2, -0.9, -2));
	cameraVertices->push_back(osg::Vec3(1.2, 0.9, -2));
	cameraGeometry->setVertexArray(cameraVertices);

	// Quad Face
	//osg::ref_ptr<osg::DrawElementsUInt> camBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
	//camBase->push_back(2); 		camBase->push_back(1);		camBase->push_back(4); 		camBase->push_back(3);
	//cameraGeometry->addPrimitiveSet(camBase);

	osg::ref_ptr<osg::DrawElementsUInt> camBase3 = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
	camBase3->push_back(0);
	camBase3->push_back(1);
	camBase3->push_back(2);
	camBase3->push_back(0);
	camBase3->push_back(2);
	camBase3->push_back(3);
	camBase3->push_back(0);
	camBase3->push_back(3);
	camBase3->push_back(4);
	camBase3->push_back(0);
	camBase3->push_back(4);
	camBase3->push_back(1);
	cameraGeometry->addPrimitiveSet(camBase3);

	//Color
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
	colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
	colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
	colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
	colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
	cameraGeometry->setColorArray(colors);
	cameraGeometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	// edge
	node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), osg::Vec3(-1.2, 0.9, -2)));
	node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), osg::Vec3(-1.2, -0.9, -2)));
	node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), osg::Vec3(1.2, -0.9, -2)));
	node->addChild(drawLineSegment(osg::Vec3(0, 0, 0), osg::Vec3(1.2, 0.9, -2)));
	node->addChild(drawLineSegment(osg::Vec3(-1.2, 0.9, -2), osg::Vec3(-1.2, -0.9, -2)));
	node->addChild(drawLineSegment(osg::Vec3(-1.2, -0.9, -2), osg::Vec3(1.2, -0.9, -2)));
	node->addChild(drawLineSegment(osg::Vec3(1.2, -0.9, -2), osg::Vec3(1.2, 0.9, -2)));
	node->addChild(drawLineSegment(osg::Vec3(1.2, 0.9, -2), osg::Vec3(-1.2, 0.9, -2)));

	// transpanrent
	osg::ref_ptr<osg::StateSet> ss = cameraGeode->getOrCreateStateSet();
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	// Point cloud
	//osg::ref_ptr<osg::Node> ptcNode;
	//std::string PLY_FILE_NAME = "C:/Review/Code/virtual2real/virtual2real/model/reconByTraj_00007816.ply";
	//CPointCloud* pointCloud = new CPointCloud(PLY_FILE_NAME, ptcNode);
	//node->addChild(ptcNode.get());

	osgViewer::Viewer viewer;
	viewer.setSceneData(node);
	return viewer.run();
}