#include "PointCloud.h"

CPointCloud::CPointCloud()
{
}

void CPointCloud::loadPointCloud(std::string file, osg::ref_ptr<osg::MatrixTransform> node)
{
	shader = false;
	usePointSprites = true;
	forcePointMode = true;

	// read the scene from the list of file specified commandline args.
	osg::ref_ptr<osg::Node> loadModel = osgDB::readNodeFile(file);

	// if no model has been successfully loaded report failure.
	if (!loadModel)
	{
		std::cout << "No data loaded" << std::endl;
		loadModel = NULL;
	}
	osg::StateSet *stateset = loadModel->getOrCreateStateSet();

	// Set point size
	osg::ref_ptr<osg::Point> _point = new osg::Point;
	_point->setDistanceAttenuation(osg::Vec3(0.0, 0.0000, 0.05f));
	_point->setSize(2);
	stateset->setAttribute(_point.get());

	if (usePointSprites)
	{
		/// Setup cool blending
		osg::BlendFunc *fn = new osg::BlendFunc();
		stateset->setAttributeAndModes(fn, osg::StateAttribute::ON);

		/// Setup the point sprites
		osg::PointSprite *sprite = new osg::PointSprite();
		stateset->setTextureAttributeAndModes(0, sprite, osg::StateAttribute::ON);

		/// The texture for the sprites
		osg::Texture2D *tex = new osg::Texture2D();
		tex->setImage(osgDB::readImageFile("./model/particle.rgb"));
		stateset->setTextureAttributeAndModes(0, tex, osg::StateAttribute::ON);
	}

	if (forcePointMode)
	{
		/// Set polygon mode to GL_POINT
		osg::PolygonMode *pm = new osg::PolygonMode(
			osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::POINT);
		stateset->setAttributeAndModes(pm, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	}

	if (shader)
	{
		osg::StateSet *stateset = loadModel->getOrCreateStateSet();

		///////////////////////////////////////////////////////////////////
		// vertex shader using just Vec4 coefficients
		char vertexShaderSource[] =
			"void main(void) \n"
			"{ \n"
			"\n"
			"    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n"
			"}\n";

		osg::Program *program = new osg::Program;
		stateset->setAttribute(program);

		osg::Shader *vertex_shader = new osg::Shader(osg::Shader::VERTEX, vertexShaderSource);
		program->addShader(vertex_shader);

#if 0
		//////////////////////////////////////////////////////////////////
		// fragment shader
		//
		char fragmentShaderSource[] =
			"void main(void) \n"
			"{ \n"
			"    gl_FragColor = gl_Color; \n"
			"}\n";

		osg::Shader* fragment_shader = new osg::Shader(osg::Shader::FRAGMENT, fragmentShaderSource);
		program->addShader(fragment_shader);
#endif
	}

	node->addChild(loadModel);
}
