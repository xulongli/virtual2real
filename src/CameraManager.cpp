#include "stdafx.h"
#include "CameraManager.h"
#include <iostream>
#include "StateManager.h"
#include "SceneManager.h"
#include "CEGUIUpdateCallback.h"

extern PlayerInfo playerinfo[4];
bool BallUpdateCallback::change = false;
bool BallUpdateCallback::couter = true;
// ���캯��
TravelManipulator::TravelManipulator(PhysicsManager *physicsmgr, osg::ref_ptr<osgViewer::Viewer> viewer) : m_fMoveSpeed(1.0f), m_bLeftButtonDown(false), m_fpushX(0), m_fAngle(0.5), m_bPeng(true), m_fpushY(0), following(false), cam_or_revcam(true), dynamic_cam(false), tv_cam(false)
{
	// 	m_vPosition = osg::Vec3(-22.0f, -274.0f, 100.0f);
	// 	m_vRotation = osg::Vec3(osg::PI_2, 0.0f, 0.0f);
	m_pPhysicsmgr = physicsmgr;
	slider = false;
	markflag = false;
	initialangle = false;
	changecamera = false;
	maxheight = -100;
	cam_or_revcam = true;
	_viewer = viewer;
	cameraMode = 1;
	BenchpolePos = holeDataMgr->getCurrentPinCor();
	raiseCameraSignal = false;
}

TravelManipulator::~TravelManipulator(void)
{
}

void TravelManipulator::pickUp(osg::ref_ptr<osgViewer::Viewer> Viewer, const osgGA::GUIEventAdapter &ea)
{
	osgUtil::LineSegmentIntersector::Intersections intersections;

	float x = ea.getX();
	float y = ea.getY();

	if (Viewer->computeIntersections(x, y, intersections))
	{
		//for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();hitr != intersections.end();hitr++)
		osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
		if (BallUpdateCallback::toRangeing)
		{
			BenchpolePos = osg::Vec3(hitr->getWorldIntersectPoint().x(), hitr->getWorldIntersectPoint().y(), GetPosition().z() - 0.8f);
		}
		else
		{
			BenchpolePos = hitr->getWorldIntersectPoint();
		}
		//std::cout<<"Benchpole position is :"<<BenchpolePos.x()<<"   "<<BenchpolePos.y()<<"   "<<BenchpolePos.z()<<std::endl;
	}
}

// ���þ���
void TravelManipulator::setByMatrix(const osg::Matrix &matrix)
{
}

// ���������
void TravelManipulator::setByInverseMatrix(const osg::Matrix &matrix)
{
}

// �õ�����
osg::Matrixd TravelManipulator::getMatrix(void) const
{
	osg::Matrixd mat;

	mat.makeRotate(m_vRotation._v[0], osg::Vec3(1.0f, 0.0f, 0.0f),
				   m_vRotation._v[1], osg::Vec3(0.0f, 1.0f, 0.0f),
				   m_vRotation._v[2], osg::Vec3(0.0f, 0.0f, 1.0f));

	return mat * osg::Matrixd::translate(m_vPosition);
}

// �õ������
osg::Matrixd TravelManipulator::getInverseMatrix(void) const
{
	osg::Matrixd mat;

	mat.makeRotate(m_vRotation._v[0], osg::Vec3(1.0f, 0.0f, 0.0f),
				   m_vRotation._v[1], osg::Vec3(0.0f, 1.0f, 0.0f),
				   m_vRotation._v[2], osg::Vec3(0.0f, 0.0f, 1.0f));

	return osg::Matrixd::inverse(mat * osg::Matrixd::translate(m_vPosition));
}

// �¼���������
bool TravelManipulator::handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &us)
{
	// ��ȡ���λ��
	float mouseX = ea.getX();
	float mouseY = ea.getY();
	if (CEGUIUpdateCallback::MouseKeyOnoff)
	{
		switch (ea.getEventType())
		{
		case (osgGA::GUIEventAdapter::KEYDOWN):
		{
			// �ո��
			if (ea.getKey() == 0x20)
			{
				us.requestRedraw();
				us.requestContinuousUpdate(false);

				return true;
			}

			// ���Ƽ�  KEY_Home = 0xFF50,
			if (ea.getKey() == 0xFF50)
			{
				ChangePosition(osg::Vec3(0, 0, m_fMoveSpeed * 0.5));

				return true;
			}

			// ���Ƽ�  KEY_End = 0xFF57,
			if (ea.getKey() == 0xFF57)
			{
				ChangePosition(osg::Vec3(0, 0, -m_fMoveSpeed * 0.5));

				return true;
			}

			// ����
			if (ea.getKey() == 0x2B)
			{
				m_fMoveSpeed += 1.0f;

				return true;
			}

			// �����ٶ�
			if (ea.getKey() == 0x2D)
			{
				m_fMoveSpeed -= 0.1f;

				if (m_fMoveSpeed < 1.0f)
				{
					m_fMoveSpeed = 1.0f;
				}

				return true;
			}

			// ǰ��   KEY_Up = 0xFF52
			if (ea.getKey() == 0xFF52 || ea.getKey() == 0x57 || ea.getKey() == 0x77)
			{
				ChangePosition(osg::Vec3(0, m_fMoveSpeed * sinf(osg::PI_2 + m_vRotation._v[2]), 0));
				ChangePosition(osg::Vec3(m_fMoveSpeed * cosf(osg::PI_2 + m_vRotation._v[2]), 0, 0));

				return true;
			}

			// ����  KEY_Down = 0xFF54
			if (ea.getKey() == 0xFF54 || ea.getKey() == 0x53 || ea.getKey() == 0x73)
			{
				ChangePosition(osg::Vec3(0, -m_fMoveSpeed * sinf(osg::PI_2 + m_vRotation._v[2]), 0));
				ChangePosition(osg::Vec3(-m_fMoveSpeed * cosf(osg::PI_2 + m_vRotation._v[2]), 0, 0));

				return true;
			}

			// ����
			if (ea.getKey() == 0x41 || ea.getKey() == 0x61)
			{
				ChangePosition(osg::Vec3(0, m_fMoveSpeed * cosf(osg::PI_2 + m_vRotation._v[2]), 0));
				ChangePosition(osg::Vec3(-m_fMoveSpeed * sinf(osg::PI_2 + m_vRotation._v[2]), 0, 0));

				return true;
			}

			// ����
			if (ea.getKey() == 0x44 || ea.getKey() == 0x64)
			{
				ChangePosition(osg::Vec3(0, -m_fMoveSpeed * cosf(osg::PI_2 + m_vRotation._v[2]), 0));
				ChangePosition(osg::Vec3(m_fMoveSpeed * sinf(osg::PI_2 + m_vRotation._v[2]), 0, 0));

				return true;
			}

			// ����ת
			if (ea.getKey() == 0xFF53)
			{
				m_vRotation._v[2] -= osg::DegreesToRadians(m_fAngle);
			}

			// ����ת
			if (ea.getKey() == 0xFF51)
			{
				m_vRotation._v[2] += osg::DegreesToRadians(m_fAngle);
			}

			// �ı���Ļ�Ƕ�F��
			if (ea.getKey() == 0x46 || ea.getKey() == 0x66)
			{
				m_fAngle -= 0.5;

				return true;
			}

			//G��
			if (ea.getKey() == 0x47 || ea.getKey() == 0x66)
			{
				m_fAngle += 0.5;

				return true;
			}
			//C��
			// 			if (ea.getKey() == 'c' || ea.getKey() == 'C')
			// 			{
			// 				if(cameraFlag)
			// 					cameraFlag=false;
			// 				else
			// 					cameraFlag=true;
			// 				return true;
			// 			}
			// 			/*	 F1 Hole2	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F1 )
			// 			{
			// 				m_vPosition = holeData[0].mViewPos_0;
			// 				m_vRotation = holeData[0].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F2 Hole2	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F2 )
			// 			{
			// 				m_vPosition = holeData[1].mViewPos_0;
			// 				m_vRotation = holeData[1].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F3 Hole3	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F3 )
			// 			{
			// 				m_vPosition = holeData[2].mViewPos_0;
			// 				m_vRotation = holeData[2].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F4 Hole4	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F4 )
			// 			{
			// 				m_vPosition = holeData[3].mViewPos_0;
			// 				m_vRotation = holeData[3].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F5 Hole5	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F5 )
			// 			{
			// 				m_vPosition = holeData[4].mViewPos_0;
			// 				m_vRotation = holeData[4].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F6 Hole6	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F6 )
			// 			{
			// 				m_vPosition = holeData[5].mViewPos_0;
			// 				m_vRotation = holeData[5].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F7 Hole7	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F7 )
			// 			{
			// 				m_vPosition = holeData[6].mViewPos_0;
			// 				m_vRotation = holeData[6].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F8 Hole8	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F8 )
			// 			{
			// 				m_vPosition = holeData[7].mViewPos_0;
			// 				m_vRotation = holeData[7].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*	 F9 Hole9	*/
			// 			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_F9 )
			// 			{
			// 				m_vPosition = holeData[8].mViewPos_0;
			// 				m_vRotation = holeData[8].mViewAng_0;
			// 				return true;
			// 			}
			// 			/*R: Reset to Green*/
			// 			if (ea.getKey() == 'r' || ea.getKey() == 'R' )
			// 			{
			// 				m_vPosition = holeData[Current_Hole].mViewPos_0;
			// 				m_vRotation = holeData[Current_Hole].mViewAng_0;
			// 				return true;
			// 			}
			return true;
		}

		case (osgGA::GUIEventAdapter::PUSH):

			if (ea.getButton() == 1)
			{
				m_fpushX = mouseX;
				m_fpushY = mouseY;

				m_bLeftButtonDown = true;
				if (!CEGUIDrawable::PoleLock)
				{
					CEGUIDrawable::pushBenchpole = true;
					pickUp(_viewer, ea);
					SceneManager::m_pBenchpoleMat->setMatrix(osg::Matrix::translate(BenchpolePos));
					if (BallUpdateCallback::toRangeing)
					{
						SceneManager::m_pBenchpoleMat->preMult(osg::Matrix::rotate(osg::Vec3(0, 1, 0), computeCameraToTargetAng(BenchpolePos)));
					}
					if (computeCameraToBenchpoleDis(BenchpolePos) <= 24)
					{
						SceneManager::m_pBenchpoleMat->preMult(osg::Matrix::scale(computeCameraToBenchpoleDis(BenchpolePos) * 0.07, computeCameraToBenchpoleDis(BenchpolePos) * 0.07, computeCameraToBenchpoleDis(BenchpolePos) * 0.03));
					}
					else if (computeCameraToBenchpoleDis(BenchpolePos) > 24)
					{
						SceneManager::m_pBenchpoleMat->preMult(osg::Matrix::scale(2.1, 2.1, 1.2));
					}
					benchpos = BenchpolePos;
					markflag = true;
					ChangePositionToTaget(CameraComputePos(m_pPhysicsmgr->getBallCurrentPosition(), benchpos));
					m_vRotation = CameraComputeAngforchangposition(BenchpolePos - m_pPhysicsmgr->getBallCurrentPosition());
				}
			}

			return true;

			// �϶�
		case (osgGA::GUIEventAdapter::DRAG):

			if (m_bLeftButtonDown && slider == false)
			{
				m_vRotation._v[2] -= osg::DegreesToRadians(m_fAngle * (mouseX - m_fpushX)) / 200;
				m_vRotation._v[0] += osg::DegreesToRadians(1.1 * (mouseY - m_fpushY)) / 200;

				if (m_vRotation._v[0] >= 3.14)
				{
					m_vRotation._v[0] = 3.14;
				}

				if (m_vRotation._v[0] <= 0)
				{
					m_vRotation._v[0] = 0;
				}
			}

			return false;

			// ����ͷ�
		case (osgGA::GUIEventAdapter::RELEASE):

			if (ea.getButton() == 1)
			{
				m_bLeftButtonDown = false;
			}

			return false;

		default:

			return false;
		}
	}
}

// λ�ñ任����
bool TravelManipulator::ChangePosition(osg::Vec3 &delta)
{
	// ��ײ���
	if (m_bPeng)
	{
		osg::Vec3 newPos1;

		newPos1 = m_vPosition + delta;
		osg::Vec3 hitPos;
		float hitFraction;

		if (!m_pPhysicsmgr->rayCastTerrain(m_vPosition, newPos1, hitPos, hitFraction))
		{
			m_vPosition += delta;
		}
		else
			ChangePositionToTaget(newPos1);

		//
		// 		osgUtil::IntersectVisitor ivXY;//�����߶ε��࣬�����б���ڵ㽻��
		//
		// 		// �����µ�λ�õõ������߶μ��        ������ʼ����յ�
		// 		osg::ref_ptr<osg::LineSegment>lineXY = new osg::LineSegment(newPos1, m_vPosition);
		//
		// 		osg::ref_ptr<osg::LineSegment>lineZ = new osg::LineSegment(newPos1 + osg::Vec3(0.0f, 0.0f, 0.4f), newPos1 - osg::Vec3(0.0f, 0.0f, 0.4f));
		//
		// 		if (!lineXY->valid()||!lineZ->valid())
		// 		{
		// 			return false;
		// 		}
		//
		// 		ivXY.addLineSegment(lineZ.get());//����һ���߶ε��б���
		// 		ivXY.addLineSegment(lineXY.get());//����һ���߶ε��б���
		//
		//
		//
		// 		// �ṹ�������
		// 		m_pHostViewer->getSceneData()->accept(ivXY);
		//
		// 		// �õ��µ�λ��
		//
		// 		// ���û����ײ
		// 		if (!ivXY.hits())
		// 		{
		// 			m_vPosition += delta;
		// 		}
	}
	else
	{
		m_vPosition += delta;
	}
	return true;
}

bool TravelManipulator::ChangePositionToTaget(osg::Vec3 taget)
{
	// ��ײ���
	if (m_bPeng)
	{
		osg::Vec3 newPosFrom = taget;
		osg::Vec3 newPosTo = taget + osg::Vec3(0, 0, 10.4);

		osg::Vec3 hitPos;
		float hitFraction;

		if (!m_pPhysicsmgr->rayCastTerrain(newPosFrom, newPosTo, hitPos, hitFraction))
		{
			m_vPosition = taget;
			raiseCameraSignal = false;
		}
		else
		{
			m_vPosition = hitPos + osg::Vec3(0, 0, 1.5);
			raiseCameraSignal = true;
		}
		// 		osg::Vec3 newPos1;
		//
		// 		//newPos1 = m_vPosition + delta;
		// 		newPos1 = taget;
		// 		osg::Vec3 intersectposition;
		//
		//
		// 		osgUtil::IntersectVisitor ivXY;//�����߶ε��࣬�����б���ڵ㽻��
		// 		osgUtil::IntersectVisitor ivZUp;//�����߶ε��࣬�����б���ڵ㽻��
		// 		osgUtil::IntersectVisitor ivZDown;//�����߶ε��࣬�����б���ڵ㽻��
		//
		// 		// �����µ�λ�õõ������߶μ��        ������ʼ����յ�
		// 		osg::ref_ptr<osg::LineSegment>lineXY = new osg::LineSegment(newPos1, m_vPosition);
		//
		// 		osg::ref_ptr<osg::LineSegment>lineZUp = new osg::LineSegment(newPos1 , newPos1 + osg::Vec3(0.0f, 0.0f, 10.4f));
		// 		osg::ref_ptr<osg::LineSegment>lineZDown = new osg::LineSegment(newPos1 + osg::Vec3(0.0f, 0.0f, 10.4f) , newPos1);
		// 		if (!lineXY->valid()||!lineZUp->valid()||!lineZDown->valid())
		// 		{
		// 			return false;
		// 		}
		//
		// 		ivXY.addLineSegment(lineXY.get());//����һ���߶ε��б���
		// 		ivZDown.addLineSegment(lineZDown.get());//����һ���߶ε��б���
		// 		ivZUp.addLineSegment(lineZUp.get());//����һ���߶ε��б���
		//
		//
		// 		// �ṹ�������
		// 		m_pHostViewer->getSceneData()->accept(ivZUp);
		//
		// 		if (ivZUp.hits())
		// 		{
		// 			osgUtil::IntersectVisitor::HitList& hitList = ivZUp.getHitList(lineZUp.get());;
		// 			if (!hitList.empty())
		// 			{
		// 				intersectposition = hitList.front().getWorldIntersectPoint();
		// 			}
		// 			m_vPosition = intersectposition + osg::Vec3(0, 0, 1.5);
		// 			raiseCameraSignal=true;
		// 		}
		//
		//
		// 		// �õ��µ�λ��
		//
		//
		//
		// 		// ���û����ײ
		// 		if (!ivZUp.hits())
		// 		{
		// 			m_vPosition = taget;
		// 			raiseCameraSignal=false;
		// 		}
	}
	else
	{
		m_vPosition = taget;
	}
	return true;
}

/*bool TravelManipulator::SteppingChangePositionToTaget(osg::Vec3 taget);*/

// �����ٶ�
void TravelManipulator::setSpeed(float &sp)
{
	m_fMoveSpeed = sp;
}

// ��õ�ǰ�ٶ�
float TravelManipulator::getSpeed()
{
	return m_fMoveSpeed;
}

// ������ʼ��λ��
void TravelManipulator::SetPosition(osg::Vec3 &position)
{
	m_vPosition = position;
}

// �õ���ǰ����λ��
osg::Vec3 TravelManipulator::GetPosition()
{
	return m_vPosition;
}
// �õ���˵�ʵʱ����
osg::Vec3 TravelManipulator::GetBenchpolePosition()
{
	return BenchpolePos;
}
/**************************************************************************

// �������ܣ�����������
// ���������holeNum    ��ǰ��
// �����������
// ����ֵ��bool��1���ɹ�
// �����ߣ���ԣ��           ���ڣ�2012-3-25
***************************************************************************/
bool TravelManipulator::FollowingCamera()
{
	osg::Vec3 startpoint = playerinfo[BallUpdateCallback::currentnumber].vecBallPreviousPos;
	osg::Vec3 aim;
	aim = GetAimPoint(startpoint);
	mTargetPos = m_pPhysicsmgr->getBallCurrentPosition(); //С���ʵʱλ��

	//��ǰ�����꣬holeData[Current_Hole].mHole
	hole_dir = holeDataMgr->getCurrentPinCor(); //��ȡ��ǰ������
	Tee_dir = holeDataMgr->getCurrentTeeCor(playerinfo[BallUpdateCallback::currentnumber].teeType);
	//balltohole=mTargetPos-hole_dir;//С�����򶴵�λʸ
	osg::Vec3 balltoaim = aim - hole_dir;   //С�����򶴵�λʸ
	balltocam = mTargetPos - GetPosition(); //С���������λʸ
	osg::Vec3 Teetoaim = Tee_dir - aim;
	//���»�ȡС���ٶ�
	speedXYZ = m_pPhysicsmgr->getBallLinarVelocity();
	speedx = speedXYZ.x();
	speedy = speedXYZ.y();
	speedz = speedXYZ.z();
	speed = speedXYZ.length();
	speedXOY = sqrt(speedx * speedx + speedy * speedy); //��XOYƽ���ϵķ�����ģ
	balltocamlength = sqrt(balltocam.x() * balltocam.x() + balltocam.y() * balltocam.y());
	osg::Vec3 ballspeed = osg::Vec3(speedx, speedy, speedz);

	dis = 4; //���������С���ˮƽ����
	dis2 = sqrt(balltocam.x() * balltocam.x() + balltocam.y() * balltocam.y());
	//dis3=6.5;//���������С��Ŀռ����
	dis4 = sqrt(balltoaim.x() * balltoaim.x() + balltoaim.y() * balltoaim.y());
	float height = 1.5;
	//std::cout<<"speed="<<speed<<std::endl;

	if (speed > 10)
	{
		toCam = mTargetPos + osg::Vec3(-dis * speedx / speedXOY, -dis * speedy / speedXOY, height); //��������������Ŀ���

		//toCam = mTargetPos+osg::Vec3(dis*speedx/speedXOY,dis*speedy/speedXOY,height); //��������������Ŀ���
	}
	if (speed <= 10 && speed >= 0)
	{
		toCam = mTargetPos + osg::Vec3(-dis * balltocam.x() / balltocamlength, -dis * balltocam.y() / balltocamlength, height); //����ʱ��������е����Ŀ���

		//toCam = mTargetPos+osg::Vec3(-dis*balltocam.x()/balltocam.length(),-dis*balltocam.y()/balltocam.length(),height);//����ʱ��������е����Ŀ���
	}
	//if (speed==0)
	//{
	//	toCam = mTargetPos+osg::Vec3(dis*balltohole.x()/dis4,dis*balltohole.y()/dis4,height);
	//	//toCam = mTargetPos+osg::Vec3(-dis*balltocam.x()/balltocam.length(),-dis*balltocam.y()/balltocam.length(),height);
	//}

	float followFactor;
	if (speed > 4)
	{
		followFactor = 1.0;
	}
	if (speed < 4)
	{
		followFactor = 1.0;
	}

	pos = (toCam - GetPosition()) * followFactor;
	ChangePosition(pos); //ʹ��changPosition���Լ����ײ���������������pos�ƶ�

	if (speed > 10)
	{
		camangle = CameraComputeAng(ballspeed);
		if (raiseCameraSignal)
		{
			camangle = /*osg::Vec3(0,0,osg::PI)+*/ CameraComputeAngforchangposition(ballspeed);
		}
	}

	if (speed <= 10 && speed >= 0)
	{
		camangle = CameraComputeAng(balltocam);
		if (raiseCameraSignal)
		{
			camangle = CameraComputeAngforchangposition(balltocam);
		}
	}

	/*if (speed==0)
		{
		camangle=osg::Vec3(0,0,osg::PI)+CameraComputeAng(balltohole);
		}*/

	m_vRotation = camangle; //ʵʱ��������ĸ����ǣ�����ת�Ǻ�ƫ����

	return true;
}
/**************************************************************************

// �������ܣ�����������
// ���������holeNum    ��ǰ��
// �����������
// ����ֵ��bool��1���ɹ�
// �����ߣ���ԣ��           ���ڣ�2012-3-25
***************************************************************************/
bool TravelManipulator::ReverCamera()
{

	mTargetPos = m_pPhysicsmgr->getBallCurrentPosition(); //С���ʵʱλ��

	//��ǰ�����꣬holeData[Current_Hole].mHole
	hole_dir = holeDataMgr->getCurrentPinCor(); //��ȡ��ǰ������
	Tee_dir = holeDataMgr->getCurrentTeeCor(playerinfo[BallUpdateCallback::currentnumber].teeType);
	balltohole = mTargetPos - hole_dir;		 //С�����򶴵�λʸ
	balltocam = -mTargetPos + GetPosition(); //�����С���λʸ,д����������������෴��7-12��
	Teetohole = Tee_dir - hole_dir;
	balltocamlength = sqrt(balltocam.x() * balltocam.x() + balltocam.y() * balltocam.y());
	//���»�ȡС���ٶ�
	speedXYZ = m_pPhysicsmgr->getBallLinarVelocity();
	speedx = speedXYZ.x();
	speedy = speedXYZ.y();
	speedz = speedXYZ.z();
	speed = speedXYZ.length();
	speedXOY = sqrt(speedx * speedx + speedy * speedy); //��XOYƽ���ϵķ�����ģ
	osg::Vec3 ballspeed = osg::Vec3(speedx, speedy, speedz);

	dis = 4; //���������С���ˮƽ����
	dis2 = sqrt(balltocam.x() * balltocam.x() + balltocam.y() * balltocam.y());
	//dis3=6.5;//���������С��Ŀռ����
	dis4 = sqrt(balltohole.x() * balltohole.x() + balltohole.y() * balltohole.y());
	float height = 1.5;

	if (speed > 10)
	{
		toCam = mTargetPos + osg::Vec3(dis * speedx / speedXOY, dis * speedy / speedXOY, height); //��������������Ŀ���
	}

	if (speed <= 10 && speed >= 0)
	{
		toCam = mTargetPos + osg::Vec3(dis * balltocam.x() / balltocamlength, dis * balltocam.y() / balltocamlength, height); //����ʱ��������е����Ŀ���
		if (BallUpdateCallback::couter && speedx * balltocam.x() + speedy * balltocam.y() < 0)
		{
			toCam = mTargetPos + osg::Vec3(-dis * balltocam.x() / balltocamlength, -dis * balltocam.y() / balltocamlength, height); //����ʱ��������е����Ŀ���
		}
	}

	// 		if (speed==0)
	// 		{
	// 			toCam = mTargetPos+osg::Vec3(dis*balltohole.x()/dis4,dis*balltohole.y()/dis4,height);
	// 		}

	// 		if (speed==0)
	// 		{
	// 			camangle=CameraComputeAng(balltohole);
	//
	// 		}

	float followFactor;
	if (speed > 4)
	{
		followFactor = 1.0;
	}
	if (speed < 4)
	{
		followFactor = 1.0;
	}

	pos = (toCam - GetPosition()) * followFactor;
	ChangePosition(pos); //ʹ��changPosition���Լ����ײ���������������pos�ƶ�

	if (speed > 10)
	{
		camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(ballspeed);
		if (raiseCameraSignal)
		{
			camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAngforchangposition(ballspeed);
		}
	}

	if (speed <= 10 && speed >= 0)
	{
		camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(balltocam);
		if (raiseCameraSignal)
		{
			camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAngforchangposition(balltocam);
		}
		if (BallUpdateCallback::couter && speedx * balltocam.x() + speedy * balltocam.y() < 0)
		{
			BallUpdateCallback::couter = false;
			camangle = CameraComputeAng(balltocam);
			if (raiseCameraSignal)
			{
				camangle = CameraComputeAngforchangposition(balltocam);
			}
		}
	}

	m_vRotation = camangle; //ʵʱ��������ĸ����ǣ�����ת�Ǻ�ƫ����

	return true;
}
/**************************************************************************

// �������ܣ���������Ƕ�
// ���������x��y    ��άʸ����ǰ��ά
// ���������sita  ƫ����
// ����ֵ��sita
// �����ߣ���ԣ��           ���ڣ�2012-3-7
***************************************************************************/

osg::Vec3 TravelManipulator::CameraComputeAng(osg::Vec3 a)
{
	float sita;
	sita = 3.14 + acos(-a.y() / sqrt(a.x() * a.x() + a.y() * a.y()));
	if (a.x() < 0)
	{
		sita = 2 * osg::PI - sita;
	}
	osg::Vec3 CamAng = osg::Vec3(0.93 * osg::PI_2, 0, sita);

	return CamAng;
}
/**************************************************************************

// �������ܣ���������Ƕ�
// ���������x��y    ��άʸ����ǰ��ά
// ���������sita  ƫ����
// ����ֵ��sita
// �����ߣ���ԣ��           ���ڣ�2012-6-18
***************************************************************************/

osg::Vec3 TravelManipulator::CameraComputeAngforchangposition(osg::Vec3 a)
{
	float sita;
	float realdisz = m_vPosition.z() - m_pPhysicsmgr->getBallCurrentPosition().z();
	sita = 3.14 + acos(-a.y() / sqrt(a.x() * a.x() + a.y() * a.y()));
	if (a.x() < 0)
	{
		sita = 2 * osg::PI - sita;
	}
	osg::Vec3 CamAng = osg::Vec3(0.93 * osg::PI_2, 0, sita);

	if (raiseCameraSignal)
	{
		float disxoy = 4.0;
		float disz = 1.5;
		float elveOfpitchingangle = atan(realdisz / disxoy) - atan(disz / disxoy);
		CamAng = osg::Vec3(0.93 * osg::PI_2 - elveOfpitchingangle, 0, sita);
	}
	return CamAng;
}

/**************************************************************************

 // �������ܣ���������������
 // ���������ballPos������㣬�����Ҫ��׼�ĵ㣻TagetPos������㣬ballPosָ��TagetPos�ķ���Ϊ���Ҫע�ӵķ���
 // ���������CamPos  ���������λ�á�
 // ����ֵ����ά����CamPos
 // �����ߣ���ԣ��           ���ڣ�2012-4-23
 ***************************************************************************/
osg::Vec3 TravelManipulator::CameraComputePos(osg::Vec3 ballPos, osg::Vec3 TagetPos)
{
	osg::Vec3 TargetToBall = ballPos - TagetPos;
	TargetToBall = osg::Vec3(TargetToBall.x(), TargetToBall.y(), 0);
	TargetToBall.normalize();
	osg::Vec3 CamPos = ballPos + osg::Vec3(4 * TargetToBall.x(), 4 * TargetToBall.y(), 1.5);
	return CamPos;
	/*float ABlenght=sqrt((ballPos-TagetPos).x()*(ballPos-TagetPos).x()+(ballPos-TagetPos).y()*(ballPos-TagetPos).y());
	 osg::Vec2 vecAtoB=osg::Vec2((TagetPos-ballPos).x()/ABlenght,(TagetPos-ballPos).y()/ABlenght);
	 float camX=-4*vecAtoB.x();
	 float camY=-4*vecAtoB.y();
	 osg::Vec3 CamPos=ballPos+osg::Vec3(camX,camY,1.5);
	 return CamPos;*/
}

//
//
//
bool TravelManipulator::initialCamera()
{

	//holeDataMgr->setCurrentPinCor(playerinfo[0].teeType);
	//��ʼ�����λ���ڵ�һ����Աѡ��Teę
	ChangePositionToTaget(CameraComputePos(holeDataMgr->getCurrentTeeCor(playerinfo[0].teeType), GetAimPoint(m_pPhysicsmgr->getBallCurrentPosition())));

	//��ʼ������Ƕ��ڵ�һ����Աѡ��Teę
	m_vRotation = CameraComputeAng(GetAimPoint(m_pPhysicsmgr->getBallCurrentPosition()) - holeDataMgr->getCurrentTeeCor(playerinfo[0].teeType));

	return true;
}

/**************************************************************************
 // �������ܣ���̬���
 // �����������
 // 
 // 
 ***************************************************************************/
bool TravelManipulator::Dynamic_Camera()
{
	//��һ���ڽ���ѡ��̬�����ֹʱҲִ�ж�̬�������
	if (!BallUpdateCallback::ResponseToStop)
	{
		mTargetPos = m_pPhysicsmgr->getBallCurrentPosition(); //��ֹʱС��λ��
		if (!markflag)
		{
			osg::Vec3 aimtoball = mTargetPos - GetAimPoint(mTargetPos); //aim��ָ���������
			osg::Vec3 aim_ball = osg::Vec3(aimtoball.x(), aimtoball.y(), 0);
			aim_ball.normalize();
			toCam = mTargetPos + osg::Vec3(4 * aim_ball.x(), 4 * aim_ball.y(), 1.5);
			camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(aimtoball);
			m_vRotation = camangle;
		}
		else if (markflag)
		{
			osg::Vec3 benchtoball = mTargetPos - benchpos; //�������ָ���������
			osg::Vec3 bench_ball = osg::Vec3(benchtoball.x(), benchtoball.y(), 0);
			bench_ball.normalize();
			toCam = mTargetPos + osg::Vec3(4 * bench_ball.x(), 4 * bench_ball.y(), 1.5);
			camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(benchtoball);
			m_vRotation = camangle;
		}
	}
	else if (BallUpdateCallback::ResponseToStop)
	{
		osg::Vec3 startpoint = playerinfo[BallUpdateCallback::currentnumber].vecBallPreviousPos;
		if (!markflag)
		{
			if (!initialangle)
			{
				osg::Vec3 aimtoball = mTargetPos - GetAimPoint(startpoint); //aim��ָ���������
				camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(aimtoball);
				m_vRotation = camangle;
				initialangle = true;
			}
			osg::Vec3 aimtostart = startpoint - GetAimPoint(startpoint); //aim��ָ���������
			osg::Vec3 aim_start = osg::Vec3(aimtostart.x(), aimtostart.y(), 0);
			aim_start.normalize();
			toCam = startpoint + osg::Vec3(4 * aim_start.x(), 4 * aim_start.y(), 1.5);
			mTargetPos = m_pPhysicsmgr->getBallCurrentPosition();
			balltocam = mTargetPos - GetPosition(); //С���������λʸ
			Dynamic_angle(balltocam, camangle);
			m_vRotation = camangle;
		}
		else if (markflag)
		{
			if (!initialangle)
			{
				osg::Vec3 benchtoball = mTargetPos - benchpos; //aim��ָ���������
				camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(benchtoball);
				m_vRotation = camangle;
				initialangle = true;
			}
			osg::Vec3 benchtostart = startpoint - benchpos; //aim��ָ���������
			osg::Vec3 bench_start = osg::Vec3(benchtostart.x(), benchtostart.y(), 0);
			bench_start.normalize();
			toCam = startpoint + osg::Vec3(4 * bench_start.x(), 4 * bench_start.y(), 1.5);
			mTargetPos = m_pPhysicsmgr->getBallCurrentPosition();
			balltocam = mTargetPos - GetPosition(); //С���������λʸ
			Dynamic_angle(balltocam, camangle);
			m_vRotation = camangle;
		}
	}
	pos = toCam - GetPosition();
	ChangePosition(pos); //ʹ��changPosition���Լ����ײ���������������pos�ƶ�
	//m_vRotation = camangle;//ʵʱ��������ĸ����ǣ�����ת�Ǻ�ƫ����

	return true;
}

/**************************************************************************
 // �������ܣ���̬����Ƕȼ���
 // ����������������ʸ��
 // 
 ***************************************************************************/
void TravelManipulator::Dynamic_angle(osg::Vec3 Ball_Camera, osg::Vec3 &Angle)
{
	osg::Vec3 eye;
	osg::Vec3 at;
	osg::Vec3 up;
	//�õ��ӽǷ��������,�����䵥λ��
	_viewer->getCamera()->getViewMatrixAsLookAt(eye, at, up);
	osg::Vec3 viewDir = at - eye;

	osg::Vec3 viewDirXOY = osg::Vec3(viewDir.x(), viewDir.y(), 0);
	viewDir.normalize();

	Ball_Camera.normalize();
	//�õ��ӽǷ���������������������нǣ���λ��
	float Zangle;
	osg::Vec3 Znormal = osg::Vec3(0, 0, 1);
	Zangle = acos(Ball_Camera * Znormal);
	//ƫ������ת�Ƕ�
	osg::Vec3 ballcameraXOY = osg::Vec3(Ball_Camera.x(), Ball_Camera.y(), 0);
	ballcameraXOY.normalize();
	viewDirXOY.normalize();
	osg::Vec3 CrossMult = viewDirXOY ^ ballcameraXOY;
	float HorizAngle;
	HorizAngle = asin(CrossMult.z()); //����Ϊ��������Ϊ��

	//��������ת�Ƕ�
	float VertAngle;
	VertAngle = acos(Znormal * viewDir) - acos(Znormal * Ball_Camera);

	if (Zangle <= (osg::PI_2))
	{
		Angle = Angle + osg::Vec3(VertAngle, 0, HorizAngle);
	}
	if (Zangle > (osg::PI_2))
	{
		Angle = Angle + osg::Vec3(osg::PI_2 - Angle.x(), 0, HorizAngle);
	}
}

/**************************************************************************
 // �������ܣ�TVģʽ����Ƕȼ���
 // ����������������ʸ��
 // 
 ***************************************************************************/
void TravelManipulator::TV_angle(osg::Vec3 Ball_Camera, osg::Vec3 &Angle)
{
	osg::Vec3 eye;
	osg::Vec3 at;
	osg::Vec3 up;
	//�õ��ӽǷ��������,�����䵥λ��
	_viewer->getCamera()->getViewMatrixAsLookAt(eye, at, up);
	osg::Vec3 viewDir = at - eye;

	osg::Vec3 viewDirXOY = osg::Vec3(viewDir.x(), viewDir.y(), 0);
	viewDir.normalize();

	Ball_Camera.normalize();
	//�������ʸ����Z��н�
	float Zangle;
	osg::Vec3 Znormal = osg::Vec3(0, 0, 1);
	Zangle = acos(Ball_Camera * Znormal);
	//ƫ������ת�Ƕ�
	osg::Vec3 ballcameraXOY = osg::Vec3(Ball_Camera.x(), Ball_Camera.y(), 0);
	ballcameraXOY.normalize();
	viewDirXOY.normalize();
	osg::Vec3 CrossMult = viewDirXOY ^ ballcameraXOY;
	float HorizAngle;
	HorizAngle = asin(CrossMult.z()); //����Ϊ��������Ϊ��
	if (Zangle < osg::PI_2)
	{
		//��������ת�Ƕ�
		float VertAngle;
		VertAngle = acos(Znormal * viewDir) - acos(Znormal * Ball_Camera);
		Angle = Angle + osg::Vec3(VertAngle, 0, HorizAngle);
	}
	else if (Zangle >= osg::PI_2)
	{
		Angle = Angle + osg::Vec3(osg::PI / 2 - Angle.x(), 0, HorizAngle);
	}
}

/**************************************************************************
 // �������ܣ�TVģʽ���
 // �����������
 // 
 ***************************************************************************/
bool TravelManipulator::TV_Camera()
{
	//��һ���ڽ���ѡ��̬�����ֹʱҲִ��TV�������
	if (!BallUpdateCallback::ResponseToStop)
	{
		mTargetPos = m_pPhysicsmgr->getBallCurrentPosition(); //��ֹʱС��λ��
		if (!markflag)
		{
			osg::Vec3 aimtoball = mTargetPos - GetAimPoint(mTargetPos); //aim��ָ���������
			osg::Vec3 aim_ball = osg::Vec3(aimtoball.x(), aimtoball.y(), 0);
			aim_ball.normalize();
			toCam = mTargetPos + osg::Vec3(4 * aim_ball.x(), 4 * aim_ball.y(), 1.5);
			camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(aimtoball);
			m_vRotation = camangle;
		}
		else if (markflag)
		{
			osg::Vec3 benchtoball = mTargetPos - benchpos; //�������ָ���������
			osg::Vec3 bench_ball = osg::Vec3(benchtoball.x(), benchtoball.y(), 0);
			bench_ball.normalize();
			toCam = mTargetPos + osg::Vec3(4 * bench_ball.x(), 4 * bench_ball.y(), 1.5);
			camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(benchtoball);
			m_vRotation = camangle;
		}
	}
	//�����������TVģʽ���
	else if (BallUpdateCallback::ResponseToStop)
	{
		//����ɵĵ�����
		osg::Vec3 startpoint = playerinfo[BallUpdateCallback::currentnumber].vecBallPreviousPos;
		if (!markflag)
		{
			//����л���
			osg::Vec3 ChangePoint;
			osg::Vec3 purposepoint;
			//��ʼ�����λ�úͽǶ�
			if (!initialangle)
			{
				osg::Vec3 aimtoball = mTargetPos - GetAimPoint(startpoint); //aim��ָ���������
				camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(aimtoball);
				m_vRotation = camangle;
				initialangle = true;
			}
			//�����׶����λ�úͽǶȣ��ж�С��ʲôʱ��ʼ�½���������С���½����������λ�ú�ת��
			if (!changecamera)
			{
				osg::Vec3 aimtostart = startpoint - GetAimPoint(startpoint); //aim��ָ���������
				osg::Vec3 aim_start = osg::Vec3(aimtostart.x(), aimtostart.y(), 0);
				aim_start.normalize();
				toCam = startpoint + osg::Vec3(4 * aim_start.x(), 4 * aim_start.y(), 1.5);
				mTargetPos = m_pPhysicsmgr->getBallCurrentPosition();
				balltocam = mTargetPos - GetPosition(); //С���������λʸ
				Dynamic_angle(balltocam, camangle);
				m_vRotation = camangle;

				if (maxheight < mTargetPos.z())
				{
					maxheight = mTargetPos.z();
				}
				else if (maxheight >= mTargetPos.z())
				{
					ChangePoint = mTargetPos;
					GetPointAngle(toCam, ChangePoint, purposepoint, camangle);
					toCam = purposepoint;
					m_vRotation = camangle;
					changeangle = camangle;
					changecamera = true;
				}
			}

			//�½��׶�
			else if (changecamera)
			{
				mTargetPos = m_pPhysicsmgr->getBallCurrentPosition();
				balltocam = mTargetPos - GetPosition();
				speedXYZ = m_pPhysicsmgr->getBallLinarVelocity();

				osg::Vec3 Znormal(0, 0, 1);
				//�������ʸ����Z��н�
				osg::Vec3 balltocam_Nor = balltocam;
				balltocam_Nor.normalize();
				float Zangle = acos(balltocam_Nor * Znormal);
				if ((balltocam.length() >= 6) && (Zangle > osg::PI - changeangle.x()))
				{
					TV_angle(balltocam, camangle);
					m_vRotation = camangle;
				}
				else if ((balltocam.length() < 6) || (speedXYZ.length() <= 4))
				{
					toCam = mTargetPos + osg::Vec3(-5 * balltocam_Nor.x(), -5 * balltocam_Nor.y(), 1.5);
					TV_angle(balltocam, camangle);
					m_vRotation = camangle;
				}
			}
		}
		else if (markflag)
		{
			//����л���
			osg::Vec3 ChangePoint;
			osg::Vec3 purposepoint;
			//��ʼ�����λ�úͽǶ�
			if (!initialangle)
			{
				osg::Vec3 benchtoball = mTargetPos - benchpos; //aim��ָ���������
				camangle = osg::Vec3(0, 0, osg::PI) + CameraComputeAng(benchtoball);
				m_vRotation = camangle;
				initialangle = true;
			}
			//�����׶����λ�úͽǶȣ��ж�С��ʲôʱ��ʼ�½���������С���½����������λ�ú�ת��
			if (!changecamera)
			{
				osg::Vec3 benchtostart = startpoint - benchpos; //aim��ָ���������
				osg::Vec3 bench_start = osg::Vec3(benchtostart.x(), benchtostart.y(), 0);
				bench_start.normalize();
				toCam = startpoint + osg::Vec3(4 * bench_start.x(), 4 * bench_start.y(), 1.5);
				mTargetPos = m_pPhysicsmgr->getBallCurrentPosition();
				balltocam = mTargetPos - GetPosition(); //С���������λʸ
				Dynamic_angle(balltocam, camangle);
				m_vRotation = camangle;

				if (maxheight < mTargetPos.z())
				{
					maxheight = mTargetPos.z();
				}
				else if (maxheight >= mTargetPos.z())
				{
					ChangePoint = mTargetPos;
					GetPointAngle(toCam, ChangePoint, purposepoint, camangle);
					toCam = purposepoint;
					m_vRotation = camangle;
					changeangle = camangle;
					changecamera = true;
				}
			}
			//�½��׶�
			else if (changecamera)
			{
				mTargetPos = m_pPhysicsmgr->getBallCurrentPosition();
				balltocam = mTargetPos - GetPosition();
				speedXYZ = m_pPhysicsmgr->getBallLinarVelocity();
				toCam = purposepoint;

				osg::Vec3 Znormal(0, 0, 1);
				//�������ʸ����Z��н�
				osg::Vec3 balltocam_Nor = balltocam;
				balltocam_Nor.normalize();
				float Zangle = acos(balltocam_Nor * Znormal);
				if ((balltocam.length() >= 6) && (Zangle > (osg::PI - changeangle.x())))
				{
					TV_angle(balltocam, camangle);
					m_vRotation = camangle;
				}
				else if ((balltocam.length() < 6) || (speedXYZ.length() <= 4))
				{
					toCam = mTargetPos + osg::Vec3(-5 * balltocam_Nor.x(), -5 * balltocam_Nor.y(), 1.5);
					TV_angle(balltocam, camangle);
					m_vRotation = camangle;
				}
			}
		}
	}
	pos = (toCam - GetPosition());
	ChangePosition(pos); //ʹ��changPosition���Լ����ײ���������������pos�ƶ�
	//m_vRotation = camangle;//ʵʱ��������ĸ����ǣ�����ת�Ǻ�ƫ����
	return true;
}
/**************************************************************************
 // �������ܣ�TVģʽ�����С���½�ʱ���Ӧ���õĵ�
 // ��������������ʼλ��    С����ߵ�
 // ���������С���½��׶����λ�á�����Ƕ�
 ***************************************************************************/
void TravelManipulator::GetPointAngle(osg::Vec3 campos, osg::Vec3 changepoint, osg::Vec3 &point, osg::Vec3 &angle)
{
	osg::Vec3 CamtoPoint;
	CamtoPoint = changepoint - campos;
	//������л���ʸ����ˮƽ���ʸ������
	CamtoPoint = osg::Vec3(CamtoPoint.x(), CamtoPoint.y(), 0);
	//���ˮƽ�ƶ�����
	float Trans_Dis = 2 * CamtoPoint.length() + 5;
	CamtoPoint.normalize();

	osg::Vec3 Point_Up = campos + osg::Vec3(Trans_Dis * CamtoPoint.x(), Trans_Dis * CamtoPoint.y(), 300);
	osg::Vec3 Point_Down = campos + osg::Vec3(Trans_Dis * CamtoPoint.x(), Trans_Dis * CamtoPoint.y(), -300);
	float fraction;
	osg::Vec3 Point;
	m_pPhysicsmgr->rayCastTerrain(Point_Up, Point_Down, Point, fraction);
	//���λ��
	point = Point + osg::Vec3(0, 0, 1.5);
	//����Ƕ�
	angle = angle + osg::Vec3(-osg::PI / 10, 0, osg::PI);
}
/**************************************************************************
 // �������ܣ����������aim��ȷ��
 // �����������
 // ���������osg::Vec3   AimPoint   ���aim��
 ***************************************************************************/
osg::Vec3 TravelManipulator::GetAimPoint(osg::Vec3 BallCor)
{
	osg::Vec3 Aimpoint;
	hole_dir = holeDataMgr->getCurrentPinCor();														//��ȡ��ǰ������
	Tee_dir = holeDataMgr->getCurrentTeeCor(playerinfo[BallUpdateCallback::currentnumber].teeType); //��ǰTeę����

	osg::Vec3 TeeAim;
	TeeAim = holeDataMgr->getCurrentAimCor(playerinfo[BallUpdateCallback::currentnumber].teeType);

	osg::Vec3 AimtoHole;
	AimtoHole = hole_dir - TeeAim;
	AimtoHole = osg::Vec3(AimtoHole.x(), AimtoHole.y(), 0);
	//��teę��Ӧaim�㵽�򶴾���С��ʱ�������꼴Ϊaim������
	if (AimtoHole.length() <= 100)
	{
		Aimpoint = hole_dir;
	}
	//��teę��Ӧaim�㵽�򶴾������ʱ�������aim���л�
	else if (AimtoHole.length() > 100)
	{
		balltohole = BallCor - hole_dir; //С�����򶴵�λʸ
		balltohole = osg::Vec3(balltohole.x(), balltohole.y(), 0);
		osg::Vec3 balltoaim = BallCor - TeeAim;
		balltoaim = osg::Vec3(balltoaim.x(), balltoaim.y(), 0);

		if ((balltohole.length() <= (AimtoHole.length() + 70)) || (balltohole.length() <= balltoaim.length()))
		{
			Aimpoint = hole_dir;
		}
		else
		{
			Aimpoint = TeeAim;
		}
	}
	//cout<<"Aimpoint :"<<"("<<Aimpoint.x()<<","<<Aimpoint.y()<<","<<Aimpoint.z()<<")"<<endl<<endl;
	return Aimpoint;
}
/**************************************************************************
 // �������ܣ��õ�������С��aim��ľ��룬��λΪ��
 // �����������
 // ���������float    aimdistance
 ***************************************************************************/
float TravelManipulator::GetAimDis()
{
	float aimdistance;
	if ((!BallUpdateCallback::ResponseToStop) && (BallUpdateCallback::playgolfing))
	{
		mTargetPos = m_pPhysicsmgr->getBallCurrentPosition(); //С���ʵʱλ��
		aimdistance = (((mTargetPos - GetAimPoint(mTargetPos)).length()) * 100) / 100.0;
	}
	return aimdistance;
}

float TravelManipulator::computeCameraToBenchpoleDis(osg::Vec3 BenchpolePos)
{
	float CameraToBallDis = 0.0f;
	CameraToBallDis = (((GetPosition() - BenchpolePos).length()) * 100) / 100.0;

	return CameraToBallDis;
}

osg::Vec3 TravelManipulator::computeCameraToTargetAng(osg::Vec3 BenchpolePos)
{
	osg::Vec3 CameraToTargetAng(0, 0, 0);
	CameraToTargetAng = (GetPosition() - BenchpolePos);

	return CameraToTargetAng;
}

/**************************************************************************
  // �������ܣ������������ȷ��һ���㣬��ȷ�����γ���
  // �����������
  // ���������osg::Vec3 PiontFromm_vRotation   
  ***************************************************************************/
osg::Vec3 TravelManipulator::getPiontFromm_vRotation()
{
	float sita = m_vRotation.z() + osg::PI_2;
	float piontX = m_pPhysicsmgr->getBallCurrentPosition().x() + 200.1 * cos(sita);
	float piontY = m_pPhysicsmgr->getBallCurrentPosition().y() + 200.1 * sin(sita);
	float piontZ = m_pPhysicsmgr->getBallCurrentPosition().z();
	osg::Vec3 PiontFromm_vRotation = osg::Vec3(piontX, piontY, piontZ);
	return PiontFromm_vRotation;
}