
#version 150 core 

in vec3 InstancePosition; 

uniform mat4 MVP; 

out vec4 OrigPosition; 
flat out int objectVisible; 


void main(void) { 

   OrigPosition = vec4(InstancePosition,1.0); 

   vec4 SSpos = MVP * OrigPosition; 
   bvec4 outside; 

   outside.w = SSpos.w<0.0; 
   SSpos = abs(SSpos); 
   outside.xyz = greaterThan(SSpos.xyz,vec3(SSpos.w,SSpos.w,SSpos.w)); 
   objectVisible = int(!any(outside)); 

} 