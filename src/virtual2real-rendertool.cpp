#include <osg/TexMat>
#include "Camera.h"
#include "INIReader.h"
#include "PointCloud.h"
#include "Util.h"
#include "Render2ImgCallback.h"

bool SET_TEXTURE = true;

using namespace virtual2real;

int remdertool_main()
//int main()
{
	// Read Config file
	INIReader reader("config/config.ini");
	string CALIB_FILE_PATH = reader.Get("Config", "CALIB_FILE_PATH", "");


	//创建Viewer对象
	osg::ref_ptr<osgViewer::Viewer>viewer = new osgViewer::Viewer();
	osg::ref_ptr<osg::PositionAttitudeTransform> scene = new osg::PositionAttitudeTransform;

	// 读取地形 Set the ground (only receives shadow)
	CUtil *utl = new CUtil();
	osg::ref_ptr<osg::MatrixTransform> groundNode = new osg::MatrixTransform;
	utl->drawFloor(groundNode);
	//scene->addChild(groundNode);

	// Dyna Human 
	//osg::ref_ptr<osg::PositionAttitudeTransform> dynaPT = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::Node> dynaNode = osgDB::readNodeFile("./model/dyna_male_50002_hips_81.osgt");
	//scene->addChild(dynaNode);

	if (SET_TEXTURE)
	{
		// set up the texture state.
		osg::Texture2D* texture = new osg::Texture2D;
		texture->setDataVariance(osg::Object::DYNAMIC); // protect from being optimized away as static state.
		texture->setBorderColor(osg::Vec4(1.0f, 1.0f, 1.0f, 0.5f)); // only used when wrap is set to CLAMP_TO_BORDER
		osg::Image* img;
		img = osgDB::readImageFile("./model/checkerboard.jpg");

		if (!img)
			std::cout << "Texture image not found!" << std::endl;

		texture->setImage(img);
		texture->setWrap(osg::Texture2D::WrapParameter::WRAP_S, osg::Texture2D::REPEAT);
		texture->setWrap(osg::Texture2D::WrapParameter::WRAP_T, osg::Texture2D::REPEAT);
		texture->setWrap(osg::Texture2D::WrapParameter::WRAP_R, osg::Texture2D::REPEAT);

		osg::Material *material = new osg::Material;
		osg::StateSet* stateset = dynaNode->getOrCreateStateSet();

		stateset->setTextureAttribute(0, texture, osg::StateAttribute::ON);
		stateset->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
		stateset->setTextureMode(0, GL_TEXTURE_GEN_S, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
		stateset->setTextureMode(0, GL_TEXTURE_GEN_T, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
		stateset->setAttribute(material, osg::StateAttribute::OVERRIDE);
		stateset->setMode(GL_BLEND, osg::StateAttribute::ON);
		stateset->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

		float scale = 10;
		osg::TexMat* texmat = new osg::TexMat;
		texmat->setScaleByTextureRectangleSize(true); 
		texmat->setMatrix(osg::Matrix::scale(scale, scale, 1.0));
		stateset->setTextureAttributeAndModes(0, texmat, osg::StateAttribute::ON);

	}

	// Point cloud
	osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform; 
	string PLY_FILE_NAME = reader.Get("Config", "PLY_FILE_NAME", "");
	CPointCloud* pointCloud = new CPointCloud();
	pointCloud->loadPointCloud(PLY_FILE_NAME, ptcNode);
	scene->addChild(ptcNode);


	// VGA / HD / Kinect / Projector
	CCamUtil* camUtil = CCamUtil::getInstance();
	camUtil->_viewer = viewer;
	camUtil->sceneNode = scene.get();
	camUtil->initialVGA(CALIB_FILE_PATH, scene);
	camUtil->initialHD(CALIB_FILE_PATH, scene);
	camUtil->initialKinect(CALIB_FILE_PATH, scene);
	camUtil->calcDomeCent();

	// Main Viewer
	{
		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 0;
		traits->y = yoffset + 0;
		traits->width = 640;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// add this slave camera to the viewer, with a shift left of the projection matrix
		viewer->addSlave(camera.get(), osg::Matrixd::translate(0.0, 0.0, 0.0), osg::Matrixd());
		std::cout << "Create main slave 0: " << traits->x << "," << traits->y << "," << traits->width << "," << traits->height << endl;
		
		// HD/VGA/Kinect Slave
		// camUtil->getSetOrCreateSlaveViewer(HD_PANEL_INDEX, 0); // HD 
		// viewer->addSlave(camUtil->mainSlave);
		// camUtil->mainSlave->setPostDrawCallback(SnapImage::getInstance());
		// camUtil->mainSlave->setUpdateCallback(new Render2ImgCallback(HD_NODE_MASK));

		camUtil->getSetOrCreateSlaveViewer(1, 1);	//VGA 01_01
		viewer->addSlave(camUtil->vgaSlave);
		camUtil->vgaSlave->setPostDrawCallback(SnapImage::getInstance());
		camUtil->vgaSlave->setUpdateCallback(new Render2ImgCallback(VGA_NODE_MASK));


	}

	// 优化场景数据
	osgUtil::Optimizer optimizer;
	optimizer.optimize(scene.get());

	viewer->setLightingMode(osg::View::SKY_LIGHT);
	utl->setLight(scene);

	viewer->setCameraManipulator(new osgGA::TrackballManipulator);
	viewer->setSceneData(scene);
	viewer->realize();

	CCamUtil::getInstance()->rendering2Img = true;


	viewer->run();
	return 0;
}

