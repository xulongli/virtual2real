
#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
// include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/Group>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osgViewer/Viewer>
#include <osg/PositionAttitudeTransform>
#include <osg/LineWidth>
#include <osgText/Text>
#include <iostream>
#include <AntTweakBar/AntTweakBar.h>
#include "Camera.h"

TwBar *g_twBar = NULL;
char g_fileName[64] = "lz.osg";
float position[3] = { 0.0f };
float rotation[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

// Shapes scale
float g_Zoom = 1.0f;

// Light parameter
float g_LightMultiplier = 1.0f;
float g_LightDirection[] = { -0.57735f, -0.57735f, -0.57735f };

// Shapes material
float g_MatAmbient[] = { 0.5f, 0.0f, 0.0f, 1.0f };
float g_MatDiffuse[] = { 1.0f, 1.0f, 0.0f, 1.0f };

// Auto rotate
int g_AutoRotate = 0;
int g_RotateTime = 0;
float g_RotateStart[] = { 0.0f, 0.0f, 0.0f, 1.0f };

// Projector
bool g_Proj[5] = { true };
bool g_VGA[24 * 20] = { true };
bool g_HD[24 * 20] = { true };
bool g_Kinect[10] = { true };

//VGA
int g_CurrentVgaPanelIdx = 1;
int g_CurrentVgaCamIdx = 1;
int g_CurrentHdCamIdx = 1;
int g_CurrentKinectCamIdx = 1;
int g_CurrentProjCamIdx = 1;

int g_PreVgaPanelIdx = 1;
int g_PreVgaCamIdx = 1;
int g_PreHdCamIdx = 1;
int g_PreKinectCamIdx = 1;
int g_PreProjCamIdx = 1;


class TwGUIManager : public osgGA::GUIEventHandler, public osg::Camera::DrawCallback
{
public:
	TwGUIManager(osg::Group* node = 0) : _scene(node)
	{
		TwInit(TW_OPENGL, NULL);
		g_twBar = TwNewBar("virtual2real");
		_util = CCamUtil::getInstance();
		initializeTwGUI();
	}

	TwGUIManager(const TwGUIManager& copy, const osg::CopyOp& op = osg::CopyOp::SHALLOW_COPY)
		: _eventsToHandle(copy._eventsToHandle), _scene(copy._scene)
	{
	}

	META_Object(osg, TwGUIManager)

	static void TW_CALL loadModelFunc(void* clientData)
	{
		TwGUIManager* manager = (TwGUIManager*)clientData;
		if (manager && manager->_scene.valid())
		{
			manager->_scene->removeChild(0, manager->_scene->getNumChildren());
			manager->_scene->addChild(osgDB::readNodeFile(g_fileName));
		}
	}

	static void TW_CALL Rendering2Img(void* clientData)
	{
		TwGUIManager* manager = (TwGUIManager*)clientData;
		if (manager && manager->_scene.valid())
		{
			CCamUtil::getInstance()->rendering2Img = true;
		}
	}


	void initializeTwGUI()
	{
		TwDefine(" virtual2real size='240 600' color='0 82 156' ");

		//TwAddVarRW(g_twBar, "ModelName", TW_TYPE_CSSTRING(sizeof(g_fileName)), g_fileName, " label='Model name' ");
		//TwAddButton(g_twBar, "LoadButton", TwGUIManager::loadModelFunc, this, " label='Load model from file' ");
		TwAddSeparator(g_twBar, NULL, NULL);
		// TwAddVarRW(g_twBar, "PosX", TW_TYPE_FLOAT, &(position[0]), " step=0.1 ");
		// TwAddVarRW(g_twBar, "PosY", TW_TYPE_FLOAT, &(position[1]), " step=0.1 ");
		// TwAddVarRW(g_twBar, "PosZ", TW_TYPE_FLOAT, &(position[2]), " step=0.1 ");
		TwAddVarRW(g_twBar, "Rotation", TW_TYPE_QUAT4F, &(rotation[0]), NULL);

		// Add 'g_Zoom' to 'bar': this is a modifable (RW) variable of type TW_TYPE_FLOAT. Its key shortcuts are [z] and [Z].
		TwAddVarRW(g_twBar, "Zoom", TW_TYPE_FLOAT, &g_Zoom,
			" min=0.01 max=2.5 step=0.01 keyIncr=z keyDecr=Z help='Scale the object (1=original size).' ");

		// Add callback to toggle auto-rotate mode (callback functions are defined above).
		//TwAddVarCB(g_twBar, "AutoRotate", TW_TYPE_BOOL32, SetAutoRotateCB, GetAutoRotateCB, NULL,
		//	" label='Auto-rotate' key=space help='Toggle auto-rotate mode.' ");

		// Add 'g_LightMultiplier' to 'bar': this is a variable of type TW_TYPE_FLOAT. Its key shortcuts are [+] and [-].
		TwAddVarRW(g_twBar, "Multiplier", TW_TYPE_FLOAT, &g_LightMultiplier,
			" label='Light booster' min=0.1 max=4 step=0.02 keyIncr='+' keyDecr='-' help='Increase/decrease the light power.' ");

		// Add 'g_LightDirection' to 'bar': this is a variable of type TW_TYPE_DIR3F which defines the light direction
		TwAddVarRW(g_twBar, "LightDir", TW_TYPE_DIR3F, &g_LightDirection,
			" label='Light direction' opened=true help='Change the light direction.' ");

		// Add 'g_MatAmbient' to 'bar': this is a variable of type TW_TYPE_COLOR3F (3 floats color, alpha is ignored)
		// and is inserted into a group named 'Material'.
		TwAddVarRW(g_twBar, "Ambient", TW_TYPE_COLOR3F, &g_MatAmbient, " group='Material' ");

		// Add 'g_MatDiffuse' to 'bar': this is a variable of type TW_TYPE_COLOR3F (3 floats color, alpha is ignored)
		// and is inserted into group 'Material'.
		TwAddVarRW(g_twBar, "Diffuse", TW_TYPE_COLOR3F, &g_MatDiffuse, " group='Material' ");


		//{
		//	TwAddVarRW(g_twBar, "Camera", TW_TYPE_BOOLCPP, &_util->draw_cam, " group='visCam' ");
		//	TwAddVarRW(g_twBar, "Axis", TW_TYPE_BOOLCPP, &_util->draw_cam_axis, " group='visCam' ");
		//	TwAddVarRW(g_twBar, "Index", TW_TYPE_BOOLCPP, &_util->draw_cam_index, " group='visCam' ");
		//
		//}
		{
			TwAddVarRW(g_twBar, "VGA Panel", TW_TYPE_INT8, &g_CurrentVgaPanelIdx,
				" min=1 max=20 step=1  help='Change to VGA view (1=original). ' group='VGA' ");

			TwAddVarRW(g_twBar, "VGA Camera", TW_TYPE_INT8, &g_CurrentVgaCamIdx,
				" min=1 max=24 step=1 keyIncr=v keyDecr=V help='Change to VGA view (1=original). ' group='VGA' ");

			TwAddVarRW(g_twBar, "Vis VGA Camera", TW_TYPE_BOOLCPP, &_util->visCam[0], " group='VGA' ");
			TwAddVarRW(g_twBar, "Vis VGA Axis", TW_TYPE_BOOLCPP, &_util->visCamAxis[0], " group='VGA' ");
			TwAddVarRW(g_twBar, "Vis VGA Label", TW_TYPE_BOOLCPP, &_util->visCamIndex[0], " group='VGA' ");
		}
		{
			TwAddVarRW(g_twBar, "HD Index", TW_TYPE_INT8, &g_CurrentHdCamIdx,
				" min=1 max=31 step=1 keyIncr=d keyDecr=D help='Change to HD view (1=original). ' group='HD' ");
			TwAddVarRW(g_twBar, "vis HD Camera", TW_TYPE_BOOLCPP, &_util->visCam[1], " group='HD' ");
			TwAddVarRW(g_twBar, "vis HD Axis", TW_TYPE_BOOLCPP, &_util->visCamAxis[1], " group='HD' ");
			TwAddVarRW(g_twBar, "vis HD Label", TW_TYPE_BOOLCPP, &_util->visCamIndex[1], " group='HD' ");
		}
		{
			TwAddVarRW(g_twBar, "Kinect Index", TW_TYPE_INT8, &g_CurrentKinectCamIdx,
			" min=1 max=10 step=1 keyIncr=k keyDecr=K help='Change to Kinect view (1=original). ' group='Kinect' ");
			TwAddVarRW(g_twBar, "Vis Kinect", TW_TYPE_BOOLCPP, &_util->visCam[2], " group='Kinect' ");
			TwAddVarRW(g_twBar, "Vis Kinect Axis", TW_TYPE_BOOLCPP, &_util->visCamAxis[2], " group='Kinect' ");
			TwAddVarRW(g_twBar, "Vis Kinect Label", TW_TYPE_BOOLCPP, &_util->visCamIndex[2], " group='Kinect' ");
		}
		// 
			// TwAddVarRW(g_twBar, "Projector", TW_TYPE_INT8, &projCamIdx,
			// 	" min=1 max=5 step=1 keyIncr=p keyDecr=P help='Projector (1=original size). ' group='Projector' ");
			// 
			// TwAddVarRW(g_twBar, "Projector 1", TW_TYPE_BOOLCPP, &g_Proj[0],
			// 	" group='Projector' "); // 'Wireframe' is put in the group 'Display' (which is then created)
			// 
			// TwAddVarRW(g_twBar, "Projector 2", TW_TYPE_BOOLCPP, &g_Proj[1],
			// 	" group='Projector' "); // 'Wireframe' is put in the group 'Display' (which is then created)
			// 
			// TwAddVarRW(g_twBar, "Projector 3", TW_TYPE_BOOLCPP, &g_Proj[2],
			// 	" group='Projector' "); // 'Wireframe' is put in the group 'Display' (which is then created)
			// 
			// TwAddVarRW(g_twBar, "Projector 4", TW_TYPE_BOOLCPP, &g_Proj[3],
			// 	" group='Projector' "); // 'Wireframe' is put in the group 'Display' (which is then created)
			// 
			// TwAddVarRW(g_twBar, "Projector 5", TW_TYPE_BOOLCPP, &g_Proj[4],
			//	" group='Projector' "); // 'Wireframe' is put in the group 'Display' (which is then created)
		//}



		TwAddSeparator(g_twBar, NULL, NULL);
		TwAddButton(g_twBar, "Rendering2Img", TwGUIManager::Rendering2Img, this, " label='Rendering2Img' ");

	}

	void updateEvents() const
	{
		unsigned int size = _eventsToHandle.size();
		for (unsigned int i = 0; i<size; ++i)
		{
			const osgGA::GUIEventAdapter& ea = *(_eventsToHandle.front());
			float x = ea.getX(), y = ea.getWindowHeight() - ea.getY();

			switch (ea.getEventType())
			{
				case osgGA::GUIEventAdapter::PUSH:
					TwMouseMotion(x, y);
					TwMouseButton(TW_MOUSE_PRESSED, getTwButton(ea.getButton()));
					break;
				case osgGA::GUIEventAdapter::RELEASE:
					TwMouseMotion(x, y);
					TwMouseButton(TW_MOUSE_RELEASED, getTwButton(ea.getButton()));
					{
						// Camera view port
						if (g_CurrentHdCamIdx != g_PreHdCamIdx)
						{
							_util->getSetOrCreateSlaveViewer(HD_PANEL_INDEX, g_CurrentHdCamIdx);
							g_PreHdCamIdx = g_CurrentHdCamIdx;
						}

						if (g_CurrentKinectCamIdx != g_PreKinectCamIdx)
						{
							_util->getSetOrCreateSlaveViewer(Kinect_PANEL_INDEX, g_CurrentKinectCamIdx);
							g_PreKinectCamIdx = g_CurrentKinectCamIdx;
						}

						if (g_CurrentVgaPanelIdx != g_PreVgaPanelIdx || g_CurrentVgaCamIdx != g_PreVgaCamIdx)
						{
							_util->getSetOrCreateSlaveViewer(g_CurrentVgaPanelIdx, g_CurrentVgaCamIdx);
							g_PreVgaPanelIdx = g_CurrentVgaPanelIdx;
							g_PreVgaCamIdx = g_CurrentVgaCamIdx;
						}

						// Draw camera or not
						if (_util->visCam[0] != _util->preVisCam[0])
						{
							_util->drawOrHideCamera(_util->visCam[0], VGA_NODE_MASK);
							_util->preVisCam[0] = _util->visCam[0];
						}
						if (_util->visCam[1] != _util->preVisCam[1])
						{
							_util->drawOrHideCamera(_util->visCam[1], HD_NODE_MASK);
							_util->preVisCam[1] = _util->visCam[1];
						}
						if (_util->visCam[2] != _util->preVisCam[2])
						{
							_util->drawOrHideCamera(_util->visCam[2], Kinect_NODE_MASK);
							_util->preVisCam[2] = _util->visCam[2];
						}


						// Draw camera label
						if (_util->visCamIndex[0] != _util->preVisCamIndex[0])
						{
							_util->drawOrHideCamLabel(_util->visCamIndex[0],VGA_NODE_MASK);
							_util->preVisCamIndex[0] = _util->visCamIndex[0];
						}
						if (_util->visCamIndex[1] != _util->preVisCamIndex[1])
						{
							_util->drawOrHideCamLabel(_util->visCamIndex[1], HD_NODE_MASK);
							_util->preVisCamIndex[1] = _util->visCamIndex[1];
						}
						if (_util->visCamIndex[2] != _util->preVisCamIndex[2])
						{
							_util->drawOrHideCamLabel(_util->visCamIndex[2], Kinect_NODE_MASK);
							_util->preVisCamIndex[2] = _util->visCamIndex[2];
						}

						// Draw camera axis
						if (_util->visCamAxis[0] != _util->preVisCamAxis[0])
						{
							_util->drawOrHideCamAxis(_util->visCamAxis[0], VGA_NODE_MASK);
							_util->preVisCamAxis[0] = _util->visCamAxis[0];
						}
						if (_util->visCamAxis[1] != _util->preVisCamAxis[1])
						{
							_util->drawOrHideCamAxis(_util->visCamAxis[1], HD_NODE_MASK);
							_util->preVisCamAxis[1] = _util->visCamAxis[1];
						}
						if (_util->visCamAxis[2] != _util->preVisCamAxis[2])
						{
							_util->drawOrHideCamAxis(_util->visCamAxis[2], Kinect_NODE_MASK);
							_util->preVisCamAxis[2] = _util->visCamAxis[2];
						}
					}
					break;
				case osgGA::GUIEventAdapter::DRAG:
				case osgGA::GUIEventAdapter::MOVE:
					TwMouseMotion(x, y);
					break;
				case osgGA::GUIEventAdapter::KEYDOWN:
				{
					bool useCtrl = false;
					if (ea.getModKeyMask()&osgGA::GUIEventAdapter::MODKEY_CTRL) useCtrl = true;
					TwKeyPressed(getTwKey(ea.getKey(), useCtrl), getTwModKeyMask(ea.getModKeyMask()));
				}
					break;
				default: 
					break;
			}
			const_cast<TwGUIManager*>(this)->_eventsToHandle.pop();
		}
	}

	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
	{
		switch (ea.getEventType())
		{
		case osgGA::GUIEventAdapter::FRAME:  // Update transform values
			if (_scene.valid())
			{
				// osg::Vec3 pos(position[0], position[1], position[2]);
				// osg::Quat quat(rotation[0], rotation[1], rotation[2], rotation[3]);
				// _scene->setMatrix(osg::Matrix::rotate(osg::Quat(-osg::PI_2,osg::Vec3(1,0,0)) * quat) * osg::Matrix::translate(pos));


				
			}
			return false;
		}

		// As AntTweakBar handle all events within the OpenGL context, we have to record the event here
		// and process it later in the draw callback
		_eventsToHandle.push(&ea);
		return false;
	}

	virtual void operator()(osg::RenderInfo& renderInfo) const
	{
		osg::Viewport* viewport = renderInfo.getCurrentCamera()->getViewport();
		if (viewport) TwWindowSize(viewport->width(), viewport->height());
		updateEvents();
		TwDraw();
	}

protected:
	CCamUtil* _util ;

	virtual ~TwGUIManager()
	{
		TwTerminate();
	}

	TwMouseButtonID getTwButton(int button) const
	{
		switch (button)
		{
		case osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON: return TW_MOUSE_LEFT;
		case osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON: return TW_MOUSE_MIDDLE;
		case osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON: return TW_MOUSE_RIGHT;
		}
		return static_cast<TwMouseButtonID>(0);
	}

	int getTwKey(int key, bool useCtrl) const
	{
		switch (key)
		{
		case osgGA::GUIEventAdapter::KEY_BackSpace: return TW_KEY_BACKSPACE;
		case osgGA::GUIEventAdapter::KEY_Tab: return TW_KEY_TAB;
		case osgGA::GUIEventAdapter::KEY_Return: return TW_KEY_RETURN;
		case osgGA::GUIEventAdapter::KEY_Escape: return TW_KEY_ESCAPE;
		case osgGA::GUIEventAdapter::KEY_Left: return TW_KEY_LEFT;
		case osgGA::GUIEventAdapter::KEY_Right: return TW_KEY_RIGHT;
		case osgGA::GUIEventAdapter::KEY_Up: return TW_KEY_UP;
		case osgGA::GUIEventAdapter::KEY_Down: return TW_KEY_DOWN;
		case osgGA::GUIEventAdapter::KEY_Home: return TW_KEY_HOME;
		case osgGA::GUIEventAdapter::KEY_End: return TW_KEY_END;
		case osgGA::GUIEventAdapter::KEY_Insert: return TW_KEY_INSERT;
		case osgGA::GUIEventAdapter::KEY_Delete: return TW_KEY_DELETE;
		case osgGA::GUIEventAdapter::KEY_Page_Up: return TW_KEY_PAGE_UP;
		case osgGA::GUIEventAdapter::KEY_Page_Down: return TW_KEY_PAGE_DOWN;
		case osgGA::GUIEventAdapter::KEY_F1: return TW_KEY_F1;
		case osgGA::GUIEventAdapter::KEY_F2: return TW_KEY_F2;
		case osgGA::GUIEventAdapter::KEY_F3: return TW_KEY_F3;
		case osgGA::GUIEventAdapter::KEY_F4: return TW_KEY_F4;
		case osgGA::GUIEventAdapter::KEY_F5: return TW_KEY_F5;
		case osgGA::GUIEventAdapter::KEY_F6: return TW_KEY_F6;
		case osgGA::GUIEventAdapter::KEY_F7: return TW_KEY_F7;
		case osgGA::GUIEventAdapter::KEY_F8: return TW_KEY_F8;
		case osgGA::GUIEventAdapter::KEY_F9: return TW_KEY_F9;
		case osgGA::GUIEventAdapter::KEY_F10: return TW_KEY_F10;
		case osgGA::GUIEventAdapter::KEY_F11: return TW_KEY_F11;
		case osgGA::GUIEventAdapter::KEY_F12: return TW_KEY_F12;
		}
		if (useCtrl && key<27) key += 'a' - 1;
		return key;
	}

	int getTwModKeyMask(int modkey) const
	{
		int twModkey = 0;
		if (modkey&osgGA::GUIEventAdapter::MODKEY_SHIFT) twModkey |= TW_KMOD_SHIFT;
		if (modkey&osgGA::GUIEventAdapter::MODKEY_ALT) twModkey |= TW_KMOD_ALT;
		if (modkey&osgGA::GUIEventAdapter::MODKEY_CTRL) twModkey |= TW_KMOD_CTRL;
		return twModkey;
	}

	std::queue< osg::ref_ptr<const osgGA::GUIEventAdapter> > _eventsToHandle;
	osg::observer_ptr<osg::Group> _scene;
};
