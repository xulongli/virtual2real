
#include "PointCloud.h"

int main(int argc, char **argv)
{
	osg::ref_ptr<osg::Group> node = new osg::Group();

	// Point cloud
	osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform;
	node->addChild(ptcNode.get());

	std::string PLY_FILE_NAME = "model/reconByTraj_00007816.ply";
	CPointCloud *pointCloud = new CPointCloud;
	pointCloud->loadPointCloud(PLY_FILE_NAME, ptcNode);

	// construct the viewer.
	osgViewer::Viewer viewer;

	// set the scene to render
	viewer.setSceneData(node.get());

	// register the handler for modifying the point size
	viewer.addEventHandler(new PointCloudKeyboardEventHandler(viewer.getCamera()->getOrCreateStateSet()));

	return viewer.run();
}

int _2main(int argc, char **argv)
{

	// construct the viewer.
	osgViewer::Viewer viewer;

	bool shader = true;
	bool usePointSprites = false;
	bool forcePointMode = true;

	// read the scene from the list of file specified commandline args.
	osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFile("model/reconByTraj_00007816.ply");

	// if no model has been successfully loaded report failure.
	if (!loadedModel)
	{
		return 1;
	}

	// optimize the scene graph, remove redundant nodes and state etc.
	//osgUtil::Optimizer optimizer;
	//optimizer.optimize(loadedModel.get());

	// set the scene to render
	viewer.setSceneData(loadedModel.get());

	osg::StateSet *stateset = loadedModel->getOrCreateStateSet();
	if (usePointSprites)
	{
		/// Setup cool blending
		osg::BlendFunc *fn = new osg::BlendFunc();
		stateset->setAttributeAndModes(fn, osg::StateAttribute::ON);

		/// Setup the point sprites
		osg::PointSprite *sprite = new osg::PointSprite();
		stateset->setTextureAttributeAndModes(0, sprite, osg::StateAttribute::ON);

		/// The texture for the sprites
		osg::Texture2D *tex = new osg::Texture2D();
		tex->setImage(osgDB::readImageFile("model/particle.rgb"));
		stateset->setTextureAttributeAndModes(0, tex, osg::StateAttribute::ON);
	}

	if (forcePointMode)
	{
		/// Set polygon mode to GL_POINT
		osg::PolygonMode *pm = new osg::PolygonMode(
			osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::POINT);
		stateset->setAttributeAndModes(pm, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	}

	// register the handler for modifying the point size
	//viewer.addEventHandler(new KeyboardEventHandler(viewer.getCamera()->getOrCreateStateSet()));

	if (shader)
	{
		osg::StateSet *stateset = loadedModel->getOrCreateStateSet();

		///////////////////////////////////////////////////////////////////
		// vertex shader using just Vec4 coefficients
		char vertexShaderSource[] =
			"void main(void) \n"
			"{ \n"
			"\n"
			"    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n"
			"}\n";

		osg::Program *program = new osg::Program;
		stateset->setAttribute(program);

		osg::Shader *vertex_shader = new osg::Shader(osg::Shader::VERTEX, vertexShaderSource);
		program->addShader(vertex_shader);

#if 0
		//////////////////////////////////////////////////////////////////
		// fragment shader
		//
		char fragmentShaderSource[] =
			"void main(void) \n"
			"{ \n"
			"    gl_FragColor = gl_Color; \n"
			"}\n";

		osg::Shader* fragment_shader = new osg::Shader(osg::Shader::FRAGMENT, fragmentShaderSource);
		program->addShader(fragment_shader);
#endif
	}

	return viewer.run();
}
