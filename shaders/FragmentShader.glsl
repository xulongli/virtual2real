uniform sampler2D projectionMap;
varying vec4 projCoord;
void main()
{
	vec4 dividedCoord = projCoord / projCoord.w ;
	vec4 color =  texture2D(projectionMap,dividedCoord.st);
  	gl_FragColor =	 color * gl_Color;
}