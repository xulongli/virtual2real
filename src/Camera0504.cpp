#include "Camera.h"
#include "INIReader.h"
using namespace std;
using namespace Eigen;

//#define Y_UP_ALONG_Z_MINUS
#define Y_DOWN_ALONG_Z

string CCamera::calibPath = "./calib/";

CCamera::CCamera()
{

	// Color for camera edge
	eg_color = new osg::Vec4Array;
	eg_color_VGA = new osg::Vec4Array;
	eg_color_HD = new osg::Vec4Array;
	eg_color_Kinect = new osg::Vec4Array;
	eg_color_Projector = new osg::Vec4Array;

	eg_color->push_back(osg::Vec4(1.0, 0.0, 0.0, 0.5));				  //Red
	eg_color_VGA->push_back(osg::Vec4(1.0, 0.0, 0.0, 0.5));			  //Red
	eg_color_HD->push_back(osg::Vec4(0.0, 1.0, .0, 0.5));			  //Green
	eg_color_Kinect->push_back(osg::Vec4(0.1f, 0.1f, 0.8f, 0.5));	 //Blue
	eg_color_Projector->push_back(osg::Vec4(0.39f, 0.25f, 0.0, 0.5)); //#ffa000 ORANGE

	//eg_color->push_back(			osg::Vec4(1.0, 0.0, 0.0, 1.0)); //Red
	//eg_color_VGA->push_back(		osg::Vec4(0.0, 1.0, 0.0, 1.0)); //Green
	//eg_color_HD->push_back(			osg::Vec4(0.0, 0.0, 1.0, 1.0)); //Blue
	//eg_color_Kinect->push_back(		osg::Vec4(0.39f, 0.25f, 0.0, 1.0)); //#ffa000 ORANGE
	//eg_color_Projector->push_back(	osg::Vec4(0.32f, 0.187f, 0.22f, 1.0)); //#D07090 PALEVIOLETRED
}

CCamera::~CCamera()
{
}

void CCamera::quat2rot(double *q, double *R)
{
	// QUAT2ROT - Quaternion to rotation matrix transformation
	//
	//  Usage: quat2rot(q, R); q: quaternion, R: rotation matrix

	double x, y, z, w, x2, y2, z2, w2, xy, xz, yz, wx, wy, wz;
	double norm_q = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);

	x = q[0] / norm_q;
	y = q[1] / norm_q;
	z = q[2] / norm_q;
	w = q[3] / norm_q;

	x2 = x * x;
	y2 = y * y;
	z2 = z * z;
	w2 = w * w;
	xy = 2 * x * y;
	xz = 2 * x * z;
	yz = 2 * y * z;
	wx = 2 * w * x;
	wy = 2 * w * y;
	wz = 2 * w * z;

	// Format: {R[0] R[1] R[2]; R[3] R[4] R[5]; R[6] R[7] R[8];}
	R[0] = w2 + x2 - y2 - z2;
	R[1] = xy - wz;
	R[2] = xz + wy;
	R[3] = xy + wz;
	R[4] = w2 - x2 + y2 - z2;
	R[5] = yz - wx;
	R[6] = xz - wy;
	R[7] = yz + wx;
	R[8] = w2 - x2 - y2 + z2;
}

osg::ref_ptr<osg::Geometry> CCamera::drawLineSegment(osg::Vec3 sp, osg::Vec3 ep)
{
	//osg::Vec3 sp(0, -180, 120);
	//osg::Vec3 ep(0, 480, 120);
	osg::ref_ptr<osg::Geometry> beam(new osg::Geometry);
	osg::ref_ptr<osg::Vec3Array> points = new osg::Vec3Array;
	points->push_back(sp);
	points->push_back(ep);
	//osg::ref_ptr<osg::Vec4Array> color = new osg::Vec4Array;
	//color->push_back(osg::Vec4(1.0, 0.0, 0.0, 1.0));
	beam->setVertexArray(points.get());
	beam->setColorArray(eg_color.get());
	beam->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);
	beam->addPrimitiveSet(new osg::DrawArrays(GL_LINES, 0, 2));

	// transpanrent
	osg::ref_ptr<osg::StateSet> ss = beam->getOrCreateStateSet();
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	return beam;
}

void CCamera::drawCamera(osg::ref_ptr<osg::PositionAttitudeTransform> node)
{

	// http://trac.openscenegraph.org/projects/osg//wiki/Support/Tutorials/BasicGeometry
	// http://stackoverflow.com/questions/11489391/openscenegraph-drawing-a-3d-wall
	// http://domedb.perception.cs.cmu.edu/tools/generator/test/
	osg::ref_ptr<osg::Vec3Array> cameraVertices = new osg::Vec3Array;
	osg::ref_ptr<osg::Geometry> cameraGeometry = new osg::Geometry();
	osg::ref_ptr<osg::Geode> cameraGeode = new osg::Geode();

	cameraGeode->addDrawable(cameraGeometry);
	node->addChild(cameraGeode);

	if (0)
	{
		// vertices
		cameraVertices->push_back(osg::Vec3(-0.4, 0.3, 0.5)); //
		cameraVertices->push_back(osg::Vec3(-0.4, -0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(0.4, -0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(0.4, 0.3, 0.5));
		cameraVertices->push_back(osg::Vec3(-0.4, 0.3, -1)); //
		cameraVertices->push_back(osg::Vec3(-0.4, -0.3, -1));
		cameraVertices->push_back(osg::Vec3(0.4, -0.3, -1));
		cameraVertices->push_back(osg::Vec3(0.4, 0.3, -1));
		cameraVertices->push_back(osg::Vec3(-1.2, 0.9, -2)); //
		cameraVertices->push_back(osg::Vec3(-1.2, -0.9, -2));
		cameraVertices->push_back(osg::Vec3(1.2, -0.9, -2));
		cameraVertices->push_back(osg::Vec3(1.2, 0.9, -2));
		cameraGeometry->setVertexArray(cameraVertices);

		// Quad Face
		osg::DrawElementsUInt *camBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		camBase->push_back(0);
		camBase->push_back(1);
		camBase->push_back(2);
		camBase->push_back(3);
		camBase->push_back(0);
		camBase->push_back(4);
		camBase->push_back(5);
		camBase->push_back(1);
		camBase->push_back(0);
		camBase->push_back(3);
		camBase->push_back(7);
		camBase->push_back(4);
		camBase->push_back(0);
		camBase->push_back(4);
		camBase->push_back(5);
		camBase->push_back(1);
		camBase->push_back(3);
		camBase->push_back(2);
		camBase->push_back(6);
		camBase->push_back(7);
		camBase->push_back(2);
		camBase->push_back(1);
		camBase->push_back(5);
		camBase->push_back(6);
		camBase->push_back(5);
		camBase->push_back(4);
		camBase->push_back(7);
		camBase->push_back(6);
		camBase->push_back(5);
		camBase->push_back(4);
		camBase->push_back(8);
		camBase->push_back(9);
		camBase->push_back(5);
		camBase->push_back(9);
		camBase->push_back(10);
		camBase->push_back(6);
		camBase->push_back(7);
		camBase->push_back(6);
		camBase->push_back(10);
		camBase->push_back(11);
		camBase->push_back(7);
		camBase->push_back(11);
		camBase->push_back(8);
		camBase->push_back(4);
		camBase->push_back(9);
		camBase->push_back(8);
		camBase->push_back(11);
		camBase->push_back(10);
		cameraGeometry->addPrimitiveSet(camBase);
	}
	else
	{
		double s = 0.1;
		float width_2 = 1.0;
		float height_2 = 0.6;
		float depth = 3.0;

#ifdef Y_DOWN_ALONG_Z
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0));
		cameraVertices->push_back(osg::Vec3d(-width_2, height_2, depth)); //
		cameraVertices->push_back(osg::Vec3d(-width_2, -height_2, depth));
		cameraVertices->push_back(osg::Vec3d(width_2, -height_2, depth));
		cameraVertices->push_back(osg::Vec3d(width_2, height_2, depth));
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(-width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0), osg::Vec3d(width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, height_2, depth), osg::Vec3d(-width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(-width_2, -height_2, depth), osg::Vec3d(width_2, -height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, -height_2, depth), osg::Vec3d(width_2, height_2, depth)));
		node->addChild(drawLineSegment(osg::Vec3d(width_2, height_2, depth), osg::Vec3d(-width_2, height_2, depth)));
#endif

#ifdef Z_UP_ALONG_Y
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, 2, 0.9) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, 2, -0.9) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, 2, -0.9) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, 2, 0.9) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, 2, 0.9) * s, osg::Vec3d(-1.2, 2, -0.9) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, 2, -0.9) * s, osg::Vec3d(1.2, 2, -0.9) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, 2, -0.9) * s, osg::Vec3d(1.2, 2, 0.9) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, 2, 0.9) * s, osg::Vec3d(-1.2, 2, 0.9) * s));
#endif

#ifdef Y_UP_ALONG_Z
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, 0.9, 2) * s, osg::Vec3d(-1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.2, -0.9, 2) * s, osg::Vec3d(1.2, -0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, -0.9, 2) * s, osg::Vec3d(1.2, 0.9, 2) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.2, 0.9, 2) * s, osg::Vec3d(-1.2, 0.9, 2) * s));
#endif

#ifdef Y_UP_ALONG_Z_MINUS
		// Vertices
		cameraVertices->push_back(osg::Vec3d(0, 0, 0) * s);
		cameraVertices->push_back(osg::Vec3d(-1.0, 0.6, -3) * s); //
		cameraVertices->push_back(osg::Vec3d(-1.0, -0.6, -3) * s);
		cameraVertices->push_back(osg::Vec3d(1.0, -0.6, -3) * s);
		cameraVertices->push_back(osg::Vec3d(1.0, 0.6, -3) * s);
		cameraGeometry->setVertexArray(cameraVertices);

		// edge
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(-1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(0, 0, 0) * s, osg::Vec3d(1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.0, 0.6, -3) * s, osg::Vec3d(-1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(-1.0, -0.6, -3) * s, osg::Vec3d(1.0, -0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.0, -0.6, -3) * s, osg::Vec3d(1.0, 0.6, -3) * s));
		node->addChild(drawLineSegment(osg::Vec3d(1.0, 0.6, -3) * s, osg::Vec3d(-1.0, 0.6, -3) * s));
#endif

		//// Quad Face
		osg::ref_ptr<osg::DrawElementsUInt> camBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		camBase->push_back(2);
		camBase->push_back(1);
		camBase->push_back(4);
		camBase->push_back(3);
		cameraGeometry->addPrimitiveSet(camBase);

		osg::ref_ptr<osg::DrawElementsUInt> camBase3 = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
		camBase3->push_back(0);
		camBase3->push_back(1);
		camBase3->push_back(2);
		camBase3->push_back(0);
		camBase3->push_back(2);
		camBase3->push_back(3);
		camBase3->push_back(0);
		camBase3->push_back(3);
		camBase3->push_back(4);
		camBase3->push_back(0);
		camBase3->push_back(4);
		camBase3->push_back(1);
		cameraGeometry->addPrimitiveSet(camBase3);

		//Color
		osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
		colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
		colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
		colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
		colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
		colors->push_back(osg::Vec4(0.6f, 0.6f, 0.6f, 0.1f));
		cameraGeometry->setColorArray(colors);
		cameraGeometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

		// transpanrent
		osg::ref_ptr<osg::StateSet> ss = cameraGeode->getOrCreateStateSet();
		ss->setMode(GL_BLEND, osg::StateAttribute::ON);
		ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	}

	//osg::Vec3f pos = node->getBound().center();
	//osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	//float scale = 1 / node->getBound().radius();
	//node->setMatrix( osg::Matrix::rotate(quat) * osg::Matrix::translate(pos));

	// position
	//osg::Vec3f pos = node->getBound().center();
	//cout << "Before Trans:" << pos.x() << "," << pos.y() << "," << pos.z() << endl;
	//node->setMatrix(osg::Matrix::translate( -node->getBound().center()) );
	//pos = node->getBound().center();	cout << "After Trans:" << pos.x() << "," << pos.y() << "," << pos.z() << endl;

	// scale
	//cout << "Before scale: " << node->getBound().radius() << endl;
	//float s = 1 / node->getBound().radius();
	//node->setMatrix(osg::Matrix::scale(s,s,s));
	//cout << "After scale: " << node->getBound().radius() << endl;

	// rotation
	//makeRotate(value_type angle1, const Vec3f &axis1, value_type angle2, const Vec3f &axis2, value_type angle3, const Vec3f &axis3)
	//node->setMatrix(osg::Matrix::rotate(osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3))));

	// rotation + tranlate
	//osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	//node->setMatrix(osg::Matrixd() *osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	// rotation + tranlate
	//node->setMatrix(osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));
	//node->setMatrix(osg::Matrixd() *osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	// Rotation + tranlation + scale
	double s = 0.1;
	// node->setPosition(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))); // node->setPosition(osg::Vec3(vecCent(0), vecCent(2), -vecCent(1)));
	// node->setAttitude(osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3)) * osg::Quat(-osg::PI_2, osg::Vec3(1, 0, 0)) );

	// node->setAttitude(osgQ.inverse() * osg::Quat(osg::PI, osg::Vec3(0, 1, 0)) );				osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0));
	node->setAttitude(osgQ.inverse());
	node->setScale(osg::Vec3(s, s, s));
	node->setPosition(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));

	// if ((camIdx == 10) && (panelIdx == HD_PANEL_INDEX))
	// {
	// 	osg::Vec3 vec = node->getPosition();
	// 	CCamUtil::getInstance()->printVec(vec, "HD10");
	// }
}

bool CCamera::readMatrix(const char *filename, float *mat)
{
	std::ifstream file(filename);
	std::string str = "";
	//float _mat[14];

	if (file.fail())
	{
		return true;
	}
	else
	{
		int i = 0;
		while (file >> mat[i])
		{
			i++;
		}
	}

	//cout << _mat[0] << "," << _mat[1] << "," << _mat[2] << endl;

	return false;
};

bool CCamera::isValidCam()
{
	return bValid;
}

void CCamera::setDrawColor(int NODE_MASK)
{
	switch (NODE_MASK)
	{
	case VGA_NODE_MASK:
		eg_color = eg_color_VGA;
		break;

	case HD_NODE_MASK:
		eg_color = eg_color_HD;
		break;

	case Kinect_NODE_MASK:
		eg_color = eg_color_Kinect;
		break;

	case Proj_NODE_MASK:
		eg_color = eg_color_Projector;
		break;

	default:
		eg_color = eg_color_VGA;
		break;
	}
}

void CCamera::setName(int _panelIdx, int _camIdx)
{
	stringstream _name;
	_name << "VGA_" << _panelIdx << "_" << _camIdx;

	this->name = _name.str();
	this->panelIdx = _panelIdx;
	this->camIdx = _camIdx;
}

bool CCamera::initCameraPrameters(string _calibPath, int _panelIdx, int _camIdx)
{
	std::stringstream intrFilePath;
	std::stringstream extrFilePath;

	intrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << ".txt";
	extrFilePath << _calibPath << "/" << std::setfill('0') << std::setw(2) << _panelIdx << "_" << std::setfill('0') << std::setw(2) << _camIdx << "_ext.txt";

	//cout << "intrFilePath: " << intrFilePath.str() << endl;
	//cout << "extFilePath: " << extrFilePath.str() << endl;

	// Intrinsic Parameter : (panelIdx)_(camIdx).txt
	//
	//	Composed of 11 floating point numbers as follows.
	//	K11 K12 K13 K21 K22 K23 K31 K32 K33 distortionParam1 distortionParam2 xxx
	//	All the sequences contain rectified images, thus both distortion parameters are zeros.
	//
	float intr_mat[14];
	if (readMatrix(intrFilePath.str().c_str(), intr_mat))
		return true;

	// Extrinsic Parameter : (panelIdx)_(camIdx)_ext.txt
	//
	// 	Composed of 7 floating point numbers
	// 	The first 4 numbers are quaternion representation of R
	// 	The last 3 numbers are the camera center, which is - invR x t
	// 	Thus, t = -R*Center
	float extr_mat[7];
	if (readMatrix(extrFilePath.str().c_str(), extr_mat))
		return true;

	// Intrisic matrix
	matM << intr_mat[0], intr_mat[1], intr_mat[2],
		intr_mat[3], intr_mat[4], intr_mat[5],
		intr_mat[6], intr_mat[7], intr_mat[8];

	// Distortion matrix
	Map<RowVectorXf> _matDistortion(intr_mat + 9, 5);
	matDistortion = _matDistortion;

	// Rotation quternion
	Map<RowVectorXf> _matRq(extr_mat, 4);
	matRq = _matRq;

	// Camera center
	Map<RowVectorXf> _vecCent(extr_mat + 4, 3);
	vecCent = _vecCent;

	// Rotation matrix
	double q[4] = {matRq(1), matRq(2), matRq(3), matRq(0)};
	double rot[9];
	quat2rot(q, rot);
	matR << rot[0], rot[1], rot[2],
		rot[3], rot[4], rot[5],
		rot[6], rot[7], rot[8];

	//osgR = osg::Matrixd(matR(0, 0), matR(0, 1), matR(0, 2), 0,
	//					matR(1, 0), matR(1, 1), matR(1, 2), 0,
	//					matR(2, 0), matR(2, 1), matR(2, 2), 0,
	//					matR(3, 0), matR(3, 1), matR(3, 2), 1);

	osgR = osg::Matrixd(matR(0, 0), matR(1, 0), matR(2, 0), 0,
						matR(0, 1), matR(1, 1), matR(2, 1), 0,
						matR(0, 2), matR(1, 2), matR(2, 2), 0,
						matR(0, 3), matR(1, 3), matR(2, 3), 1);

	//osgQ = osg::Quat(q[0],q[1],q[2],q[3]);
	osgQ = osg::Quat(q[1], q[2], q[3], q[0]);

	vecT = -matR * vecCent.transpose();
	osgT = osg::Vec3(vecT(0), vecT(1), vecT(2));

	// Translate vector, t = -R*Center			@TODO

	// std::cout << "matM: \n" << matM << endl;
	// std::cout << "matDistortion: \n" << matDistortion << endl;
	// std::cout << "matRq: \n" << matRq << endl;
	// std::cout << "vecT: \n" << vecT << endl;
	// std::cout << "vecCent: \n" << vecCent << endl;

	return false;
}

osg::Matrixd CCamera::getProjectionMatrix(int width, int height, float zfar, float znear)
{
	float depth = zfar - znear;
	float q = -(zfar + znear) / depth;
	float qn = -2 * (zfar * znear) / depth;

	float _K00 = matM(0, 0);
	float _K01 = matM(0, 1);
	float _K02 = matM(0, 2);
	float _K11 = matM(1, 1);
	float _K12 = matM(1, 2);

	float x0 = 0.0f;
	float y0 = 0.0f;

	osg::Matrixd p = osg::Matrixf(2 * _K00 / width, -2 * _K01 / width, (-2 * _K02 + width + 2 * x0) / width, 0,
								  0, 2 * _K11 / height, (2 * _K12 - height + 2 * y0) / height, 0,
								  0, 0, q, qn,
								  0, 0, -1, 0);

	osg::Matrixd pT;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			pT(i, j) = p(j, i);
		}
	}

	return pT;
	//return osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));
}

osg::Matrixd CCamera::getModelViewMatrix()
{
	// Usage:
	// Create parent MatrixTransform to transform the view volume by
	// the inverse ModelView matrix.
	// osg::MatrixTransform* mt = new osg::MatrixTransform;
	// mt->setMatrix(osg::Matrixd::inverse(mv));
	// mt->addChild(geode);

	//return osgR;

	//#ifdef Y_UP_ALONG_Z_MINUS
	//	parentView.makeRotate(-osg::PI_2, osg::Vec3(1, 0, 0));
	//#endif

	// osg::Matrixd modelViewMat = osgR;
	// modelViewMat.makeTranslate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));
	//
	// //osg::Matrixd modelViewMat = osg::Matrixd();
	// modelViewMat.makeTranslate(parentView.getTrans());
	//
	// return modelViewMat;
	//return parentView*modelViewMat;

	// return osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2)));

	osg::Quat q = (osg::Quat(osg::PI_2 / 2, osg::Vec3(1, 0, 0)) * osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3)));
	double rVec[9];
	double qVec[4] = {q.x(), q.y(), q.z(), q.w()};
	quat2rot(qVec, rVec);

	return osg::Matrixd(rVec);
}

//*********************************************
//				VGA
//*********************************************

CCamVGA::CCamVGA(string _calibPath, int _panelIdx, int _camIdx)
{
	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing VGA camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamVGA::drawVGACam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawVGACam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(VGA_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	//osg::Vec3f pos = geoCam->getBound().center();
	// osg::Vec3f pos = osg::Vec3(vecCent(0), vecCent(1), vecCent(2));
	// osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	// float scale = 0.0001* geoCam->getBound().radius();
	//
	// osg::Matrix m;
	// 	m.makeScale(osg::Vec3(scale,scale,scale));
	// m.setTrans(pos);
	// m.setRotate(quat);
	//
	// geoCam->setMatrix(m);

	//geoCam->setMatrix(osg::Matrix::rotate(quat) * osg::Matrix::translate(pos));

	// rotation @TODO getMatrix and setMatrix
	//geoCam->setMatrix(osg::Matrix::rotate( osg::Vec4(matRq(0), matRq(1), matRq(2), matRq(3) )));

	// position

	// Rotatiton + Postion
	//osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	//geoCam->setMatrix( osg::Matrixd() *osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	//osg::Matrixd osgRt(	matR(0, 0), matR(1, 0), matR(2, 0), 0,
	//					matR(0, 1), matR(1, 1), matR(2, 1), 0,
	//					matR(0, 2), matR(1, 2), matR(2, 2), 0,
	//					matR(0, 3), matR(1, 3), matR(2, 3), 1);

	//geoCam->setMatrix(osg::Matrixd::inverse(osgR) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));
	return true;
}

osg::ref_ptr<osg::Camera> CCamVGA::createVirtualCam()
{
	osg::ref_ptr<osg::Camera> renderCam;

	//renderCam->setProjectionMatrix();
	//renderCam->setClearColor();
	//renderCam->setCullMask();

	return renderCam;
}

//*********************************************
//				HD
//*********************************************

CCamHD::CCamHD(string _calibPath, int _camIdx)
{

	int _panelIdx = HD_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing HD camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamHD::drawHDCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawHDCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(HD_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	// Rotatiton + Postion
	//osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	//geoCam->setMatrix(osg::Matrix::rotate(quat) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	return true;
}

//*********************************************
//				Kinect
//*********************************************

CCamKinect::CCamKinect(string _calibPath, int _camIdx)
{
	int _panelIdx = Kinect_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing Kinect camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamKinect::drawKinectCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawKinectCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(Kinect_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	// Rotatiton + Postion
	// osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	// geoCam->setMatrix(osg::Matrix::rotate(quat) * osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	return true;
}

//*********************************************
//				Projctor
//*********************************************

CCamProj::CCamProj(string _calibPath, int _camIdx)
{
	int _panelIdx = Proj_PANEL_INDEX;

	CCamera::calibPath = _calibPath;

	setName(_panelIdx, _camIdx);

	bool status = initCameraPrameters(_calibPath, _panelIdx, _camIdx);
	if (status)
	{
		cout << "Error occure when constructing Projctor camera: " << _panelIdx << "_" << _camIdx << endl;
		bValid = false;
		// exit(-1);
	}
	else
		bValid = true;
}

bool CCamProj::drawProjCam(osg::ref_ptr<osg::PositionAttitudeTransform> geoCam)
{
	if (!bValid)
	{
		cout << "drawProjCam skipped camera: " << this->panelIdx << "_" << this->camIdx << " due to not valid" << endl;
		return NULL;
	}

	// Set draw color
	setDrawColor(Proj_NODE_MASK);

	// Start draw
	drawCamera(geoCam);

	// Rotatiton + Postion
	// osg::Quat quat = osg::Quat(matRq(0), matRq(1), matRq(2), matRq(3));
	// geoCam->setMatrix( osg::Matrix::translate(osg::Vec3(vecCent(0), vecCent(1), vecCent(2))));

	return true;
}

//*********************************************
//				Util
//*********************************************
osg::ref_ptr<CCamUtil> CCamUtil::instance = NULL;

// VGA initialize
void CCamUtil::initialVGA(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{

	for (int panelIdx = 1; panelIdx <= 20; panelIdx++)
	{												 //20
		for (int camIdx = 1; camIdx <= 24; camIdx++) //24
		{
			//if (camIdx == 5)
			//	continue;
			//cout << "VGA Camera: " << panelIdx << "," << camIdx << endl;

			CCamVGA *camVGA = new CCamVGA(CALIB_FILE_PATH, panelIdx, camIdx);
			camVGAList.push_back(camVGA);

			osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
			if (camVGA->drawVGACam(node))
				scene->addChild(node);

			node->setNodeMask(VGA_NODE_MASK);
			camVGAGeoList.push_back(node);
		}
	}
}

//HD initialize
void CCamUtil::initialHD(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{

	for (int camIdx = 1; camIdx <= 31; camIdx++)
	{
		//cout << "HD Camera: " << camIdx << endl;

		CCamHD *camHD = new CCamHD(CALIB_FILE_PATH, camIdx);
		camHDList.push_back(camHD);

		osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
		if (camHD->drawHDCam(node))
			scene->addChild(node);

		node->setNodeMask(HD_NODE_MASK);
		camHDGeoList.push_back(node);
	};
}

//Kinect initialize
void CCamUtil::initialKinect(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{

	for (int camIdx = 1; camIdx <= 10; camIdx++)
	{
		//cout << "Kinect Camera: "<< camIdx << endl;

		CCamKinect *camKinect = new CCamKinect(CALIB_FILE_PATH, camIdx);
		camKinectList.push_back(camKinect);

		osg::ref_ptr<osg::PositionAttitudeTransform> node = new osg::PositionAttitudeTransform;
		if (camKinect->drawKinectCam(node))
			scene->addChild(node);

		node->setNodeMask(Kinect_NODE_MASK);
		camKinectGeoList.push_back(node);
	};
}

// Projector
void CCamUtil::initialProj(string CALIB_FILE_PATH, osg::ref_ptr<osg::PositionAttitudeTransform> scene)
{
}

// VGA get instance by index
CCamVGA *CCamUtil::getVGAInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamVGA *>::iterator it = camVGAList.begin(); it != camVGAList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// HD get instance by index
CCamHD *CCamUtil::getHDInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamHD *>::iterator it = camHDList.begin(); it != camHDList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// Kinect get instance by index
CCamKinect *CCamUtil::getKinectInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamKinect *>::iterator it = camKinectList.begin(); it != camKinectList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

// HD get instance by index
CCamProj *CCamUtil::getProjInstance(int panelIdx, int camIdx)
{
	for (std::vector<CCamProj *>::iterator it = camProjList.begin(); it != camProjList.end(); ++it)
	{
		if (((*it)->panelIdx == panelIdx) && ((*it)->camIdx == camIdx))
			return (*it);
	}
	return NULL;
}

void CCamUtil::getMatrixOfProjector(int panelIdx, int camIdx)
{
	//	Used for setting slave viewer camera
	// 1. get in/extrisic matrix from projector calibration data
	// 2. get master render camera tranlate and rotation data
	// 3. get salve render camera tranlate and rotation data, return

	CCamProj *camProj = getProjInstance(Proj_PANEL_INDEX, 1);

	osg::Matrixd myCameraMatrix;
	osg::Matrixd cameraRotation;
	osg::Matrixd cameraTrans;

	cameraRotation.makeRotate(
		osg::DegreesToRadians(-20.0), osg::Vec3(0, 1, 0), // roll
		osg::DegreesToRadians(-15.0), osg::Vec3(1, 0, 0), // pitch
		osg::DegreesToRadians(10.0), osg::Vec3(0, 0, 1)); // heading

	// 60 meters behind and 7 meters above the tank model
	//cameraTrans.makeTranslate(10, -50, 15);
	cameraTrans.makeTranslate(1.0f, -5.0f, 1.5f);

	myCameraMatrix = cameraRotation * cameraTrans;
	osg::Matrixd i = myCameraMatrix.inverse(myCameraMatrix);
	//viewer.setViewByMatrix((
	//	Producer::Matrix(i.ptr()))
	//	* Producer::Matrix::rotate(-M_PI / 2.0, 1, 0, 0));
}

void CCamUtil::createSlaveViewer(osg::ref_ptr<osgViewer::Viewer> viewer)
{
	int xoffset = 40;
	int yoffset = 40;

	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->x = xoffset + 600;
	traits->y = yoffset + 0;
	traits->width = 600;
	traits->height = 480;
	traits->windowDecoration = true;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;

	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);
	camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

	// add this slave camera to the viewer, with a shift left of the projection matrix
	viewer->addSlave(camera.get(), osg::Matrixd::translate(-1.0, 0.0, 0.0), osg::Matrixd());
}

void CCamUtil::createVGASlaveViewer(osg::ref_ptr<osgViewer::Viewer> viewer, int panelIdx, int camIdx)
{

	CCamVGA *vgaCam = getVGAInstance(panelIdx, camIdx);
	if (vgaCam)
	{
		// Set parnet camer to zero
		// viewer->getCamera()->setProjectionMatrix(osg::Matrixd());

		// P
		osg::Matrixd proj = vgaCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);

		// P*V
		// proj.makeTranslate(-proj.getTrans()); // trans to zeros
		// proj.makeRotate(proj.getRotate().inverse()); // rotate to identity
		//
		//
		osg::Quat q;
		q.set(osg::Matrixd::inverse(vgaCam->osgR));
		proj.makeTranslate(osg::Vec3(vgaCam->vecCent(0), vgaCam->vecCent(1), vgaCam->vecCent(2))); //trans to camera
		proj.makeRotate(q);																		   // rotate to camera

		osg::Matrixd mv = vgaCam->getModelViewMatrix();

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the projection matrix
		camera->setProjectionMatrix(proj);
		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera->setViewMatrix(mv);

		// MVP
		viewer->addSlave(camera.get(), osg::Matrixd(), osg::Matrixd());

		//std::cout << camera->getProjectionMatrix() << "," << camera->getViewMatrix() << endl;
	}
}

osg::ref_ptr<osg::Camera> CCamUtil::createHDMainViewer(osg::ref_ptr<osgViewer::Viewer> viewer, int panelIdx, int camIdx)
{

	CCamHD *hdCam = getHDInstance(panelIdx, camIdx);
	if (hdCam)
	{

		osg::Matrixd proj = hdCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);

		osg::Matrixd mv = hdCam->getModelViewMatrix();

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::Camera> camera = viewer->getCamera();

		// set the projection matrix
		camera->setProjectionMatrix(proj);

		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		mv.makeTranslate(osg::Vec3(hdCam->vecCent(0), hdCam->vecCent(1), hdCam->vecCent(2))); //trans to camera

		//osg::Vec3 cent = osg::Vec3(-4, 0, -3);
		//osg::Vec3 up = osg::Vec3(0, -1, 0);

		// osg::Vec3 cent = osg::Vec3(0, -1, 0);	cent = hdCam->osgQ*cent;		// R*[0 0 1]calib ==>	R*[0 1 0]
		// osg::Vec3 up = osg::Vec3(0, 0, 1);		up = hdCam->osgQ*up;			// R*[0 1 0]calib ==>	R*[0 0 -1]

		// osg::Vec3 eye = osg::Vec3(hdCam->vecCent(0), hdCam->vecCent(2), -hdCam->vecCent(1)); // Zosg  = Ycalib ;	Yosg  = -Zcalib;		Xosg = Xcalib;
		// osg::Vec3 cent = osg::Vec3(0, 0, 1);	cent = hdCam->osgQ*cent;	cent = osg::Vec3(cent.x(), cent.z(), -cent.y());
		// osg::Vec3 up = osg::Vec3(0, 1, 0);		up = hdCam->osgQ*up;		up = osg::Vec3(up.x(), up.z(), -up.y());

		osg::Vec3 eye = osg::Vec3(hdCam->vecCent(0), -hdCam->vecCent(2), hdCam->vecCent(1)); // Zosg  = Ycalib ;	Yosg  = -Zcalib;		Xosg = Xcalib;
		osg::Vec3 cent = osg::Vec3(-4, 2, -4);												 // osg::Vec3 cent = osg::Vec3(0, 0, 1);		cent = hdCam->osgQ.inverse()*cent;	cent = osg::Vec3(cent.x(), -cent.z(), cent.y());
		osg::Vec3 up = osg::Vec3(0, -1, 0);													 //up = hdCam->osgQ.inverse()*up;		up = osg::Vec3(up.x(), -up.z(), up.y());

		// osg::Vec3 eye = osg::Vec3(hdCam->vecCent(0), hdCam->vecCent(1), hdCam->vecCent(2)); // Zosg  = Ycalib ;	Yosg  = -Zcalib;		Xosg = Xcalib;
		// osg::Vec3 cent = osg::Vec3(0, 0, 1);	cent = hdCam->osgQ*cent;	cent = osg::Vec3(cent.x(), cent.z(), -cent.y());
		// osg::Vec3 up = osg::Vec3(0, 1, 0);		up = hdCam->osgQ*up;		up = osg::Vec3(up.x(), up.z(), -up.y());

		mv = osg::Matrixd();
		mv.makeLookAt(eye, cent, up);
		camera->setViewMatrix(mv);

		printVec(eye, "eye");
		printVec(cent, "cent");
		printVec(up, "up");

		// MVP
		//viewer->addSlave(camera.get());

		// Print matrix
		osg::Matrixd hdSalveProjMat = camera->getProjectionMatrix();
		osg::Matrixd hdSalveViewMat = camera->getViewMatrix();
		osg::Vec3 vecProjTrans = hdSalveProjMat.getTrans();
		osg::Vec3 vecViewjTrans = hdSalveViewMat.getTrans();

		printMatrix(hdSalveProjMat, "hdSalveProjMat");
		printMatrix(hdSalveViewMat, "hdSalveViewMat");
		printVec(vecProjTrans, "vecProjTrans");
		printVec(vecViewjTrans, "vecViewjTrans");

		return camera;
	}

	return NULL;
}

osg::ref_ptr<osg::Camera> CCamUtil::createHDSlaveViewer(osg::ref_ptr<osgViewer::Viewer> viewer, int panelIdx, int camIdx)
{

	CCamHD *hdCam = getHDInstance(panelIdx, camIdx);
	if (hdCam)
	{
		// Set parnet camer to zero
		// viewer->getCamera()->setProjectionMatrix(osg::Matrixd());

		// P
		osg::Matrixd proj = hdCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);

		// P*V
		// proj.makeTranslate(-proj.getTrans()); // trans to zeros
		// proj.makeRotate(proj.getRotate().inverse()); // rotate to identity
		//
		//
		// osg::Quat q;
		// q.set(osg::Matrixd::inverse(hdCam->osgR));
		// proj.makeTranslate(osg::Vec3(hdCam->vecCent(0), hdCam->vecCent(1), hdCam->vecCent(2))); //trans to camera
		// proj.makeRotate(q); // rotate to camera

		// Usage:
		// Create parent MatrixTransform to transform the view volume by
		// the inverse ModelView matrix.
		// osg::MatrixTransform* mt = new osg::MatrixTransform;
		// mt->setMatrix(osg::Matrixd::inverse(mv));
		// mt->addChild(geode);
		// 考虑：
		// (1) 相对Master Camera变化	camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		// (2) 标定坐标系与虚拟相机坐标系的坐标变换
		// (3) Root的旋转，但是相机没有旋转

		osg::Matrixd mv = hdCam->getModelViewMatrix();

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		camera->setProjectionResizePolicy(osg::Camera::HORIZONTAL);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the projection matrix
		camera->setProjectionMatrix(proj);
		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		mv.makeTranslate(osg::Vec3(hdCam->vecCent(0), hdCam->vecCent(1), hdCam->vecCent(2))); //trans to camera

		osg::Vec3 eye = osg::Vec3(0, 0, 1);  // Camera coordinate
		osg::Vec3 cent = osg::Vec3(0, 0, 0); // Camera coordinate
		osg::Vec3 up = osg::Vec3(0, -1, 0);  // Camera coordinate

		osg::Quat q1 = hdCam->osgQ.inverse();					  // Camera coordinate to  world coordinate system roation( both Y down for sure)
		osg::Vec3 T = hdCam->osgT;								  // Camera coordinate to  world coordinate system translation( both Y down for sure)
		osg::Quat q2 = osg::Quat(-osg::PI_2, osg::Vec3(1, 0, 0)); // Y down tp Z Up

		eye = q2 * q1 * (eye - T); // RX+T = eye
		cent = q2 * q1 * (cent - T);
		up = q2 * q1 * (up - T);

		mv = osg::Matrixd();
		mv.makeLookAt(eye, cent, up);
		camera->setViewMatrix(mv);

		// MVP
		viewer->addSlave(camera.get());

		// Print matrix
		osg::Matrixd hdSalveProjMat = camera->getProjectionMatrix();
		osg::Matrixd hdSalveViewMat = camera->getViewMatrix();
		osg::Vec3 vecProjTrans = hdSalveProjMat.getTrans();
		osg::Vec3 vecViewjTrans = hdSalveViewMat.getTrans();

		printMatrix(hdSalveProjMat, "hdSalveProjMat");
		printMatrix(hdSalveViewMat, "hdSalveViewMat");
		printVec(vecProjTrans, "vecProjTrans");
		printVec(vecViewjTrans, "vecViewjTrans");

		return camera;
	}

	return NULL;
}

void CCamUtil::createKinectSlaveViewer(osg::ref_ptr<osgViewer::Viewer> viewer, int panelIdx, int camIdx)
{

	CCamKinect *kinectCam = getKinectInstance(panelIdx, camIdx);
	if (kinectCam)
	{
		// Set parnet camer to zero
		// viewer->getCamera()->setProjectionMatrix(osg::Matrixd());

		// P
		osg::Matrixd proj = kinectCam->getProjectionMatrix(HD_WIDTH, HD_HEIGHT, 0.01, 100);

		osg::Quat q;
		q.set(osg::Matrixd::inverse(kinectCam->osgR));
		proj.makeTranslate(osg::Vec3(kinectCam->vecCent(0), kinectCam->vecCent(1), kinectCam->vecCent(2))); //trans to camera
		proj.makeRotate(q);																					// rotate to camera

		osg::Matrixd mv = kinectCam->getModelViewMatrix();

		int xoffset = 40;
		int yoffset = 40;

		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 600;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);

		// only clear the depth buffer
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// draw subgraph after main camera view.
		camera->setRenderOrder(osg::Camera::PRE_RENDER);

		// we don't want the camera to grab event focus from the viewers main camera(s).
		camera->setAllowEventFocus(false);

		// set the projection matrix
		camera->setProjectionMatrix(proj);
		// set the view matrix
		camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera->setViewMatrix(mv);

		// MVP
		viewer->addSlave(camera.get(), osg::Matrixd(), osg::Matrixd());

		//std::cout << camera->getProjectionMatrix() << "," << camera->getViewMatrix() << endl;
	}
}

void CCamUtil::createSlaveViewerOfProjector(osg::ref_ptr<osgViewer::Viewer> viewer, int camIdx)
{
	INIReader reader("config/config.ini");
	int MONITOR_WIDTH = reader.GetReal("Config", "MONITOR_WIDTH", 1280);
	int MONITOR_HEIGHT = reader.GetReal("Config", "MONITOR_HEIGHT", 800);
	int PROJECTOR_WIDTH = reader.GetReal("Config", "PROJECTOR_WIDTH", 1280);
	int PROJECTOR_HEIGHT = reader.GetReal("Config", "PROJECTOR_HEIGHT", 800);
	std::cout << MONITOR_WIDTH << MONITOR_HEIGHT << PROJECTOR_WIDTH << PROJECTOR_HEIGHT << std::endl;

	int xoffset = MONITOR_WIDTH;
	int yoffset = 0;

	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->x = xoffset + PROJECTOR_WIDTH * (camIdx + 1);
	traits->y = yoffset + 0;
	traits->width = PROJECTOR_WIDTH;
	traits->height = PROJECTOR_HEIGHT;
	traits->windowDecoration = false;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;

	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);
	camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

	// add this slave camera to the viewer, with a shift right of the projection matrix
	viewer->addSlave(camera.get(), osg::Matrixd::translate(-1.0, 0.0, 0.0), osg::Matrixd());
}

//Print osg::Matrixd
void CCamUtil::printMatrix(osg::Matrixd mat, std::string name = "Mat")
{
	std::cout << name << ": " << std::endl;
	for (unsigned int row = 0; row < 4; ++row)
	{
		for (unsigned int col = 0; col < 4; ++col)
		{
			std::cout << std::setw(10) << mat(row, col);
			if (col != 3)
			{
				std::cout << ", ";
			}
		}
		std::cout << std::endl;
	}
}

void CCamUtil::printVec(osg::Vec3 vec, std::string name = "Vec")
{
	std::cout << name << ": " << vec.x() << "," << vec.y() << "," << vec.z() << std::endl;
}

osg::ref_ptr<CCamUtil> CCamUtil::getInstance(void)
{
	if (!instance)
		instance = new CCamUtil();

	return instance;
}
