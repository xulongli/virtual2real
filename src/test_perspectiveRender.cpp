/* -*-c++-*- 
* 20160404 Chapter 5 Recipe 7 OpenSceneGraph Cookbook
* 20160713 fork from C:\Review\Code\virtual2real\version0\svn\src\test_LightShader.cpp
*/

/* 
Given a mesh, reander all 480 images to folder, with the same folder hierarchy as Han's
*/

// #define perspectiveRender

#ifdef _WIN32
// do something for windows like include <windows.h>
#include <windows.h>
#include <direct.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <string>
#include <sys/stat.h>
#include <string>

#include <osg/Program>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>

#include "Util.h"
#include "Camera.h"
#include "PointCloud.h"
#include "INIReader.h"
#include "Render2ImgCallback.h"

osg::Matrixd _slaveMVPMat;

static char const *vertSource = {
	"uniform vec3 lightPosition;\n"
	"varying vec3 normal, eyeVec, lightDir;\n"
	"in vec3 vertex_pos; \n"
	"uniform mat4 matMVP;\n"
	"void main()\n"
	"{\n"
	"    vec4 vertexInEye = gl_ModelViewMatrix * gl_Vertex;\n"
	"    eyeVec = -vertexInEye.xyz;\n"
	"    lightDir = vec3(lightPosition - vertexInEye.xyz);\n"
	"    normal = gl_NormalMatrix * gl_Normal;\n"
	"	gl_Position = matMVP * vec4(vertex_pos,1); \n	"
	"	gl_Position.y = 1-gl_Position.y; \n"
	"}\n"};

static char const *fragSource = {
	"uniform vec4 lightDiffuse;\n"
	"uniform vec4 lightSpecular;\n"
	"uniform float shininess;\n"
	"varying vec3 normal, eyeVec, lightDir;\n"
	"void main (void)\n"
	"{\n"
	"  vec4 finalColor = gl_FrontLightModelProduct.sceneColor;\n"
	"  vec3 N = normalize(normal);\n"
	"  vec3 L = normalize(lightDir);\n"
	"  float lambert = dot(N,L);\n"
	"  if (lambert > 0.0)\n"
	"  {\n"
	"    finalColor += lightDiffuse * lambert;\n"
	"    vec3 E = normalize(eyeVec);\n"
	"    vec3 R = reflect(-L, N);\n"
	"    float specular = pow(max(dot(R, E), 0.0), shininess);\n"
	"    finalColor += lightSpecular * specular;\n"
	"  }\n"
	"  gl_FragColor = finalColor;\n"
	"}\n"};

bool dirExists(const std::string &dirName_in)
{
	// https://github.com/cocos2d/cocos2d-x/issues/19058
#if defined _MSC_VER
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false; //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true; // this is a directory!

	return false; // this is not a directory!

#elif defined __GNUC__
	if (dirName_in.empty())
	{
		return false;
	}

	std::string strPath = dirName_in;
	struct stat sts;
	return (stat(strPath.c_str(), &sts) != -1) ? true : false;
#endif
}

bool is_file_exist(const std::string &name)
{
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

class CLightPosCallback_2 : public osg::Uniform::Callback
{
  public:
	virtual void operator()(osg::Uniform *uniform, osg::NodeVisitor *nv)
	{
		const osg::FrameStamp *fs = nv->getFrameStamp();
		if (!fs)
			return;

		float angle = osg::inDegrees((float)fs->getFrameNumber());
		uniform->set(osg::Vec3(20.0f * cosf(angle), 20.0f * sinf(angle), 1.0f));
	}
};

// #ifdef perspectiveRender
// 	int main(int argc, char** argv)
// #else
// 	int perspectiveRender_main(int argc, char** argv)
// #endif
int main(int argc, char **argv)
{
	std::string modelPath;
	std::string imagePath;
	int camMode;

	if (argc == 4)
	{
		modelPath = argv[1];
		imagePath = argv[2];
		camMode = atoi(argv[3]);
	}
	else
	{

		cout << "Useage: perspectiveRender MESH_PATH OUTPUT_PATH NUM_CAMS" << endl;
		cout << "eg: perspectiveRender ./model/dyna_male_50002_hips_81.osgt  ./model/dyna_male_50002_hips_81/00000081 VGA_480" << endl;

		modelPath = "./model/dyna_male_50002_hips_81.osgt";
		imagePath = "./model/dyna_male_50002_hips/00000081";
		camMode = 480;
	}

	//Check input 3d model
	if (!is_file_exist(modelPath))
	{
		cout << modelPath << "no exist! " << endl;
		return -1;
	}

// Set output folder
#if defined _MSC_VER
	_mkdir(imagePath.c_str());
#elif defined __GNUC__
	mkdir(imagePath.c_str(), 0777);
#endif

	// Check camera model
	if (camMode <= 0 || camMode > 480)
	{
		cout << camMode << " should between 0~480! " << endl;
		return -1;
	}

	// Read Config file
	CConfig config;
	//cout << "Calibration: " << CALIB_FILE_PATH << endl;

	osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer;
	osg::ref_ptr<osg::Group> root = new osg::Group();
	osg::ref_ptr<osg::PositionAttitudeTransform> scene = new osg::PositionAttitudeTransform;
	root->addChild(scene);

	// Set task queue and Dome Camera
	CCamUtil *camUtil = CCamUtil::getInstance();
	camUtil->_viewer = viewer;
	camUtil->sceneNode = scene;
	camUtil->initialVGA(CConfig::CALIB_FILE_PATH, scene);

	// Dyna Human
	osg::ref_ptr<osg::PositionAttitudeTransform> dynaPT = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::Node> dynaNode = osgDB::readNodeFile(modelPath);
	dynaPT->addChild(dynaNode);
	dynaPT->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)));
	dynaPT->setScale(osg::Vec3f(1.f, 1.f, 1.f));
	dynaPT->setPosition(osg::Vec3f(-4, 1, -3));
	//dynaPT->setUpdateCallback(new Render2ImgCallback());
	scene->addChild(dynaPT);
	// Point cloud
	osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform;
	CPointCloud *pointCloud = new CPointCloud();
	pointCloud->loadPointCloud(CConfig::PLY_FILE_NAME, ptcNode);
	//scene->addChild(ptcNode);
	root->addChild(ptcNode);
	cout << "point cloud done" << endl;

	//Shader
	osg::ref_ptr<osg::Program> program = new osg::Program;
	program->addShader(new osg::Shader(osg::Shader::VERTEX, vertSource));
	program->addShader(new osg::Shader(osg::Shader::FRAGMENT, fragSource));

	osg::StateSet *stateset = dynaNode->getOrCreateStateSet();
	stateset->setAttributeAndModes(program.get());
	stateset->addUniform(new osg::Uniform("lightDiffuse", osg::Vec4(0.8f, 0.8f, 0.8f, 1.0f)));
	stateset->addUniform(new osg::Uniform("lightSpecular", osg::Vec4(1.0f, 1.0f, 0.4f, 1.0f)));
	stateset->addUniform(new osg::Uniform("shininess", 64.0f));

	osg::ref_ptr<osg::Uniform> _slaveMVPUniform = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "matMVP");
	_slaveMVPUniform->setUpdateCallback(new UniformUpdate(&_slaveMVPMat));
	stateset->addUniform(_slaveMVPUniform.get());

	osg::ref_ptr<osg::Uniform> lightPos = new osg::Uniform("lightPosition", osg::Vec3());
	lightPos->setUpdateCallback(new CLightPosCallback);
	stateset->addUniform(lightPos.get());

	viewer->setCameraManipulator(new osgGA::TrackballManipulator);
	viewer->setSceneData(root);
	viewer->realize();

	//Snap Image
	viewer->getCamera()->setPostDrawCallback(SnapImage::getInstance());

	int camCount = 0;
	while (!viewer->done())
	{
		viewer->frame();

		// waiting for renderring to image, and set to next camera, or exit when done
		int panelIdx = int(camCount / 24) + 1;
		int camIdx = camCount % 24 + 1;
		CCamVGA *vgaCam = camUtil->getVGAInstance(panelIdx, camIdx);

		cout << "Rendering: " << panelIdx << "_" << camIdx << endl;
		if (vgaCam && vgaCam->isValidCam())
		{
			//set next frame name
			cout << SnapImage::getInstance() << endl;

			//SnapImage::getInstance()->_filename = sprintf("%s%6d_%6d.png", imagePath.c_str(), panelIdx, camIdx);

			//update matrix
			camUtil->updateMVPMatrix(vgaCam);

			// do it in next callback
			CCamUtil::getInstance()->rendering2Img = false; //flush frame is done
			SnapImage::getInstance()->_snapImage = true;	// need sanp it
		}

		camCount = camCount + 1;

		if (camCount >= camMode)
			viewer->setDone(true);

		//// Rendering to image
		//CCamUtil::getInstance()->rendering2Img = true;
	}
	return 0;
}
// int main(int argc, char **argv)
// {
// 	return 0;
// }