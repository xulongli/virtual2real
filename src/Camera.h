#pragma once

#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <iostream>
#include <sstream> 
#include <stdio.h>
#include <iomanip>
#include <vector>
#include <fstream>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/Group>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osgViewer/Viewer>
#include <osg/PositionAttitudeTransform>
#include <osg/LineWidth>
#include <osgText/Text>
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>
#include <Eigen/Dense>
#include "Util.h"

	using namespace virtual2real;
using namespace std;
using namespace Eigen;
using namespace cv;

// Node Mask
const int VGA_NODE_MASK = 0x10;
const int HD_NODE_MASK = 0x20;
const int Kinect_NODE_MASK = 0x30;
const int Proj_NODE_MASK = 0x40;

const int VGA_LABEL_MASK = 0x11;
const int HD_LABEL_MASK = 0x21;
const int Kinect_LABEL_MASK = 0x31;
const int Proj_LABEL_MASK = 0x41;

const int VGA_AXIS_MASK = 0x12;
const int HD_AXIS_MASK = 0x22;
const int Kinect_AXIS_MASK = 0x32;
const int Proj_AXIS_MASK = 0x42;

// Panel Index
const int HD_PANEL_INDEX = 0;
const int Kinect_PANEL_INDEX = 50;
const int Proj_PANEL_INDEX = 60;

// Image Height/Width
const int VGA_WIDTH = 640;
const int VGA_HEIGHT = 480;
const int HD_WIDTH = 1920;
const int HD_HEIGHT = 1080;
const int Kinect_WIDTH = 1920;
const int Kinect_HEIGHT = 1080;
const int Projector_WIDTH = 1280;
const int Projector_HEIGHT = 800;
const int Monitor_WIDTH = 1280;


class CCamera
{
protected:

	static string calibPath;

	// camera valid or not
	bool bValid;


	// EIGEN matrix from text	
	bool readMatrix(const char *filename, float * mat);

	// Quaternion to rotation matrix
	void quat2rot(double *q, double *R);

	//edge color or camera
	osg::ref_ptr<osg::Vec4Array> eg_color;
	osg::ref_ptr<osg::Vec4Array> eg_color_VGA;
	osg::ref_ptr<osg::Vec4Array> eg_color_HD;
	osg::ref_ptr<osg::Vec4Array> eg_color_Kinect;
	osg::ref_ptr<osg::Vec4Array> eg_color_Projector;

	// camera name
	string name;

	// Set name of camera, to be like: VGA_PANELINDEX_CAMERAIDX
	void setName(int _panelIdx, int _camIdx);

	//load camera parameters
	bool initCameraPrameters(string _calibPath, int _panelIdx, int _camIdx);

	// unsigned int img_width;
	// unsigned int img_height;


	// Camera parameter
	Eigen::Matrix3f matM;
	Eigen::RowVectorXf matRq;
	Eigen::RowVectorXf vecCent;
	Eigen::RowVectorXf matDistortion;
	Eigen::Matrix3f matR;
	Eigen::MatrixXf vecT;

public:
	CCamera();
	~CCamera();
	std::string getName();

	// Image size
	int imgWidth;
	int imgHeight;

	// draw line
	osg::ref_ptr< osg::Geometry > drawLineSegment(osg::Vec3 sp, osg::Vec3 ep);
	osg::ref_ptr<osg::Geometry> drawLineSegment(osg::Vec3 sp, osg::Vec3 ep, osg::Vec4 color, float lineWidth = 2.0f);


	void build_opengl_projection_for_intrinsics(GLfloat* frustum, double fx, double fy, double skew, double u0, double v0, int width, int height, double near_clip, double far_clip);
	void SettingModelViewMatrixGL(); //to visualize in opengl 

	osg::Matrix getModelMatrix(osg::ref_ptr<osgViewer::Viewer> viewer);

	void validateModelView(osg::ref_ptr<osgViewer::Viewer> viewer);

	// Camera parameter
	osg::Quat osgQ;
	osg::Vec3 osgCent;
	osg::Vec3 osgT;
	osg::Matrixd osgR;

	// Intrinsic Parameter : (panelIdx)_(camIdx).txt
	//
	//	Composed of 11 floating point numbers as follows.
	//	K11 K12 K13 K21 K22 K23 K31 K32 K33 distortionParam1 distortionParam2 xxx
	//	All the sequences contain rectified images, thus both distortion parameters are zeros.
	float intr_mat[14];

	// Extrinsic Parameter : (panelIdx)_(camIdx)_ext.txt
	// 
	// 	Composed of 7 floating point numbers
	// 	The first 4 numbers are quaternion representation of R
	// 	The last 3 numbers are the camera center, which is - invR x t
	// 	Thus, t = -R*Center
	float extr_mat[7];
	
	// camera index
	int panelIdx;
	int	camIdx;

	// Draw camera
	void drawCamera(osg::ref_ptr<osg::PositionAttitudeTransform> node);
	void drawOrHideCamera(osg::ref_ptr<osg::PositionAttitudeTransform> node);

	// Draw Text Label
	void drawCamLabel(osg::ref_ptr<osg::PositionAttitudeTransform> node);
	void drawOrHideCamLabel(osg::ref_ptr<osg::PositionAttitudeTransform> node);

	// Draw Axis
	void drawCamAxis(osg::ref_ptr<osg::PositionAttitudeTransform> node);
	void drawOrHideCamAxis(osg::ref_ptr<osg::PositionAttitudeTransform> node);

	// valide camera or not
	bool isValidCam();

	// Set visual camera color
	void setDrawColor(int NODE_MASK);

	// Projection and ModelView matrices
	osg::Matrixd getProjectionMatrix(int width, int height, float zfar, float znear);
	
	// MVP matrix
	osg::Matrixd updateMVPMatrix( double fx, double fy, double skew, double u0, double v0,
		int imageWidth, int imageHeight,
		double near_clip, double far_clip, double matRq[4], float vecCent[3]);

	osg::Matrixd updateMVPMatrix(int imageWidth = 1920, int imageHeight = 1080, double near_clip = 0.001, double far_clip = 100000);


	// Create slave viewer
	void createSlaveViewer(osg::ref_ptr<osg::Camera> &cam, int width, int height, int xOffset = 600, int yOffset = 0);

	// Set slave viewer
	void setSlaveViewer(osg::ref_ptr<osg::Camera> camera, int width, int height);


	// Create slave viewer
	void createSlaveViewerShader(osg::ref_ptr<osg::Camera> &cam, int width, int height, int xOffset = 600, int yOffset = 0);

	// Set slave viewer
	void setSlaveViewerShader(osg::ref_ptr<osg::Camera> camera, int width, int height);
};



class CCamVGA : public CCamera
{
public:
	CCamVGA(string calibPath, int _panelIdx, int _camIdx);

	// Draw Cam
	bool drawVGACam(osg::ref_ptr<osg::PositionAttitudeTransform>);

	// Get or create virtual camera 
	osg::ref_ptr<osg::Camera> createVirtualCam();

};


class CCamHD : public CCamera
{
public:
	CCamHD(string calibPath, int _camIdx);

	// Draw Cam
	bool drawHDCam(osg::ref_ptr<osg::PositionAttitudeTransform>);

	// Get or create virtual camera 
	osg::ref_ptr<osg::Camera> createVirtualCam();
};


class CCamKinect : public CCamera
{
public:
	CCamKinect(string calibPath, int _camIdx);

	// Draw Cam
	bool drawKinectCam(osg::ref_ptr<osg::PositionAttitudeTransform>);

	// Get or create virtual camera 
	osg::ref_ptr<osg::Camera> createVirtualCam();
};



class CCamProj : public CCamera
{
public:
	CCamProj(string calibPath, int _camIdx);

	// Draw Cam
	bool drawProjCam(osg::ref_ptr<osg::PositionAttitudeTransform>);

	// Get or create virtual camera 
	osg::ref_ptr<osg::Camera> createVirtualCam();

	//Set Viewer of projector
	//http://forum.openscenegraph.org/viewtopic.php?t=1164
	bool createOrRemoveProViewer();
	bool createOrRemoveProViewer(int projIdx);

};



class CCamUtil :public osg::Referenced
{
private:
	static osg::ref_ptr<CCamUtil> instance;
	CCamUtil();

	osg::Node* setNamedNode(const std::string& searchName, const int setNodeMask, osg::Node* currNode);
	osg::Node* setNamedNode(const int searchNodeMask, const int setNodeMask, osg::Node* currNode);


public:
	
	// instance
	static osg::ref_ptr<CCamUtil> getInstance(void);

	//Scene Node
	osg::Node* sceneNode;

	// Dome center
	osg::Vec3 domeCent;
	void calcDomeCent();

	// viewer
	osg::ref_ptr<osgViewer::Viewer> _viewer;

	osg::ref_ptr<osg::Group> vgaGroup;
	osg::ref_ptr<osg::Group> hdGroup;
	osg::ref_ptr<osg::Group> kinectGroup;
	osg::ref_ptr<osg::Group> projGroup;


	// VGA
	std::vector<CCamVGA*> camVGAList;
	std::vector< osg::ref_ptr<osg::PositionAttitudeTransform> > camVGAGeoList;
	void initialVGA(std::string calibPath, osg::ref_ptr<osg::PositionAttitudeTransform> node);
	CCamVGA* getVGAInstance(int panelIdx, int camIdx);


	// HD
	std::vector<CCamHD*> camHDList;
	std::vector< osg::ref_ptr<osg::PositionAttitudeTransform> > camHDGeoList;
	void initialHD(std::string calibPath, osg::ref_ptr<osg::PositionAttitudeTransform> node);
	CCamHD* getHDInstance(int panelIdx, int camIdx);


	//Kinect
	std::vector<CCamKinect*> camKinectList;
	std::vector< osg::ref_ptr<osg::PositionAttitudeTransform> > camKinectGeoList;
	void initialKinect(std::string calibPath, osg::ref_ptr<osg::PositionAttitudeTransform> node);
	CCamKinect* getKinectInstance(int panelIdx, int camIdx);


	//Projector
	std::vector<CCamProj*> camProjList;
	std::vector< osg::ref_ptr<osg::PositionAttitudeTransform> > camProjGeoList;
	void initialProj(std::string calibPath, osg::ref_ptr<osg::PositionAttitudeTransform> node);
	CCamProj* getProjInstance(int panelIdx, int camIdx);


	// Slave Viewers 
	void updateMVPMatrix(CCamera* cam);
	osg::ref_ptr<osg::Camera> createHDSlaveViewer_SUCCESS(int panelIdx, int camIdx);
	osg::ref_ptr<osg::Camera> getSetOrCreateSlaveViewer(int panelIdx, int camIdx);
	osg::ref_ptr<osg::Camera> createGUIViewer();

	//Print osg::Matrixd
	void printMatrix(osg::Matrixd mat, std::string name = "Matrix");
	void printVec(osg::Vec3 vec, std::string name = "Vec");
	void printVec(osg::Quat vec, std::string name = "Vec");

	void drawCamAxis(osg::MatrixTransform* paNode,int panelIdx, int camIdx);

	void drawCamLabel(osg::MatrixTransform* paNode, int panelIdx, int camIdx);


	// Draw camera
	void drawOrHideCamera(bool reDraw, int camType);

	// Draw Text Label
	void drawOrHideCamLabel(bool reDraw, int camType);

	// Draw Axis
	void drawOrHideCamAxis(bool reDraw, int camType);



	// Draw camera geomtry and label/ axis Flag: VGA/HD/ Kinect/ Projector
	bool visCam[4];
	bool visCamAxis[4];
	bool visCamIndex[4];

	bool preVisCam[4];
	bool preVisCamAxis[4];
	bool preVisCamIndex[4];


	// Current Slave view
	int current_slave_panelIdx;
	int current_slave_camIdx;

	osg::ref_ptr<osg::Camera> mainSlave; // For HD & kinect, 1920x1080
	osg::ref_ptr<osg::Camera> projSlave[5]; // For projector, 1080x960
	osg::ref_ptr<osg::Camera> vgaSlave; // For vga, 800x600

	int pre_slave_panelIdx;
	int pre_slave_camIdx;

	// 
	bool rendering2Img;

	// Reprojector 3d to projecotr 2d
	void reproject2Projector();
	void reproject2HD();

	Point_<double> ApplyUndistort_openCV(const Point_<double>& pt_d, double m_distortionParams[5], Mat m_K);
	Point_<double> ApplyDistort_openCV(const Point_<double>& pt_d, double m_distortionParams[5], Mat m_K);

	// Calibration
	int Calib_Project_Intervel;
	string Calib_PROJ_0_IMG;
	string Calib_PROJ_1_IMG;
	string Calib_PROJ_2_IMG;
	string Calib_PROJ_3_IMG;
	string Calib_PROJ_4_IMG;

	bool projectPattern(int projIdx);


	//Slave MVP matrix
	osg::Matrixd _slaveMVPMat; //	osg::Matrixd shaderMVPMat;
	osg::Matrixd shaderProjMat;
	osg::Matrixd shaderViewMat;
	osg::Vec3 shaderLightPos;

	osg::ref_ptr<osg::Uniform> _slaveMVPUniform;
	osg::ref_ptr<osg::Uniform> _slaveModelViewUniform;

};




class UniformUpdate : public osg::Uniform::Callback
{
public:
	UniformUpdate(osg::Matrixd* _mat) :mat(_mat) {
		mode = false;
	}
	UniformUpdate(osg::Vec3* _vec) :vec(_vec) {
		mode = true;
	}
	virtual void operator()(osg::Uniform* uniform, osg::NodeVisitor* nv){
		if (mode)
			uniform->set(*vec);
		else
			uniform->set(*mat);

	}

private:
	osg::Matrixd* mat;
	osg::Vec3* vec;
	bool mode;
};


class CLightPosCallback : public osg::Uniform::Callback
{
public:
	virtual void operator()(osg::Uniform* uniform, osg::NodeVisitor* nv)
	{
		const osg::FrameStamp* fs = nv->getFrameStamp();
		if (!fs) return;

		float angle = osg::inDegrees((float)fs->getFrameNumber());
		uniform->set(osg::Vec3(20.0f * cosf(angle), 1.0f * sinf(angle), 100.0f));
	}
};

