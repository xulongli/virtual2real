#pragma once
#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
//include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <iomanip>
#include <vector>
#include <fstream>
#include <Eigen/Dense>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/Group>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osgViewer/Viewer>

	using namespace std;
using namespace Eigen;

const int VGA_NODE_MASK = 0x11;
const int HD_NODE_MASK = 0x12;
const int Kinect_NODE_MASK = 0x13;
const int Proj_NODE_MASK = 0x14;

const int HD_PANEL_INDEX = 0;
const int Kinect_PANEL_INDEX = 50;
const int Proj_PANEL_INDEX = 60;

const int HD_WIDTH = 1080;
const int HD_HEIGHT = 720;

class CCamera
{
  protected:
	static string calibPath;

	// camera valid or not
	bool bValid;

	// draw line
	osg::ref_ptr<osg::Geometry> CCamera::drawLineSegment(osg::Vec3 sp, osg::Vec3 ep);

	// EIGEN matrix from text
	bool readMatrix(const char *filename, float *mat);

	// Quaternion to rotation matrix
	void quat2rot(double *q, double *R);

	//edge color or camera
	osg::ref_ptr<osg::Vec4Array> eg_color;
	osg::ref_ptr<osg::Vec4Array> eg_color_VGA;
	osg::ref_ptr<osg::Vec4Array> eg_color_HD;
	osg::ref_ptr<osg::Vec4Array> eg_color_Kinect;
	osg::ref_ptr<osg::Vec4Array> eg_color_Projector;

	// camera name
	string name;

	// Set name of camera, to be like: VGA_PANELINDEX_CAMERAIDX
	void setName(int _panelIdx, int _camIdx);

	//load camera parameters
	bool initCameraPrameters(string _calibPath, int _panelIdx, int _camIdx);

  public:
	CCamera();
	~CCamera();

	// Camera parameter
	Eigen::Matrix3f matM;
	Eigen::RowVectorXf matRq;
	Eigen::RowVectorXf vecCent;
	Eigen::RowVectorXf matDistortion;
	Eigen::Matrix3f matR;
	Eigen::MatrixXf vecT;
	osg::Matrixd osgR;

	// camera index
	int panelIdx;
	int camIdx;

	// Draw camera
	void CCamera::drawCamera(osg::ref_ptr<osg::MatrixTransform> node);

	// valide camera or not
	bool isValidCam();

	// Set visual camera color
	void setDrawColor(int NODE_MASK);

	// Projection and ModelView matrices
	osg::Matrixd getProjectionMatrix(int width, int height, float zfar, float znear);
	osg::Matrixd getModelViewMatrix();
};

class CCamVGA : public CCamera
{
  public:
	CCamVGA(string calibPath, int _panelIdx, int _camIdx);

	// Draw Cam
	bool drawVGACam(osg::ref_ptr<osg::MatrixTransform>);

	// Get or create virtual camera
	osg::ref_ptr<osg::Camera> createVirtualCam();
};

class CCamHD : public CCamera
{
  public:
	CCamHD(string calibPath, int _camIdx);

	// Draw Cam
	bool drawHDCam(osg::ref_ptr<osg::MatrixTransform>);

	// Get or create virtual camera
	osg::ref_ptr<osg::Camera> createVirtualCam();
};

class CCamKinect : public CCamera
{
  public:
	CCamKinect(string calibPath, int _camIdx);

	// Draw Cam
	bool drawKinectCam(osg::ref_ptr<osg::MatrixTransform>);

	// Get or create virtual camera
	osg::ref_ptr<osg::Camera> createVirtualCam();
};

class CCamProj : public CCamera
{
  public:
	CCamProj(string calibPath, int _camIdx);

	// Draw Cam
	bool drawProjCam(osg::ref_ptr<osg::MatrixTransform>);

	// Get or create virtual camera
	osg::ref_ptr<osg::Camera> createVirtualCam();

	//Set Viewer of projector
	//http://forum.openscenegraph.org/viewtopic.php?t=1164
	bool createOrRemoveProViewer();
	bool createOrRemoveProViewer(int projIdx);
};

class CCamUtil
{
  public:
	// VGA
	std::vector<CCamVGA *> camVGAList;
	std::vector<osg::ref_ptr<osg::MatrixTransform>> camVGAGeoList;
	void initialVGA(std::string calibPath, osg::ref_ptr<osg::MatrixTransform> node);
	CCamVGA *getVGAInstance(int panelIdx, int camIdx);

	// HD
	std::vector<CCamHD *> camHDList;
	std::vector<osg::ref_ptr<osg::MatrixTransform>> camHDGeoList;
	void initialHD(std::string calibPath, osg::ref_ptr<osg::MatrixTransform> node);
	CCamHD *getHDInstance(int panelIdx, int camIdx);

	//Kinect
	std::vector<CCamKinect *> camKinectList;
	std::vector<osg::ref_ptr<osg::MatrixTransform>> camKinectGeoList;
	void initialKinect(std::string calibPath, osg::ref_ptr<osg::MatrixTransform> node);
	CCamKinect *getKinectInstance(int panelIdx, int camIdx);

	//Projector
	std::vector<CCamProj *> camProjList;
	std::vector<osg::ref_ptr<osg::MatrixTransform>> camProjGeoList;
	void initialProj(std::string calibPath, osg::ref_ptr<osg::MatrixTransform> node);
	CCamProj *getProjInstance(int panelIdx, int camIdx);

	void getMatrixOfProjector(int panelIdx, int camIdx);
	void createSlaveViewer(osg::ref_ptr<osgViewer::Viewer>);

	void createVGASlaveViewer(osg::ref_ptr<osgViewer::Viewer> viewer, int panelIdx, int camIdx);
	void createHDSlaveViewer(osg::ref_ptr<osgViewer::Viewer> viewer, int panelIdx, int camIdx);
	void createKinectSlaveViewer(osg::ref_ptr<osgViewer::Viewer> viewer, int panelIdx, int camIdx);
	void createSlaveViewerOfProjector(osg::ref_ptr<osgViewer::Viewer> viewer, int projIdx);

	//Print osg::Matrixd
	void printMatrix(osg::Matrixd mat, std::string name);
	void printVec(osg::Vec3 vec, std::string name);
};
