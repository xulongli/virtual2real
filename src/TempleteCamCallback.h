#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
//include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osg/PositionAttitudeTransform>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgGA/GUIEventHandler>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <iostream>
#include <queue>

	class WindowResizedHandler : public osgGA::GUIEventHandler
{
  public:
	WindowResizedHandler()
	{
	}

	virtual bool handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa, osg::Object *object, osg::NodeVisitor *nv)
	{
		osg::Camera *camera = dynamic_cast<osg::Camera *>(object);
		if (!camera)
			return false;

		const osg::FrameStamp *fs = nv->getFrameStamp();

		if (ea.getEventType() == osgGA::GUIEventAdapter::RESIZE)
		{
			OSG_NOTICE << "Window resized event context=" << ea.getGraphicsContext() << " frameNumber = " << fs->getFrameNumber() << std::endl;
			OSG_NOTICE << "WindowX=" << ea.getWindowX() << std::endl;
			OSG_NOTICE << "WindowY=" << ea.getWindowY() << std::endl;
			OSG_NOTICE << "WindowWidth=" << ea.getWindowWidth() << std::endl;
			OSG_NOTICE << "WindowHeight=" << ea.getWindowHeight() << std::endl;

			// reset the Camera viewport and associated Texture's to make sure it tracks the new window size.
			camera->resize(ea.getWindowWidth(), ea.getWindowHeight());
		}
		return false;
	}
};

class tankNodeCallback : public osg::NodeCallback
{
  public:
	virtual void operator()(osg::Node *node, osg::NodeVisitor *nv)
	{
		traverse(node, nv);
	}
};

// camera->addEventCallback(new WindowResizedHandler());
