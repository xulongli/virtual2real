#include <osg/MatrixTransform>
#include <osg/Projection>
#include <osg/Geometry>
#include <osg/Texture>
#include <osg/TexGen>
#include <osg/Geode>
#include <osg/ShapeDrawable>
#include <osg/PolygonOffset>
#include <osg/CullSettings>
#include <osg/TextureCubeMap>
#include <osg/TexMat>
#include <osg/MatrixTransform>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/PolygonOffset>
#include <osg/CullFace>
#include <osg/Material>
#include <osg/PositionAttitudeTransform>
#include <osg/ArgumentParser>
#include <osg/TextureRectangle>
#include <osg/Texture2D>
#include <osg/Camera>
#include <osg/TexGenNode>
#include <osg/View>
#include <osg/io_utils>
#include <osgGA/TrackballManipulator>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgDB/FileUtils>
#include <osgViewer/ViewerEventHandlers>

#include <stdio.h>
#include <stdexcept>
#include <sstream>
#include <iostream>

void testTexture(osg::ref_ptr<osg::MatrixTransform> node)
{
	osg::ref_ptr<osg::Node> loadModel = osgDB::readNodeFile("./model/dyna_male_50002_hips_81.osgt");

	// set up the texture state.
	osg::Texture2D *texture = new osg::Texture2D;
	texture->setDataVariance(osg::Object::DYNAMIC);				// protect from being optimized away as static state.
	texture->setBorderColor(osg::Vec4(1.0f, 1.0f, 1.0f, 0.5f)); // only used when wrap is set to CLAMP_TO_BORDER
	osg::Image *img;
	img = osgDB::readImageFile("./model/checkerboard.jpg");

	if (!img)
		std::cout << "Texture image not found!" << std::endl;

	texture->setImage(img);
	texture->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
	texture->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
	texture->setWrap(osg::Texture::WRAP_R, osg::Texture::REPEAT);

	osg::Material *material = new osg::Material;
	osg::StateSet *stateset = loadModel->getOrCreateStateSet();

	stateset->setTextureAttribute(0, texture, osg::StateAttribute::ON);
	stateset->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	stateset->setTextureMode(0, GL_TEXTURE_GEN_S, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	stateset->setTextureMode(0, GL_TEXTURE_GEN_T, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	stateset->setAttribute(material, osg::StateAttribute::OVERRIDE);
	stateset->setMode(GL_BLEND, osg::StateAttribute::ON);
	stateset->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	node->addChild(loadModel);
}

// int test_texture_main()
int main()
{
	osg::ref_ptr<osg::MatrixTransform> node = new osg::MatrixTransform();

	testTexture(node);

	osgViewer::Viewer viewer;
	viewer.setSceneData(node);
	return viewer.run();
}
