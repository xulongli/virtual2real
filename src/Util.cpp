#include "Util.h"
namespace virtual2real
{

std::string GetCurrentWorkingDir(void)
{
	char buff[FILENAME_MAX];
	GetCurrentDir(buff, FILENAME_MAX);
	std::string current_working_dir(buff);
	return current_working_dir;
}

osg::ref_ptr<SnapImage> SnapImage::instance = NULL;

std::string CConfig::CALIB_FILE_PATH;
std::string CConfig::PLY_FILE_NAME;
int CConfig::MONITOR_WIDTH;
int CConfig::MONITOR_HEIGHT;

int CConfig::PROJECTOR_WIDTH;
int CConfig::PROJECTOR_HEIGHT;

bool CConfig::DO_CALIBRATION;
int CConfig::Calib_Project_Intervel;
bool CConfig::Calib_Project_Loop;
std::string CConfig::Calib_PROJ_IMG_0;
std::string CConfig::Calib_PROJ_IMG_1;
std::string CConfig::Calib_PROJ_IMG_2;
std::string CConfig::Calib_PROJ_IMG_3;
std::string CConfig::Calib_PROJ_IMG_4;
std::string CConfig::Calib_PROJ_IMG_BLACK;

std::string CConfig::PROJ_IMG_0;
std::string CConfig::PROJ_IMG_1;
std::string CConfig::PROJ_IMG_2;
std::string CConfig::PROJ_IMG_3;
std::string CConfig::PROJ_IMG_4;

CConfig::CConfig()
{
	// Read Config file
	INIReader reader("config/config.ini");
	CALIB_FILE_PATH = reader.Get("Config", "CALIB_FILE_PATH", "./Calib/");
	PLY_FILE_NAME = reader.Get("Config", "PLY_FILE_NAME", "pointClound.ply");

	MONITOR_WIDTH = reader.GetInteger("Config", "MONITOR_WIDTH", 1280);
	MONITOR_HEIGHT = reader.GetInteger("Config", "MONITOR_WIDTH", 800);

	PROJECTOR_WIDTH = reader.GetInteger("Config", "PROJECTOR_WIDTH", 1280);
	PROJECTOR_HEIGHT = reader.GetInteger("Config", "PROJECTOR_HEIGHT", 800);

	DO_CALIBRATION = reader.GetBoolean("Config", "DO_CALIBRATION", false);
	Calib_Project_Intervel = reader.GetInteger("Config", "Calib_Project_Intervel", 5);
	Calib_Project_Loop = reader.GetBoolean("Config", "Calib_Project_Loop", false);
	Calib_PROJ_IMG_0 = reader.Get("Config", "Calib_PROJ_IMG_0", "pattern.png");
	Calib_PROJ_IMG_1 = reader.Get("Config", "Calib_PROJ_IMG_1", "pattern.png");
	Calib_PROJ_IMG_2 = reader.Get("Config", "Calib_PROJ_IMG_2", "pattern.png");
	Calib_PROJ_IMG_3 = reader.Get("Config", "Calib_PROJ_IMG_3", "pattern.png");
	Calib_PROJ_IMG_4 = reader.Get("Config", "Calib_PROJ_IMG_4", "pattern.png");
	Calib_PROJ_IMG_BLACK = reader.Get("Config", "Calib_PROJ_IMG_BLACK", "pattern.png");

	PROJ_IMG_0 = reader.Get("Config", "PROJ_IMG_0", "p0.png");
	PROJ_IMG_1 = reader.Get("Config", "PROJ_IMG_1", "p1.png");
	PROJ_IMG_2 = reader.Get("Config", "PROJ_IMG_2", "p2.png");
	PROJ_IMG_3 = reader.Get("Config", "PROJ_IMG_3", "p3.png");
	PROJ_IMG_4 = reader.Get("Config", "PROJ_IMG_4", "p4.png");
}

void CUtil::drawFloor(osg::ref_ptr<osg::MatrixTransform> node)
{
	// create the geometry for the wall.

	osg::Geometry *geom = new osg::Geometry;

	osg::Vec3 top_left(-100, 0, 100);
	osg::Vec3 bottom_left(-100, 0, -100);
	osg::Vec3 bottom_right(100, 0, -100);
	osg::Vec3 top_right(100, 0, 100);
	osg::Vec3 center(0, 0, 0);

	osg::Vec3Array *vertices = new osg::Vec3Array(4);
	(*vertices)[0] = top_left;
	(*vertices)[1] = bottom_left;
	(*vertices)[2] = bottom_right;
	(*vertices)[3] = top_right;
	geom->setVertexArray(vertices);

	osg::Vec2Array *texcoords = new osg::Vec2Array(4);
	//(*texcoords)[0].set(-1.0f, 2.0f);
	//(*texcoords)[1].set(-1.0f, -1.0f);
	//(*texcoords)[2].set(2.0f, -1.0f);
	//(*texcoords)[3].set(2.0f, 2.0f);
	(*texcoords)[3].set(0.0f, 0.0f);
	(*texcoords)[2].set(100.0f, 0.0f);
	(*texcoords)[1].set(100.0f, 100.0f);
	(*texcoords)[0].set(0.0f, 100.0f);
	geom->setTexCoordArray(0, texcoords);

	osg::Vec3Array *normals = new osg::Vec3Array(1);
	(*normals)[0].set(-1.0f, 0.0f, 0.0f);
	geom->setNormalArray(normals, osg::Array::BIND_OVERALL);

	osg::Vec4Array *colors = new osg::Vec4Array(1);
	(*colors)[0].set(1.0f, 1.0f, 1.0f, 1.0f);
	geom->setColorArray(colors, osg::Array::BIND_OVERALL);

	geom->addPrimitiveSet(new osg::DrawArrays(GL_QUADS, 0, 4));

	osg::Geode *geom_geode = new osg::Geode;
	geom_geode->addDrawable(geom);

	// set up the texture state.
	osg::Texture2D *texture = new osg::Texture2D;
	texture->setDataVariance(osg::Object::DYNAMIC);				// protect from being optimized away as static state.
	texture->setBorderColor(osg::Vec4(1.0f, 1.0f, 1.0f, 0.5f)); // only used when wrap is set to CLAMP_TO_BORDER
	osg::Image *img;
	std::string _path = GetCurrentWorkingDir() + "/model/checkerboard.png";
	img = osgDB::readImageFile(_path.c_str());
	if (!img)
		std::cout << "Texture image not found: " + _path << std::endl;
	texture->setImage(img);
	texture->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
	texture->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);

	osg::StateSet *stateset = geom->getOrCreateStateSet();
	stateset->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	stateset->setMode(GL_BLEND, osg::StateAttribute::ON);
	stateset->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	node->addChild(geom_geode);
	node->setMatrix(osg::Matrix::translate(0.0f, 3.0f, 0.0f));
	//node->setMatrix(osg::Matrix::scale(0.01, 0.01, 0.01));
}

void CUtil::setLight(osg::Group *root)
{
	// Light
	osg::StateSet *state = root->getOrCreateStateSet(); // create the lighting state set
	state->setMode(GL_LIGHTING, osg::StateAttribute::ON);
	state->setMode(GL_LIGHT0, osg::StateAttribute::ON); // for first light
	state->setMode(GL_LIGHT1, osg::StateAttribute::ON); // for second light
	state->setMode(GL_LIGHT2, osg::StateAttribute::ON); // for third light
	state->setMode(GL_LIGHT3, osg::StateAttribute::ON); // for fouth light

	osg::ref_ptr<osg::Light> light1 = new osg::Light;
	light1->setAmbient(osg::Vec4(0.6, 0.6, 0.6, 1.0));
	light1->setDiffuse(osg::Vec4(0.8, 0.8, 0.8, 1.0));
	light1->setSpecular(osg::Vec4(0.5, 0.5, 0.5, 1.0)); // some examples don't have this one
	light1->setPosition(osg::Vec4(10.0f, -20.0f, 10.0f, 0.0));

	osg::ref_ptr<osg::Light> light2 = new osg::Light;
	light2->setAmbient(osg::Vec4(0.6, 0.6, 0.6, 1.0));
	light2->setDiffuse(osg::Vec4(0.8, 0.8, 0.8, 1.0));
	light2->setSpecular(osg::Vec4(0.5, 0.5, 0.5, 1.0)); // some examples don't have this one
	light2->setPosition(osg::Vec4(10.0f, -20.0f, -10.0f, 0.0));

	osg::ref_ptr<osg::Light> light3 = new osg::Light;
	light3->setAmbient(osg::Vec4(0.6, 0.6, 0.6, 1.0));
	light3->setDiffuse(osg::Vec4(0.8, 0.8, 0.8, 1.0));
	light3->setSpecular(osg::Vec4(0.5, 0.5, 0.5, 1.0)); // some examples don't have this one
	light3->setPosition(osg::Vec4(-10.0f, -20.0f, 10.0f, 0.0));

	osg::ref_ptr<osg::Light> light4 = new osg::Light;
	light4->setAmbient(osg::Vec4(0.6, 0.6, 0.6, 1.0));
	light4->setDiffuse(osg::Vec4(0.8, 0.8, 0.8, 1.0));
	light4->setSpecular(osg::Vec4(0.5, 0.5, 0.5, 1.0)); // some examples don't have this one
	light4->setPosition(osg::Vec4(-10.0f, -20.0f, 10.0f, 0.0));

	osg::ref_ptr<osg::LightSource> source = new osg::LightSource;
	source->setLight(light1.get());
	source->setLight(light2.get());
	source->setLight(light3.get());
	source->setLight(light4.get());
	root->addChild(source); // create a light source object and add the light to it
}

osg::Camera *createHUDCamera(double left, double right, double bottom, double top)
{
	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera->setRenderOrder(osg::Camera::POST_RENDER);
	camera->setAllowEventFocus(false);
	camera->setProjectionMatrix(osg::Matrix::ortho2D(left, right, bottom, top));
	camera->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	return camera.release();
}

osg::Geode *createScreenQuad(float width, float height, float scale)
{
	osg::Geometry *geom = osg::createTexturedQuadGeometry(
		osg::Vec3(), osg::Vec3(width, 0.0f, 0.0f), osg::Vec3(0.0f, height, 0.0f),
		0.0f, 0.0f, width * scale, height * scale);
	osg::ref_ptr<osg::Geode> quad = new osg::Geode;
	quad->addDrawable(geom);

	int values = osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED;
	quad->getOrCreateStateSet()->setAttribute(
		new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::FILL), values);
	quad->getOrCreateStateSet()->setMode(GL_LIGHTING, values);
	return quad.release();
}

osgText::Text *createText(const osg::Vec3 &pos, const std::string &content, float size)
{
	osg::ref_ptr<osgText::Font> g_font = osgText::readFontFile("fonts/arial.ttf");
	osg::ref_ptr<osgText::Text> text = new osgText::Text;

	text->setDataVariance(osg::Object::DYNAMIC);
	text->setFont(g_font.get());
	text->setCharacterSize(size);
	text->setAxisAlignment(osgText::TextBase::XY_PLANE);
	text->setPosition(pos);
	text->setText(content);
	return text.release();
}

} // namespace virtual2real