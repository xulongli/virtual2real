#
# This file was genared by /home/xulongli/proj2cmake/proj2cmake and will be overwritten on it's next run!
# Please put all configurations in the cmake_conf/*.cmake files.
#
function(print_list)
  foreach(msg ${ARGV})
    message("${msg}")
  endforeach()
endfunction()


cmake_minimum_required(VERSION 2.8)

PROJECT(virtual2real)

INCLUDE("${virtual2real_SOURCE_DIR}/cmake_conf/virtual2real.cmake")

# Find OpenCV, you may need to set OpenCV_DIR variable
# to the absolute path to the directory containing OpenCVConfig.cmake file
# via the command line or GUI
# /usr/local/lib/cmake/
find_package(OpenCV REQUIRED)

# include_directories("/usr/include/asm-generic/")
include_directories("/usr/local/MATLAB/R2016b/extern/include/")

# executable file path
file(MAKE_DIRECTORY "bin")
set(EXECUTABLE_OUTPUT_PATH "bin")

# 1) test case
INCLUDE("virtual2real_test.cmake")

# https://stackoverflow.com/questions/14306642/adding-multiple-executables-in-cmake
# https://answers.ros.org/question/284889/cmake-iterate-over-multiple-source-files-for-multiple-executables/
FILE( GLOB_RECURSE virtual2real_TEST_SRC RELATIVE "${virtual2real_SOURCE_DIR}" src/test_*.cpp)
print_list(${virtual2real_TEST_SRC})

FOREACH( file ${virtual2real_TEST_SRC})
    STRING( REPLACE ".cpp" "" name ${file})
    STRING( REPLACE "src/" "" name ${name})
    print_list(${name})
    print_list(${file})
    ADD_EXECUTABLE( ${name} 
            "src/INIReader.cpp"
            "src/PointCloud.cpp"
            "src/Util.cpp"
            "src/ini.c"
            "src/TravelManipulator.cpp"
            "src/KeyboardHandler.cpp"
            "src/Camera.cpp"
            "src/CameraModel.cpp"            
            "src/DisplaySurfaceGeometry.cpp"
            ${file})
    TARGET_LINK_LIBRARIES( ${name}  
            ${OpenCV_LIBS}
            ${virtual2real_DEPS}
            ${virtual2real_ADDITIONAL_DEPS}
            ${SOLUTION_APP_DEPS}
            ${SOLUTION_GENERAL_DEPS})    
ENDFOREACH( file ${virtual2real_TEST_SRC} )



# 2) main programme
INCLUDE("virtual2real.cmake")

ADD_EXECUTABLE(virtual2real
            ${virtual2real_SRC})

TARGET_LINK_LIBRARIES(virtual2real
            ${OpenCV_LIBS}
            ${virtual2real_DEPS}
            ${virtual2real_ADDITIONAL_DEPS}
            ${SOLUTION_APP_DEPS}
            ${SOLUTION_GENERAL_DEPS})
