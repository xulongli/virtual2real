#version 110

in vec3 vertex_pos;
in vec3 VertexPosition;	
in vec3 VertexNormal;uniform vec3 lightPosition;

varying vec3 normal, eyeVec, lightDir;

uniform mat4 matMVP;
uniform mat4 matModelView;

void main(void )	
{
	vec4 vertexInEye = gl_ModelViewMatrix * vec4(vertex_pos,1.0);
	//vec4 vertexInEye = matModelView * gl_Vertex;
	
	eyeVec = -vertexInEye.xyz;
	lightDir = vec3(lightPosition - vertexInEye.xyz);
	normal = gl_NormalMatrix * gl_Normal;
	
	gl_Position = matMVP * vec4(vertex_pos,1.0);
	gl_Position.y = 1.0-gl_Position.y; 
}