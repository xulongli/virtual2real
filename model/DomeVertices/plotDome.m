% anti-clock wise
x = Verts(:,1);
y = Verts(:,2);
z = Verts(:,3);




echo "Normal OK!"
                                                      
plotFace(x,y,z,[93	99	90	82	85]);                 
plotFace(x,y,z,[54	51	36	33	44]);                 
plotFace(x,y,z,[3	10	16	15	6]);                  
plotFace(x,y,z,[84	75	81	91	94]);                 
plotFace(x,y,z,[41	29	27	38	49]);                 
plotFace(x,y,z,[ 43	47	61	65	53]);             
plotFace(x,y,z,[ 1	2	7	11	9	4  ]);            
plotFace(x,y,z,[ 4	9	18	24	19	12 ]);            
plotFace(x,y,z,[ 19	24	35	46	38	27 ]);        
plotFace(x,y,z,[ 35	40	50	59	56	46 ]);        
plotFace(x,y,z,[ 56	59	70	78	76	63 ]);        
plotFace(x,y,z,[ 76	78	88	97	91	81 ]);        
plotFace(x,y,z,[ 91	97	103	105	98	94 ]);        
plotFace(x,y,z,[ 103	101	100	102	104	105]);        
plotFace(x,y,z,[ 98	105	104	99	93	92 ]);        
plotFace(x,y,z,[ 104	102	96	89	90	99 ]);        
plotFace(x,y,z,[ 96	86	80	74	79	89 ]);        
plotFace(x,y,z,[ 90	89	79	69	71	82 ]);        
plotFace(x,y,z,[ 84	94	98	92	83	77 ]);        
plotFace(x,y,z,[ 68	77	83	72	65	61 ]);        
plotFace(x,y,z,[ 65	72	73	66	52	53 ]);        
plotFace(x,y,z,[ 52	66	64	54	44	42 ]);        
plotFace(x,y,z,[ 64	71	69	58	51	54 ]);        
plotFace(x,y,z,[ 79	74	62	55	58	69 ]);        
plotFace(x,y,z,[ 51	58	55	45	34	36 ]);        
plotFace(x,y,z,[ 34	45	37	30	20	22 ]);        
plotFace(x,y,z,[ 83	92	93	85	73	72 ]);        
plotFace(x,y,z,[ 1	3	6	8	5	2 ]);             
plotFace(x,y,z,[ 5	8	17	22	20	13 ]);            
plotFace(x,y,z,[ 20	22	34	45	37	30 ]);    
plotFace(x,y,z,[ 87	95	101	103	97	88 ]);                 
plotFace(x,y,z,[ 60 67 75 84 77 68 ]);         
plotFace(x,y,z,[ 63	76	81	75	67	57]);         
plotFace(x,y,z,[ 49 38 46 56 63 57]);               	
plotFace(x,y,z,[ 49	57	67	60	48	41]);             
plotFace(x,y,z,[47	39	48	60 68	61	]);           
plotFace(x,y,z,[39	28	23	29 41	48	]);           
plotFace(x,y,z,[31	32	43	53 52	42	]);           
plotFace(x,y,z,[32	26	28	39 47	43	]);           
plotFace(x,y,z,[17	8 	6	15 21	25	]);           
plotFace(x,y,z,[25	33	36	34 22	17	]);           
plotFace(x,y,z,[21	15	16	26 32	31	]);           
plotFace(x,y,z,[16	10	14	23 28	26	]);           
plotFace(x,y,z,[14	12	19	27 29	23	]);           
plotFace(x,y,z,[25	21	31	42 44	33	]);           
plotFace(x,y,z,[1	4	12	14 10	3	]);          
plotFace(x,y,z,[73 85  82  71  64  66	]);

%% Triangle mesh, out VertsTris include cent of pannel
[VertsTris,Tris] = plotDomeTrisMesh(Verts);
