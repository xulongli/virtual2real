#include "stdafx.h"
#include "SceneManager.h"
#include "BulletPhysic.h"
#include "CEGUIDrawable.h"
#include "CEGUIUpdateCallback.h"
#include "AudioSystem.h"
#include "StateManager.h"
#include "Input.h"
#include <iostream>
#include <osgViewer/GraphicsWindowWin32>
//#include "TestMinimizeHandler.h"
#include "resource.h"
//#include "vld.h"

#pragma warning(disable : 4099)
using namespace CEGUI;

PlayerInfo playerinfo[4];
StructCourse holeData[18];
StructPixel PixelData[18];
SavedShot savedshotstat;
SaveRoundStat saveroundstat;
RangePixel RangePixeldata;
Range range;
DistanceToHole distanceToHole;
PlayerInfoRecord playerinforecord[4];
Stableford stableford;
ShotData shotdata;
Choice choicedata;

bool BallUpdateCallback::Havenewdata = false;
bool BallUpdateCallback::holein = false;
bool BallUpdateCallback::keepholeininfo = false;
bool BallUpdateCallback::waterin = false;
int BallUpdateCallback::inboundsMask = TRN_INBOUNDS;
bool BallUpdateCallback::underWaterResponse = false;
bool BallUpdateCallback::ballstate = STOP;
bool BallUpdateCallback::stay = false;
bool BallUpdateCallback::ResponseToStop = false;
int BallUpdateCallback::continuejudge = 0;
int BallUpdateCallback::Initial = 0;
int BallUpdateCallback::playnumber = 0;
int BallUpdateCallback::playnumberreccord = 0;
int BallUpdateCallback::precurrentnumber = 0;
int BallUpdateCallback::currentnumber = 0;
bool BallUpdateCallback::keepkitball = false;
bool BallUpdateCallback::skipkitball = false;
bool BallUpdateCallback::skipkitballoOnAndOff = false;
bool BallUpdateCallback::removeplayer = false;
bool BallUpdateCallback::mtoyard = true;
int BallUpdateCallback::GameMode = 1; //1Ϊ�ȸ�����2Ϊ�ȶ�����3Ϊ�������4Ϊ��Զ�Ӹ�����5Ϊ�ප�������6Ϊstableford����
bool BallUpdateCallback::saveround = false;
bool BallUpdateCallback::changeCouseJudge = false;
int BallUpdateCallback::distanceOfOKball = 0;
int BallUpdateCallback::terrain = 256;
bool BallUpdateCallback::OK = false;
bool BallUpdateCallback::doonce = false;
bool BallUpdateCallback::playgolfing = false;
bool BallUpdateCallback::toRangeing = false;
bool BallUpdateCallback::tanchuang = false;							//�����õ�
osg::Vec3 BallUpdateCallback::collisionPos = osg::Vec3(0, 0, 0);	//�����ڵĵ�������
osg::Vec3 BallUpdateCallback::DropPosIntrecct = osg::Vec3(0, 0, 0); //�����ڵĵ�������

bool FlybyEventHandler::bFlyby = false;

bool SceneManager::bFirstTeeoff = true;
bool SceneManager::hasOcean = false;
float SceneManager::fOceanHeight = -200;

bool CEGUIDrawable::dropbllsigh = true;
bool CEGUIDrawable::Nodropball = false;
bool CEGUIDrawable::PoleLock = false;
bool CEGUIDrawable::pushBenchpole = false;
bool CEGUIDrawable::changeShotType = false; //��������ѡ�����ʽ��־λ

bool TextHUD::playgolfing = false;
bool TextHUD::toRangeing = false;

bool SavedShotReplayCallback::flyover = false;
bool SavedShotReplayCallback::saveshotstop = false;

const char ConfigFile[] = "../media/ball/step.ini";
Config configSettings(ConfigFile);

btScalar maxSubStep = (btScalar)configSettings.Read("maxSubStep", 0.);
btScalar fixedTimeStep = (btScalar)configSettings.Read("fixedTimeStep", 0.);

int main(int argc, char *argv[])
{
	//��ֹ�����������
	HANDLE hMutex = CreateMutex(NULL, true, "UnionGolfGame-BUPT");
	if (hMutex != 0 && GetLastError() == ERROR_ALREADY_EXISTS)
	{
		MessageBoxA(NULL, "A INSTANCE HAS ALREADY BEEN STARTED!", "Warning", MB_OK);
		return 0;
	}

	HWND hwnd = GetForegroundWindow(); //ֱ�ӻ��ǰ�����ڵľ��
	SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)LoadIcon(NULL, (LPCSTR)IDI_ICON1));

	osg::notify(osg::NOTICE) << "Golf Simulator - VR Lab @ BUPT " << std::endl
							 << std::endl;

	//�������ڵ�
	osg::ref_ptr<osg::Group> root = new osg::Group;

	SpecialEffect *specialEffect = new SpecialEffect();
	InfoVisitor *infoVisitor = new InfoVisitor();

	//����������ڵ�
	PhysicsManager *physicsMgr = new PhysicsManager(infoVisitor);
	btDynamicsWorld *world = physicsMgr->initializePhysicWorld();

	//����Viewer
	osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer();
	//	viewer->setUpViewInWindow(-10,0,800,800);
	//viewer->getCamera()->setClearMask(GL_DEPTH_TEST);//��Ȳ���
	viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);

	//����������
	osg::ref_ptr<TravelManipulator> mainCamera = new TravelManipulator(physicsMgr, viewer);
	viewer->setCameraManipulator(mainCamera);
	mainCamera->m_pHostViewer = viewer;

	//����������
	SceneManager *sceneMgr = new SceneManager(root, world, viewer, mainCamera, physicsMgr, specialEffect, infoVisitor);
	PhysicsManager::_scene = sceneMgr;
	PhysicsManager::m_pInfovisitor = infoVisitor;
	PhysicsManager::_root = root;

	double fov;
	double ratio;
	double nearPlane;
	double farPlane;
	//��ȡ�Գ��Ӿ�����������ʾ��ÿ����Ļ���ж�Ӧ��ratio�����Ա�����get��set
	viewer->getCamera()->getProjectionMatrixAsPerspective(fov, ratio, nearPlane, farPlane);
	//cout<<"fov:"<<fov<<"\nratio:"<<ratio<<"\nnearPlane:"<<nearPlane<<"\nfarPlane:"<<farPlane<<endl;
	//�����Ӿ���������yzƽ����Ұ�Ƕ���Ϊ50��
	viewer->getCamera()->setProjectionMatrixAsPerspective(50.0f, ratio, 1, 500.0);

	viewer->getCamera()->setNearFarRatio(0.000003f);
	viewer->setSceneData(root);
	viewer->setUpViewAcrossAllScreens();
	viewer->realize();
	viewer->setReleaseContextAtEndOfFrameHint(false);
	viewer->setKeyEventSetsDone(0); //ESC
	viewer->setLightingMode(osg::View::LightingMode::HEADLIGHT);
	//viewer->addEventHandler()

	//SceneSoundManager SoundCreate;
	osg::ref_ptr<osgAudio::SoundNode> KeySoundNode = SoundCreate->createKeystrokeSound("../Media/Sound/KeyWater.WAV");
	root->addChild(KeySoundNode);
	osg::ref_ptr<osgAudio::SoundNode> ClickSoundNode = SoundCreate->createClickstrokeSound("../Media/Sound/strokeball.WAV");
	root->addChild(ClickSoundNode);

	//Weather
	Weather *weather = NULL;
	weather = Weather::getInstance();
	weather->setViewer(viewer);
	weather->setPhysics(physicsMgr);
	root->addEventCallback(weather);

	osg::ref_ptr<osgViewer::GraphicsWindowWin32> gcwin32 = dynamic_cast<osgViewer::GraphicsWindowWin32 *>(viewer->getCamera()->getGraphicsContext());
	//root->addEventCallback(new TestMinimizeHandler(gcwin32));

	//UI
	viewer->getCamera()->getGraphicsContext()->makeCurrent();
	//root->addChild(CreateBackground());							//��������ͼƬ
	osg::ref_ptr<osg::Geode> geode = new osg::Geode;
	osg::ref_ptr<CEGUIDrawable> cd = new CEGUIDrawable(sceneMgr, KeySoundNode, viewer, ClickSoundNode, gcwin32); //����ڵ�
	geode->addDrawable(cd.get());
	cd->Init();					 //��ʼ���¼���������pinfo
	root->addChild(geode.get()); //��ʼ��UI
	root->addEventCallback(new CEGUIUpdateCallback(geode, cd, sceneMgr, physicsMgr));
	//geode->getOrCreateStateSet()->setRenderBinDetails(120, "DepthSortedBin");

	//root->getOrCreateStateSet()->setRenderBinDetails(10, "DepthSortedBin");

	//���
	osg::Camera *camera = viewer->getCamera();
	osgViewer::GraphicsWindow *window = dynamic_cast<osgViewer::GraphicsWindow *>(camera->getGraphicsContext());
	window->setWindowDecoration(false);
	//window->setWindowRectangle(100,100,800,600);

	//������
	root->addUpdateCallback(new InputDataUpdateCallback(physicsMgr, viewer, mainCamera));

	//�����Ż�
	osgUtil::Optimizer optimizer;
	optimizer.optimize(root);
	//���ñ����޳�

	osg::ref_ptr<osg::StateSet> stateset = new osg::StateSet;
	stateset = root->getOrCreateStateSet();
	osg::ref_ptr<osg::CullFace> cullface = new osg::CullFace();
	cullface->setMode(osg::CullFace::BACK);
	stateset->setAttribute(cullface.get());
	stateset->setMode(GL_CULL_FACE, osg::StateAttribute::ON);

	osg::ref_ptr<osg::LOD> _lod = new osg::LOD();
	osg::ref_ptr<osg::Node> _node = sceneMgr->getCourseNode();

	_lod->addChild(_node.get(), 1, 100);
	root->addChild(_lod.get());

	osg::Timer_t frame_tick = osg::Timer::instance()->tick();
	while (!viewer->done())
	{
		// Physics update
		osg::Timer_t now_tick = osg::Timer::instance()->tick();
		float dt = osg::Timer::instance()->delta_s(frame_tick, now_tick);
		frame_tick = now_tick;
		/* int numSimSteps = */
		world->stepSimulation(dt, maxSubStep, btScalar(1.) / btScalar(fixedTimeStep)); //, 10, 0.01);
		world->stepSimulation(1.0 / fixedTimeStep);
		world->updateAabbs();

		viewer->frame();
	}

	return 0;
}