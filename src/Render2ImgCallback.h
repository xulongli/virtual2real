#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
// include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif

#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osg/PositionAttitudeTransform>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <osg/Texture2D>

#include <iostream>
#include <queue>

#include "Camera.h"
#include "Util.h"

	using namespace virtual2real;

class Render2ImgCallback : public osg::NodeCallback
{

  public:
	Render2ImgCallback(int nodeMask)
	{
		node4Render = nodeMask; // VGA_NODE_MASK/HD_NODE_MASK ...

		snap = SnapImage::getInstance();

		currentHDView = 0;
		currentVGAView = 0;

		currentSaveImgName.str("");
		currentSaveImgName << "render/View_unknown.png";

		setTask();
	}

	~Render2ImgCallback()
	{
	}

	virtual void operator()(osg::Node *node, osg::NodeVisitor *nv)
	{
		if (CCamUtil::getInstance()->rendering2Img && !snap->_snapImage)
		{
			// Set Model and view
			if (!setCapture(node))
			{
				// Start capture
				snap->_snapImage = true;
				snap->_filename = currentSaveImgName.str();
				// sanp->setOutPath = outPath;
				// snap->setOutName = outName;
			}
			else
			{
				// Done!
				CCamUtil::getInstance()->rendering2Img = false;
				SnapImage::getInstance()->_snapImage = false;
				SnapImage::getInstance()->_filename = "Screen.png";

				cout << "Export done!" << endl;
			}
		}

		//

		traverse(node, nv);
	}

  private:
	int node4Render;

	osg::ref_ptr<SnapImage> snap;
	std::string outPath;
	std::string outName;

	int currentHDView;
	int currentVGAView;
	std::stringstream currentSaveImgName;

	std::queue<std::string> dynaSeq;
	string currentSeqName;

	//Construct task
	void setTask()
	{
		dynaSeq.push("./model/dyna_male_50002_hips_81.osgt");
	}

	// Set model
	bool setModel(osg::Node *node)
	{
		// Remove old node
		osg::Node *foundNode;
		for (unsigned int i = 0; i < node->asGroup()->getNumChildren(); i++)
		{
			//foundNode = node->asGroup()->getChild(i);
			//node->asGroup()->removeChild(foundNode);
		}

		// Add new model as node
		while (!dynaSeq.empty())
		{
			currentSeqName = dynaSeq.front();
			dynaSeq.pop();

			//cout << "currentSeqName: " << currentSeqName << endl;

			osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFile(currentSeqName);
			if (loadedModel)
			{
				//node->asGroup()->addChild(loadedModel);
				return false;
			}
			else
			{
				cout << "Reandering: " << currentSeqName << " ,view:  " << currentHDView << " ,remaining task: " << dynaSeq.size() << ", status: "
					 << "Model not loaded" << endl;
				continue;
			}
		}

		// Task done
		return true;
	}

	// Set capture view
	bool setCapture(osg::Node *node)
	{
		bool status;

		if (node4Render == VGA_NODE_MASK)
			status = setVGACapture(node);
		else if (node4Render == HD_NODE_MASK)
			status = setHDCapture(node);

		return status;
	}

	// Set HD capture
	bool setHDCapture(osg::Node *node)
	{
		currentSaveImgName.str("");
		currentSaveImgName << "render/00_" << setfill('0') << setw(2) << currentHDView << ".png";

		//cout << "currentHDView: " << currentHDView << endl;
		if (currentHDView == 0 || currentHDView == 30 + 1) // 0~31
		{
			currentHDView = 0;

			// load model
			if (setModel(node))
				return true;
		}

		// Update light position
		CCamUtil::getInstance()->shaderLightPos =
			CCamUtil::getInstance()->getHDInstance(HD_PANEL_INDEX, currentHDView)->osgCent +
			CCamUtil::getInstance()->domeCent +
			osg::Vec3(0, -10, 0);

		// Update projection matrix:mvp
		CCamUtil::getInstance()->getSetOrCreateSlaveViewer(HD_PANEL_INDEX, currentHDView);
		currentHDView++;

		cout << "Reandering: " << currentSeqName << " ,view:  " << currentHDView - 1 << " ,remaining task: " << dynaSeq.size() << endl;
		return false;
	}

	// Set VGA capture
	bool setVGACapture(osg::Node *node)
	{
		int panelIdx = int(currentVGAView / 24) + 1; //	panelIdx : 1~20
		int camIdx = (currentVGAView % 24) + 1;		 //	camIdx: 1~24

		currentSaveImgName.str("");
		currentSaveImgName << "render/" << setfill('0') << setw(2) << panelIdx << "_" << setfill('0') << setw(2) << camIdx << ".png";

		cout << "currentVGAView: " << currentVGAView << endl;
		if (currentVGAView == 0 || currentVGAView == 480) // 1~480
		{
			currentVGAView = 0;
			panelIdx = int(currentVGAView / 24) + 1; //	panelIdx : 1~20
			camIdx = (currentVGAView % 24) + 1;		 //	camIdx: 1~24

			// load model
			if (setModel(node))
				return true;
		}
		// Update light position
		CCamUtil::getInstance()->shaderLightPos =
			CCamUtil::getInstance()->getVGAInstance(panelIdx, camIdx)->osgCent +
			CCamUtil::getInstance()->domeCent +
			osg::Vec3(0, -10, 0);
		cout << "shaderLightPos: "
			 << CCamUtil::getInstance()->shaderLightPos.x() << ","
			 << CCamUtil::getInstance()->shaderLightPos.y() << ","
			 << CCamUtil::getInstance()->shaderLightPos.z() << endl;

		// Update projection matrix:mvp
		CCamUtil::getInstance()->getSetOrCreateSlaveViewer(panelIdx, camIdx);
		currentVGAView++;

		cout << "Reandering: " << currentSeqName << " ,pannel:  " << panelIdx << "_" << camIdx << " ,remaining task: " << dynaSeq.size() << endl;
		return false;
	}

	// bool setView()
	// {
	// 	CCamUtil::getInstance()->getSetOrCreateSlaveViewer(HD_PANEL_INDEX, currentHDView);
	// }
};
