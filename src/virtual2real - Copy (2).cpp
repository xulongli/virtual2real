#include <windows.h>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <iostream>
#include <queue>


#include "TravelManipulator.h"
#include "Camera.h"
#include "INIReader.h"
#include "PointCloud.h"
#include "AWGUI.h"
#include "Util.h"

using namespace virtual2real;

int main()
{	
	// Read Config file
	INIReader reader("config/config.ini");
	string CALIB_FILE_PATH = reader.Get("Config", "CALIB_FILE_PATH", "");
	//cout << "Calibration: " << CALIB_FILE_PATH << endl;

	osg::notify(osg::NOTICE) << " leexulong@gmail.com " << std::endl << std::endl;


	CUtil *utl = new CUtil();
	CCamUtil* camUtil = new CCamUtil();


	//创建Viewer对象
	osg::ref_ptr<osgViewer::Viewer>viewer = new osgViewer::Viewer();

	//添加状态事件
	//viewer.get()->addEventHandler( new osgGA::StateSetManipulator(viewer->getCamera()->getOrCreateStateSet()) );

	//窗口大小变化事件
	//viewer.get()->addEventHandler(new osgViewer::WindowSizeHandler);

	//添加一些常用状态设置
	//viewer.get()->addEventHandler(new osgViewer::StatsHandler);

	// 把漫游器加入到场景中
	//osg::ref_ptr<TravelManipulator> mainCamera = new TravelManipulator(viewer);
	//viewer->setCameraManipulator(mainCamera);
	
	osg::ref_ptr<osg::MatrixTransform> root = new osg::MatrixTransform();
	osg::ref_ptr<osg::MatrixTransform>scene = new osg::MatrixTransform();
	root->addChild(scene);

	// 读取地形 Set the ground (only receives shadow)
	osg::ref_ptr<osg::MatrixTransform> groundNode = new osg::MatrixTransform;
	utl->drawFloor(groundNode);
	scene->addChild(groundNode.get());
	osg::ref_ptr<osg::Node> axis = osgDB::readNodeFile("axes.osgt");
	groundNode->addChild(axis);

	// Point cloud
	osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform;
	scene->addChild(ptcNode.get());
	
	string PLY_FILE_NAME = reader.Get("Config", "PLY_FILE_NAME", "");
	CPointCloud* pointCloud = new CPointCloud();
	pointCloud->loadPointCloud(PLY_FILE_NAME, ptcNode);

	// Light	
	osg::StateSet* state = root->getOrCreateStateSet();// create the lighting state set
	state->setMode(GL_LIGHTING, osg::StateAttribute::ON);
	state->setMode(GL_LIGHT0, osg::StateAttribute::ON); // for first light
	state->setMode(GL_LIGHT1, osg::StateAttribute::ON); // for second light
	state->setMode(GL_LIGHT2, osg::StateAttribute::ON); // for third light

	osg::ref_ptr<osg::Light> light1= new osg::Light;
	light1->setAmbient(osg::Vec4(1.0, 1.0, 1.0, 1.0));
	light1->setDiffuse(osg::Vec4(0.5, 0.5, 0.5, 1.0));
	light1->setSpecular(osg::Vec4(0.5, 0.5, 0.5, 1.0));  // some examples don't have this one
	light1->setPosition(osg::Vec4(10.0f, 10.0f, 100.0f,0.0));

	osg::ref_ptr<osg::Light> light2 = new osg::Light;
	light2->setAmbient(osg::Vec4(1.0, 1.0, 1.0, 1.0));
	light2->setDiffuse(osg::Vec4(0.5, 0.5, 0.5, 1.0));
	light2->setSpecular(osg::Vec4(0.5, 0.5, 0.5, 1.0));  // some examples don't have this one
	light2->setPosition(osg::Vec4(10.0f, -10.0f, 100.0f, 0.0));

	osg::ref_ptr<osg::Light> light3 = new osg::Light;
	light3->setAmbient(osg::Vec4(1.0, 1.0, 1.0, 1.0));
	light3->setDiffuse(osg::Vec4(0.5, 0.5, 0.5, 1.0));
	light3->setSpecular(osg::Vec4(0.5, 0.5, 0.5, 1.0));  // some examples don't have this one
	light3->setPosition(osg::Vec4(-10.0f, 10.0f, 100.0f, 0.0));

	osg::ref_ptr<osg::LightSource> source = new osg::LightSource;
	source->setLight(light1.get());
	source->setLight(light2.get());
	source->setLight(light3.get());
	root->addChild(source);	// create a light source object and add the light to it


	// VGA / HD / Kinect / Projector
	camUtil->initialVGA(CALIB_FILE_PATH, scene);
	camUtil->initialHD(CALIB_FILE_PATH, scene);
	camUtil->initialKinect(CALIB_FILE_PATH, scene);
	camUtil->initialProj(CALIB_FILE_PATH, scene);

	// Root node Rotation
	//osg::Matrix mx;
	//mx.makeRotate(-osg::PI_2, osg::Vec3(1, 0, 0));
	//scene->setMatrix(mx);

	// 优化场景数据
	osgUtil::Optimizer optimizer;
	optimizer.optimize(root.get());
	viewer->setSceneData(root.get());

	//相机
	osg::ref_ptr<osg::Camera> masterCam = new osg::Camera;
	//masterCam = viewer->getCamera();
	//masterCam->getProjectionMatrixAsPerspective();
	//masterCam->setProjectionMatrixAsPerspective();
	//
	//osg::Matrix mat;
	//masterCam->setProjectionMatrix(mat);
	
	//Snap Image
	SnapImage* postDrawCallback = new SnapImage("PostDrawCallback.png");
	//viewer->getCamera()->setPostDrawCallback(postDrawCallback);
	viewer->addEventHandler(new SnapeImageHandler('p', postDrawCallback));

	//viewer->getCamera()->setProjectionMatrix(osg::Matrixd());

	int xoffset = 40;
	int yoffset = 40;

	// left window + left slave camera
	{
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->x = xoffset + 0;
		traits->y = yoffset + 0;
		traits->width = 600;
		traits->height = 480;
		traits->windowDecoration = true;
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		osg::ref_ptr<osg::Camera> camera = new osg::Camera;
		camera->setGraphicsContext(gc.get());
		camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
		GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
		camera->setDrawBuffer(buffer);
		camera->setReadBuffer(buffer);
		camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

		// add this slave camera to the viewer, with a shift left of the projection matrix
		viewer->addSlave(camera.get(), osg::Matrixd::translate(0.0, 0.0, 0.0), osg::Matrixd());

		// GUI
		osg::ref_ptr<TwGUIManager> twGUI = new TwGUIManager(root.get());
		viewer->addEventHandler(twGUI.get());
		//camera->setUpdateCallback(twGUI.get());
		camera->setFinalDrawCallback(twGUI.get());
		camera->setPostDrawCallback(postDrawCallback);
		//camera->setFinalDrawCallback(finalDrawCallback);

		// Event
		camera->addEventCallback(new osgGA::StateSetManipulator(camera->getOrCreateStateSet()));	
	}

	// right window + right slave camera
	// camUtil->creatSlaveViewer(viewer);
	//camUtil->createHDSlaveViewer(viewer, HD_PANEL_INDEX, 10);


	//Projector	
	//camUtil->creatSlaveViewerOfProjector(viewer, 0);
	//camUtil->creatSlaveViewerOfProjector(viewer, 1);
	//camUtil->creatSlaveViewerOfProjector(viewer, 2);
	//camUtil->creatSlaveViewerOfProjector(viewer, 3);
	//camUtil->creatSlaveViewerOfProjector(viewer, 4);


	viewer->run();
	//viewer->realize();
	//while (!viewer->done())
	//{
	//	//viewer->getCamera()->setViewMatrix(viewMatrix);
	//	//viewer->getCamera()->setProjectionMatrix(projectionMatrix);
	//	viewer->frame();
	//
	//}

	return 0;
}

