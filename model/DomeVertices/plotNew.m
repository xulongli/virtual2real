

S = [SX,SY,SZ];
E = [EX,EY,EZ];

% S = S - repmat(mean([S;E],1),size(S,1),1);
% E = E - repmat(mean([S;E],1),size(S,1),1);

Verts = [S;E];
Verts = unique(Verts,'rows');

% for i = 1:150
% 	line([SX(i); EX(i)],[SY(i); EY(i)], [SZ(i); EZ(i)]);
% end
figure, hold on;
for i = 1:150
	line([S(i,1); E(i,1)],[S(i,2); E(i,2)], [S(i,3); E(i,3)]);
end	
    
for i = 1: size(Verts,1)
    text(Verts(i,1),Verts(i,2),Verts(i,3),num2str(i));
end

Conn = zeros(size(S,1),2);
adjacency  = zeros(size(S,1),size(S,1));
 
for i = 1:size(S,1)
	Conn(i,1) = find(ismember(Verts,S(i,:),'rows'),1);
	Conn(i,2) = find(ismember(Verts,E(i,:),'rows'),1);
	adjacency(Conn(i,1),Conn(i,2)) = 1;
	adjacency(Conn(i,2),Conn(i,1)) = 1;	
end

cycle = findcycles(sparse(adjacency));
for i = 1:numel(cycle)
	if (numel(cycle{i}) == 5) || (numel(cycle{i} == 6))
		cycle{i}
	end
end