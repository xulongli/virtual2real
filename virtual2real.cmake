#
# This file was genared by /home/xulongli/proj2cmake/proj2cmake and will be overwritten on it's next run!
# Please put all configurations in the cmake_conf/*.cmake files.
#

SET(virtual2real_SRC
    "src/Camera.cpp"
    "src/CameraModel.cpp"
    "src/DisplaySurfaceGeometry.cpp"
   #  "src/linux_Engine.cpp"
   #  "src/linux_EngineOSG.cpp"
   #  "src/linux_depth.cpp"
    "src/INIReader.cpp"
    "src/KeyboardHandler.cpp"
    "src/PointCloud.cpp"
    "src/RenderUtil.cpp"
    "src/TravelManipulator.cpp"
    "src/Util.cpp"
    "src/ini.c"
   #  "src/virtual2real-rendertool.cpp"
    "src/virtual2real.cpp"
   )

SET(virtual2real_DEPS
   )
