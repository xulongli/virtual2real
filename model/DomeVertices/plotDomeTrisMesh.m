function [Verts,Tris] = plotDomeTrisMesh(Verts)
% anti-clock wise
x = Verts(:,1);
y = Verts(:,2);
z = Verts(:,3);
Tris = [];

index = [93	99	90	82	85];			cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [54	51	36	33	44];			cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [3	10	16	15	6]; 			cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [84	75	81	91	94];			cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [41	29	27	38	49];			cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 43	47	61	65	53];		cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 1	2	7	11	9	4  ];		cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 4	9	18	24	19	12 ];		cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 19	24	35	46	38	27 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 35	40	50	59	56	46 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 56	59	70	78	76	63 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 76	78	88	97	91	81 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 91	97	103	105	98	94 ]; 	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 103	101	100	102	104	105];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 98	105	104	99	93	92 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 104	102	96	89	90	99 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 96	86	80	74	79	89 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 90	89	79	69	71	82 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 84	94	98	92	83	77 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 68	77	83	72	65	61 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 65	72	73	66	52	53 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 52	66	64	54	44	42 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 64	71	69	58	51	54 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 79	74	62	55	58	69 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 51	58	55	45	34	36 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 34	45	37	30	20	22 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 83	92	93	85	73	72 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 1	3	6	8	5	2 ];		cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 5	8	17	22	20	13 ];		cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 20	22	34	45	37	30 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 87	95	101	103	97	88 ];	cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 60 67 75 84 77 68 ];			cent = getcent(x,y,z,index);   Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 63	76	81	75	67	57];	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 49 38 46 56 63 57];  			cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [ 49	57	67	60	48	41];	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [47	39	48	60 68	61	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [39	28	23	29 41	48	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [31	32	43	53 52	42	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [32	26	28	39 47	43	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [17	8 	6	15 21	25	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [25	33	36	34 22	17	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [21	15	16	26 32	31	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [16	10	14	23 28	26	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [14	12	19	27 29	23	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [25	21	31	42 44	33	];  	cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [1	4	12	14 10	3	]; 		cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];
index = [73 85  82  71  64  66	];		cent = getcent(x,y,z,index);    Verts = [Verts; 	cent];	Tris = [Tris;getTrisIndex(index,size(Verts,1))];

end


function 	cent = getcent( x,y,z,Index )
%PLOTFACE Summary of this function goes here
%   Detailed explanation goes here
xx = x(Index);
yy = y(Index);
zz = z(Index);

Xc = mean(xx);
Yc = mean(yy);
Zc = mean(zz);

	cent = [Xc,Yc,Zc];
end

function tris = getTrisIndex(Index,indexCent)
	tris = [];

	for cnt = 1:numel(Index)
		if cnt+1 <= numel(Index)
			i = Index(cnt);
			j = Index(cnt+1);
		else
			i = Index(cnt);
			j = Index(1);
		end
		tris = [tris;i,j,indexCent];
	end
end

