#
# This file was genared by /home/xulongli/proj2cmake/proj2cmake and will be overwritten on it's next run!
# Please put all configurations in the cmake_conf/*.cmake files.
#

SET(virtual2real_TEST_SRC
    "src/Camera.cpp"
    "src/CameraModel.cpp"
    "src/DisplaySurfaceGeometry.cpp"
   #  "src/linux_Engine.cpp"
   #  "src/linux_EngineOSG.cpp"
   #  "src/linux_depth.cpp"
    "src/INIReader.cpp"
    "src/KeyboardHandler.cpp"
    "src/PointCloud.cpp"
    "src/RenderUtil.cpp"
    "src/TravelManipulator.cpp"
    "src/Util.cpp"
    "src/ini.c"
    "src/test_fullScreenTexture.cpp"
    "src/test_render2img.cpp"
    "src/test_renderer.cpp"
    "src/test_PerspectiveShader.cpp"
    "src/test_PerspectiveShader.bak.cpp"
    "src/test_FBO.cpp"
    "src/test_perspectiveRender.cpp"
    "src/test_BrickShader.cpp"
    "src/test_follow_camera.cpp"
    "src/test_drawCamera.cpp"
    "src/test_textureShader.cpp"
    "src/test_calib2projMat.cpp"
    "src/test_LightShader.cpp"
    "src/test_perspectiveTextureShader.cpp"
    "src/test_PointCloud.cpp"
    "src/test_multiple_windows.cpp"
    "src/test_multileProjector2TextureShader.cpp"
    "src/test_texture.cpp"
    "src/test_colorShader.cpp"
    "src/test_SimpleShader.cpp"
    "src/test_SphereShader.cpp"
    "src/test_drawfloor.cpp"
    # "src/virtual2real-rendertool.cpp"
    # "src/virtual2real.cpp"
   )

SET(virtual2real_DEPS
   )
