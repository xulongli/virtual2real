#pragma once

#ifndef __GUI__
#define __GUI__
#endif

#include <iostream>
#include <AntTweakBar.h>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>

#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>

#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>

using namespace std;

class TwGUIManager : public osgGA::GUIEventHandler, public osg::Camera::DrawCallback
{
  public:
	TwGUIManager(osg::MatrixTransform *node = 0);

	TwGUIManager(const TwGUIManager &copy, const osg::CopyOp &op = osg::CopyOp::SHALLOW_COPY);

	META_Object(osg, TwGUIManager)

		static void TW_CALL loadModelFunc(void *clientData);

	void initializeTwGUI();

	void updateEvents();

	virtual bool handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa);

	virtual void operator()(osg::RenderInfo &renderInfo);

  protected:
	virtual ~TwGUIManager();

	TwMouseButtonID getTwButton(int button);

	int getTwKey(int key, bool useCtrl);

	int getTwModKeyMask(int modkey);

	std::queue<osg::ref_ptr<const osgGA::GUIEventAdapter>> _eventsToHandle;
	osg::observer_ptr<osg::MatrixTransform> _scene;
};
