out vec4 FragColor;

in vec3 normal, eyeVec, lightDir;

uniform vec4 lightDiffuse;	
uniform vec4 lightSpecular;	
uniform float shininess;

void main(void)
{

	vec4 finalColor = vec3(0.0);
	
	vec3 N = normalize(normal);
	vec3 L = normalize(lightDir);
	
	float lambert = dot(N,L);
	if (lambert > 0.0)
	{
	  finalColor += lightDiffuse * lambert;
	  vec3 E = normalize(eyeVec);
	  vec3 R = reflect(-L, N);
	  float specular = pow(max(dot(R, E), 0.0), shininess);
	  finalColor += lightSpecular * specular;
	}	
	
	FragColor = finalColor; 
	FragColor = vec4(0.2,0.7,0.7,1.0); 
}