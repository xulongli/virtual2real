
#ifdef _WIN32
// do something for windows like include <windows.h>
include<windows.h>
#elif defined __unix__
// do something for unix like include <unistd.h>
// include<unistd.h>
#elif defined __APPLE__
// do something for mac
#endif 

#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateAttribute>
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/FrontFace>
#include <osgText/Text>
#include <osg/Geometry>
#include <osg/PositionAttitudeTransform>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/CompositeViewer>
#include <osgFX/Scribe>
#include <osg/io_utils>
#include <iostream>
#include <queue>

#include "TravelManipulator.h"
#include "Camera.h"
#include "PointCloud.h"
#include "AwGUI.h"
#include "Util.h"
#include "Render2ImgCallback.h"

using namespace virtual2real;

#define USE_SLAVE_CAM

int main()
{
	// Read Config file
	CConfig config;
	//cout << "Calibration: " << CConfig::CALIB_FILE_PATH << endl;

	osg::notify(osg::NOTICE) << " leexulong@gmail.com " << std::endl << std::endl;

	osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
	if (!wsi)
	{
		osg::notify(osg::NOTICE) << "Error, no WindowSystemInterface available, cannot create windows." << std::endl;
		exit(-1);
	}
	unsigned int _width, _height;
	wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0), _width, _height);
	std::cout << "Resolution: " << _width << "x" << _height << endl;


	//����Viewer����
	osg::ref_ptr<osgViewer::Viewer>viewer = new osgViewer::Viewer();

	//����״̬�¼�
	//viewer.get()->addEventHandler( new osgGA::StateSetManipulator(viewer->getCamera()->getOrCreateStateSet()) );

	//���ڴ�С�仯�¼�
	//viewer.get()->addEventHandler(new osgViewer::WindowSizeHandler);

	//����һЩ����״̬����
	//viewer.get()->addEventHandler(new osgViewer::StatsHandler);

	// �����������뵽������
	//osg::ref_ptr<TravelManipulator> mainCamera = new TravelManipulator(viewer);
	//viewer->setCameraManipulator(mainCamera);

	osg::ref_ptr<osg::Group> root = new osg::Group();
	osg::ref_ptr<osg::PositionAttitudeTransform> scene = new osg::PositionAttitudeTransform;
	root->addChild(scene);

	// ��ȡ���� Set the ground (only receives shadow)
	CUtil *utl = new CUtil();
	osg::ref_ptr<osg::MatrixTransform> groundNode = new osg::MatrixTransform;
	//utl->drawFloor(groundNode);
	//scene->addChild(groundNode);

	// Axis
	osg::ref_ptr<osg::Node> axisNode = osgDB::readNodeFile("axes.osgt");
	osg::ref_ptr<osg::PositionAttitudeTransform>  axisPT = new osg::PositionAttitudeTransform;
	axisPT->setPosition(osg::Vec3f(-4, 0, -3));
	axisPT->addChild(axisNode);
	scene->addChild(axisPT);
	scene->addChild(axisNode);
	cout << "Axes done" << endl;

	////Add a cow entity
	osg::ref_ptr<osg::PositionAttitudeTransform> cowPT = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::Node> cowNode = osgDB::readNodeFile("cow.osgt");
	//cowPT->addChild(cowNode);
	cowPT->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)));
	cowPT->setScale(osg::Vec3f(0.2f, 0.2f, 0.2f));
	cowPT->setPosition(osg::Vec3f(-4, 0, -3));
	//scene->addChild(cowPT);
	cout << "cow done" << endl;

	// Dyna Human 
	osg::ref_ptr<osg::PositionAttitudeTransform> dynaPT = new osg::PositionAttitudeTransform;
	osg::ref_ptr<osg::Node> dynaNode = osgDB::readNodeFile("./model/dyna_male_50002_hips_81.osgt");
	dynaPT->addChild(dynaNode);
	dynaPT->setAttitude(osg::Quat(osg::PI_2, osg::Vec3(1, 0, 0)));
	dynaPT->setScale(osg::Vec3f(1.f, 1.f, 1.f));
	dynaPT->setPosition(osg::Vec3f(-4, 1, -3));
	scene->addChild(dynaPT);
	dynaPT->setUpdateCallback(new Render2ImgCallback(HD_NODE_MASK));
	cout << "dyna done" << endl;

	// Point cloud
	osg::ref_ptr<osg::MatrixTransform> ptcNode = new osg::MatrixTransform;
	CPointCloud* pointCloud = new CPointCloud();
	pointCloud->loadPointCloud(CConfig::PLY_FILE_NAME, ptcNode);
	//scene->addChild(ptcNode);
	root->addChild(ptcNode);
	cout << "point cloud done" << endl;

	// Light
	viewer->setLightingMode(osg::View::SKY_LIGHT); //HEAD_LIGHT  SKY_LIGHT NO_LIGHT
	utl->setLight(root);

	// Root node Rotation
	//osg::Matrix mx;
	//mx.makeRotate(-osg::PI_2, osg::Vec3(1, 0, 0));
	//scene->setMatrix(mx);
	//scene->setPosition(osg::Vec3(0,0,0));
	//scene->setAttitude(osg::Quat(-osg::PI_2, osg::Vec3(1, 0, 0)));

	// VGA / HD / Kinect / Projector
	CCamUtil* camUtil = CCamUtil::getInstance();
	camUtil->_viewer = viewer;
	camUtil->sceneNode = scene.get();
	camUtil->initialVGA(CConfig::CALIB_FILE_PATH, scene);
	camUtil->initialHD(CConfig::CALIB_FILE_PATH, scene);
	camUtil->initialKinect(CConfig::CALIB_FILE_PATH, scene);
	camUtil->initialProj(CConfig::CALIB_FILE_PATH, scene);

	// Reproject to projector
	camUtil->reproject2Projector();

	// Repproject to HD
	//camUtil->reproject2HD();


	// �Ż���������
	osgUtil::Optimizer optimizer;
	optimizer.optimize(root.get());
	viewer->setSceneData(root.get());
	viewer->setCameraManipulator(new osgGA::TrackballManipulator);

	// Main Viewer
	int xoffset = 40;
	int yoffset = 40;

	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->x = xoffset + 0;
	traits->y = yoffset + 0;
	traits->width = 800;
	traits->height = 600;
	traits->windowDecoration = true;
	traits->doubleBuffer = true;
	traits->sharedContext = 0;

	osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	camera->setGraphicsContext(gc.get());
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
	camera->setDrawBuffer(buffer);
	camera->setReadBuffer(buffer);
	camera->setClearColor(osg::Vec4(0.0f, 0.0, 0.0f, 1.0f));

	// add this slave camera to the viewer, with a shift left of the projection matrix
	viewer->addSlave(camera.get(), osg::Matrixd::translate(0.0, 0.0, 0.0), osg::Matrixd());
	//viewer->setCamera(camera.get());
	std::cout << "Create main GUI: " << traits->x << "," << traits->y << "," << traits->width << "," << traits->height << endl;

	// FPS etc
	viewer->addEventHandler(new osgViewer::StatsHandler);

	// GUI
	osg::ref_ptr<TwGUIManager> twGUI = new TwGUIManager(root.get());
	viewer->addEventHandler(twGUI.get());
	camera->setFinalDrawCallback(twGUI.get());

	// Event
	camera->addEventCallback(new osgGA::StateSetManipulator(camera->getOrCreateStateSet()));
	// Disable OSG's default behavior of quitting when the Escape key is pressed.
	// Not disabling this causes Delta3D and OSG to get into a bad state when the
	// Escape key is pressed.
	viewer->setKeyEventSetsDone(0);

	// HD Slave
	// camUtil->getSetOrCreateSlaveViewer(HD_PANEL_INDEX, 15);
	// viewer->addSlave(camUtil->mainSlave);
	//camUtil->mainSlave->addChild(scene);

	// // Shader
	// osg::ref_ptr<osg::Shader> _vShader = new osg::Shader(osg::Shader::VERTEX, vertexShader);
	// osg::ref_ptr<osg::Shader> _fShader = new osg::Shader(osg::Shader::FRAGMENT, fragShader);
	// 
	// osg::ref_ptr<osg::Program> _program = new osg::Program;
	// _program->addShader(_vShader.get());
	// _program->addShader(_fShader.get());
	// 
	// osg::ref_ptr<osg::StateSet> _stateSet = root->getOrCreateStateSet();
	// _stateSet->addUniform(uMVP.get());
	// _stateSet->setAttributeAndModes(_program.get(), osg::StateAttribute::ON);

	// Projector	
	if (CConfig::DO_CALIBRATION){
		viewer->addSlave(camUtil->getSetOrCreateSlaveViewer(Proj_PANEL_INDEX, 0));
		viewer->addSlave(camUtil->getSetOrCreateSlaveViewer(Proj_PANEL_INDEX, 1));
		viewer->addSlave(camUtil->getSetOrCreateSlaveViewer(Proj_PANEL_INDEX, 2));
		viewer->addSlave(camUtil->getSetOrCreateSlaveViewer(Proj_PANEL_INDEX, 3));
		viewer->addSlave(camUtil->getSetOrCreateSlaveViewer(Proj_PANEL_INDEX, 4));

		camUtil->projectPattern(0);
	}



	//Snap Image
	// SnapImage* postDrawCallback = SnapImage::getInstance();
	//viewer->getCamera()->setPostDrawCallback(postDrawCallback);
	//viewer->addEventHandler(new SnapeImageHandler('p', postDrawCallback));
	//camera->setPostDrawCallback(postDrawCallback);
	//// camUtil->mainSlave->setPostDrawCallback(SnapImage::getInstance());
	viewer->getCamera()->setPostDrawCallback(SnapImage::getInstance());


	// Shader
	if (0){
		osg::StateSet* ss = scene->getOrCreateStateSet();

		osg::Program* program = new osg::Program;
		osg::Shader* vertex_shader = new osg::Shader(osg::Shader::VERTEX);
		osg::Shader* fragment_shader = new osg::Shader(osg::Shader::FRAGMENT);

		program->addShader(vertex_shader);
		//program->addShader(fragment_shader);

		vertex_shader->loadShaderSourceFromFile("shaders/cam.vert");
		//fragment_shader->loadShaderSource(fragment_shader, "shaders/cam.frag");

		//ss->setAttributeAndModes(program, osg::StateAttribute::ON);
	}


	// viewer->realize();
	// viewer->run();


	osg::Timer_t _currentTime = osg::Timer::instance()->tick();
	osg::Timer_t _lastTime = osg::Timer::instance()->tick();

	int selectProjIdx = 0;
	//const int projOrderList[5] = { 4, 2, 3, 0, 1 };
	const int projOrderList[5] = { 4, 3, 1, 2, 0 }; //ID by Location in dome
	if (CConfig::DO_CALIBRATION)
		system("pause");

	while (!viewer->done())
	{
		// Intervel
		_currentTime = osg::Timer::instance()->tick();
		float t = osg::Timer::instance()->delta_s(_lastTime, _currentTime);

		if (CConfig::DO_CALIBRATION && (t > CConfig::Calib_Project_Intervel))
		{
			// Next view
			camUtil->projectPattern(projOrderList[selectProjIdx % 5]);

			// Loop or not
			if ( (!CConfig::Calib_Project_Loop) && (selectProjIdx >= 4) )
			{
				CConfig::DO_CALIBRATION = false;
			}

			cout << "ProjId: " << selectProjIdx << ", LoopStatus: " << CConfig::Calib_Project_Loop << ", Continue:" << CConfig::DO_CALIBRATION << endl;


			_lastTime = _currentTime;
			selectProjIdx++;
		}

		viewer->frame();
	}
	return 0;
}

