
SET(SOLUTION_GENERAL_DEPS
    # Add libraries here, that will be linked to every target
   )

SET(SOLUTION_APP_DEPS
    # Add libraries here, that will be linked to every application target
    -lOpenThreads -losg -losgDB -losgUtil -losgGA -losgText -losgViewer -lGL -lGLU -lX11 -lXext -lXmu -lXrandr -lm -lGLEW -lglut -lAntTweakBar -ljansson
   )

SET(SOLUTION_STATIC_LIB_DEPS
    # Add libraries here, that will be linked to every static lib target
   )

SET(SOLUTION_SHARED_LIB_DEPS
    # Add libraries here, that will be linked to every shared lib target
   )
   
      