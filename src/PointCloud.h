#pragma once
#include <osgDB/ReadFile>
#include <osgUtil/Optimizer>
#include <osgViewer/Viewer>

#include <osg/Point>
#include <osg/BlendFunc>
#include <osg/Texture2D>
#include <osg/PointSprite>
#include <osg/PolygonMode>
#include <osg/MatrixTransform>

#include <iostream>

class CPointCloud
{
  public:
	CPointCloud();
	void loadPointCloud(std::string file, osg::ref_ptr<osg::MatrixTransform> node);

  private:
	bool shader;
	bool usePointSprites;
	bool forcePointMode;
};

class PointCloudKeyboardEventHandler : public osgGA::GUIEventHandler
{
  public:
	PointCloudKeyboardEventHandler(osg::StateSet *stateset) : _stateset(stateset)
	{
		_point = new osg::Point;
		_point->setDistanceAttenuation(osg::Vec3(0.0, 0.0000, 0.05f));
		_stateset->setAttribute(_point.get());
	}

	virtual bool handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &)
	{
		switch (ea.getEventType())
		{
		case (osgGA::GUIEventAdapter::KEYDOWN):
		{
			if (ea.getKey() == '+' || ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Add)
			{
				changePointSize(1.0f);
				return true;
			}
			else if (ea.getKey() == '-' || ea.getKey() == osgGA::GUIEventAdapter::KEY_KP_Subtract)
			{
				changePointSize(-1.0f);
				return true;
			}
			else if (ea.getKey() == '<')
			{
				changePointAttenuation(1.1f);
				return true;
			}
			else if (ea.getKey() == '>')
			{
				changePointAttenuation(1.0f / 1.1f);
				return true;
			}
			break;
		}
		default:
			break;
		}
		return false;
	}

	float getPointSize() const
	{
		return _point->getSize();
	}

	void setPointSize(float psize)
	{
		if (psize > 0.0)
		{
			_point->setSize(psize);
		}
		std::cout << "Point size " << psize << std::endl;
	}

	void changePointSize(float delta)
	{
		setPointSize(getPointSize() + delta);
	}

	void changePointAttenuation(float scale)
	{
		_point->setDistanceAttenuation(_point->getDistanceAttenuation() * scale);
	}

	osg::ref_ptr<osg::StateSet> _stateset;
	osg::ref_ptr<osg::Point> _point;
};
